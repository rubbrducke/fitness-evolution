package sccapstone.fitnessevolution;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.ParseUser;

import org.junit.Assert;
import org.junit.Test;

import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.Utilities.UtilitiesMaps;

/**
 * Created by wagner, Tyler on 2/15/2016.
 */
public class LocalTest
{
    /********************** TESTING GLOBAL METHODS ****************************/
    @Test
    public void testCurrentUser()
    {
        //TODO: Figure out this test
        ParseUser globalUser = Utilities.user;
        ParseUser parseUser = ParseUser.getCurrentUser();

        if(Utilities.loggedIn)
            Assert.assertEquals(globalUser, parseUser);
        else
            Assert.assertTrue(parseUser == null);
    }

    @Test
    public void createMarkerPerson()
    {
        ParseUser inputUser = null;
        LatLng inputLocation = null;
        final double LAT = 0.0;
        final double LONG = 0.0;

        MarkerOptions returnedMO = UtilitiesMaps.createMarkerPerson(inputUser, inputLocation);

        Assert.assertNull(returnedMO);

        inputUser = new ParseUser();
        inputUser.setUsername("username");
        inputUser.add("last_name", "last");
        inputUser.add("first_name", "first");
        inputUser.add("gender", "Male");

        returnedMO = UtilitiesMaps.createMarkerPerson(inputUser, inputLocation);
        Assert.assertNull(returnedMO);

//        inputLocation = new LatLng(LAT, LONG);
//        returnedMO = UtilitiesMaps.createMarkerPerson(inputUser, inputLocation);
//
//        MarkerOptions outputMO = new MarkerOptions();
//        outputMO.position(inputLocation);
//        outputMO.title(inputUser.getUsername());
//        outputMO.snippet(inputUser.getString("last_name") + ", " + inputUser.getString("first_name"));
//        outputMO.icon(BitmapDescriptorFactory.fromResource(R.drawable.male));
//
//        Assert.assertEquals(returnedMO, outputMO);
    }

    /*********************** TESTING HELPER METHODS ***************************/

    @Test
    public void timeTo24Hour()
    {
        String input = "1:41 PM";
        String output = "13:41";

        String returned = Utilities.convertTo24Hour(input);

        Assert.assertEquals(returned, output);
    }

    @Test
    public void timeTo12Hour()
    {
        String input = "13:41";
        String output = "1:41 PM";

        String returned = Utilities.convertTo12Hour(input);

        Assert.assertEquals(returned, output);
    }

    @Test
    public void trimLeadingZeroesOnDate()
    {
        String input = "01/15/2016";
        String output = "1/15/2016";

        String returned = Utilities.trimLeadingZeroesOnDate(input);

        Assert.assertEquals(returned, output);
    }

    /*********************** TESTING HELPER METHODS ***************************/
}//LocalTest Class
