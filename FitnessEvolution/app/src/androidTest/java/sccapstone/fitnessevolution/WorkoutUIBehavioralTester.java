package sccapstone.fitnessevolution;

/**
 * Created by benaa on 2/14/2016.
 */

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isSelected;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class WorkoutUIBehavioralTester {

    //@Rule
    //public ActivityTestRule<ActivityMain> timePicker = new ActivityTestRule<ActivityMain>(ActivityMain.class);

    @Test
    public void opensDietaryPage(){
        onView(withText("Workout & Dietary Logs")).perform(click());
        onView(withText("Dietary")).perform(click());
        onView(withText("Dietary")).check(matches(isSelected()));

    }
}
