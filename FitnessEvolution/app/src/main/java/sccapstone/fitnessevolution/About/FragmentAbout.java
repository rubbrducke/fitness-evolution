package sccapstone.fitnessevolution.About;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.widget.TextView;

import sccapstone.fitnessevolution.Activites.ActivityMain;
import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.R;

/**
 * @author wagner, Tyler
 *
 * Fragment that displays the app version, and the developers.
 *   Uses the about_fragment layout
 */
public class FragmentAbout extends Fragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        Activity activity = getActivity();
        ((ActivityMain) activity).setActionBarTitle( getResources().getString(R.string.nav_about));

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.about_fragment, container, false);

        TextView txtDevs = (TextView) view.findViewById(R.id.txt_devs);

        //Gets the developers
        String[] devs = getResources().getStringArray(R.array.app_devs);

        String txtDisplayed = "";

        for(int i = 0; i < devs.length; i++)
        {
            txtDisplayed += devs[i] + "\n";
        }
        txtDevs.setText(txtDisplayed);

        // Hide the keyboard if it is currently shown
        Utilities.hideKeyboard(getContext(), getActivity());

        return view;
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FragmentAbout newInstance(int sectionNumber)
    {
        Log.v("FragmentSettings", "newInstance");
        FragmentAbout fragment = new FragmentAbout();
        Bundle args = new Bundle();
        args.putInt(Utilities.ARG_SECTION_NUMBER_KEY, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Attaches the Fragment to the UI.
     * @param activity
     */
    @Override
    public void onAttach(Context activity) {
        Log.v("onAttach", "FragmentAbout");
        super.onAttach(activity);
        ((ActivityMain) activity).onSectionAttached(
                getArguments().getInt(Utilities.ARG_SECTION_NUMBER_KEY));
//        scrollToBottomOfListView();
    }
}//FragmentAbout Class
