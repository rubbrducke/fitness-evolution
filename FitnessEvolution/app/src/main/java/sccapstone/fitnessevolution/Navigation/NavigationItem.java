package sccapstone.fitnessevolution.Navigation;

/**
 * Created by login on 4/2/2016.
 */
public class NavigationItem
{
    int icon;
    String title;

    public NavigationItem(String title, int icon)
    {
        this.title = title;
        this.icon = icon;
    }
}
