package sccapstone.fitnessevolution.Navigation;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import sccapstone.fitnessevolution.R;

/**
 * Created by login on 4/2/2016.
 */
public class CustomNavigationAdapter extends ArrayAdapter<NavigationItem>
{
    public CustomNavigationAdapter(Context context, String[] titles, TypedArray icons)
    {
        super(context, 0);

        for( int i = 0; i < titles.length; ++i )
        {
            add( new NavigationItem( titles[i], icons.getResourceId(i, 0) ));
        }
    }

    public void addItem(String title, int icon)
    {
        add(new NavigationItem(title, icon));
    }

    public void addItem( NavigationItem item )
    {
        add( item );
    }

    public View getView(int position,View convertView, ViewGroup parent)
    {
        NavigationItem item = getItem(position);

        if( convertView == null )
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.navigation_list_item, parent, false);
        }

        ImageView image = (ImageView) convertView.findViewById(R.id.nav_icon);
        TextView title = (TextView) convertView.findViewById(R.id.nav_title);

        title.setText( item.title );

        Log.v("NAVIGATION ADAPTER", "Setting image resource: " + item.icon );
        image.setImageResource( item.icon );

        return convertView;
    }
}
