package sccapstone.fitnessevolution.Coach;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;
import java.util.Map;

import sccapstone.fitnessevolution.Logging.DietaryLogging.DayStatusReport;
import sccapstone.fitnessevolution.Logging.DietaryLogging.RoutineDietReport;
import sccapstone.fitnessevolution.Logging.WorkoutLogging.FitnessAnalysis.RoutineStatusReport;
import sccapstone.fitnessevolution.Logging.WorkoutLogging.FitnessAnalysis.WorkoutAnalysis;


/**
 * Created by Ben Aaron on 2/25/2016.
 * Last updated on 4/13/2016 by Ben Aaron
 *
 * This class uses a random number generator to choose the occasion for a coach phrase to be selected.
 * After the occasion is selected, the method analyzes the daily diet report and routine workout report,
 * and with this data chooses the sub-occasion for the phrase.
 * Another random number generator is used to select the positivity and the intensity of the phrase.
 * A phrase is chosen that matches the occasion, intensity and positivity, and this phrase is returned.
 */
public class CoachWorkoutAnalyzer {
    private RoutineStatusReport report = WorkoutAnalysis.getRoutineReport();
    private RoutineDietReport dietReport = new RoutineDietReport();
    private Map<String, DayStatusReport> reportMap = dietReport.getFullReport();
    private DayStatusReport dayStatusReport;
    private ArrayList<String> phrases = new ArrayList<>();
    private ArrayList<String> positivity = new ArrayList<>();
    private ArrayList<String> intensity = new ArrayList<>();
    private ArrayList<String> occasions = new ArrayList<>();
    private String phrase = "Empty";
    private String coach = "";
    private String date = "";
    //The double value needed to be exceeded to have Carol say something positive
    private final double CAR_POS_INDEX = 0.85;
    //The double value needed to be exceeded to have Chris say something positive
    private final double CHR_POS_INDEX = 0.5;
    //The double value needed to be exceeded to have Madeleine say something positive
    private final double MAD_POS_INDEX = 0.15;
    //The double value used to find the intensity value for Carol
    private final double CAR_INT_INDEX = 0.3;
    //The double value used to find the intensity value for Chris
    private final double CHR_INT_INDEX = 0.35;
    //The double value used to find the intensity value for Madeleine
    private final double MAD_INT_INDEX = 0.4;
    //The double value needed to be exceeded for a workout phrase to be selected
    private final double OC_GEN_INDEX = 0.7;
    //The double value needed to be exceeded for a diet phrase to be selected
    private final double OC_DIET_INDEX = 0.9;
    //The double value used to find the sub-occasion for a workout phrase
    private final double OC_WORK_INDEX =0.33;
    //The double value used to find the sub-occasion for a diet phrase
    private final double OC_DIET_SUB_INDEX = 0.25;
    //Array of all of the muscles used in the routine workout report
    private String[] muscles = new String[]{"Abdominal", "Adductors", "Biceps", "Calves", "Chest", "Forearms", "Glutes",
            "Hamstrings", "Lats", "Lower Back", "Middle Back", "Neck", "Quadriceps", "Shoulders", "Traps", "Triceps"};
    //List of exercised muscles as given by the workout report
    private ArrayList<String> exMuscles =  new ArrayList<>();
    //List of overworked muscles as given by the workout report
    private ArrayList<String> ovMuscles = new ArrayList<>();
    //List of underworked muscles as given by the workout report
    private ArrayList<String> unMuscles = new ArrayList<>();
    //List of locations (i.e. numbers i such that list.get(i) yields a possible phrase
    private ArrayList<Integer> locations = new ArrayList<>();
    //List of possible phrases to be returned
    private ArrayList<String> possiblePhrases = new ArrayList<>();

    /**
     * Method to setup all objects used in this class.
     * @param cp - list of coach phrases
     * @param po - list of positivity values
     * @param in - list of intensity values
     * @param oc - list of occasions
     * @param c - coach name
     */
    public void start(ArrayList<String> cp, ArrayList<String> po, ArrayList<String> in, ArrayList<String> oc, String c){
        phrases=cp;
        positivity=po;
        intensity=in;
        occasions=oc;
        coach = c;
        Date today = Calendar.getInstance().getTime();
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        date = df.format(today);
        dayStatusReport = reportMap.get(date);
        initial();
    }

    /**
     * Method to select a phrase using random number generators and the final values defined above.
     */
    private void analyze(){
        //Random number by which the occasion for the possible phrase is selected
        double o_type = Math.random();
        if(o_type>OC_GEN_INDEX && o_type<OC_DIET_INDEX) {
            //Workout phrase selected
            Log.v("CoachWorkoutAnalyzer", "Workout phrase selected.");
            double m_type = Math.random();
            if(m_type < OC_WORK_INDEX) {
                //Exercised sub-occasion selected
                //Calls findExMuscles to find the number of exercised muscles and fill the corresponding list of muscles
                if(findExMuscles()>0) {
                    //There are exercised muscles recorded
                    for (int i = 0; i < occasions.size(); i++) {
                        //Fill the locations list with the i values for which occasions.get(i) equals exercised
                        if (occasions.get(i).equals("exercised")) {
                            locations.add((Integer) i);
                        }
                    }
                    if (!locations.isEmpty()) {
                        //There are phrases to choose from within this sub-occasion
                        //Random number by which the positivity value is selected
                        double p_type = Math.random();
                        //Random number by which the intensity value is selected
                        double i_type = Math.random();
                        //String representing the positivity value
                        String pos = "";
                        //String representing the intensity value
                        String intense = "";
                        //Find intensity and positivity values for each coach
                        if (coach.equals("Madeleine")) {
                            if (p_type > MAD_POS_INDEX) {
                                pos = "positive";
                            } else {
                                pos = "negative";
                            }
                            if (i_type < MAD_INT_INDEX) {
                                intense = "1";
                            } else if (i_type >= 2 * MAD_INT_INDEX) {
                                intense = "3";
                            } else {
                                intense = "2";
                            }
                        } else if (coach.equals("Chris")) {
                            if (p_type > CHR_POS_INDEX) {
                                pos = "positive";
                            } else {
                                pos = "negative";
                            }
                            if (i_type < CHR_INT_INDEX) {
                                intense = "1";
                            } else if (i_type >= 2 * CHR_INT_INDEX) {
                                intense = "3";
                            } else {
                                intense = "2";
                            }
                        } else if (coach.equals("Carol")) {
                            if (p_type > CAR_POS_INDEX) {
                                pos = "positive";
                            } else {
                                pos = "negative";
                            }
                            if (i_type < CAR_INT_INDEX) {
                                intense = "1";
                            } else if (i_type >= 2 * CAR_INT_INDEX) {
                                intense = "3";
                            } else {
                                intense = "2";
                            }
                        }
                        //Fill the list of possible phrases with phrases that match the positivity, intensity and occasion selected
                        for (Integer i : locations) {
                            if (positivity.get(i).equals(pos)) {
                                if (intensity.get(i).equals(intense)) {
                                    possiblePhrases.add(phrases.get(i));
                                }
                            }
                        }
                        if (!possiblePhrases.isEmpty()) {
                            //There exist possible phrases
                            //Random number to find the phrase to return
                            int whichPhrase = randomOverRange(0, possiblePhrases.size() - 1);
                            phrase = possiblePhrases.get(whichPhrase);
                            String filler = "";
                            //Replaces the $S in the phrase with a list of the exercised muscles
                            for (int i = 0; i < exMuscles.size(); i++) {
                                if (i == 0) {
                                    filler = exMuscles.get(i);
                                } else {
                                    filler = filler.concat(", ");
                                    filler = filler.concat(exMuscles.get(i));
                                    //Log.v("CoachWorkoutAnalyzer", "Phrase is " + phrase);
                                }
                            }
                            phrase = phrase.replace("$S", filler);
                            Log.v("CoachWorkoutAnalyzer", "Phrase is " + phrase);
                        }
                        else
                            phrase = "No phrase for this situation.";
                    } else
                        phrase = "Empty exercised list.";
                }
                else
                    initial();
            }
            else if (m_type >= 2*OC_WORK_INDEX) {
                //Overworked sub-occasion selected
                //See exercised sub-occasion above for full explanation of the code
                //The following methods are much the same as those above
                if(findOvMuscles()>0) {
                    for (int i = 0; i < occasions.size(); i++) {
                        if (occasions.get(i).equals("overworked")) {
                            locations.add((Integer) i);
                        }
                    }
                    if (!locations.isEmpty()) {
                        double p_type = Math.random();
                        double i_type = Math.random();
                        String pos = "";
                        String intense = "";
                        if (coach.equals("Madeleine")) {
                            if (p_type > MAD_POS_INDEX) {
                                pos = "positive";
                            } else {
                                pos = "negative";
                            }
                            if (i_type < MAD_INT_INDEX) {
                                intense = "1";
                            } else if (i_type >= 2 * MAD_INT_INDEX) {
                                intense = "3";
                            } else {
                                intense = "2";
                            }
                        } else if (coach.equals("Chris")) {
                            if (p_type > CHR_POS_INDEX) {
                                pos = "positive";
                            } else {
                                pos = "negative";
                            }
                            if (i_type < CHR_INT_INDEX) {
                                intense = "1";
                            } else if (i_type >= 2 * CHR_INT_INDEX) {
                                intense = "3";
                            } else {
                                intense = "2";
                            }
                        } else if (coach.equals("Carol")) {
                            if (p_type > CAR_POS_INDEX) {
                                pos = "positive";
                            } else {
                                pos = "negative";
                            }
                            if (i_type < CAR_INT_INDEX) {
                                intense = "1";
                            } else if (i_type >= 2 * CAR_INT_INDEX) {
                                intense = "3";
                            } else {
                                intense = "2";
                            }
                        }
                        for (Integer i : locations) {
                            if (positivity.get(i).equals(pos)) {
                                if (intensity.get(i).equals(intense)) {
                                    possiblePhrases.add(phrases.get(i));
                                }
                            }
                        }
                        if (!possiblePhrases.isEmpty()) {
                            int whichPhrase = randomOverRange(0, possiblePhrases.size() - 1);
                            phrase = possiblePhrases.get(whichPhrase);
                            String filler = "";
                            for (int i = 0; i < ovMuscles.size(); i++) {
                                if (i == 0) {
                                    filler = ovMuscles.get(i);
                                    Log.v("CoachWorkoutAnalyzer", "Phrase is " +phrase);
                                } else {
                                    filler = filler.concat(", ");
                                    filler = filler.concat(ovMuscles.get(i));
                                    //Log.v("CoachWorkoutAnalyzer", "Phrase is " + phrase);
                                }
                            }
                            phrase = phrase.replace("$S", filler);
                            Log.v("CoachWorkoutAnalyzer", "Phrase is " + phrase);
                        }
                        else
                            phrase = "No phrase for this instance.";
                    }
                    else
                        phrase = "Empty overworked list.";
                }
                else
                    initial();
            }
            else {
                //Underworked sub-occasion selected
                //See exercised sub-occasion above for full explanation of the code
                //The following methods are much the same as those above
                if (findUnMuscles() > 0) {
                    for (int i = 0; i < occasions.size(); i++) {
                        if (occasions.get(i).equals("underworked")) {
                            locations.add((Integer) i);
                        }
                    }
                    if (!locations.isEmpty()) {
                        double p_type = Math.random();
                        double i_type = Math.random();
                        String pos = "";
                        String intense = "";
                        if (coach.equals("Madeleine")) {
                            if (p_type > MAD_POS_INDEX) {
                                pos = "positive";
                            } else {
                                pos = "negative";
                            }
                            if (i_type < MAD_INT_INDEX) {
                                intense = "1";
                            } else if (i_type >= 2 * MAD_INT_INDEX) {
                                intense = "3";
                            } else {
                                intense = "2";
                            }
                        } else if (coach.equals("Chris")) {
                            if (p_type > CHR_POS_INDEX) {
                                pos = "positive";
                            } else {
                                pos = "negative";
                            }
                            if (i_type < CHR_INT_INDEX) {
                                intense = "1";
                            } else if (i_type >= 2 * CHR_INT_INDEX) {
                                intense = "3";
                            } else {
                                intense = "2";
                            }
                        } else if (coach.equals("Carol")) {
                            if (p_type > CAR_POS_INDEX) {
                                pos = "positive";
                            } else {
                                pos = "negative";
                            }
                            if (i_type < CAR_INT_INDEX) {
                                intense = "1";
                            } else if (i_type >= 2 * CAR_INT_INDEX) {
                                intense = "3";
                            } else {
                                intense = "2";
                            }
                        }
                        for (Integer i : locations) {
                            if (positivity.get(i).equals(pos)) {
                                if (intensity.get(i).equals(intense)) {
                                    possiblePhrases.add(phrases.get(i));
                                }
                            }
                        }
                        if (!possiblePhrases.isEmpty()) {
                            int whichPhrase = randomOverRange(0, possiblePhrases.size() - 1);
                            phrase = possiblePhrases.get(whichPhrase);
                            String filler = "";
                            for (int i = 0; i < unMuscles.size(); i++) {
                                if (i == 0) {
                                    filler = unMuscles.get(i);
                                    Log.v("CoachWorkoutAnalyzer", "Phrase is " +phrase);
                                } else {
                                    filler = filler.concat(", ");
                                    filler = filler.concat(unMuscles.get(i));
                                    //Log.v("CoachWorkoutAnalyzer", "Phrase is " +phrase);
                                }
                            }
                            phrase = phrase.replace("$S", filler);
                            Log.v("CoachWorkoutAnalyzer", "Phrase is " +phrase);
                        } else
                            phrase = "No phrase for this instance.";
                    } else
                        phrase = "Empty underworked list.";
                }
                else
                    initial();
            }
        }
        else if (o_type>OC_DIET_INDEX){
            //Diet phrase selected
            Log.v("CoachWorkoutAnalyzer", "Diet phrase selected.");
            if (dayStatusReport == null){
                //No daily diet report for today
                phrase = "Have you eaten yet today? Fill out your diet logs for today.";
                return;
            }
            double d_type = Math.random();
            if (d_type<OC_DIET_SUB_INDEX){
                //Calories sub-occasion selected
                String occasion = "";
                if (dayStatusReport.tooFewCalories){
                    occasion = "tooFewCalories";
                }
                else if (dayStatusReport.tooManyCalories) {
                    occasion = "tooManyCalories";
                }
                else if (!dayStatusReport.tooManyCalories && !dayStatusReport.tooFewCalories){
                    occasion = "enoughCalories";
                }
                //Fill the list of locations with the i values such that occasions.get(i) is the given occasion
                for (int i=0; i < occasions.size(); i++ ) {
                    if (occasions.get(i).equals(occasion)){
                        locations.add((Integer) i);
                    }
                }
                if (!locations.isEmpty()){
                    //Possible phrases exist
                    //Random number to find the positivity value for the given coach
                    double p_type = Math.random();
                    //Random number to find the intensity value for the given coach
                    double i_type = Math.random();
                    //String to hold the positivity value
                    String pos = "";
                    //String to hold the intensity value
                    String intense = "";
                    //Find intensity and positivity values for each coach
                    if (coach.equals("Madeleine")){
                        if (p_type>MAD_POS_INDEX){
                            pos = "positive";
                        }
                        else {
                            pos = "negative";
                        }
                        if (i_type<MAD_INT_INDEX){
                            intense = "1";
                        }
                        else if (i_type>=2*MAD_INT_INDEX){
                            intense = "3";
                        }
                        else {
                            intense = "2";
                        }
                    }
                    else if(coach.equals("Chris")){
                        if (p_type>CHR_POS_INDEX){
                            pos = "positive";
                        }
                        else {
                            pos = "negative";
                        }
                        if (i_type<CHR_INT_INDEX){
                            intense = "1";
                        }
                        else if (i_type>=2*CHR_INT_INDEX){
                            intense = "3";
                        }
                        else {
                            intense = "2";
                        }
                    }
                    else if (coach.equals("Carol")){
                        if (p_type>CAR_POS_INDEX){
                            pos = "positive";
                        }
                        else {
                            pos = "negative";
                        }
                        if (i_type<CAR_INT_INDEX){
                            intense = "1";
                        }
                        else if (i_type>=2*CAR_INT_INDEX){
                            intense = "3";
                        }
                        else {
                            intense = "2";
                        }
                    }
                    //Fill the possible phrases list with phrases which match the positivity, intensity and occasion selected
                    for (Integer i: locations) {
                        if (positivity.get(i).equals(pos)){
                            if (intensity.get(i).equals(intense)){
                                possiblePhrases.add(phrases.get(i));
                            }
                        }
                    }
                    if (!possiblePhrases.isEmpty()){
                        //There exist potential phrases to be returned
                        //Random number to find the phrase to be returned
                        int whichPhrase = randomOverRange(0, possiblePhrases.size() - 1);
                        phrase = possiblePhrases.get(whichPhrase);
                        Log.v("CoachWorkoutAnalyzer", "Phrase is " +phrase);
                    }
                    else {
                        //There aren't any potential phrases matching all of the above conditions, so intensity and positivity are taken out
                        possiblePhrases.clear();
                        for (Integer i: locations) {
                            possiblePhrases.add(phrases.get(i));
                        }
                        if (!possiblePhrases.isEmpty()){
                            int whichPhrase = randomOverRange(0, possiblePhrases.size() - 1);
                            phrase = possiblePhrases.get(whichPhrase);
                            Log.v("CoachWorkoutAnalyzer", "Phrase is " +phrase);
                        }
                        else
                            phrase = "No phrase for this instance.";
                    }

                }
                else {
                    phrase = "Empty calories list.";
                }
            }
            else if (d_type<OC_DIET_SUB_INDEX*2){
                //Fat sub-occasion selected
                //See Calories above for more information about the code below
                //The methods used below are much the same as those above
                String occasion = "";
                if (dayStatusReport.tooLittleFat){
                    occasion = "tooLittleFat";
                }
                else if (dayStatusReport.tooMuchFat) {
                    occasion = "tooMuchFat";
                }
                else if (!dayStatusReport.tooMuchFat && !dayStatusReport.tooLittleFat){
                    occasion = "enoughFat";
                }
                for (int i=0; i < occasions.size(); i++ ) {
                    if (occasions.get(i).equals(occasion)){
                        locations.add((Integer) i);
                    }
                }
                if (!locations.isEmpty()){
                    double p_type = Math.random();
                    double i_type = Math.random();
                    String pos = "";
                    String intense = "";
                    if (coach.equals("Madeleine")){
                        if (p_type>MAD_POS_INDEX){
                            pos = "positive";
                        }
                        else {
                            pos = "negative";
                        }
                        if (i_type<MAD_INT_INDEX){
                            intense = "1";
                        }
                        else if (i_type>=2*MAD_INT_INDEX){
                            intense = "3";
                        }
                        else {
                            intense = "2";
                        }
                    }
                    else if(coach.equals("Chris")){
                        if (p_type>CHR_POS_INDEX){
                            pos = "positive";
                        }
                        else {
                            pos = "negative";
                        }
                        if (i_type<CHR_INT_INDEX){
                            intense = "1";
                        }
                        else if (i_type>=2*CHR_INT_INDEX){
                            intense = "3";
                        }
                        else {
                            intense = "2";
                        }
                    }
                    else if (coach.equals("Carol")){
                        if (p_type>CAR_POS_INDEX){
                            pos = "positive";
                        }
                        else {
                            pos = "negative";
                        }
                        if (i_type<CAR_INT_INDEX){
                            intense = "1";
                        }
                        else if (i_type>=2*CAR_INT_INDEX){
                            intense = "3";
                        }
                        else {
                            intense = "2";
                        }
                    }
                    for (Integer i: locations) {
                        if (positivity.get(i).equals(pos)){
                            if (intensity.get(i).equals(intense)){
                                possiblePhrases.add(phrases.get(i));
                            }
                        }
                    }
                    if (!possiblePhrases.isEmpty()){
                        int whichPhrase = randomOverRange(0, possiblePhrases.size() - 1);
                        phrase = possiblePhrases.get(whichPhrase);
                        Log.v("CoachWorkoutAnalyzer", "Phrase is " +phrase);
                    }
                    else {
                        possiblePhrases.clear();
                        for (Integer i: locations) {
                            possiblePhrases.add(phrases.get(i));
                        }
                        if (!possiblePhrases.isEmpty()){
                            int whichPhrase = randomOverRange(0, possiblePhrases.size() - 1);
                            phrase = possiblePhrases.get(whichPhrase);
                            Log.v("CoachWorkoutAnalyzer", "Phrase is " +phrase);
                        }
                        else
                            phrase = "No phrase for this instance.";
                    }

                }
                else {
                    phrase = "Empty fat list.";
                }
            }
            else if (d_type<OC_DIET_SUB_INDEX*3){
                //Carbohydrates sub-occasion selected
                //See Calories above for more information about the code below
                //The methods used below are much the same as those above
                String occasion = "";
                if (dayStatusReport.tooFewCarbohydrates){
                    occasion = "tooFewCarbohydrates";
                }
                else if (dayStatusReport.tooManyCarbohydrates) {
                    occasion = "tooManyCarbohydrates";
                }
                else if (!dayStatusReport.tooManyCarbohydrates && !dayStatusReport.tooFewCarbohydrates){
                    occasion = "enoughCarbohydrates";
                }
                for (int i=0; i < occasions.size(); i++ ) {
                    if (occasions.get(i).equals(occasion)){
                        locations.add((Integer) i);
                    }
                }
                if (!locations.isEmpty()){
                    double p_type = Math.random();
                    double i_type = Math.random();
                    String pos = "";
                    String intense = "";
                    if (coach.equals("Madeleine")){
                        if (p_type>MAD_POS_INDEX){
                            pos = "positive";
                        }
                        else {
                            pos = "negative";
                        }
                        if (i_type<MAD_INT_INDEX){
                            intense = "1";
                        }
                        else if (i_type>=2*MAD_INT_INDEX){
                            intense = "3";
                        }
                        else {
                            intense = "2";
                        }
                    }
                    else if(coach.equals("Chris")){
                        if (p_type>CHR_POS_INDEX){
                            pos = "positive";
                        }
                        else {
                            pos = "negative";
                        }
                        if (i_type<CHR_INT_INDEX){
                            intense = "1";
                        }
                        else if (i_type>=2*CHR_INT_INDEX){
                            intense = "3";
                        }
                        else {
                            intense = "2";
                        }
                    }
                    else if (coach.equals("Carol")){
                        if (p_type>CAR_POS_INDEX){
                            pos = "positive";
                        }
                        else {
                            pos = "negative";
                        }
                        if (i_type<CAR_INT_INDEX){
                            intense = "1";
                        }
                        else if (i_type>=2*CAR_INT_INDEX){
                            intense = "3";
                        }
                        else {
                            intense = "2";
                        }
                    }
                    for (Integer i: locations) {
                        if (positivity.get(i).equals(pos)){
                            if (intensity.get(i).equals(intense)){
                                possiblePhrases.add(phrases.get(i));
                            }
                        }
                    }
                    if (!possiblePhrases.isEmpty()){
                        int whichPhrase = randomOverRange(0, possiblePhrases.size() - 1);
                        phrase = possiblePhrases.get(whichPhrase);
                        Log.v("CoachWorkoutAnalyzer", "Phrase is " +phrase);
                    }
                    else {
                        possiblePhrases.clear();
                        for (Integer i: locations) {
                            possiblePhrases.add(phrases.get(i));
                        }
                        if (!possiblePhrases.isEmpty()){
                            int whichPhrase = randomOverRange(0, possiblePhrases.size() - 1);
                            phrase = possiblePhrases.get(whichPhrase);
                            Log.v("CoachWorkoutAnalyzer", "Phrase is " +phrase);
                        }
                        else
                            phrase = "No phrase for this instance.";
                    }

                }
                else {
                    phrase = "Empty carbohydrates list.";
                }
            }
            else {
                //Protein sub-occasion selected
                //See Calories above for more information about the code below
                //The methods used below are much the same as those above
                String occasion = "";
                if (dayStatusReport.tooLittleProtein) {
                    occasion = "tooLittleProtein";
                } else if (dayStatusReport.tooMuchProtein) {
                    occasion = "tooMuchProtein";
                } else if (!dayStatusReport.tooLittleProtein && !dayStatusReport.tooMuchProtein) {
                    occasion = "enoughProtein";
                }
                for (int i = 0; i < occasions.size(); i++) {
                    if (occasions.get(i).equals(occasion)) {
                        locations.add((Integer) i);
                    }
                }
                if (!locations.isEmpty()) {
                    double p_type = Math.random();
                    double i_type = Math.random();
                    String pos = "";
                    String intense = "";
                    if (coach.equals("Madeleine")) {
                        if (p_type > MAD_POS_INDEX) {
                            pos = "positive";
                        } else {
                            pos = "negative";
                        }
                        if (i_type < MAD_INT_INDEX) {
                            intense = "1";
                        } else if (i_type >= 2 * MAD_INT_INDEX) {
                            intense = "3";
                        } else {
                            intense = "2";
                        }
                    } else if (coach.equals("Chris")) {
                        if (p_type > CHR_POS_INDEX) {
                            pos = "positive";
                        } else {
                            pos = "negative";
                        }
                        if (i_type < CHR_INT_INDEX) {
                            intense = "1";
                        } else if (i_type >= 2 * CHR_INT_INDEX) {
                            intense = "3";
                        } else {
                            intense = "2";
                        }
                    } else if (coach.equals("Carol")) {
                        if (p_type > CAR_POS_INDEX) {
                            pos = "positive";
                        } else {
                            pos = "negative";
                        }
                        if (i_type < CAR_INT_INDEX) {
                            intense = "1";
                        } else if (i_type >= 2 * CAR_INT_INDEX) {
                            intense = "3";
                        } else {
                            intense = "2";
                        }
                    }
                    for (Integer i : locations) {
                        if (positivity.get(i).equals(pos)) {
                            if (intensity.get(i).equals(intense)) {
                                possiblePhrases.add(phrases.get(i));
                            }
                        }
                    }
                    if (!possiblePhrases.isEmpty()) {
                        int whichPhrase = randomOverRange(0, possiblePhrases.size() - 1);
                        phrase = possiblePhrases.get(whichPhrase);
                        Log.v("CoachWorkoutAnalyzer", "Phrase is " + phrase);
                    } else {
                        possiblePhrases.clear();
                        for (Integer i : locations) {
                            possiblePhrases.add(phrases.get(i));
                        }
                        if (!possiblePhrases.isEmpty()) {
                            int whichPhrase = randomOverRange(0, possiblePhrases.size() - 1);
                            phrase = possiblePhrases.get(whichPhrase);
                            Log.v("CoachWorkoutAnalyzer", "Phrase is " + phrase);
                        } else
                            phrase = "No phrase for this instance.";
                    }

                } else {
                    phrase = "Empty carbohydrates list.";
                }
            }
        }
        else{
            //General phrase selected
            //Fill the locations list with the i values such that occasions.get(i) equals general
            for (int i=0; i < occasions.size(); i++ ) {
                if (occasions.get(i).equals("general")){
                    locations.add((Integer) i);
                }
            }
            if (!locations.isEmpty()){
                //There exist general phrases
                //Random number by which the positivity value will be chosen
                double p_type = Math.random();
                //Random number by which the intensity value will be chosen
                double i_type = Math.random();
                //String to hold the positivity value
                String pos = "";
                //String to hold the intensity value
                String intense = "";
                //Find the intensity and positivity values for each coach
                if (coach.equals("Madeleine")){
                    if (p_type>MAD_POS_INDEX){
                        pos = "positive";
                    }
                    else {
                        pos = "negative";
                    }
                    if (i_type<MAD_INT_INDEX){
                        intense = "1";
                    }
                    else if (i_type>=2*MAD_INT_INDEX){
                        intense = "3";
                    }
                    else {
                        intense = "2";
                    }
                } else if(coach.equals("Chris")){
                    if (p_type>CHR_POS_INDEX){
                        pos = "positive";
                    }
                    else {
                        pos = "negative";
                    }
                    if (i_type<CHR_INT_INDEX){
                        intense = "1";
                    }
                    else if (i_type>=2*CHR_INT_INDEX){
                        intense = "3";
                    }
                    else {
                        intense = "2";
                    }
                }
                else if (coach.equals("Carol")){
                    if (p_type>CAR_POS_INDEX){
                        pos = "positive";
                    }
                    else {
                        pos = "negative";
                    }
                    if (i_type<CAR_INT_INDEX){
                        intense = "1";
                    }
                    else if (i_type>=2*CAR_INT_INDEX){
                        intense = "3";
                    }
                    else {
                        intense = "2";
                    }
                }
                //Fill the possible phrases list with all of the phrases that match the positivity, intensity and occasion
                for (Integer i: locations) {
                    if (positivity.get(i).equals(pos)){
                        if (intensity.get(i).equals(intense)){
                            possiblePhrases.add(phrases.get(i));
                        }
                    }
                }
                if (!possiblePhrases.isEmpty()){
                    //There exist potential phrases to return
                    //Random number by which the phrase to return is found
                    int whichPhrase = randomOverRange(0, possiblePhrases.size() - 1);
                    phrase = possiblePhrases.get(whichPhrase);
                    Log.v("CoachWorkoutAnalyzer", "Phrase is " +phrase);
                }
                else
                    phrase = "No phrase for this instance.";
            }
            else
                phrase = "Empty general list.";
        }

    }

    /**
     * Method to find all of the exercised muscles from the routine workout report and return the number of them
     * @return n
     */
    private int findExMuscles(){
        int n = 0;
        for (String muscle: muscles) {
            if (report.isExercised(muscle)) {
                exMuscles.add(muscle);
                Log.v("CoachWorkoutAnalyzer", "Found muscle " + muscle);
                n++;
            }
        }
        return n;
    }

    /**
     * Method to find all of the overworked muscles from the routine workout report and return the number of them
     * @return n
     */
    private int findOvMuscles(){
        int n = 0;
        for (String muscle: muscles) {
            if (report.isOverworked(muscle)) {
                ovMuscles.add(muscle);
                Log.v("CoachWorkoutAnalyzer", "Found muscle " + muscle);
                n++;
            }
        }
        return n;
    }

    /**
     * Method to find all of the underworked muscles from the routine workout report and return the number of them
     * @return n
     */
    private int findUnMuscles(){
        int n = 0;
        for (String muscle: muscles) {
            if (report.isUnderworked(muscle)) {
                unMuscles.add(muscle);
                Log.v("CoachWorkoutAnalyzer", "Found muscle " + muscle);
                n++;
            }
        }
        return n;
    }

    /**
     * Method called by MyFitnessPage to start the analyzation process and return the generated phrase for the coach to say
     * @return phrase
     */
    public String getPhrase(){
        analyze();
        return phrase;
    }

    /**
     * Method to find a random int within a range of integers instead of just between 0 and 1
     * @param min
     * @param max
     * @return randomOverRange
     */
    private int randomOverRange(int min, int max)
    {
        int range = Math.abs(max - min) + 1;
        return (int)(Math.random() * range) + (min <= max ? min : max);
    }

    /**
     * Method to set the phrase to an initial value for each coach, in the case that no workout or diet phrases are available
     */
    private void initial(){
        if (coach.equals("Madeleine")){
            phrase = new String("Hi! Welcome to FitnessEvolution! I'll be your coach, Madeleine. For future messages, please come back to this screen. I wish you a long and healthy new life!");
        }
        else if(coach.equals("Chris")){
            phrase = new String("Sup man! Welcome to FitnessEvolution! I'll be your coach Chris. Come back here to hear from me again. Now let's get swole!");
        }
        else if (coach.equals("Carol")){
            phrase = new String("Welcome to FitnessEvolution. I'll be your coach, Carol. In the next few days you'll come to hate me, but you'll be healthier than ever before. Come back here everyday or stay weak. Your choice.");
        }
    }
}
