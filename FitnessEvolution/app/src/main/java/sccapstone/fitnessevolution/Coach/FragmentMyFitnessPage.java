package sccapstone.fitnessevolution.Coach;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;

import sccapstone.fitnessevolution.Activites.ActivityMain;
import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.R;

/**
 * Created by Ben Aaron on 2/16/2016
 * Last updated by Ben Aaron on 4/14/2016
 *
 * This fragment contains the coach functionality for the app. Each time the fragment is entered, a new coach phrase is generated
 * by the CoachWorkoutAnalyzer from the phrases created by the GenerateCoachDialogue and displayed at the top of the screen.
 * A picture of the coach is shown below that. At the bottom of the screen is a button by which a random inspirational message
 * from the list of messages created by the GenerateCoachDialogue is selected and displayed.
 */
public class FragmentMyFitnessPage extends Fragment implements View.OnClickListener{

    private ArrayList<String> inspirationalMessages = new ArrayList<>();
    private ArrayList<String> coachPhrases = new ArrayList<String>();
    private ArrayList<String> cpPositivity = new ArrayList<String>();
    private ArrayList<String> cpIntensity = new ArrayList<String>();
    private ArrayList<String> cpOccasions = new ArrayList<String>();
    private boolean isSetup=false;
    private String whichCoach = "";
    private TextView inspMsg;
    private TextView coachMsg;
    private ImageView coachPic;
    private CoachWorkoutAnalyzer analyzer = new CoachWorkoutAnalyzer();
    private GenerateCoachDialogue gCD = new GenerateCoachDialogue();
    //private boolean bool;
    public FragmentMyFitnessPage() {
        // Required empty public constructor
    }

    /**
     * onStart method for fragment sets up the GenerateCoachDialogue object, and once finished calls on setup to pick a phrase
     */
    @Override
    public void onStart() {
        super.onStart();
        gCD.start();
        if(gCD.isDone()) {
            setup();
        }
        else
            Log.v("MyFitnessPage", "Not done.");
    }

    /**
     * Basic onCreate fragment method
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //isSetup=setup();
        if (getArguments() != null) {
        }
    }

    /**
     * Basic newInstance fragment method which returns a new instance of FragmentMyFitnessPage
     * @param sectionNumber
     * @return new fragment
     */
    public static FragmentMyFitnessPage newInstance(int sectionNumber) {
        Log.v("FragmentMyFitnessPage", "newInstance");
        FragmentMyFitnessPage fragment = new FragmentMyFitnessPage();
        Bundle args = new Bundle();
        args.putInt(Utilities.ARG_SECTION_NUMBER_KEY, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * onCreateView fragment method creates objects from the inflated FragmentMyFitnessPage.xml file so that their values
     * can be changed dynamically
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //Sets the title of the action bar along the top of the screen
        Activity activity = getActivity();
        ((ActivityMain) activity).setActionBarTitle( getResources().getString(R.string.nav_coach));

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.my_fitness_fragment, container, false);

        //Set up all features
        Button inspBtn= (Button) view.findViewById(R.id.buttonNewInspirationalPhrase);
        inspBtn.setOnClickListener(this);
        inspMsg = (TextView) view.findViewById(R.id.inspirationalMessageView);
        inspMsg.setSelected(true);
        coachMsg = (TextView) view.findViewById(R.id.coachPhraseView);
        coachMsg.setSelected(true);
        coachPic = (ImageView) view.findViewById(R.id.coachImage);

        //Ensures the User is logged in
        if(ParseUser.getCurrentUser() != null)
        {
            //Sets the picture for each coach
            if(ParseUser.getCurrentUser().get("coach").equals("Madeleine"))
                coachPic.setImageBitmap(BitmapFactory.decodeResource(getResources(),
                        R.drawable.coach_madeleine));
            else if(ParseUser.getCurrentUser().get("coach").equals("Carol"))
                coachPic.setImageBitmap(BitmapFactory.decodeResource(getResources(),
                        R.drawable.coach_carol));
            else if(ParseUser.getCurrentUser().get("coach").equals("Chris"))
                coachPic.setImageBitmap(BitmapFactory.decodeResource(getResources(),
                        R.drawable.coach_chris));
        }

        // Return the inflated layout
        return view;
    }

    /**
     * onClick fragment method keeps track of what object have been clicked by a user and stimulates activity based on what was pressed.
     * The only clickable button in this fragment is that for a new Inspirational Message, so in this case each time that button is pressed
     * this method calls on newInspirationalMessage to generate a new message.
     * @param v
     */
    @Override
    public void onClick(View v) {
        //Log.v("onClick", "" + v.getId());
        switch (v.getId()) {
            case R.id.buttonNewInspirationalPhrase:
                if(isSetup)
                    newInspirationalMessage();
                else {
                    Log.v("MyFitnessPage", "Not set up");
                    inspMsg.setText("Inspirational Message");
                    coachMsg.setText("Phrase");
                }
                break;
        }
    }

    /**
     * Basic onAttach fragment method
     * @param activity
     */
    @Override
    public void onAttach(Context activity) {
        Log.v("onAttach", "FragmentMyFitnessPage");
        super.onAttach(activity);
        ((ActivityMain) activity).onSectionAttached(getArguments().getInt(Utilities.ARG_SECTION_NUMBER_KEY));
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    /**
     * Method to query parse for the current user's coach and set up the lists of possible phrases and their corresponding values
     * (i.e. positivity, intensity and occasion), so that a phrase may be selected from these.
     */
    private void setup(){

        //Queries the Parse database to get the current user's coach
        ParseQuery<ParseUser> coachQuery = ParseUser.getQuery();
        //coachQuery.whereEqualTo("objectID", ParseUser.getCurrentUser().getObjectId());
        coachQuery.getInBackground(ParseUser.getCurrentUser().getObjectId(), new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser object, ParseException e) {
                if (e == null) {
                    if (object != null) {
                        //Log.v("MyFitnessPage", "Coach is " + object.getString("coach"));
                        if (object.getString("coach").equals("Madeleine") ||
                                object.getString("coach").equals("Chris") || object.getString("coach").equals("Carol")) {
                            whichCoach = object.getString("coach");
                           // Log.v("MyFitnessPage", "Made it this far.");
                        } else
                            Log.v("MyFitnessPage", "Error finding coach.");
                    } else
                        Log.v("MyFitnessPage", "Error null object.");
                } else
                    Log.v("MyFitnessPage", "Error accessing user object: " + e.getMessage());

                //The user does not have a coach
                if(whichCoach==null){
                    isSetup=false;
                    //Log.v("MyFitnessPage", "Made it this far 2.");

                }
                //Set up the phrase lists for each coach
                else {
                    //Log.v("MyFitnessPage", "Made it this far 3.");
                    if (whichCoach.equals("Madeleine")){
                        coachPhrases=gCD.getMadeleine().getPhrases();
                        cpPositivity=gCD.getMadeleine().getPositivity();
                        cpIntensity=gCD.getMadeleine().getIntensity();
                        cpOccasions=gCD.getMadeleine().getOccasions();
                        inspirationalMessages=gCD.getMessages();
                    }
                    else if (whichCoach.equals("Chris")) {
                        coachPhrases=gCD.getChris().getPhrases();
                        cpPositivity=gCD.getChris().getPositivity();
                        cpIntensity=gCD.getChris().getIntensity();
                        cpOccasions=gCD.getChris().getOccasions();
                        inspirationalMessages=gCD.getMessages();
                    }
                    else if (whichCoach.equals("Carol")) {
                        coachPhrases=gCD.getCarol().getPhrases();
                        cpPositivity=gCD.getCarol().getPositivity();
                        cpIntensity=gCD.getCarol().getIntensity();
                        cpOccasions=gCD.getCarol().getOccasions();
                        inspirationalMessages=gCD.getMessages();
                    }
                    isSetup=true;
                    //Phrases exist and there can be a phrase for the coach to say
                    if (!coachPhrases.isEmpty()&&!cpOccasions.isEmpty()&&!cpIntensity.isEmpty()&&!cpPositivity.isEmpty()) {
                        setCoachPhrase();
                    }
                    //Something went wrong
                    else {
                        Log.v("MyFitnessPage", "Size of coachPhrases "+coachPhrases.size());
                        Log.v("MyFitnessPage", "Size of positivity "+cpPositivity.size());
                        Log.v("MyFitnessPage", "Size of intensity "+cpIntensity.size());
                        Log.v("MyFitnessPage", "Size of occasions " + cpOccasions.size());
                    }
                }

            }
        });
    }
/*
    private void setRelatedMessages(String coachObjectID){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Coach");
        //query.whereEqualTo("objectId", coachObjectID);

        query.getInBackground(coachObjectID, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    if (object != null) {
                        Log.v("MyFitnessPage", "In setRelatedMessages: found coach");
                        putMsgInMsgArray(object);
                        putDialogueInDialogueArray(object);
                    } else
                        Log.v("MyFitnessPage", "Error: no coach found");
                } else {
                    Log.v("MyFitnessPage", "Error accessing coach messages: " + e.getMessage());
                }
            }
        });
    }

    private void putMsgInMsgArray(ParseObject coach){
        ParseRelation relation = coach.getRelation("saysMessage");
        try {
            ParseQuery<ParseObject> rQ = relation.getQuery();
            rQ.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> objects, ParseException e) {
                    if (e == null) {
                        if (!objects.isEmpty()) {
                            for (int i = 0; i < objects.size(); i++) {
                                Log.v("MyFitnessPage", "In putMsgInMsgArray: message is \"" + objects.get(i).getString("Messages") + "\".");
                                inspirationalMessages.add(i, objects.get(i).getString("Messages"));
                            }
                        } else
                            Log.v("MyFitnessPage", "Error: empty inspirational messages list.");
                    } else {
                        Log.v("MyFitnessPage", "Error accessing related messages: " + e.getMessage());
                    }
                }
            });
        } catch (Exception e){
            Log.v("MyFitnessPage", "Error in putMsgInMsgArray: " + e.getMessage());
        }
    }

    private void putDialogueInDialogueArray(ParseObject coach) {
        ParseRelation relation = coach.getRelation("saysDialogue");
        try {
            ParseQuery<ParseObject> rQ = relation.getQuery();
            rQ.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> objects, ParseException e) {
                    if (e == null) {
                        if (!objects.isEmpty()) {
                            for (int i = 0; i < objects.size(); i++) {
                                Log.v("MyFitnessPage", "In putDialogueInDialogueArray: dialogue text is \"" + objects.get(i).get("dialogueText").toString() + "\".");
                                coachPhrases.add(i, objects.get(i).getString("dialogueText"));
                                Log.v("MyFitnessPage", "In putDialogueInDialogueArray: positivity is \"" + objects.get(i).get("positiveNegative").toString() + "\".");
                                cpPositivity.add(i, objects.get(i).getString("positiveNegative"));
                                Log.v("MyFitnessPage", "In putDialogueInDialogueArray: intensity is \"" + objects.get(i).get("intensity").toString() + "\".");
                                cpIntensity.add(i, objects.get(i).getString("intensity"));
                                Log.v("MyFitnessPage", "In putDialogueInDialogueArray: occasion is \"" + objects.get(i).get("Occasion").toString() + "\".");
                                cpOccasions.add(i, objects.get(i).getString("Occasion"));
                            }
                        } else {
                            Log.v("MyFitnessPage", "Error: empty coach phrases list");
                        }
                    } else {
                        Log.v("MyFitnessPage", "Error accessing related phrases: " + e.getMessage());
                    }
                }
            });
        } catch (Exception e) {
            Log.v("MyFitnessPage", "Error in putDialogueInDialogueArray: " + e.getMessage());
        }
    }
*/

    /**
     * Method that updates the text value in inspMsg view field to another inspirational message (i.e. gets a randomly selected message
     * from messages generated by the GenerateCoachDialogue to be displayed on the screen)
     */
    private void newInspirationalMessage(){
        int i= (int) (Math.random()*inspirationalMessages.size());
        String s=inspirationalMessages.get(i);
        inspMsg.setText(s);
        //inspMsg.setText("Be inspired!");
    }

    /**
     * Method that calls on the CoachWorkoutAnalyzer to get a new coach phrase to be displayed
     */
    private void setCoachPhrase(){
        analyzer.start(coachPhrases, cpPositivity, cpIntensity, cpOccasions, whichCoach);
        String s=analyzer.getPhrase();
        //Log.v("MyFitnessPage", "Phrase from analyzer is " +s);
        coachMsg.setText(s);
    }

}
