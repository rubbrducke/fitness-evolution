package sccapstone.fitnessevolution.Coach;

import java.util.ArrayList;

/**
 * Created by Ben Aaron on 2/26/2016.
 * Last updated by Ben Aaron on 4/13/2016
 *
 * This class creates a CoachDialogue object for a given coach. This object holds all of the phrases that the coach will say, as well as
 * the positivity and intensity of the phrase, and the occasion that precipitates this phrase being selected.
 */
public class CoachDialogue {
    private String coachID = "";
    private ArrayList<String> phrases=new ArrayList<>();
    private ArrayList<String> positivity = new ArrayList<>();
    private ArrayList<String> intensity = new ArrayList<>();
    private ArrayList<String> occasions = new ArrayList<>();

    private int sizeOf = 0;

    /**
     * Method to get a string representing the coachID (Either the ID for Carol, Chris or Madeleine)
     * @return coachID
     */
    public String getCoachID(){
        return coachID;
    }

    /**
     * Method to set the coachID, and thus coach, to either Carol, Chris or Madeleine
     * @param s
     */
    public void setCoachID(String s){
        coachID = s;
    }

    /**
     * Method to get the list of all phrases this coach says
     * @return phrases
     */
    public ArrayList<String> getPhrases(){
        return phrases;
    }

    /**
     * Method to get the list of all phrases' positivity values (i.e. whether or not a phrase is positive or negative.
     * @return positivity
     */
    public ArrayList<String> getPositivity(){
        return positivity;
    }

    /**
     * Method to get the list of all phrases' intensity values (a number from 1-3 representing the intensity of the phrase)
     * @return intensity
     */
    public ArrayList<String> getIntensity(){
        return intensity;
    }

    /**
     * Method to get the list of all phrases' occasions (a string representing the situation where this phrase should be uttered)
     * @return occasions
     */
    public ArrayList<String> getOccasions(){
        return occasions;
    }

    /**
     * Method to set the coach phrases to a given arraylist of strings
     * Also checks to make sure that the size of the given array is equal to the size of all of the other lists
     * @param strings
     */
    public void setPhrases(ArrayList<String> strings){
        phrases=strings;
        if (phrases.size() == positivity.size() && positivity.size() == intensity.size() && intensity.size() == occasions.size()){
            sizeOf=phrases.size();
        }
        else {
            sizeOf = -1;
        }
    }
    /**
     * Method to set the coach positivity values to a given arraylist of strings
     * Also checks to make sure that the size of the given array is equal to the size of all of the other lists
     * @param strings
     */
    public void setPositivity(ArrayList<String> strings){
        positivity=strings;
        if (phrases.size() == positivity.size() && positivity.size() == intensity.size() && intensity.size() == occasions.size()){
            sizeOf=phrases.size();
        }
        else {
            sizeOf = -1;
        }
    }
    /**
     * Method to set the coach intensity values to a given arraylist of strings
     * Also checks to make sure that the size of the given array is equal to the size of all of the other lists
     * @param strings
     */
    public void setIntensity(ArrayList<String> strings){
        intensity=strings;
        if (phrases.size() == positivity.size() && positivity.size() == intensity.size() && intensity.size() == occasions.size()){
            sizeOf=phrases.size();
        }
        else {
            sizeOf = -1;
        }
    }
    /**
     * Method to set the coach occasions to a given arraylist of strings
     * Also checks to make sure that the size of the given array is equal to the size of all of the other lists
     * @param strings
     */
    public void setOccasions(ArrayList<String> strings){
        occasions=strings;
        if (phrases.size() == positivity.size() && positivity.size() == intensity.size() && intensity.size() == occasions.size()){
            sizeOf=phrases.size();
        }
        else {
            sizeOf = -1;
        }
    }

    /**
     * Method to add an array of strings representing a phrase and all of it's accompanying data (i.e. occasion, intensity, positivity)
     * @param strings
     * @return whether or not the array was added correctly
     */
    public boolean addTo(String[] strings){
        if (strings.length != 4){
            return false;
        }
        else {
            phrases.add(strings[0]);
            positivity.add(strings[1]);
            intensity.add(strings[2]);
            occasions.add(strings[3]);
            sizeOf++;
            return true;
        }
    }

    /**
     * Method to get an array representing the ith phrase in the coach list along with it's accompanying data
     * @param i
     * @return s
     */
    public String[] getArray(int i){
        String[] s = new String[4];
        if(i<sizeOf) {
            s[0] = phrases.get(i);
            s[1] = positivity.get(i);
            s[2] = intensity.get(i);
            s[3] = occasions.get(i);
        }
        return s;
    }

    /**
     * Method to get the current size of all of the lists
     * @return sizeOf
     */
    public int getSizeOf(){
        return sizeOf;
    }
}
