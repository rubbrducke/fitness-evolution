package sccapstone.fitnessevolution.Coach;

import java.util.ArrayList;

import sccapstone.fitnessevolution.Coach.CoachDialogue;

/**
 * Created by Ben Aaron on 2/26/2016.
 * Last updated by Ben Aaron on 4/13/2016
 *
 * This class creates three coach dialogue holders and fills them with possible coach phrase arrays.
 * Also creates an arraylist containing all of the possible inspirational messages
 * Calls upon Coach_AI class only once when new phrases are added to update the data on Parse
 */
public class GenerateCoachDialogue {
    private CoachDialogue madeleine = new CoachDialogue();
    private CoachDialogue chris = new CoachDialogue();
    private CoachDialogue carol= new CoachDialogue();
    private final static String MADELEINE_ID = "M1FNJyykku";
    private final static String CHRIS_ID = "P59Up7HLFN";
    private final static String CAROL_ID = "acjfPH7UbT";
    private ArrayList<String> phrases=new ArrayList<>();
    private ArrayList<String> positivity = new ArrayList<>();
    private ArrayList<String> intensity = new ArrayList<>();
    private ArrayList<String> occasions = new ArrayList<>();
    private ArrayList<String> messages = new ArrayList<>();
    private boolean done=false;

    /**
     * Method to setup the three holder classes and start the process of filling them with phrases
     */
    public void start(){
        //Log.v("PRBTEST", "Started GenerateCoachDialogue");
        madeleine.setCoachID(MADELEINE_ID);
        chris.setCoachID(CHRIS_ID);
        carol.setCoachID(CAROL_ID);
        generateMadeleinePhrases();
        generateChrisPhrases();
        generateCarolPhrases();
        done=true;
    }

    /**
     * Method to fill the Madeleine dialogue holder with phrases.
     */
    public void generateMadeleinePhrases(){
        //Workout phrases
        madeleine.addTo(new String[]{"Good job hitting $S today! You should start seeing results soon.", "positive", "1", "exercised"});
        madeleine.addTo(new String[]{"Great job hitting $S today! You should start seeing results in a week or two.", "positive", "2", "exercised"});
        madeleine.addTo(new String[]{"Fantastic job hitting $S today! You should start seeing results in a week or two. I'm so proud of you!", "positive", "3", "exercised"});
        madeleine.addTo(new String[]{"You worked out $S today, but next week I'd like to see you hit it harder.", "negative", "1", "exercised"});
        madeleine.addTo(new String[]{"You worked out $S today, but it wasn't enough. To see results you need to train harder.", "negative", "2", "exercised"});
        madeleine.addTo(new String[]{"You may have worked out $S today, but that was hardly enough. If you want to get better you'll have to be in a lot more effort.", "negative", "3", "exercised"});
        madeleine.addTo(new String[]{"You may need to spend some more time on your $S soon. You haven't exercised them in a while.","positive","1","underworked"});
        madeleine.addTo(new String[]{"You need to spend more time on your $S soon. With hard work and dedication comes great rewards.","positive","2","underworked"});
        madeleine.addTo(new String[]{"You really need to spend more time on your $S soon. You'll be reaping the rewards as soon as you do!","positive","3","underworked"});
        madeleine.addTo(new String[]{"You may need to spend some more time on your $S soon. You haven't exercised them in a while.","negative","1","underworked"});
        madeleine.addTo(new String[]{"You need to spend more time on your $S soon. If you don't they will get weaker and stiffer.","negative","2","underworked"});
        madeleine.addTo(new String[]{"You really need to spend more time on your $S soon. You aren't showing enough dedication to working out, and it's beginning to show.","negative","3","underworked"});
        madeleine.addTo(new String[]{"You have spent too much time working out your $S. You've earned a rest.","positive","1","overworked"});
        madeleine.addTo(new String[]{"You have spent too much time working out your $S. It's great that you've put in so much effort, but they need to rest now.","positive","2","overworked"});
        madeleine.addTo(new String[]{"You have spent too much time working out your $S. Your hard work and dedication is astounding, but everyone needs to rest now and again.","positive","3","overworked"});
        madeleine.addTo(new String[]{"You have spent too much time working out your $S. You need to rest them.","negative","1","overworked"});
        madeleine.addTo(new String[]{"You have spent too much time working out your $S. You shouldn't work these muscles for a few days.","negative","2","overworked"});
        madeleine.addTo(new String[]{"You have spent far too much time working out your $S. If you don't rest now you could seriously injure yourself.","negative","3","overworked"});
        //General phrases
        madeleine.addTo(new String[]{"Today is a great day for a run.","positive","1","general"});
        madeleine.addTo(new String[]{"Go for a hike today.","positive","1","general"});
        madeleine.addTo(new String[]{"Namaste! Take a yoga class today!","positive","1","general"});
        madeleine.addTo(new String[]{"Just keep pushing forward!","positive","2","general"});
        madeleine.addTo(new String[]{"Just keep pushing forward!","positive","2","general"});
        madeleine.addTo(new String[]{"Don't give up!","positive","2","general"});
        madeleine.addTo(new String[]{"You are amazing!","positive","3","general"});
        madeleine.addTo(new String[]{"You need to go a little harder.","negative","1","general"});
        madeleine.addTo(new String[]{"Try harder!","negative","2","general"});
        madeleine.addTo(new String[]{"Your lack of discipline is disappointing.", "negative", "3", "general"});
        //Diet phrases
        madeleine.addTo(new String[]{"Eat fewer calories tomorrow.","positive","1","tooFewCalories"});
        madeleine.addTo(new String[]{"Eat more calories tomorrow.","positive","1","tooManyCalories"});
        madeleine.addTo(new String[]{"You reached your caloric goals today.","positive","1","enoughCalories"});
        madeleine.addTo(new String[]{"You didn't eat enough fat today.","negative","1","tooLittleFat"});
        madeleine.addTo(new String[]{"You ate too much fat today.","negative","1","tooMuchFat"});
        madeleine.addTo(new String[]{"You ate the appropriate amount of fat today.","1","positive","enoughFat"});
        madeleine.addTo(new String[]{"You didn't eat enough carbohydrates today.","1","negative","tooFewCarbohydrates"});
        madeleine.addTo(new String[]{"Eat less carbohydrates tomorrow.","1","positive","tooManyCarbohydrates"});
        madeleine.addTo(new String[]{"You ate the correct amount of carbohydrates today.","1","positive","enoughCarbohydrates"});
        madeleine.addTo(new String[]{"You ate too much protein today.","1","negative","tooMuchProtein"});
        madeleine.addTo(new String[]{"Eat more protein tomorrow.","1","positive","tooLittleProtein"});
        madeleine.addTo(new String[]{"Great job eating protein today!","2","positive","enoughProtein"});

        generateMessages();
        phrases=madeleine.getPhrases();
        positivity=madeleine.getPositivity();
        intensity=madeleine.getIntensity();
        occasions=madeleine.getOccasions();
        //Run once every time new phrases are added
        //Coach_AI.createCoachSaysDialogueRecord(MADELEINE_ID, phrases, positivity, occasions, intensity);
        //Coach_AI.createCoachSaysMessageRecord(messages, MADELEINE_ID);
    }

    /**
     * Method to fill the Chris dialogue holder with phrases.
     */
    public void generateChrisPhrases(){
        //Workout Phrases
        chris.addTo(new String[]{"You hit $S today pretty hard! Maybe some day you'll be as ripped as me.", "positive", "1", "exercised"});
        chris.addTo(new String[]{"You hit $S today hard! You're getting ripped!", "positive", "2", "exercised"});
        chris.addTo(new String[]{"You hit $S today crazy hard! Its amazing how ripped you're getting!", "positive", "3", "exercised"});
        chris.addTo(new String[]{"You hit $S today pretty weak. You need to go harder next week.", "negative", "1", "exercised"});
        chris.addTo(new String[]{"You hit $S today like a weakling. Go hard or go home.", "negative", "2", "exercised"});
        chris.addTo(new String[]{"You were a real baby hitting $S today. You made my muscles cry. Do better!", "negative", "3", "exercised"});
        chris.addTo(new String[]{"You've been hitting $S a little too hard lately. Take a break, bro.", "positive", "1", "overworked"});
        chris.addTo(new String[]{"You've been hitting $S too hard lately. I'm proud of your dedication, but even I need to take breaks.", "positive", "2", "overworked"});
        chris.addTo(new String[]{"You've been hitting $S way too hard lately. Your dedication is sickening, bro, but if you want to grow you need to rest and eat.", "positive", "3", "overworked"});
        chris.addTo(new String[]{"You've been hitting $S a little too hard lately. You need to rest, bro.", "negative", "1", "overworked"});
        chris.addTo(new String[]{"You've been hitting $S too hard lately. Work smarter not harder.", "negative", "2", "overworked"});
        chris.addTo(new String[]{"You've been hitting $S way too hard lately. You're going to seriously hurt yourself if you don't rest.", "negative", "3", "overworked"});
        chris.addTo(new String[]{"You haven't hit $S in a while. You should get on that soon.", "positive", "1", "underworked"});
        chris.addTo(new String[]{"You haven't been hitting $S enough. You should take a break from your other muscles and pull these up to scratch.", "positive", "2", "underworked"});
        chris.addTo(new String[]{"You really haven't been hitting $S lately. Get on that soon and you'll be ripped and sweet like me.", "positive", "3", "underworked"});
        chris.addTo(new String[]{"You haven't hit $S in a while. You should get on that soon. You don't want to get weak, do you?", "negative", "1", "underworked"});
        chris.addTo(new String[]{"You haven't been hitting $S enough. If you don't hit them soon you're going to get weak and flabby, which is totally not sweet.", "negative", "2", "underworked"});
        chris.addTo(new String[]{"You really haven't been hitting $S lately. I'm disappointed in your lack of effort. When you get weak don't blame it on me.", "negative", "3", "underworked"});
        //General Phrases
        chris.addTo(new String[]{"When are we going to the gym today?","positive","1","general"});
        chris.addTo(new String[]{"Push it!","positive","2","general"});
        chris.addTo(new String[]{"Get Swole!","positive","3","general"});
        chris.addTo(new String[]{"Train smarter, not harder.","negative","1","general"});
        chris.addTo(new String[]{"No pain, no gain.","negative","1","general"});
        chris.addTo(new String[]{"Bro, you've got to try harder.","negative","2","general"});
        chris.addTo(new String[]{"Don't be a weakling!", "negative", "3", "general"});
        //Diet Phrases
        chris.addTo(new String[]{"You need to eat more calories tomorrow, bro.","1","positive","tooFewCalories"});
        chris.addTo(new String[]{"You kind of pigged out today. Lean off the calories tomorrow.","1","negative","tooManyCalories"});
        chris.addTo(new String[]{"You hit the sweet spot. Good caloric loading today.","1","positive","enoughCalories"});
        chris.addTo(new String[]{"Eat some more fat tomorrow.","1","positive","tooLittleFat"});
        chris.addTo(new String[]{"Don't go overboard on the fatty foods, bro.","1","negative","tooMuchFat"});
        chris.addTo(new String[]{"Way to eat that fat, bro. Moderation is key.","1","positive","enoughFat"});
        chris.addTo(new String[]{"You need more carbs in your diet, bro.","1","negative","tooFewCarbohydrates"});
        chris.addTo(new String[]{"Chillax on the carbs tomorrow. You've had enough.","1","negative","tooManyCarbohydrates"});
        chris.addTo(new String[]{"Great carbo loading. Sweet!","2","positive","enoughCarbohydrates"});
        chris.addTo(new String[]{"Hey maybe ease up on the protein tomorrow. You can have too much of a good thing.","1","positive","tooMuchProtein"});
        chris.addTo(new String[]{"You need more protein if you wanna get swole like me.","1","negative","tooLittleProtein"});
        chris.addTo(new String[]{"Nice getting in your protein today.","1","positive","enoughProtein"});

        generateMessages();
        phrases=chris.getPhrases();
        positivity=chris.getPositivity();
        intensity=chris.getIntensity();
        occasions=chris.getOccasions();
        //Run once every time new phrases are added
        //Coach_AI.createCoachSaysDialogueRecord(CHRIS_ID, phrases, positivity, occasions, intensity);
        //Coach_AI.createCoachSaysMessageRecord(messages, CHRIS_ID);
    }

    /**
     * Method to fill the Carol dialogue holder with phrases.
     */
    public void generateCarolPhrases(){
        //Workout Phrases
        carol.addTo(new String[]{"You managed to finish working out $S today. Maybe I am getting through to you.", "positive", "1", "exercised"});
        carol.addTo(new String[]{"You managed to finish working out $S today. I guess you aren't the maggot I thought you were.", "positive", "2", "exercised"});
        carol.addTo(new String[]{"You did a good job working out $S today. I'm actually proud of you.", "positive", "3", "exercised"});
        carol.addTo(new String[]{"You may think you did a good job hitting $S today. You'd be wrong, but you're used to that, aren't you.", "negative", "1", "exercised"});
        carol.addTo(new String[]{"You call that working out $S today? You're pathetic.", "negative", "2", "exercised"});
        carol.addTo(new String[]{"Alright you disgusting maggot! If you don't bring me five times as much energy the next time you work $S, I will make your life a living hell! You may think you have it tough now, but you've haven't seen anything yet.", "negative", "3", "exercised"});
        carol.addTo(new String[]{"You've been working out $S too much lately. Maybe I am getting through to you.", "positive", "1", "overworked"});
        carol.addTo(new String[]{"You've been working out $S too much lately. No pain, no gain, but you need to take a break.", "positive", "2", "overworked"});
        carol.addTo(new String[]{"You've been working out $S way too much lately. I've definitely been doing my job, but you need to take a break now.", "positive", "3", "overworked"});
        carol.addTo(new String[]{"You've been working out $S too much lately. Quit being stupid and work out something else.", "negative", "1", "overworked"});
        carol.addTo(new String[]{"You've been working out $S too much lately. Work smarter not harder you idiot! Take a break!", "negative", "2", "overworked"});
        carol.addTo(new String[]{"You've been working out $S way too much lately. When you kill yourself don't come crying to me!", "negative", "3", "overworked"});
        carol.addTo(new String[]{"You haven't been working out $S enough lately. Do it now.", "positive", "1", "underworked"});
        carol.addTo(new String[]{"You haven't been working out $S enough lately. Do it now!", "positive", "2", "underworked"});
        carol.addTo(new String[]{"You have not been working out $S nearly enough lately. If you don't do it now, I'm quitting.", "positive", "3", "underworked"});
        carol.addTo(new String[]{"You haven't been working out $S enough lately, you lazy bum. Do it now!", "negative", "1", "underworked"});
        carol.addTo(new String[]{"You haven't been working out $S enough lately, you fat slob. Do it now!!", "negative", "2", "underworked"});
        carol.addTo(new String[]{"You have not been working out $S nearly enough lately, you fat, lazy pig! If you don't do it now, I'm quitting.", "negajtive", "3", "underworked"});
        //General Phrases
        carol.addTo(new String[]{"I want you to do at least 20 minutes of cardio today.","positive","1","general"});
        carol.addTo(new String[]{"If I can hear you talking you aren't going hard enough.","positive","2","general"});
        carol.addTo(new String[]{"Just do it!","positive","3","general"});
        carol.addTo(new String[]{"Stop being lazy!","negative","1","general"});
        carol.addTo(new String[]{"20 push-ups now!","negative","1","general"});
        carol.addTo(new String[]{"30 curl-ups now!","negative","1","general"});
        carol.addTo(new String[]{"Get off your butt and run!","negative","2","general"});
        carol.addTo(new String[]{"Go to the gym or I'll make you go to the gym!", "negative", "3", "general"});
        //Diet Phrases
        carol.addTo(new String[]{"You need to eat more calories tomorrow. Then maybe you'll be able to keep up with me.","1","positive","tooFewCalories"});
        carol.addTo(new String[]{"You ate way to much today. You're going to regret that tomorrow, maggot.","1","negative","tooManyCalories"});
        carol.addTo(new String[]{"You ate the right amount of calories today. Good. Now get back to work.","1","positive","enoughCalories"});
        carol.addTo(new String[]{"Eat more fat, maggot!","2","negative","tooLittleFat"});
        carol.addTo(new String[]{"You went overboard on the fatty foods today maggot. I'll make you wish you hadn't.","1","negative","tooMuchFat"});
        carol.addTo(new String[]{"You ate the right amount of fat today. Good. Now get back to work.","1","positive","enoughFat"});
        carol.addTo(new String[]{"You didn't eat enough carbs today. You don't want to go into ketosis, do you?","1","negative","tooFewCarbohydrates"});
        carol.addTo(new String[]{"Lay off the sugar, maggot. You had more than enough of it today.","1","negative","tooManyCarbohydrates"});
        carol.addTo(new String[]{"You ate the right amount of carbohydrates today. Good. Now get back to work.","1","positive","enoughCarbohydrates"});
        carol.addTo(new String[]{"More protein isn't always a good thing, but at least you're trying...maggot.","1","positive","tooMuchProtein"});
        carol.addTo(new String[]{"Eat more protein or you will always be the weak and scrawny maggot that you are today!","1","positive","tooLittleProtein"});
        carol.addTo(new String[]{"You ate the right amount of protein today. Good. Now get back to work.","1","positve","enoughProtein"});

        generateMessages();
        phrases=carol.getPhrases();
        positivity=carol.getPositivity();
        intensity=carol.getIntensity();
        occasions=carol.getOccasions();
    }

    /**
     * Method to return the Madeleine coach dialogue holder object
     * @return madeleine
     */
    public CoachDialogue getMadeleine(){
        return madeleine;
    }

    /**
     * Method to return the Chris coach dialogue holder object
     * @return chris
     */
    public CoachDialogue getChris(){
        return chris;
    }

    /**
     * Method to return the Carol coach dialogue holder object
     * @return carol
     */
    public CoachDialogue getCarol(){
        return carol;
    }

    /**
     * Method to return the list of inspirational messages
     * @return messages
     */
    public ArrayList<String> getMessages(){
        return messages;
    }

    /**
     * Method to see if this class has finished generating all objects
     * @return done
     */
    public boolean isDone(){
        return done;
    }

    /**
     * Method to fill the messages list with inspirational messages
     */
    private void generateMessages(){
        //messages.add("Attitude: It is our best friend or our worst enemy.");

        String[] msgArray = new String[]{"Attitude: It is our best friend or our worst enemy.",

        "Your attitude determines your altitude!",

        "A strong positive attitude will create more miracles than any wonder drug.",

        "The greatest revolution of our generation is the discovery that human beings, by changing the inner attitudes of their minds, can change the outer aspects of their lives.",

        "The world of achievement has always belonged to the optimist.",

        "If you change the way you look at things, the things you look at change.",

        "Nothing can stop the man with the right mental attitude from achieving his goal; nothing on earth can help the man with the wrong mental attitude.",

        "You can complain that roses have thorns, or rejoice that thorns have roses.",

        "It is our attitude at the beginning of a difficult task which, more than anything else, will affect its successful outcome.",

        "Life is 10% what happens to us and 90% how we react to it.",

        "The last of the human freedoms is to choose one’s attitude in any given set of circumstances.",

        "Everyone faces defeat. It may be a stepping-stone or a stumbling block, depending on the mental attitude with which it is faced.",
        "Life is a grindstone. Whether it grinds us down or polishes us up depends on us.",

        "If you don’t like something, change it. If you can’t change it, change your attitude. Don’t complain.",

        "Progress is impossible without change and those who cannot change their minds cannot change anything.",

        "If you change the way you look at things, the things you look at change.",

        "The difference between a mountain and a molehill is your perspective."

        };
        for (int i=0; i<msgArray.length; i++){
            messages.add(msgArray[i]);
        }
    }
}
