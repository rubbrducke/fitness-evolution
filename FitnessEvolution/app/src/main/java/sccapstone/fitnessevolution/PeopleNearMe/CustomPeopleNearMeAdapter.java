package sccapstone.fitnessevolution.PeopleNearMe;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseUser;

import java.util.ArrayList;

import sccapstone.fitnessevolution.UserProfile.FragmentViewProfile;
import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.R;

/**
 * @author wagner, Tyler
 *
 * Extends an ArrayAdapter that holds the people near the user in a listview.
 *   Allows the user to add or remove those that have been discovered.
 *
 *   Uses the nearme_list_item layout
 */
public class CustomPeopleNearMeAdapter extends ArrayAdapter<ParseUser>
{
    private ArrayList<ParseUser> peopleNearMeList;
    private FragmentPeopleNearMe mFragPeopleNearMe;

    /**
     * Initializes the data
     * @param context
     * @param users
     * @param frag
     */
    public CustomPeopleNearMeAdapter(Context context, ArrayList<ParseUser> users, FragmentPeopleNearMe frag)
    {
        super(context, 0, users);

        //Setup the Friends List
        peopleNearMeList = users;

        mFragPeopleNearMe = frag;
    }

    /**'
     * Inflate the adapter
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {

        // Get the data item for this position
        ParseUser user = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.nearme_list_item, parent, false);
        }

        String name = user.get("first_name") + " " + user.get("last_name");
        Bitmap image = null;

        // Lookup view for user
        ImageView friendAvatar = (ImageView) convertView.findViewById(R.id.friend_avatar);
        TextView userId = (TextView) convertView.findViewById(R.id.friend_username);
        TextView userName = (TextView) convertView.findViewById(R.id.friend_name);
        Button btnAddFriend = (Button) convertView.findViewById(R.id.btn_add_friend);

//        if(Utilities.friend.isFriend(user.getObjectId()) == -1) {
            btnAddFriend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //                Toast.makeText(getContext(), "AddUser: " + peopleNearMeList.get(position).getUsername(),
                    //                        Toast.LENGTH_SHORT).show();
                    //Adding Friends, TODO: Update later
                    Utilities.friend.addFriend(mFragPeopleNearMe.getContext(), peopleNearMeList.get(position).getUsername());
                }
            });
//        }
//        else
//        {
//            btnAddFriend.setEnabled(false);
//            btnAddFriend.setVisibility(View.GONE);
//        }

        // Populate the data into the view
        userId.setText(user.getUsername());
        userName.setText(name);

        image = Utilities.getAvatar(getContext(), user);
        friendAvatar.setImageBitmap(image);

        //Sets the Clickable and on Click Listener
        convertView.setLongClickable(true);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getContext(), "You Clicked " + peopleNearMeList.get(position).getUsername(), Toast.LENGTH_LONG).show();
                FragmentViewProfile viewProfileFrag = new FragmentViewProfile().newInstance(peopleNearMeList.get(position).getObjectId());
                FragmentTransaction ft = mFragPeopleNearMe.getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, (viewProfileFrag));
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        // Return the completed view to render on screen
        return convertView;
    }

    /**
     * Tell the array adapter to refresh.
     *
     * This is during any remvoing / adding friend.
     * @param users
     */
    public void updateResults(ArrayList<ParseUser> users)
    {
        peopleNearMeList = users;

        //Triggers the list update
        this.notifyDataSetChanged();
    }
}//CustomPeopleNearMeAdapter
