package sccapstone.fitnessevolution.PeopleNearMe;

//Github Site
//https://github.com/googlemaps/android-samples/blob/

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.R;
import sccapstone.fitnessevolution.Utilities.UtilitiesMaps;
//Google Maps Activity

/**
 * @author wagner, Tyler
 *
 * Fragment that displays the people near me on a map.
 *   Allows the user to discover others via map.
 *
 *   Uses the actvity_map layout
 */
public class FragmentNearMe extends SupportMapFragment
        implements OnMapReadyCallback,
        //For GooglePlayServices
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{
    private final int MY_LOCATION_REQUEST_CODE = 1;
    private boolean isMapReady = false;
    private Location myLocation = null;
    private GoogleMap map;
    private GoogleApiClient mGoogleApiClient;

    /**
     * Inflate the layout and setup the map.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_map, container, false);


//        Log.v("Fragment", ""+this.getActivity().getSupportFragmentManager().findFragmentById(R.id.map));
        this.getMapAsync(this);

        //Sets up Google Play Services to get My Current Location
        mGoogleApiClient = new GoogleApiClient.Builder(this.getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();

        // Hide the keyboard if it is currently shown
        Utilities.hideKeyboard(getContext(), getActivity());

        // Inflate the layout for this fragment
        return view;
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FragmentNearMe newInstance(int sectionNumber)
    {
        Log.v("FragmentNearMe", "newInstance");
        FragmentNearMe fragment = new FragmentNearMe();
        Bundle args = new Bundle();
        args.putInt(Utilities.ARG_SECTION_NUMBER_KEY, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

//    @Override
//    protected void onCreate(Bundle savedInstanceState)
//    {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_map);
//
//        SupportMapFragment mapFragment =
//                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
//
//        //Sets up Google Play Services to get My Current Location
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .addApi(LocationServices.API)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .build();
//        mGoogleApiClient.connect();
//    }

    /**
     * Maps is ready to go to be used
     */
    @Override
    public void onMapReady(GoogleMap map)
    {
        this.map = map;
        this.isMapReady = true;
        Log.v("onMapReady", String.valueOf(isMapReady));
        Log.v("map nuLL?", map == null ? "null" : "not null");

        //To get the compass button clickable to go to my location (o-)
        if (ContextCompat.checkSelfPermission(this.getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED)
        {
            map.setMyLocationEnabled(true);
        } else
        {
            // Show rationale and request permission.
            Toast.makeText(this.getActivity(), "Fine Location is not granted", Toast.LENGTH_SHORT).show();
        }

        //Query People
        ParseGeoPoint userLocation = (ParseGeoPoint) Utilities.user.get("last_location");
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereNear("last_location", userLocation);
        //Ensure we don't pull the last location of the logged in user
        query.whereNotEqualTo("username", Utilities.user.getUsername());
        query.setLimit(15);
        final GoogleMap m = this.map;
        query.findInBackground(new FindCallback<ParseUser>() {
            public void done(List<ParseUser> dataList, ParseException e) {
                if (e == null)
                {
                    //No Errors
                    for (ParseUser parseUsr : dataList)
                    {
//                        if(parseUsr.getUsername().equalsIgnoreCase("prestonTestDoNotDelete"))
//                        {
                            ParseGeoPoint gp = (ParseGeoPoint) parseUsr.get("last_location");
                            LatLng person = new LatLng(gp.getLatitude(), gp.getLongitude());
                            m.addMarker(UtilitiesMaps.createMarkerPerson(parseUsr, person));
//                        }
//                        else
//                        {
//                            ParseGeoPoint gp = (ParseGeoPoint) parseUsr.get("last_location");
//                            LatLng person = new LatLng(gp.getLatitude(), gp.getLongitude());
//                            m.addMarker(new MarkerOptions()
//                                    .title(parseUsr.getUsername())
//                                    .snippet(parseUsr.getString("last_name") + ", " + parseUsr.getString("first_name"))
//                                    .position(person));
//                        }
                    }
                }
                else
                {
                    Log.v("Parse Query Geo Error", e.getMessage());
                }
            }
        });

//        LatLng africa = new LatLng(0.0, 0.0);
//        map.moveCamera(CameraUpdateFactory.newLatLngZoom(africa, 13));
//        map.addMarker(new MarkerOptions()
//                .title("0.0-0.0")
//                .snippet("Lat / Long: 0,0")
//                .position(africa));
//        this.
    }

    /**
     * Callback from Map Geo Services
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_LOCATION_REQUEST_CODE) {
            if (permissions.length == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                map.setMyLocationEnabled(true);
            }
            else
            {
                // Permission was denied. Display an error message.
                Toast.makeText(this.getActivity(), "Error: Check your GPS or Internet Connection", Toast.LENGTH_SHORT)
                        .show();
            }
        }

    }


    /**
     * Call back from GeoServices
     * @param connectionHint
     */
    //For
    @Override
    public void onConnected(Bundle connectionHint)
    {
        this.myLocation = LocationServices.FusedLocationApi.getLastLocation
                (mGoogleApiClient);
        if (myLocation != null && isMapReady)
        {
            double lat = myLocation.getLatitude();
            double lon = myLocation.getLongitude();
            Log.v("Lat", String.valueOf(lat));
            Log.v("Long", String.valueOf(lon));
            LatLng myLocation = new LatLng(this.myLocation.getLatitude(), this.myLocation.getLongitude());
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 17));

//            ParseGeoPoint userGeopoint = new ParseGeoPoint(lat, lon);
//            Utilities.user.put("last_location", userGeopoint);
//            Utilities.user.saveInBackground();

//            ParseRelation relation = Global_Settings.user.getRelation("last_location");
//            ParseObject obj = new ParseObject("_User");
//
//            obj.add("username", Global_Settings.user.getUsername());
//            obj.add("last_location", myGeopoint);
//            relation.add(obj);
//            obj.saveInBackground();
//            relation.add(obj);
//            Global_Settings.user.();
//            Global_Settings.user.saveInBackground();

//            https://www.google.com/maps/@lat,long,15z
//            mLatitudeText.setText(String.valueOf(mLastLocation.getLatitude()));
//            mLongitudeText.setText(String.valueOf(mLastLocation.getLongitude()));
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

}
