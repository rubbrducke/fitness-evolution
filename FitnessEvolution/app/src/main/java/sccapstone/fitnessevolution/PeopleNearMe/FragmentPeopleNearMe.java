package sccapstone.fitnessevolution.PeopleNearMe;

/**
 * Created by tyler on 2/15/16.
 */

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import java.util.ArrayList;
import java.util.List;

import sccapstone.fitnessevolution.Activites.ActivityMain;
import sccapstone.fitnessevolution.UserProfile.FragmentViewProfile;
import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.Messaging.FragmentComposeMessage;
import sccapstone.fitnessevolution.Messaging.FragmentMessageThread;
import sccapstone.fitnessevolution.R;

/**
 * @author wagner, Tyler
 *
 * Fragment that allows the displays all of the users around him or her in a listview.
 *   Allows the user to add, remove, or message those that have been discovered.
 *
 *   Uses the nearme_fragment layout
 */
public class FragmentPeopleNearMe extends Fragment
{
    private ArrayList<ParseUser> arrayOfUsers = null;
    private CustomPeopleNearMeAdapter adapter = null;

    //For Menu Options
    private final int ID_VIEW_PROFILE = 0,
                      ID_MESSAGE_USER = 1;

    /**
     * Initializes data
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Construct the data source
        arrayOfUsers = new ArrayList<ParseUser>();
        // Create the adapter to convert the array to views
        adapter = new CustomPeopleNearMeAdapter(this.getContext(), arrayOfUsers, this);
    }

    /**
     * Inflates the views
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        Activity activity = getActivity();
        ((ActivityMain) activity).setActionBarTitle( getResources().getString(R.string.nav_social_nearme));

        View view = inflater.inflate(R.layout.nearme_fragment, container, false);

        //Set up the ListView and Array Adapter
        // Attach the adapter to a ListView and register for context menu
        ListView listView = (ListView) view.findViewById(R.id.listview_people_nearme);
        listView.setAdapter(adapter);
        registerForContextMenu(listView);

        //Query People
        ParseGeoPoint userLocation = (ParseGeoPoint) Utilities.user.get("last_location");
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        //Find the people near me
        query.whereNear("last_location", userLocation);
        //Ensure we don't pull the last location of the logged in user
        query.whereNotEqualTo("username", Utilities.user.getUsername());
        query.setLimit(25);
        query.findInBackground(new FindCallback<ParseUser>() {
            public void done(List<ParseUser> dataList, ParseException e) {
                if (e == null) {
                    //No Errors
                    Log.v("Num Users Collected", String.valueOf(dataList.size()));
//                    for (ParseUser parseUsr : dataList)
//                    {
//                        arrayOfUsers.add(parseUsr);
//                    }
                    arrayOfUsers.addAll(dataList);
                    adapter.notifyDataSetChanged();
                } else
                {
                    Log.v("Parse Query Geo Error", e.getMessage());
                }
            }
        });

        //TODO: CHECK FOR FRIENDSHIP AND DISABLE ADD FRIEND BUTTON

        // Inflate the layout for this fragment
        return view;
    }

    /****************** Context Menu ********************/
    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int clickedPosition = info.position;

        switch(item.getItemId())
        {
            case ID_VIEW_PROFILE:
                FragmentViewProfile viewProfileFrag = new FragmentViewProfile().newInstance(adapter.getItem(clickedPosition).getObjectId());
                FragmentTransaction ft = this.getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, (viewProfileFrag));
                ft.addToBackStack(null);
                ft.commit();
                return true;

            case ID_MESSAGE_USER:
                sendMessageToFriend(adapter.getItem(clickedPosition).getObjectId(),
                        adapter.getItem(clickedPosition).getUsername());
                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);

        //Set Title
        String title = "User Options";
        menu.setHeaderTitle(title);

        //Set the Rest of the Menu Options
        menu.add(Menu.NONE, ID_VIEW_PROFILE, Menu.NONE, "View Profile");
        menu.add(Menu.NONE, ID_MESSAGE_USER, Menu.NONE, "Message User");
    }
    /****************** Context Menu ********************/

    /*
        sendMessageToFriend Author: Preston Barbare
     */
    private static ArrayList<String> arrConvParticipantObjIDs; // Must be a member var due to Parse API.
    public void sendMessageToFriend(String friendObjID, final String friendUserName) {
        // Determine if we need to add to a message thread, or start a new one.
        arrConvParticipantObjIDs = new ArrayList<String>();
        arrConvParticipantObjIDs.add(ParseUser.getCurrentUser().getObjectId());
        arrConvParticipantObjIDs.add(friendObjID);

        ParseQuery<ParseObject> sentMessageQuery = ParseQuery.getQuery("Conversations");
        sentMessageQuery.whereContainsAll("convParticipantObjectIDs", arrConvParticipantObjIDs);

        //Log.v("PRBTEST","Querying table for a record with convParticipantObjectIDs of " + ParseUser.getCurrentUser().getObjectId() + " and " + friendObjID);

        sentMessageQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject messageThreadRecord, ParseException e) {
                if (e == null) {
                    // A conversation with this friend already exists, so launch the message thread fragment for that conversation
                    FragmentMessageThread newFragMessageThread = new FragmentMessageThread().newInstance(messageThreadRecord.getObjectId(), friendUserName);
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, (newFragMessageThread));
                    ft.addToBackStack(null);
                    ft.commit();
                } else {
                    // No previous conversation with this friend, so launch compose fragment.
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, (new FragmentComposeMessage().newInstance(friendUserName)));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });
    }

    @Override
    public void onAttach(Context activity)
    {
        Log.v("onAttach", "FragmentPeopleNearMe");
        super.onAttach(activity);
        ((ActivityMain) activity).onSectionAttached(
                getArguments().getInt(Utilities.ARG_SECTION_NUMBER_KEY));
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FragmentPeopleNearMe newInstance(int sectionNumber)
    {
        Log.v("FragmentPeopleNearMe", "newInstance");
        FragmentPeopleNearMe fragment = new FragmentPeopleNearMe();

        Bundle args = new Bundle();
        args.putInt(Utilities.ARG_SECTION_NUMBER_KEY, sectionNumber);
        fragment.setArguments(args);

        return fragment;
    }
}//FragmentFriends Class

