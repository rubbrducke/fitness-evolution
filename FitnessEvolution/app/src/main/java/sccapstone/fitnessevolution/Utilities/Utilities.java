 package sccapstone.fitnessevolution.Utilities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import sccapstone.fitnessevolution.Friends.Friend;
import sccapstone.fitnessevolution.R;

 /**
 *
 * Designed to hold all of the Utilities Settings the app will need to access
 *
 * @author wagner, Tyler
 * @date   11-05/15
 */
public class Utilities
{
    // For loading screen
    private static ProgressDialog dialog;
    // For Parse.com
    public final static String PARSE_APP_ID = "6VtVnpalihrgHO3Lx9qUisYQmXwNomL3BzjlVw1U";
    public final static String PARSE_CLIENT_ID = "8RSl4cLyvBaBIKMhkpiYTJVskUeCoRPQxG4co7zB";
    public final static int PARSE_NO_RESULTS_FOUND_EXCEPTION_CODE = 101;
    //For Google Maps
    public final static String MAPS_GOOGLE_API = "AIzaSyD2QhwR8sjgOkTM8JtYqJoZ_MvjdDd5_wE";

    //Parse.com push notifications
    public final static String PARSE_CHANNEL_DEFAULT = "FitnessEvolution";

    // For Navigation Drawer
    public final static int NAV_COACH = 0;
    public final static int NAV_MESSAGES = 1;
    public final static int NAV_LOGGING = 2;
    public final static int NAV_HISTOGRAM = 3;
    public final static int NAV_MY_FRIENDS = 4;
    public final static int NAV_PEOPLE_NEAR_ME = 5;
    public final static int NAV_MY_PROFILE = 6;
    public final static int NAV_SETTINGS = 7;
    public final static int NAV_ABOUT = 8;

    public final static String ARG_SECTION_NUMBER_KEY = "section_number";

    public static ParseUser user = null;
    public static Friend friend = null;
    public static boolean loggedIn = false;

    //Geo_Utils
    public static UtilitiesGeo geo;

    /****** Common Methods / Utilities ******/
    public static boolean hasNetworkConnection(Context ct)
    {
        boolean hasNetworkConnection = false;
        boolean hasWifiConnection = false;
        boolean hasMobileConnection = false;

        ConnectivityManager cm = (ConnectivityManager) ct.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();     //Using Deprecated Method because of API targeting of 19
        for (NetworkInfo ni : netInfo)
        {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
            {

                if (ni.isConnected())
                    hasWifiConnection = true;
            }
            else if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
            {
                if (ni.isConnected())
                    hasMobileConnection = true;
            }
        }
        hasNetworkConnection = hasWifiConnection || hasMobileConnection;

        //For the different types of networks.
        //Will be easy to implement 'wifi' only aspects of the app
        Log.v("Network MOBILE", String.valueOf(hasMobileConnection));
        Log.v("Network WIFI", String.valueOf(hasWifiConnection));
        Log.v("Network Connection", String.valueOf(hasNetworkConnection));


        //Higher APIs
//        ConnectivityManager cm = (ConnectivityManager) ct.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo ni = cm.getActiveNetworkInfo();
//        if ((ni != null) && (ni.isConnected()))
//        {
//            hasNetworkConnection = true;
//        }

        return hasNetworkConnection;
    }//hasNetworkConnection(Context)

    /**
     * Generic way to make Toast for the users to see
     * @param ct
     * @param text
     * @param duration
     * @return
     */
    public static Toast toastMakeText(Context ct, CharSequence text, int duration)
    {
        return Toast.makeText(ct, text, duration);
    }

    // Basic helper method to do the following:
    // Input: 01/15/2016
    // Output: 1/15/2016
    public static String trimLeadingZeroesOnDate(String date) {
        if (date.startsWith("0")) {
            date = date.substring(1,date.length());
        }
        return date;
    }

    // Takes a 24 hr time and converts to 12 hr time
    // Input: 13:41
    // Output: 1:41 PM
    public static String convertTo12Hour(String time) {
        String returnString = "";

        try {
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("h:mm a");
            Date _24HourDt = _24HourSDF.parse(time);
            returnString = _12HourSDF.format(_24HourDt);
        } catch (final java.text.ParseException e) {
            e.printStackTrace();
        }

        return returnString;
    }

    // Takes a 12 hr time and converts to 24 hr time
    // Input: 1:41 PM
    // Output: 13:41
    public static String convertTo24Hour(String time) {
        SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
        SimpleDateFormat _12HourSDF = new SimpleDateFormat("h:mm a");
        try {
            Date _12HourDt = _12HourSDF.parse(time);
            time = _24HourSDF.format(_12HourDt);
        } catch (final java.text.ParseException e) {
            e.printStackTrace();
            time = "";
        }

        return time;
    }

    /**
     * Checks to see if a user is male or not.
     *   This is used to distinguish between default avatars
     * @param gender
     * @return
     */
    public static boolean isMale(String gender)
    {
        boolean isMale = false;

        if(gender.equalsIgnoreCase("Male"))
            isMale = true;

        return isMale;
    }

    /**
     * Based on a user, get his or her avatar.
     *   If they do not have an avatar, select one based upon gender.
     * @param ct
     * @param user
     * @return
     */
    public static Bitmap getAvatar(Context ct, ParseUser user)
    {
        Bitmap image = null;

        if(user.getParseFile("avatar") != null)
        {
            ParseFile pf = user.getParseFile("avatar");
            try {
                byte[] bitmapData = pf.getData();
                image = BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length);
            }catch(ParseException pe)
            {
                Log.v("Exception", "Loading Avatar for user: " + user.getUsername());
            }

        }
        else if( isMale(user.getString("gender")) )
        {
            image = BitmapFactory.decodeResource(ct.getResources(),
                    R.drawable.male);
        }
        else
        {
            image = BitmapFactory.decodeResource(ct.getResources(),
                    R.drawable.female);
        }

        return image;
    }


    private static ParseUser foundUser = null; //Only used for getUser Method
    private static List<ParseUser> foundUsers = null;
    public static ParseUser getUserByUsername(final String username)
    {
        foundUser = null;

        //Find User by the username
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("username", username);
        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> users, ParseException e) {
                if (e == null) {
                    //Check for Empty List
                    if (users.size() == 0) {//Could not find the user;
                        Log.v("G:getUser(userName)", username + " does not exist");
                    } else {
                        foundUser = users.get(0);
                    }
                } else {
                    Log.v("Query Find User Error", e.getMessage());
                }
            }
        });

        return foundUser;
    }

    public static ParseUser getUserByObjecId(final String objectId)
    {
        foundUser = null;

        //Find User by the objectId
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.getInBackground(objectId, new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser user, ParseException e) {
                if (e == null) {
                    //Check for Empty List
                    foundUser = user;
                } else {
                    Log.v("Query Find User Error", e.getMessage());
                }
            }
        });

        return foundUser;
    }

    /**
     * Queries the users by their object ids
     * @param objectIds
     * @param users
     * @param adapter
     * @param append
     */
    public static void getUserByObjectId(final ArrayList<String> objectIds, final ArrayList<ParseUser> users,
                                                        final ArrayAdapter<Object> adapter, final boolean append)
    {
        //Find Users by the objectId
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereContainedIn("objectId", objectIds);
        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> returnedUsers, ParseException e) {
                if (e == null) {
                    Log.v("Returned:", "" + users.size());

                    if (!append) {
                        users.clear();
                    }
                    users.addAll(returnedUsers);

                    if (adapter != null)
                        adapter.notifyDataSetChanged();
                } else {
                    Log.v("Query Find User Error", e.getMessage());
                }
            }
        });
    }

    public static void showLoadingScreen(Context context) {
        if (dialog != null) {
            dialog.hide();
        }
        dialog = new ProgressDialog(context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Loading...");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public static void hideLoadingScreen() {
        dialog.hide();
    }

    /**
     * This method sets up things so that the keyboard will hide if (inside of a fragment) the
     * user clicks outside of the currently focused item.
     *
     * @param mainLayout   the ROOT layout of the FRAGMENT
     * @param parentActivity   parent activity of the fragment (fragmentXXX.getActivity())
     */
    public static void hideKeyboardOnClickOfMainFragmentLayout(View mainLayout, final Activity parentActivity) {
        mainLayout.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                InputMethodManager imm = (InputMethodManager) parentActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(parentActivity.getCurrentFocus().getWindowToken(), 0);

                return false;
            }
        });
    }

    public static void hideKeyboard(Context context, Activity activity) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null) {
            imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static int calculateAgeInYears(Date birthDate)
    {
        int years = 0;

        //create calendar object for birth day
        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeInMillis(birthDate.getTime());

        //create calendar object for current day
        long currentTime = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(currentTime);

        //Get difference between years
        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);

        //Create new Age object
        return years;
    }


    /*--------------------------------------------------------------------

        GET WEEK

        Finds the dates of the beginning and end of a specific week.

        Returns Pair< Sunday date, Saturday date > from X weeks ago

    --------------------------------------------------------------------*/

    public static Pair<Date, Date> getWeek( int weeksPast )
    {

        Date weekStart;
        Date weekEnd;

        /*--------------------------------------------------------------------
            Get calendar set to current date and time
        --------------------------------------------------------------------*/

        Calendar c = Calendar.getInstance();

        /*--------------------------------------------------------------------
            Set calendar to Sunday of the current week
        --------------------------------------------------------------------*/

        c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        c.add(Calendar.DATE, -1);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);

        /*--------------------------------------------------------------------
            Go back X weeks from current date
        --------------------------------------------------------------------*/

        c.add(Calendar.DATE, -7*weeksPast);
        weekStart = c.getTime();

        c.add(Calendar.DATE, 1);
        c.set(Calendar.HOUR_OF_DAY, 0);

        /*--------------------------------------------------------------------
            Go 6 days forward, to the end of the week
        --------------------------------------------------------------------*/

        c.add(Calendar.DATE, 6);
        weekEnd = c.getTime();

        //region Debug
        Log.d("GET WEEK", "Start of week: " + weekStart + " End of week: " + weekEnd);
        //endregion

        return new Pair<>(weekStart, weekEnd);

    } // function

}//Utilities Class
