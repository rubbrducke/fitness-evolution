package sccapstone.fitnessevolution.Utilities;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.parse.ParseGeoPoint;

import sccapstone.fitnessevolution.Manifest;

/**
 * @author wagner, Tyler
 *
 * Class the enables the app to get the location of the user.
 *
 */
public class UtilitiesGeo extends android.support.v4.app.Fragment
        //For GooglePlayServices
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public GoogleApiClient mGoogleApiClient;
    public Location currentLocation = null;
    public boolean onConnected = false;
    public boolean shouldSaveLocation = false;
    private Activity mActivity;

    public UtilitiesGeo(Activity activity) {//Setup GoogleApiClient
        //Sets up Google Play Services to get My Current Location
        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
        mActivity = activity;
    }

    public boolean hasLocationPermission(AppCompatActivity thisActivity)
    {
        boolean hasLocationPermission = false;

        // Assume thisActivity is the current activity
        int permissionCheck = ContextCompat.checkSelfPermission(thisActivity,
                Manifest.permission.ACCESS_FINE_LOCATION);

        if(permissionCheck != PackageManager.PERMISSION_GRANTED)
        {
//            ActivityCompat.requestPermissions(thisActivity,
//                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                    );
        }
        else
        {
            hasLocationPermission = true;
        }


        return hasLocationPermission;
    }

    public Location getLocationAfterPermissionCheck() {
        this.currentLocation = LocationServices.FusedLocationApi.getLastLocation
                (mGoogleApiClient);
        Log.v("getLocation", currentLocation == null ? "null" : "not null");
//        if (currentLocation != null) {
//            double lat = currentLocation.getLatitude();
//            double lon = currentLocation.getLongitude();
//            Log.v("Lat", String.valueOf(lat));
//            Log.v("Long", String.valueOf(lon));
//        }

        return currentLocation;
    }

    private void showGallery() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == GET_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLocationAfterPermissionCheck();
            }
            else {
                //Toast.makeText(getContext(), "Permission was not granted", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    final private int GET_LOCATION = 99;
    public void getLocation(Activity activity) {

        // Check permissions
        final String ACCESS_FINE_LOCATION = "android.permission.ACCESS_FINE_LOCATION";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            // Check/Request runtime permissions
            if (mActivity.checkSelfPermission(ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                getLocationAfterPermissionCheck();
            }
            else {
                if (mActivity.shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
                    Toast.makeText(mActivity,"Gallery permission is needed to select a profile image.", Toast.LENGTH_SHORT).show();
                }

                mActivity.requestPermissions(new String[]{ACCESS_FINE_LOCATION}, GET_LOCATION);
            }
        }
        else {
            // Permissions already accepted at install time
            getLocationAfterPermissionCheck();
        }

    }
    public void saveLocation()
    {
        if(onConnected)
        {
            if (currentLocation != null) {
                shouldSaveLocation = false;
                getLocation(mActivity);

//Save the Current Location to Parse
                double lat = currentLocation.getLatitude();
                double lon = currentLocation.getLongitude();
                LatLng myLocation = new LatLng(this.currentLocation.getLatitude(), this.currentLocation.getLongitude());
                ParseGeoPoint userGeopoint = new ParseGeoPoint(lat, lon);
                Utilities.user.put("last_location", userGeopoint);
                Utilities.user.saveInBackground();
            }
        }
        else
        {
            shouldSaveLocation = true;
        }
    }

    @Override
    public void onConnected(Bundle bundle)
    {
        onConnected = true;
        getLocation(mActivity);

        if(shouldSaveLocation)
        {
            saveLocation();
        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult)
    {
        onConnected = false;
    }

    @Override
    public void onConnectionSuspended(int i)
    {
        onConnected = false;
    }

}//UtilitiesGeo Class
