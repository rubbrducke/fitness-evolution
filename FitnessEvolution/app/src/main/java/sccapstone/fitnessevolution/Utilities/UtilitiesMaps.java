package sccapstone.fitnessevolution.Utilities;

import android.util.Log;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.ParseUser;

import sccapstone.fitnessevolution.R;

/**
 * Created by wagner, Tyler on 11/17/15.
 */
public class UtilitiesMaps
{
    public static MarkerOptions createMarkerPerson(ParseUser user, LatLng location)
    {
        //Validation
        if(user == null)
        {
            Log.v("createMarkerPerson", "ParseUser: null");
            return null;
        }
        else if(location == null)
        {
            Log.v("createMarkerPerson", "LatLng: null");
            return null;
        }

        MarkerOptions marker = new MarkerOptions();
        marker.position(location);
        marker.title(user.getUsername());
        marker.snippet(user.getString("last_name") + ", " + user.getString("first_name"));

        //Depending on gender, display icon
        Log.v("Gender", "" + user.getString("gender"));
        if(user.getString("gender").equalsIgnoreCase("Male"))
        {
            BitmapDescriptor image = BitmapDescriptorFactory.fromResource(R.drawable.male);
            if(image != null)
                marker.icon(image);
        }
        else
        {
            BitmapDescriptor image = BitmapDescriptorFactory.fromResource(R.drawable.female);
            if (image != null)
                marker.icon(image);
        }
        return marker;
    }//end Marker createMarkerPerson()

}//UtilitiesMaps Class
