package sccapstone.fitnessevolution.Histogram;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TabHost;

import com.jjoe64.graphview.GraphView;

import sccapstone.fitnessevolution.Activites.ActivityMain;
import sccapstone.fitnessevolution.Logging.DietaryLogging.DietaryLog;
import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.R;

/*====================================================================

    FRAGMENT HISTOGRAM
    Created by Matthew Short

    This is the code backend for the histogram fragment.

====================================================================*/

public class FragmentHistogram extends Fragment
{

    public FragmentHistogram()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }


    /*--------------------------------------------------------------------

        ON CREATE VIEW

        Overridden method where dynamic fragment functionality
        will be added.

    --------------------------------------------------------------------*/

    TabHost tabHost;
    View inflatedView = null;


    /*--------------------------------------------------------------------
        F.M.L.

        The GraphView library is one of the few graph libraries available
        that is reasonably well documented. However, it's also a bastard
        that breaks when I completely change the data of a graph. So
        instead of just modifying one graph depending on the data I want
        to display, I have to make a unique graph for each type of data.
        I no longer like using the GraphView library.
    --------------------------------------------------------------------*/

    GraphView graphSetsCurrent;
    GraphView graphSetsAverage;
    GraphView graphRepsCurrent;
    GraphView graphRepsAverage;
    GraphView graphWeightsCurrent;
    GraphView graphWeightsAverage;

    GraphView graphCaloriesCurrent;
    GraphView graphCaloriesAverage;
    GraphView graphCarbohydratesCurrent;
    GraphView graphCarbohydratesAverage;
    GraphView graphFatCurrent;
    GraphView graphFatAverage;
    GraphView graphProteinCurrent;
    GraphView graphProteinAverage;

    // Spinners
    Spinner type, workout_yAxis, dietary_yAxis;

    // Booleans
    boolean workoutHistogramSelected = true;
    boolean isCurrentWeek = true;

    boolean populatedGraphSetsCurrent = false;
    boolean populatedGraphSetsAverage = false;
    boolean populatedGraphRepsCurrent = false;
    boolean populatedGraphRepsAverage = false;
    boolean populatedGraphWeightsCurrent = false;
    boolean populatedGraphWeightsAverage = false;

    boolean populatedGraphCarbohydratesCurrent = false;
    boolean populatedGraphCarbohydratesAverage = false;
    boolean populatedGraphCaloriesCurrent = false;
    boolean populatedGraphCaloriesAverage = false;
    boolean populatedGraphProteinCurrent = false;
    boolean populatedGraphProteinAverage = false;
    boolean populatedGraphFatCurrent = false;
    boolean populatedGraphFatAverage = false;

    // Workout Histogram Options
    WorkoutHistogram.xAxis workoutXAxis = WorkoutHistogram.xAxis.currentWeek;
    WorkoutHistogram.yAxis workoutYAxis = WorkoutHistogram.yAxis.sets;

    // Dietary Histogram Options
    DietaryLog.xAxis dietXAxis = DietaryLog.xAxis.currentWeek;
    DietaryLog.yAxis dietYAxis = DietaryLog.yAxis.calories;

    // Tab Names
    final String WORKOUT_TAB = "Workout Histogram";
    final String DIETARY_TAB = "Dietary Histogram";


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Hide the keyboard if it is currently shown
        Utilities.hideKeyboard(getContext(), getActivity());

        Activity activity = getActivity();
        ((ActivityMain) activity).setActionBarTitle( getResources().getString(R.string.nav_histogram));

        this.inflatedView = inflater.inflate(R.layout.histogram_fragment, container, false);

        /*--------------------------------------------------------------------
            Initialize tabs
        --------------------------------------------------------------------*/

        tabHost = (TabHost) inflatedView.findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec spec = tabHost.newTabSpec(WORKOUT_TAB);
        spec.setContent(R.id.workoutTab);
        spec.setIndicator(WORKOUT_TAB);
        tabHost.addTab(spec);

        spec = tabHost.newTabSpec(DIETARY_TAB);
        spec.setContent(R.id.dietaryTab);
        spec.setIndicator(DIETARY_TAB);
        tabHost.addTab(spec);

        /*--------------------------------------------------------------------
            Initialize graphs
        --------------------------------------------------------------------*/

        graphSetsAverage = (GraphView) inflatedView.findViewById(R.id.graphSetsAverage);
        graphSetsCurrent = (GraphView) inflatedView.findViewById(R.id.graphSetsCurrent);

        graphRepsAverage = (GraphView) inflatedView.findViewById(R.id.graphRepsAverage);
        graphRepsCurrent = (GraphView) inflatedView.findViewById(R.id.graphRepsCurrent);

        graphWeightsAverage = (GraphView) inflatedView.findViewById(R.id.graphWeightsAverage);
        graphWeightsCurrent = (GraphView) inflatedView.findViewById(R.id.graphWeightsCurrent);

        graphCarbohydratesAverage = (GraphView) inflatedView.findViewById(R.id.graphCarbohydratesAverage);
        graphCarbohydratesCurrent = (GraphView) inflatedView.findViewById(R.id.graphCarbohydratesCurrent);

        graphCaloriesAverage = (GraphView) inflatedView.findViewById(R.id.graphCaloriesAverage);
        graphCaloriesCurrent = (GraphView) inflatedView.findViewById(R.id.graphCaloriesCurrent);

        graphProteinAverage = (GraphView) inflatedView.findViewById(R.id.graphProteinAverage);
        graphProteinCurrent = (GraphView) inflatedView.findViewById(R.id.graphProteinCurrent);

        graphFatAverage = (GraphView) inflatedView.findViewById(R.id.graphFatAverage);
        graphFatCurrent = (GraphView) inflatedView.findViewById(R.id.graphFatCurrent);

        /*--------------------------------------------------------------------
            Initialize Spinners
        --------------------------------------------------------------------*/

        type = (Spinner) inflatedView.findViewById(R.id.typeSpinner);
        dietary_yAxis = (Spinner) inflatedView.findViewById(R.id.dietary_ySpinner);
        workout_yAxis = (Spinner) inflatedView.findViewById(R.id.workout_ySpinner);

        /*--------------------------------------------------------------------
            Tab Change Listener
        --------------------------------------------------------------------*/

        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener(){
            @Override
            public void onTabChanged(String tabId) {
                if(WORKOUT_TAB.equals(tabId))
                {
                    workoutHistogramSelected = true;
                    workout_yAxis.setVisibility(View.VISIBLE);
                    dietary_yAxis.setVisibility(View.GONE);
                }
                if(DIETARY_TAB.equals(tabId))
                {
                    workoutHistogramSelected = false;
                    workout_yAxis.setVisibility(View.GONE);
                    dietary_yAxis.setVisibility(View.VISIBLE);
                }

                UpdateVisibleGraph();

            }});

        /*--------------------------------------------------------------------
            Workout/Dietary Spinner Listener
        --------------------------------------------------------------------*/

        type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                String typeStr = String.valueOf(type.getSelectedItem());

                if (typeStr.equalsIgnoreCase(getResources().getString(R.string.averageWeekHistogram)))
                {
                    isCurrentWeek = false;
                    workoutXAxis = WorkoutHistogram.xAxis.averageWeek;
                    dietXAxis = DietaryLog.xAxis.averageWeek;
                }
                else if (typeStr.equalsIgnoreCase(getResources().getString(R.string.currentWeekHistogram)))
                {
                    isCurrentWeek = true;
                    workoutXAxis = WorkoutHistogram.xAxis.currentWeek;
                    dietXAxis = DietaryLog.xAxis.currentWeek;
                }

                UpdateVisibleGraph();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        /*--------------------------------------------------------------------
            Workout Spinner Listener
        --------------------------------------------------------------------*/

        workout_yAxis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {

                String yAxisStr = String.valueOf(workout_yAxis.getSelectedItem());

                if (yAxisStr.equalsIgnoreCase(getResources().getString(R.string.repsHistogram)))
                {
                    workoutYAxis = WorkoutHistogram.yAxis.reps;
                }
                else if (yAxisStr.equalsIgnoreCase(getResources().getString(R.string.setsHistogram)))
                {
                    workoutYAxis = WorkoutHistogram.yAxis.sets;
                }
                else if (yAxisStr.equalsIgnoreCase(getResources().getString(R.string.weightsHistogram)))
                {
                    workoutYAxis = WorkoutHistogram.yAxis.weights;
                }

                UpdateVisibleGraph();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        /*--------------------------------------------------------------------
            Dietary Spinner Listener
        --------------------------------------------------------------------*/

        dietary_yAxis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                /*--------------------------------------------------------------------
                    Check what the yAxis value is
                --------------------------------------------------------------------*/

                String yAxisStr = String.valueOf( dietary_yAxis.getSelectedItem() );

                if( yAxisStr.equalsIgnoreCase( getResources().getString(R.string.caloriesHistogram) ) )
                {
                    dietYAxis = DietaryLog.yAxis.calories;
                }
                else if ( yAxisStr.equalsIgnoreCase( getResources().getString(R.string.carbohydratesHistogram) ) )
                {
                    dietYAxis = DietaryLog.yAxis.carbohydrates;
                }
                else if ( yAxisStr.equalsIgnoreCase( getResources().getString(R.string.proteinHistogram) ) )
                {
                    dietYAxis = DietaryLog.yAxis.protein;
                }
                else if ( yAxisStr.equalsIgnoreCase( getResources().getString(R.string.fatHistogram) ) )
                {
                    dietYAxis = DietaryLog.yAxis.fat;
                }

                UpdateVisibleGraph();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        // Hide the keyboard if it is currently shown
        Utilities.hideKeyboard(getContext(), getActivity());

        // Inflate the layout for this fragment
        return this.inflatedView;
    }
    

    /*--------------------------------------------------------------------

        UPDATE VISISBLE GRAPH

        Purpose:
        Hides all of the graphs, identifies which graph is
        being requested, and makes that one visible.

    --------------------------------------------------------------------*/

    public void UpdateVisibleGraph()
    {
        graphFatAverage.setVisibility(View.GONE);
        graphFatCurrent.setVisibility(View.GONE);
        graphProteinCurrent.setVisibility(View.GONE);
        graphProteinAverage.setVisibility(View.GONE);
        graphCaloriesAverage.setVisibility(View.GONE);
        graphCaloriesCurrent.setVisibility(View.GONE);
        graphCarbohydratesAverage.setVisibility(View.GONE);
        graphCarbohydratesCurrent.setVisibility(View.GONE);

        graphRepsAverage.setVisibility(View.GONE);
        graphRepsCurrent.setVisibility(View.GONE);
        graphSetsAverage.setVisibility(View.GONE);
        graphSetsCurrent.setVisibility(View.GONE);
        graphWeightsAverage.setVisibility(View.GONE);
        graphWeightsCurrent.setVisibility(View.GONE);

        if( workoutHistogramSelected )
        {
            if( isCurrentWeek )
            {
                switch( workoutYAxis )
                {
                    case sets:
                        if( !populatedGraphSetsCurrent )
                        {
                            populatedGraphSetsCurrent = true;
                            WorkoutHistogram.PopulateHistogram( graphSetsCurrent,
                                                                workoutXAxis,
                                                                workoutYAxis );
                        }
                        graphSetsCurrent.setVisibility(View.VISIBLE);

                        Log.v("HISTOGRAM", "Display Sets for Current Week");
                        break;

                    case reps:
                        if( !populatedGraphRepsCurrent )
                        {
                            populatedGraphRepsCurrent = true;
                            WorkoutHistogram.PopulateHistogram( graphRepsCurrent,
                                                                workoutXAxis,
                                                                workoutYAxis );
                        }
                        graphRepsCurrent.setVisibility(View.VISIBLE);

                        Log.v("HISTOGRAM", "Display Reps for Current Week");
                        break;

                    case weights:
                        if( !populatedGraphWeightsCurrent )
                        {
                            populatedGraphWeightsCurrent = true;
                            WorkoutHistogram.PopulateHistogram( graphWeightsCurrent,
                                                                workoutXAxis,
                                                                workoutYAxis );
                        }
                        graphWeightsCurrent.setVisibility(View.VISIBLE);

                        Log.v("HISTOGRAM", "Display Weights for Current Week");
                        break;
                }
            }

            else
            {
                switch( workoutYAxis )
                {
                    case sets:
                        if( !populatedGraphSetsAverage )
                        {
                            populatedGraphSetsAverage = true;
                            WorkoutHistogram.PopulateHistogram( graphSetsAverage,
                                                                workoutXAxis,
                                                                workoutYAxis );
                        }
                        graphSetsAverage.setVisibility(View.VISIBLE);

                        Log.v("HISTOGRAM", "Display Sets for Average Week");
                        break;

                    case reps:
                        if( !populatedGraphRepsAverage )
                        {
                            populatedGraphRepsAverage = true;
                            WorkoutHistogram.PopulateHistogram( graphRepsAverage,
                                                                workoutXAxis,
                                                                workoutYAxis );
                        }
                        graphRepsAverage.setVisibility(View.VISIBLE);

                        Log.v("HISTOGRAM", "Display Reps for Average Week");
                        break;

                    case weights:
                        if( !populatedGraphWeightsAverage )
                        {
                            populatedGraphWeightsAverage = true;
                            WorkoutHistogram.PopulateHistogram( graphWeightsAverage,
                                                                workoutXAxis,
                                                                workoutYAxis );
                        }
                        graphWeightsAverage.setVisibility(View.VISIBLE);

                        Log.v("HISTOGRAM", "Display Weights for Average Week");
                        break;
                }
            }
        }

        else
        {
            if( isCurrentWeek )
            {
                switch( dietYAxis )
                {
                    case carbohydrates:
                        if( !populatedGraphCarbohydratesCurrent )
                        {
                            populatedGraphCarbohydratesCurrent = true;
                            DietaryHistogram.PopulateHistogram( graphCarbohydratesCurrent,
                                                                dietXAxis,
                                                                dietYAxis );
                        }
                        graphCarbohydratesCurrent.setVisibility(View.VISIBLE);

                        Log.v("HISTOGRAM", "Display Carbs for Current Week");
                        break;

                    case calories:
                        if( !populatedGraphCaloriesCurrent )
                        {
                            populatedGraphCaloriesCurrent = true;
                            DietaryHistogram.PopulateHistogram( graphCaloriesCurrent,
                                                                dietXAxis,
                                                                dietYAxis );
                        }
                        graphCaloriesCurrent.setVisibility(View.VISIBLE);

                        Log.v("HISTOGRAM", "Display Calories for Current Week");
                        break;

                    case protein:
                        if( !populatedGraphProteinCurrent )
                        {
                            populatedGraphProteinCurrent = true;
                            DietaryHistogram.PopulateHistogram( graphProteinCurrent,
                                                                dietXAxis,
                                                                dietYAxis );
                        }
                        graphProteinCurrent.setVisibility(View.VISIBLE);

                        Log.v("HISTOGRAM", "Display Protein for Current Week");
                        break;

                    case fat:
                        if( !populatedGraphFatCurrent )
                        {
                            populatedGraphFatCurrent = true;
                            DietaryHistogram.PopulateHistogram( graphFatCurrent,
                                                                dietXAxis,
                                                                dietYAxis );
                        }
                        graphFatCurrent.setVisibility(View.VISIBLE);

                        Log.v("HISTOGRAM", "Display Fat for Current Week");
                        break;
                }
            }

            else
            {
                switch( dietYAxis )
                {
                    case carbohydrates:
                        if( !populatedGraphCarbohydratesAverage )
                        {
                            populatedGraphCarbohydratesAverage = true;
                            DietaryHistogram.PopulateHistogram( graphCarbohydratesAverage,
                                                                dietXAxis,
                                                                dietYAxis );
                        }
                        graphCarbohydratesAverage.setVisibility(View.VISIBLE);

                        Log.v("HISTOGRAM", "Display Carbs for Average Week");
                        break;

                    case calories:
                        if( !populatedGraphCaloriesAverage )
                        {
                            populatedGraphCaloriesAverage = true;
                            DietaryHistogram.PopulateHistogram( graphCaloriesAverage,
                                                                dietXAxis,
                                                                dietYAxis );
                        }
                        graphCaloriesAverage.setVisibility(View.VISIBLE);

                        Log.v("HISTOGRAM", "Display Calories for Average Week");
                        break;

                    case protein:
                        if( !populatedGraphProteinAverage )
                        {
                            populatedGraphProteinAverage = true;
                            DietaryHistogram.PopulateHistogram( graphProteinAverage,
                                                                dietXAxis,
                                                                dietYAxis );
                        }
                        graphProteinAverage.setVisibility(View.VISIBLE);

                        Log.v("HISTOGRAM", "Display Protein for Average Week");
                        break;

                    case fat:
                        if( !populatedGraphFatAverage )
                        {
                            populatedGraphFatAverage = true;
                            DietaryHistogram.PopulateHistogram( graphFatAverage,
                                                                dietXAxis,
                                                                dietYAxis );
                        }
                        graphFatAverage.setVisibility(View.VISIBLE);

                        Log.v("HISTOGRAM", "Display Fat for Average Week");
                        break;
                }
            }
        }
    } // method

    @Override
    public void onAttach(Context activity)
    {
        super.onAttach(activity);
        ((ActivityMain) activity).onSectionAttached(
                getArguments().getInt(Utilities.ARG_SECTION_NUMBER_KEY));
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

    public static FragmentHistogram newInstance(int sectionNumber)
    {
        Log.v("FragmentHistogram", "newInstance");
        FragmentHistogram fragment = new FragmentHistogram();

        Bundle args = new Bundle();
        args.putInt(Utilities.ARG_SECTION_NUMBER_KEY, sectionNumber);
        fragment.setArguments(args);

        return fragment;
    }
}
