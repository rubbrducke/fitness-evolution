package sccapstone.fitnessevolution.Histogram;

import android.graphics.Color;
import android.util.Log;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import sccapstone.fitnessevolution.Logging.WorkoutLogging.FitnessAnalysis.MuscleWorkout;
import sccapstone.fitnessevolution.Logging.WorkoutLogging.FitnessAnalysis.WorkoutLog;


/*====================================================================

    WORKOUT HISTOGRAM
    Created by Matthew Short

    Class that assembles a histogram and calculates averages, means, etc.

====================================================================*/

public class WorkoutHistogram
{

    /*--------------------------------------------------------------------

        POPULATE HISTOGRAM

        Purpose:
        Populate a graph with workout data.

        Arguments:
        A graph and two enums that inform the method what information to
        populate the graph with.

        Returns:
        A populated graph.

    --------------------------------------------------------------------*/

    public enum xAxis { averageWeek, currentWeek }
    public enum yAxis { weights, reps, sets }
    private static List<String> dayOfWeek = new ArrayList<>( Arrays.asList("Sunday",
                                                                                 "Monday",
                                                                                 "Tuesday",
                                                                                 "Wednesday",
                                                                                 "Thursday",
                                                                                 "Friday",
                                                                                 "Saturday") );

    public static GraphView PopulateHistogram( GraphView graph, xAxis xType, yAxis yType )
    {
        graph.removeAllSeries();

        graph.getLegendRenderer().setMargin(20);
        graph.getLegendRenderer().setTextSize(17);
        graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
        graph.getLegendRenderer().setVisible(true);

        graph.getGridLabelRenderer().setPadding(10);
        graph.getGridLabelRenderer().setTextSize(20);
        graph.getGridLabelRenderer().setHorizontalAxisTitleTextSize(25);
        graph.getGridLabelRenderer().setLabelHorizontalHeight(20);

        graph.getViewport().setScrollable(true);
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMaxX(2);
        graph.getGridLabelRenderer().setNumHorizontalLabels(3);
        graph.getGridLabelRenderer().setHorizontalAxisTitle("Day of the Week");

        /*--------------------------------------------------------------------
            Custom label formatter to show day of week
        --------------------------------------------------------------------*/

        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter()
        {
            @Override
            public String formatLabel(double value, boolean isValueX)
            {

                // Display for the X Axis
                if (isValueX)
                {
                    if ((int) (value + 0.5) < dayOfWeek.size() && (int) (value + .5) >= 0)
                    {
                        // adding 0.5 so that values greater than x.5 round up
                        // and numbers less than x.5 round down
                        return dayOfWeek.get((int) (value + .5));
                    }
                    else
                    {
                        return "";
                    }
                }
                // Display for the Y Axis
                return super.formatLabel(value, isValueX);
            }
        });

        Log.v("POPULATE HISTOGRAM", "Start of Populate Histogram");
        Log.v("POPULATE HISTOGRAM", "X-Axis: " + xType.toString()
                                    + "\nY-Axis: " + yType.toString());

        Map<String, LineGraphSeries<DataPoint>> muscleSeries = new HashMap<>();

        LineGraphSeries<DataPoint> baseline = new LineGraphSeries<>();
        baseline.setTitle("Baseline");
        muscleSeries.put("Baseline", baseline);

        /*--------------------------------------------------------------------
            If xType == averageWeek
        --------------------------------------------------------------------*/

        if ( xType == xAxis.averageWeek )
        {
            Map<String, Map<String, MuscleWorkout>> averageWeek
                    = WorkoutLog.buildHistory(WorkoutLog.historyType.average, WorkoutLog.historyLength.month);

            Log.v("POPULATE HISTOGRAM", "Populating graph of average week.");
            Log.v("POPULATE HISTOGRAM", "Average week has entries for " + averageWeek.size() + " days.");

            int dayNum = 0;
            for( String day : dayOfWeek )
            {
                Log.v("POPULATE HISTOGRAM", "Populating graph for " + day + "(index: " + dayNum + ")");

                if( averageWeek.containsKey( day ))
                {
                    for(Map.Entry<String, MuscleWorkout> dayEntry : averageWeek.get( day ).entrySet())
                    {

                        Log.v("POPULATE HISTOGRAM", "Day: " + day
                                                    + "\nMuscle: " + dayEntry.getKey() );

                        LineGraphSeries<DataPoint> thisMuscleSeries;

                        if (muscleSeries.containsKey(dayEntry.getKey()))
                        {
                            Log.v("POPULATE HISTOGRAM", "Append to line series: " + dayEntry.getKey());

                            thisMuscleSeries = muscleSeries.get(dayEntry.getKey());
                        }
                        else
                        {
                            Log.v("POPULATE HISTOGRAM", "Create new line series: " + dayEntry.getKey());
                            thisMuscleSeries = new LineGraphSeries<>();
                            thisMuscleSeries.setTitle(dayEntry.getKey());
                        }

                        switch (yType)
                        {
                            case reps:
                                Log.v("POPULATE HISTOGRAM", "Adding " + yType.toString() + ": " + dayEntry.getValue().averageReps);
                                thisMuscleSeries.appendData(new DataPoint(dayNum, dayEntry.getValue().averageReps), true, 50);
                                break;

                            case sets:
                                Log.v("POPULATE HISTOGRAM", "Adding " + yType.toString() + ": " + dayEntry.getValue().averageSets);
                                thisMuscleSeries.appendData(new DataPoint(dayNum, dayEntry.getValue().averageSets), true, 50);
                                break;

                            case weights:
                                Log.v("POPULATE HISTOGRAM", "Adding " + yType.toString() + ": " + dayEntry.getValue().totalWeightLifted);
                                thisMuscleSeries.appendData(new DataPoint(dayNum, dayEntry.getValue().totalWeightLifted), true, 50);
                                break;
                        }

                        muscleSeries.put(dayEntry.getKey(), thisMuscleSeries);
                    }
                }

                /*--------------------------------------------------------------------
                    There is no data for this day so we fill it in with zero's
                --------------------------------------------------------------------*/

                else
                {
                    for( Map.Entry<String, LineGraphSeries<DataPoint>> series : muscleSeries.entrySet() )
                    {
                        Log.v("POPULATE HISTOGRAM","Add zero DataPoint for: " + series.getKey()
                                                   + " on " + dayOfWeek.get(dayNum));

                        series.getValue().appendData(new DataPoint(dayNum, 0), true, 50);
                    }
                }
                ++dayNum;
            }
        } // if xAxis == averageWeek

        /*--------------------------------------------------------------------
            If xType == currentWeek
        --------------------------------------------------------------------*/

        else if ( xType == xAxis.currentWeek )
        {
            Map<String, Map<String, MuscleWorkout>> currentWeek
                    = WorkoutLog.buildHistory(WorkoutLog.historyType.average, WorkoutLog.historyLength.week);

            Log.v("POPULATE HISTOGRAM", "Populating graph of current week.");
            Log.v("POPULATE HISTOGRAM", "Average week has entries for " + currentWeek.size() + " days.");

            int dayNum = 0;
            for( String day : dayOfWeek )
            {
                Log.v("POPULATE HISTOGRAM", "Populating graph for " + day + "(index: " + dayNum + ")" );

                if( currentWeek.containsKey( day ))
                {
                    for(Map.Entry<String, MuscleWorkout> dayEntry : currentWeek.get( day ).entrySet())
                    {

                        Log.v("POPULATE HISTOGRAM", "Day: " + day
                                                    + "\nMuscle: " + dayEntry.getKey() );

                        LineGraphSeries<DataPoint> thisMuscleSeries;

                        if (muscleSeries.containsKey(dayEntry.getKey()))
                        {
                            Log.v("POPULATE HISTOGRAM", "Append to line series: " + dayEntry.getKey());

                            thisMuscleSeries = muscleSeries.get(dayEntry.getKey());
                        }
                        else
                        {
                            Log.v("POPULATE HISTOGRAM", "Create new line series: " + dayEntry.getKey());
                            thisMuscleSeries = new LineGraphSeries<>();
                            thisMuscleSeries.setTitle(dayEntry.getKey());
                        }

                        switch (yType)
                        {
                            case reps:
                                Log.v("POPULATE HISTOGRAM", "Adding " + yType.toString() + ": " + dayEntry.getValue().averageReps);
                                thisMuscleSeries.appendData(new DataPoint(dayNum, dayEntry.getValue().averageReps), true, 50);
                                break;

                            case sets:
                                Log.v("POPULATE HISTOGRAM", "Adding " + yType.toString() + ": " + dayEntry.getValue().averageSets);
                                thisMuscleSeries.appendData(new DataPoint(dayNum, dayEntry.getValue().totalSets), true, 50);
                                break;

                            case weights:
                                Log.v("POPULATE HISTOGRAM", "Adding " + yType.toString() + ": " + dayEntry.getValue().totalWeightLifted);
                                thisMuscleSeries.appendData(new DataPoint(dayNum, dayEntry.getValue().totalWeightLifted), true, 50);
                                break;
                        }

                        muscleSeries.put(dayEntry.getKey(), thisMuscleSeries);
                    }
                }

                /*--------------------------------------------------------------------
                    There is no data for this day so we fill it in with zero's
                --------------------------------------------------------------------*/

                else
                {
                    for( Map.Entry<String, LineGraphSeries<DataPoint>> series : muscleSeries.entrySet() )
                    {
                        Log.v("POPULATE HISTOGRAM","Add zero DataPoint for: " + series.getKey()
                                                   + " on " + dayOfWeek.get(dayNum));

                        series.getValue().appendData(new DataPoint(dayNum, 0), true, 50);
                    }
                }
                ++dayNum;
            }
        } // if xAxis == currentWeek

        /*--------------------------------------------------------------------
            Add each series to the graph.
            Use the series' names to generate a unique color for them.
        --------------------------------------------------------------------*/

        Log.v("POPULATE HISTOGRAM", "There are " + muscleSeries.size() + " series to add to graph.");

        for( LineGraphSeries<DataPoint> series : muscleSeries.values() )
        {
            Random rnd = new Random( series.getTitle().hashCode() );
            series.setColor(Color.argb(255, rnd.nextInt(255), rnd.nextInt(255), rnd.nextInt(255)));
            graph.addSeries(series);
        }

        Log.v("POPULATE HISTOGRAM", "End of Populate Histogram");

        return graph;
    } // function

} // class
