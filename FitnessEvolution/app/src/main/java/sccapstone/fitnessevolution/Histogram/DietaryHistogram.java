package sccapstone.fitnessevolution.Histogram;

import android.graphics.Color;
import android.util.Log;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

import sccapstone.fitnessevolution.Logging.DietaryLogging.DaySummary;
import sccapstone.fitnessevolution.Logging.DietaryLogging.DietaryLog;


/*====================================================================

    DIETARY HISTOGRAM
    Created by Matthew Short

    This class is used for populating GraphView's with
    dietary data.

====================================================================*/

public class DietaryHistogram
{



    private static List<String> dayOfWeek = new ArrayList<>( Arrays.asList("Sunday",
                                                                                 "Monday",
                                                                                 "Tuesday",
                                                                                 "Wednesday",
                                                                                 "Thursday",
                                                                                 "Friday",
                                                                                 "Saturday") );

    static final String CALORIES_SERIES = "Calories";
    static final String CARBOHYDRATES_SERIES = "Carbohydrates";
    static final String FAT_SERIES = "Fat";
    static final String PROTEIN_SERIES = "Protein";


    /*--------------------------------------------------------------------

        POPULATE HISTOGRAM

        Purpose:
        Populate a graph with dietary data.

        Arguments:
        A graph and two enums that inform the method what information to
        populate the graph with.

        Returns:
        A populated and styled graph.

    --------------------------------------------------------------------*/

    public static GraphView PopulateHistogram( GraphView graph, DietaryLog.xAxis xType, DietaryLog.yAxis yType )
    {
        graph.removeAllSeries();

        graph.getLegendRenderer().setMargin(20);
        graph.getLegendRenderer().setTextSize(17);
        graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
        graph.getLegendRenderer().setVisible(true);

        graph.getGridLabelRenderer().setPadding(10);
        graph.getGridLabelRenderer().setTextSize(20);
        graph.getGridLabelRenderer().setHorizontalAxisTitleTextSize(25);
        graph.getGridLabelRenderer().setLabelHorizontalHeight(20);

        graph.getViewport().setScrollable(true);
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMaxX(2);
        graph.getGridLabelRenderer().setNumHorizontalLabels(3);
        graph.getGridLabelRenderer().setHorizontalAxisTitle("Day of the Week");

        /*--------------------------------------------------------------------
            Custom label formatter to show day of week
        --------------------------------------------------------------------*/

        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter()
        {
            @Override
            public String formatLabel(double value, boolean isValueX)
            {

                // Display for the X Axis
                if (isValueX)
                {
                    if ((int) (value + 0.5) < dayOfWeek.size() && (int) (value + .5) >= 0)
                    {
                        // adding 0.5 so that values greater than x.5 round up
                        // and numbers less than x.5 round down
                        return dayOfWeek.get((int) (value + .5));
                    }
                    else
                    {
                        return "";
                    }
                }
                // Display for the Y Axis
                else
                {
                    return super.formatLabel(value, isValueX);
                }
            }
        });

        Log.v("POPULATE HISTOGRAM", "Start of Populate Histogram");
        Log.v("POPULATE HISTOGRAM", "X-Axis: " + xType.toString()
                                    + "\nY-Axis: " + yType.toString());

        LineGraphSeries<DataPoint> dietData = new LineGraphSeries<>();

        switch (yType)
        {
            case calories:
                dietData.setTitle(CALORIES_SERIES);
                break;

            case carbohydrates:
                dietData.setTitle(CARBOHYDRATES_SERIES);
                break;

            case fat:
                dietData.setTitle(FAT_SERIES);
                break;

            case protein:
                dietData.setTitle(PROTEIN_SERIES);
                break;
        }

        /*--------------------------------------------------------------------
            If xType == averageWeek
        --------------------------------------------------------------------*/

        if ( xType == DietaryLog.xAxis.averageWeek )
        {
            Map<String, DaySummary> averageWeek = DietaryLog.buildAverageHistory(xType);

            Log.v("POPULATE HISTOGRAM", "Populating graph of average week.");
            Log.v("POPULATE HISTOGRAM", "Average week has entries for " + averageWeek.size() + " days.");

            int dayNum = 0;
            for( String day : dayOfWeek )
            {
                Log.v("POPULATE HISTOGRAM", "Populating graph for " + day + "(index: " + dayNum + ")");

                if( averageWeek.containsKey( day ))
                {
                    DaySummary dayEntry = averageWeek.get(day);

                    switch (yType)
                    {
                        case calories:
                            Log.v("POPULATE HISTOGRAM", "Adding " + yType.toString() + ": " + dayEntry.allMealsSummary.calories);
                            dietData.appendData(new DataPoint(dayNum, dayEntry.allMealsSummary.calories), true, 50);
                            break;

                        case carbohydrates:
                            Log.v("POPULATE HISTOGRAM", "Adding " + yType.toString() + ": " + dayEntry.allMealsSummary.carbohydrates);
                            dietData.appendData(new DataPoint(dayNum, dayEntry.allMealsSummary.carbohydrates), true, 50);
                            break;

                        case fat:
                            Log.v("POPULATE HISTOGRAM", "Adding " + yType.toString() + ": " + dayEntry.allMealsSummary.fat);
                            dietData.appendData(new DataPoint(dayNum, dayEntry.allMealsSummary.fat), true, 50);
                            break;

                        case protein:
                            Log.v("POPULATE HISTOGRAM", "Adding " + yType.toString() + ": " + dayEntry.allMealsSummary.protein);
                            dietData.appendData(new DataPoint(dayNum, dayEntry.allMealsSummary.protein), true, 50);
                            break;
                    }
                }

                /*--------------------------------------------------------------------
                    There is no data for this day so we fill it in with zero's
                --------------------------------------------------------------------*/

                else
                {
                    dietData.appendData(new DataPoint(dayNum, 0), true, 50);
                }
                ++dayNum;
            }
        } // if xAxis == averageWeek

        /*--------------------------------------------------------------------
            If xType == currentWeek
        --------------------------------------------------------------------*/

        else if ( xType == DietaryLog.xAxis.currentWeek )
        {
            Map<String, DaySummary> averageWeek = DietaryLog.buildAverageHistory(xType);

            Log.v("POPULATE HISTOGRAM", "Populating graph of average week.");
            Log.v("POPULATE HISTOGRAM", "Average week has entries for " + averageWeek.size() + " days.");

            int dayNum = 0;
            for( String day : dayOfWeek )
            {
                Log.v("POPULATE HISTOGRAM", "Populating graph for " + day + "(index: " + dayNum + ")");

                if( averageWeek.containsKey( day ))
                {
                    DaySummary dayEntry = averageWeek.get(day);

                    switch (yType)
                    {
                        case calories:
                            Log.v("POPULATE HISTOGRAM", "Adding " + yType.toString() + ": " + dayEntry.allMealsSummary.calories);
                            dietData.appendData(new DataPoint(dayNum, dayEntry.allMealsSummary.calories), true, 50);
                            break;

                        case carbohydrates:
                            Log.v("POPULATE HISTOGRAM", "Adding " + yType.toString() + ": " + dayEntry.allMealsSummary.carbohydrates);
                            dietData.appendData(new DataPoint(dayNum, dayEntry.allMealsSummary.carbohydrates), true, 50);
                            break;

                        case fat:
                            Log.v("POPULATE HISTOGRAM", "Adding " + yType.toString() + ": " + dayEntry.allMealsSummary.fat);
                            dietData.appendData(new DataPoint(dayNum, dayEntry.allMealsSummary.fat), true, 50);
                            break;

                        case protein:
                            Log.v("POPULATE HISTOGRAM", "Adding " + yType.toString() + ": " + dayEntry.allMealsSummary.protein);
                            dietData.appendData(new DataPoint(dayNum, dayEntry.allMealsSummary.protein), true, 50);
                            break;
                    }
                }

                // because there is no data for this day we put in 0's for the DataPoints on this day
                else
                {
                    dietData.appendData(new DataPoint(dayNum, 0), true, 50);
                }
                ++dayNum;
            }

        } // if xAxis == currentWeek

        Random rnd = new Random( dietData.getTitle().hashCode() );
        dietData.setColor( Color.argb(255, rnd.nextInt(255), rnd.nextInt(255), rnd.nextInt(255)) );

        graph.addSeries(dietData);

        Log.v("POPULATE HISTOGRAM", "End of Populate Histogram");

        return graph;
    }
}
