package sccapstone.fitnessevolution.Messaging;

import android.util.Log;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import sccapstone.fitnessevolution.Utilities.Utilities;

/**
 * Created by Preston on 11/23/2015.
 */
public final class UserFunctions {
    private static ParseUser mUser = null;

    private UserFunctions() {
    }

    /*
        Sends a message to a user. Call this method if there has already been a correspondence between the two users.

        conversationObjectID:   the object ID of the message object in the PARSE table
        setFromObjectID:    sending user's object ID
        message:    message to be sent
        fragMessageThread:  fragment from which the message is being sent
     */
    public static void sendMessageNotFirstTime(String conversationObjectID, final String sentFromObjectID, final String message, final FragmentMessageThread fragMessageThread) {
        ParseQuery<ParseObject> conversationQuery = ParseQuery.getQuery("Conversations");
        conversationQuery.whereEqualTo("objectId", conversationObjectID);
        //Log.v("PRBTEST", "In sendMessageNotFirstTime(): " + conversationObjectID + " " + sentFromObjectID + " " + message);

        // Get the conversation record
        conversationQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject convRecord, ParseException e) {
                if (e == null) {
                    // Successful query
                    DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                    DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
                    String currentDateTime = Utilities.trimLeadingZeroesOnDate(dateFormat.format(new Date())) + " " + Utilities.convertTo12Hour(timeFormat.format(new Date()));

                    // Add the data to the conversation record
                    convRecord.add("sentDates", currentDateTime);
                    convRecord.add("sentFromObjectIDs", sentFromObjectID);
                    convRecord.add("messages", message);

                    // Save the record
                    convRecord.saveInBackground(new SaveCallback() {
                        public void done(ParseException e) {
                            if (e == null) {
                                // Success sending message
                                fragMessageThread.showMessages();
                            } else {
                                // Failed sending message
                                Toast.makeText(fragMessageThread.getActivity(), "Error sending message :(", Toast.LENGTH_LONG);
                            }
                        }
                    });
                } else {
                    Log.v("PRBTEST", "Error sending message not first time: " + e.getMessage());
                }
            }
        });
    }

    private static String currentDateTime = "";
    /*
        Sends a message to a user. Call this method if there has NOT already been a correspondence between the two users.

        setFromObjectID:    sending user's object ID
        sentFromUserName:   sending user's username
        setToObjectID:    receiving user's object ID
        sentToUserName:   receiving user's username
        message:    message to be sent
     */
    public static void sendMessageFirstTime(String sentFromObjectID, String sentFromUserName, String sendToObjectID, String sendToUserName, String message) {
        ParseObject convUpdateQuery = new ParseObject("Conversations");
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        currentDateTime = Utilities.trimLeadingZeroesOnDate(dateFormat.format(new Date())) + " " + Utilities.convertTo12Hour(timeFormat.format(new Date()));

        // Only need small arraylists, hence magic numbers '1' and '2'
        ArrayList<String> arrSentFromObjectID = new ArrayList<>(1);
        ArrayList<String> arrConvParticipantObjectIDs = new ArrayList<>(2);
        ArrayList<String> arrConvParticipantUserNames = new ArrayList<>(2);
        ArrayList<String> arrCurrentDateTime = new ArrayList<>(1);
        ArrayList<String> arrMessage = new ArrayList<>(1);

        arrSentFromObjectID.add(sentFromObjectID);
        arrConvParticipantObjectIDs.add(sentFromObjectID);
        arrConvParticipantObjectIDs.add(sendToObjectID);
        arrConvParticipantUserNames.add(sentFromUserName);
        arrConvParticipantUserNames.add(sendToUserName);
        arrCurrentDateTime.add(currentDateTime);
        arrMessage.add(message);

        convUpdateQuery.put("sentDates", arrCurrentDateTime);
        convUpdateQuery.put("sentFromObjectIDs", arrSentFromObjectID);
        convUpdateQuery.put("messages", arrMessage);
        convUpdateQuery.put("convParticipantObjectIDs", arrConvParticipantObjectIDs);
        convUpdateQuery.put("convParticipantUserNames", arrConvParticipantUserNames);

        convUpdateQuery.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    //Success in saving
                    Log.v("PRBTEST", "Success sending message first time");
                } else {
                    //Failed saving
                    Log.v("PRBTEST", "Error sending message first time: " + e.getMessage());
                }
            }
        });
    }
}