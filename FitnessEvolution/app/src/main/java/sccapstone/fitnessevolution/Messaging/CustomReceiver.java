package sccapstone.fitnessevolution.Messaging;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import sccapstone.fitnessevolution.Activites.ActivityMain;
import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.R;

/**
 * Created by Matthew J on 2/19/2016.
 */
public class CustomReceiver extends ParsePushBroadcastReceiver{

    public static final String TAG = "CustomReceiver";

    @Override
    public void onPushReceive(Context context, Intent intent)
    {
        super.onPushReceive(context, intent);
        if(intent == null) {
            return;
        }
        JSONObject data;
        String title = "";
        String message = "";
        try {
            JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));

            Log.v("push"+TAG,"Push received: " + json);

            data = json.getJSONObject("data");
            title = data.getString("title");
            message = data.getString("message");
        } catch (JSONException e) {
            Log.v("push:"+TAG,"JSON Exception in Custom Receiver: " + e.getMessage());
        }

        int icon = R.drawable.ic_launcher;

        Intent cintent = new Intent(context, ActivityMain.class);

        Log.v("push:"+TAG,"Customizing notification intent (extras)");
        switch(title) {
            case "Inbox":
                Log.v("push:"+TAG,"putExtra: Inbox");
                cintent.putExtra("openFromNotification", Utilities.NAV_MESSAGES);
                break;
            case "Coach":
                Log.v("push:"+TAG,"putExtra: Coach");
                cintent.putExtra("openFromNotification", Utilities.NAV_COACH);
                break;
            default:
                Log.v("push:"+TAG,"no Extra");
        }
        Log.v("push:"+TAG,"Customized intent");

        cintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(context,0,cintent,PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);

        Notification notification = mBuilder
                .setSmallIcon(icon)
                .setTicker(title)
                .setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setStyle(inboxStyle)
                .setContentIntent(resultPendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), icon))
                .setContentText(message)
                .build();

        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(101,notification);
    }

}
