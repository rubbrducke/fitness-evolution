package sccapstone.fitnessevolution.Messaging;

/**
 * Created by Preston on 11/19/2015.
 *
 * This fragment handles displaying full conversations threads between two users (the current user and
 * the user with whom the current user is conversing with), similar to text messaging apps
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import sccapstone.fitnessevolution.Activites.ActivityMain;
import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.Parse.ParseApplication;
import sccapstone.fitnessevolution.R;


public final class FragmentMessageThread extends android.support.v4.app.Fragment implements View.OnClickListener{
    private static CustomMessageAdapter mMessageAdapter; // ArrayAdapter to populate the ListView
    private static Bitmap otherUserAvatar; // TODO: dynamic avatar images
    private static ListView lv;
    private static Context context;
    private static EditText mMessageTextView;
    private static String mConversationObjectID = "";
    private static ArrayList<String> tempOtherUserObjID;

    public static final String TAG = "FragMessageThread";
    public FragmentMessageThread() {
    }

    @Override
    public void onStart() {
        super.onStart();
        showMessages();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getArguments() != null) {
            mConversationObjectID = getArguments().getString("CONVERSATION_OBJECT_ID");
            String contactName = getArguments().getString("CONTACT_NAME");
            // Set the actionbar title
            Activity activity = getActivity();
            ((ActivityMain) activity).setActionBarTitle(contactName);
        }

        context = getActivity();

        View rootView = inflater.inflate(R.layout.messaging_fragment_thread, container, false);

        // Get a reference to the ListView, and attach this adapter to it.
        lv = (ListView) rootView.findViewById(R.id.listview_message_thread);
        lv.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
        lv.setStackFromBottom(true);

        Button btnSendMessage = (Button) rootView.findViewById(R.id.send_message_button);
        btnSendMessage.setOnClickListener(this);
        mMessageTextView = (EditText) rootView.findViewById(R.id.send_message_text);
        mMessageTextView.setOnClickListener(this);


        Button btnRefreshMessages = (Button) rootView.findViewById(R.id.refresh_messages_button);
        btnRefreshMessages.setOnClickListener(this);
        return rootView;
    }

    /*
    *   showMessages() retrieves the message log history for the current authenticated user
    */
    private ArrayList<String> sentFromObjectIDs = new ArrayList<String>();
    private ArrayList<String> strContacts = new ArrayList<String>();
    private ArrayList<String> strMessages = new ArrayList<String>();
    private ArrayList<String> strDateTimes = new ArrayList<>();
    private ArrayList<String> tempStrContacts = new ArrayList<String>();
    private ArrayList<String> tempStrMessagePreviews = new ArrayList<String>();
    private ArrayList<String> tempStrDateTimes = new ArrayList<>();
    private ArrayList<Boolean> isFromCurrentUser = new ArrayList<>();
    public void showMessages() {
        strContacts.clear();
        strMessages.clear();
        strDateTimes.clear();
        isFromCurrentUser.clear();

        // Get user messages
        ParseQuery<ParseObject> messageQuery = ParseQuery.getQuery("Conversations");
        messageQuery.whereEqualTo("objectId", mConversationObjectID);

        Utilities.showLoadingScreen(getContext());
        messageQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject convRecord, ParseException e) {
                if (e == null) {
                    // Successful query
                    tempStrContacts.clear();
                    tempStrMessagePreviews.clear();
                    tempStrDateTimes.clear();

                    // Get the other user's username (not the current user's username)
                    sentFromObjectIDs = (ArrayList<String>) convRecord.get("sentFromObjectIDs");
                    for (int counter = 0; counter < sentFromObjectIDs.size(); counter++) {
                        if (sentFromObjectIDs.get(counter).equals(ParseUser.getCurrentUser().getObjectId().toString())) {
                            isFromCurrentUser.add(Boolean.TRUE);
                        } else {
                            isFromCurrentUser.add(Boolean.FALSE);
                        }
                    }

                    // Get latest message to display as a preview
                    strMessages = (ArrayList<String>) convRecord.get("messages");

                    // Get latest sent date
                    strDateTimes = (ArrayList<String>) convRecord.get("sentDates");

                    /*** Get the user avatar ***/
                    String mOtherUserObjID = "";

                    tempOtherUserObjID = (ArrayList<String>) convRecord.get("convParticipantObjectIDs");
                    if (tempOtherUserObjID.get(0).equals(ParseUser.getCurrentUser().getObjectId())) {
                        mOtherUserObjID = tempOtherUserObjID.get(1);
                    }
                    else if (tempOtherUserObjID.get(1).equals(ParseUser.getCurrentUser().getObjectId())) {
                        mOtherUserObjID = tempOtherUserObjID.get(0);
                    }
                    else {
                        mOtherUserObjID = "";
                    }

                    ParseQuery<ParseUser> query = ParseUser.getQuery();
                    query.whereEqualTo("objectId", mOtherUserObjID);
                    query.findInBackground(new FindCallback<ParseUser>() {
                        @Override
                        public void done(List<ParseUser> returnedUsers, ParseException e) {
                            if (e == null) {
                                Bitmap otherUserMessageAvatar = Utilities.getAvatar(getContext(), returnedUsers.get(0));
                                updateAdapter(strMessages, strDateTimes, isFromCurrentUser, otherUserMessageAvatar);
                            } else {
                                Log.v("PRBTEST", "Error retrieving avatars: " + e.getMessage());
                            }
                        }
                    });
                } else { // Display error message if unable to retrieve messages
                    Utilities.hideLoadingScreen();
                    Toast.makeText(getContext(), "Unable to retrieve messages :(", Toast.LENGTH_LONG).show();
                    Log.v("PRBTEST", "Error querying conversations table: " + e.getMessage());
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.send_message_button:
                sendMessage();
                break;
            case R.id.refresh_messages_button:
                showMessages();
                break;
            case R.id.send_message_text:
                scrollToBottomOfListView();
                break;
        }
    }

    /*
        sendMessage() gathers data from the current screen, and calls UserFunctions.sendMessageNotFirstTime with this data
     */
    private void sendMessage() {
        // Return and notify if user did not enter a message
        if (mMessageTextView.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), "Please enter a message", Toast.LENGTH_SHORT).show();
            return;
        }

        String message = mMessageTextView.getText().toString();
        String userToUserName = getArguments().getString("CONTACT_NAME");

        UserFunctions.sendMessageNotFirstTime(mConversationObjectID, ParseUser.getCurrentUser().getObjectId().toString(), message, this);
        mMessageTextView.setText("");

        //Log.v("push:"+TAG,"entering push function");
        ParseApplication.sendClientPush(userToUserName, Utilities.user.getUsername(), "message", message);
        //Log.v("push:" + TAG, "exiting push function");
    }

    /*
    * Updates adapter with the supplied ArrayList, which will refresh the data in the ListView
    *
    * Parameters:
    *   messages   messages to be displayed
    *   dates      dates(sent dates) to be displayed
    *   isFromCurrentUser   boolean set to TRUE if message is from current user, FALSE otherwise. This is what controls
    *                       which layout for this message is inflated
    *   otherUserAvatar     avatar of other user (we already have the current user's avatar in Global)
    */
    private void updateAdapter(ArrayList<String> messages, ArrayList<String> dates, ArrayList<Boolean> isFromCurrentUser, Bitmap otherUserAvatar) {
        if (messages != null) {
            mMessageAdapter = new CustomMessageAdapter(context, messages, dates, otherUserAvatar, isFromCurrentUser);
            lv.setAdapter(mMessageAdapter);
            Utilities.hideLoadingScreen();
        }
    }

    /*
        Scrolls to bottom of message list view.
     */
    private void scrollToBottomOfListView() {
        new Thread() {
            public void run() {
                try {
                    // Log.v("PRBTEST", "In scrollToBottomOfListView: in thread");
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mMessageAdapter != null) {
                                // Select the last row so it will scroll into view...
                                lv.setSelection(mMessageAdapter.getCount() - 1);
                            }
                        }
                    });
                    Thread.sleep(0);
                } catch (InterruptedException e) {
                    Log.v("PRBTEST", "ERROR scrolling listView: " + e.getMessage());
                }
            }
        }.start();
    }
    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FragmentMessageThread newInstance(String conversationObjectID, String contactName) {
        FragmentMessageThread fragment = new FragmentMessageThread();
        Bundle args = new Bundle();
        args.putString("CONVERSATION_OBJECT_ID", conversationObjectID); // TODO: change to global key
        args.putString("CONTACT_NAME", contactName); // TODO: change to global key
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((ActivityMain) activity).onSectionAttached(
                getArguments().getInt(Utilities.ARG_SECTION_NUMBER_KEY));
    }
}
