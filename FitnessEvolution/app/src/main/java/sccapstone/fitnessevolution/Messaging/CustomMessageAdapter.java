package sccapstone.fitnessevolution.Messaging;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseUser;

import java.util.ArrayList;

import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.R;

/**
 * Created by Preston on 11/20/2015.
 */

public class CustomMessageAdapter extends BaseAdapter{
    private ArrayList<String> mMessages;
    private ArrayList<String> mSentDateTimes;
    private ArrayList<Boolean> mIsFromCurrentUser;
    private Context context;
    private Bitmap mOtherUserAvatar;
    private Bitmap mCurrentUserAvatar;
    private static LayoutInflater inflater=null;

    public CustomMessageAdapter(Context paramContext, ArrayList<String> messages, ArrayList<String> sentDateTimes, Bitmap otherUserAvatar, ArrayList<Boolean> isFromCurrentUser) {
        mMessages=messages;
        mIsFromCurrentUser = isFromCurrentUser;
        context=paramContext;
        mOtherUserAvatar=otherUserAvatar;
        mCurrentUserAvatar = Utilities.getAvatar(context, ParseUser.getCurrentUser());
        mSentDateTimes = sentDateTimes;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return mMessages.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder
    {
        TextView firstLine;
        TextView secondLine;
        ImageView img;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View rowView;
        if (mIsFromCurrentUser.get(position)) {
            rowView = inflater.inflate(R.layout.messaging_list_sent, null);
            holder.firstLine=(TextView) rowView.findViewById(R.id.firstLine_sent);
            holder.secondLine=(TextView) rowView.findViewById(R.id.secondLine_sent);
            holder.img=(ImageView) rowView.findViewById(R.id.message_avatar_sent);
            holder.img.setImageBitmap(mCurrentUserAvatar);
        } else {
            rowView = inflater.inflate(R.layout.messaging_list_received, null);
            holder.firstLine=(TextView) rowView.findViewById(R.id.firstLine_received);
            holder.secondLine=(TextView) rowView.findViewById(R.id.secondLine_received);
            holder.img=(ImageView) rowView.findViewById(R.id.message_avatar_received);
            holder.img.setImageBitmap(mOtherUserAvatar);
        }

        holder.firstLine.setText(mMessages.get(position));
        holder.secondLine.setText(mSentDateTimes.get(position));
        /*rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "You Clicked "+mMessages.get(position), Toast.LENGTH_LONG).show();
            }
        });*/
        return rowView;
    }
}