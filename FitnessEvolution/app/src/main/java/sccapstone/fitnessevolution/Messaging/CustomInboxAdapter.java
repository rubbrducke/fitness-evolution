package sccapstone.fitnessevolution.Messaging;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import sccapstone.fitnessevolution.R;

/**
 * Created by Preston on 11/20/2015.
 */

public class CustomInboxAdapter extends BaseAdapter{
    private ArrayList<String> mConversationObjectIDs;
    private ArrayList<String> mContacts;
    private ArrayList<String> mMessagesPreview;
    private ArrayList<String> mSentDateTimes;
    private FragmentInbox mContext;
    private ArrayList<Bitmap> mMessageAvatars;
    private static LayoutInflater inflater=null;
    public CustomInboxAdapter(FragmentInbox paramContext, ArrayList<String> conversationObjectIDs, ArrayList<String> contacts, ArrayList<String> messagePreview, ArrayList<String> sentDateTimes, ArrayList<Bitmap> images) {
        mConversationObjectIDs = conversationObjectIDs;
        mContacts=contacts;
        mMessagesPreview=messagePreview;
        mContext=paramContext;
        mMessageAvatars=images;
        mSentDateTimes = sentDateTimes;
        inflater = ( LayoutInflater )mContext.getActivity().
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mMessagesPreview.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView firstLine;
        TextView secondLine;
        TextView thirdLine;
        ImageView img;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View rowView;
        String messagePreview = mMessagesPreview.get(position);

        rowView = inflater.inflate(R.layout.messaging_list_inbox_message, null);
        holder.firstLine=(TextView) rowView.findViewById(R.id.contactLine);
        holder.secondLine=(TextView) rowView.findViewById(R.id.dateLine);
        holder.thirdLine=(TextView) rowView.findViewById(R.id.messagePreviewLine);
        holder.img=(ImageView) rowView.findViewById(R.id.message_avatar);
        holder.firstLine.setText(mContacts.get(position));
        holder.secondLine.setText(mSentDateTimes.get(position));
        if (messagePreview.length() > 33) { // Trim message previews to 33 characters
            messagePreview = messagePreview.substring(0,28);
            messagePreview += "...";
        }
        holder.thirdLine.setText(messagePreview);
        holder.img.setImageBitmap(mMessageAvatars.get(position));
        rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext.getActivity(), "You Clicked " + mConversationObjectIDs.get(position), Toast.LENGTH_LONG).show();
                FragmentTransaction ft = mContext.getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, (new FragmentMessageThread().newInstance(mConversationObjectIDs.get(position), mContacts.get(position))));
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        return rowView;
    }

}