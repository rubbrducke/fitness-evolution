package sccapstone.fitnessevolution.Messaging;

/**
 * Created by Preston on 2/16/2016.
 *
 * This fragment displays summarized information about a user's conversations with other users, just like
 * an inbox in a text messaging application.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import sccapstone.fitnessevolution.Activites.ActivityMain;
import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.R;

public final class FragmentInbox extends android.support.v4.app.Fragment implements View.OnClickListener{
    private static CustomInboxAdapter mInboxAdapter; // ArrayAdapter to populate the ListView

    private ListView mListView;
    private View mRootView;
    private TextView mTextViewNoMessagesNotif;
    private Context context;
    private static ArrayList<Bitmap> messageAvatars;

    private ArrayList<String> conversationObjectIDs = new ArrayList<String>();
    private ArrayList<String> strContacts = new ArrayList<String>();
    private ArrayList<String> strMessagePreviews = new ArrayList<String>();
    private ArrayList<String> strDateTimes = new ArrayList<>();
    private ArrayList<String> tempStrContacts = new ArrayList<String>();
    private ArrayList<String> tempStrMessagePreviews = new ArrayList<String>();
    private ArrayList<String> tempStrDateTimes = new ArrayList<>();
    private ArrayList<String> tempOtherUserObjID = new ArrayList<>();
    private ArrayList<String> mOtherUserObjID = new ArrayList<>();
    private static String currUserName = ParseUser.getCurrentUser().getUsername();
    private static int usernameToDisplayIndex = -1;


    public FragmentInbox() {
    }

    @Override
    public void onStart() {
        super.onStart();

        Activity activity = getActivity();
        ((ActivityMain) activity).setActionBarTitle("Messages");

        showInbox();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Activity activity = getActivity();
        ((ActivityMain) activity).setActionBarTitle( getResources().getString(R.string.nav_coach_messages));

        context = getActivity();

        View rootView = inflater.inflate(R.layout.messaging_fragment_inbox, container, false);
        mRootView = rootView;

        ListView listView = (ListView) rootView.findViewById(R.id.listview_all_messages);
        mListView = listView;

        mTextViewNoMessagesNotif = (TextView) rootView.findViewById(R.id.textViewNoMessagesNotif);

        Button btnCompose = (Button) rootView.findViewById(R.id.compose_button);
        btnCompose.setOnClickListener(this);

        // Hide the keyboard if it is currently shown
        Utilities.hideKeyboard(getContext(), getActivity());

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.compose_button:
                launchComposeFragment();
                break;
        }
    }

    private void launchComposeFragment() {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, (new FragmentComposeMessage().newInstance("")));
        ft.addToBackStack(null);
        ft.commit();
    }

    /*
    *   ShowInbox() retrieves the most recent message, sent date, and contact name from each conversation that the current authenticated user
    *   has been involved in and calls a method to update the adapter that will populate the listview for each conversation.
    */
    private void showInbox() {
        strContacts.clear();
        strMessagePreviews.clear();
        strDateTimes.clear();
        conversationObjectIDs.clear();

        // Get user messages
        ParseQuery<ParseObject> inboxQuery = ParseQuery.getQuery("Conversations");
        inboxQuery.whereEqualTo("convParticipantObjectIDs", ParseUser.getCurrentUser().getObjectId());
        inboxQuery.orderByDescending("updatedAt"); // So that the newest message is at the top

        Utilities.showLoadingScreen(getContext());
        inboxQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> convRecordList, ParseException e) {
                if (e == null) {
                    // Successful query
                    for (ParseObject convRecord : convRecordList) {
                        tempStrContacts.clear();
                        tempStrMessagePreviews.clear();
                        tempStrDateTimes.clear();
                        tempOtherUserObjID.clear();

                        conversationObjectIDs.add(convRecord.getObjectId());

                        // Get the other user's username (not the current user's username)
                        tempStrContacts = (ArrayList<String>) convRecord.get("convParticipantUserNames");
                        //Log.v("PRBTEST", "tempStrContact: " + tempStrContacts.toString());
                        usernameToDisplayIndex = tempStrContacts.indexOf(currUserName);
                        if (usernameToDisplayIndex == 0) {
                            usernameToDisplayIndex = 1;
                        } else if (usernameToDisplayIndex == 1) {
                            usernameToDisplayIndex = 0;
                        } else if (usernameToDisplayIndex == -1) {
                            Log.v("PRBTEST", "ERROR: userName " + ParseUser.getCurrentUser().getUsername() + " not in convParticipantUsernames array.");
                            return; // todo: don't return, but just skip this record
                        }
                        strContacts.add(tempStrContacts.get(usernameToDisplayIndex));

                        tempOtherUserObjID = (ArrayList<String>) convRecord.get("convParticipantObjectIDs");
                        if (tempOtherUserObjID.get(0).equals(ParseUser.getCurrentUser().getObjectId())) {
                            mOtherUserObjID.add(tempOtherUserObjID.get(1));
                        }
                        else if (tempOtherUserObjID.get(1).equals(ParseUser.getCurrentUser().getObjectId())) {
                            mOtherUserObjID.add(tempOtherUserObjID.get(0));
                        }
                        else {
                            mOtherUserObjID.add("");
                        }

                        // Get latest message to display as a preview
                        tempStrMessagePreviews = (ArrayList<String>) convRecord.get("messages");
                        strMessagePreviews.add(tempStrMessagePreviews.get(tempStrMessagePreviews.size() - 1));

                        // Get latest sent date
                        tempStrDateTimes = (ArrayList<String>) convRecord.get("sentDates");
                        strDateTimes.add(tempStrDateTimes.get(tempStrDateTimes.size() - 1));
                    }

                    // Display a message if user has sent no messages before
                    if (convRecordList.isEmpty()) {
                        mTextViewNoMessagesNotif.setHint("We are all friends here! Click \"Compose\" to strike up a conversation!");
                    }

                    Log.v("PRBTEST", "Looking for avatars: " + mOtherUserObjID.toString());
                    messageAvatars = new ArrayList<Bitmap>();
                    // Get the user avatar
                    ParseQuery<ParseUser> query = ParseUser.getQuery();
                    query.whereContainedIn("objectId", mOtherUserObjID);
                    query.findInBackground(new FindCallback<ParseUser>() {
                        @Override
                        public void done(List<ParseUser> returnedUsers, ParseException e) {
                            if (e == null) {
                                ParseUser currContactUser = null;
                                for (int counter = 0; counter < mOtherUserObjID.size(); counter++) {
                                    // Now, sort the list of returned users using the mOtherUserObjID array as
                                    // the basis for correctly sorted.
                                    for (int x = 0; x < returnedUsers.size(); x++) {
                                        if (x < mOtherUserObjID.size() && returnedUsers.get(x).getObjectId().equals(mOtherUserObjID.get(counter))) {
                                            currContactUser = returnedUsers.get(x);
                                        }
                                    }
                                    Log.v("PRBTEST", "currContactUser: " + currContactUser.getObjectId());
                                    messageAvatars.add(Utilities.getAvatar(getContext(), currContactUser));
                                }
                                updateAdapter(conversationObjectIDs, strContacts, strMessagePreviews, strDateTimes, messageAvatars);

                            } else {
                                Log.v("PRBTEST", "Error retrieving avatars: " + e.getMessage());
                            }
                        }
                    });
                } else { // Display an error message if there was an error retrieving messages
                    Utilities.hideLoadingScreen();
                    Toast.makeText(getContext(), "Unable to retrieve messages :(", Toast.LENGTH_LONG);
                    Log.v("PRBTEST", "Error querying conversations table: " + e.getMessage());
                }
            }
        });
    }

    /*
    * Updates adapter with the supplied ArrayList, which will refresh the data in the ListView
    *
    * Parameters:
    *   dataList   the data to show in the ListView
    */
    private void updateAdapter(ArrayList<String> conversationObjectIDs, ArrayList<String> contacts, ArrayList<String> messages, ArrayList<String> dates, ArrayList<Bitmap> messageAvatars) {
        if (messages != null) {
            mInboxAdapter = new CustomInboxAdapter(this, conversationObjectIDs, contacts, messages, dates, messageAvatars);
            mListView.setAdapter(mInboxAdapter);
            Utilities.hideLoadingScreen();
        }
    }
    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FragmentInbox newInstance(int sectionNumber) {
        FragmentInbox fragment = new FragmentInbox();
        Bundle args = new Bundle();
        args.putInt(Utilities.ARG_SECTION_NUMBER_KEY, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((ActivityMain) activity).onSectionAttached(
                getArguments().getInt(Utilities.ARG_SECTION_NUMBER_KEY));
    }
}