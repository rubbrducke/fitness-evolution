package sccapstone.fitnessevolution.Messaging;

/**
 * Created by Preston on 11/19/2015.
 *
 * This fragment handles message composition.
 */

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;

import sccapstone.fitnessevolution.Activites.ActivityMain;
import sccapstone.fitnessevolution.R;

public final class FragmentComposeMessage extends android.support.v4.app.Fragment implements View.OnClickListener{
    private static EditText mMessageTextView;
    private static EditText mContactTextView;

    public FragmentComposeMessage() {
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.messaging_fragment_compose, container, false);

        Button btnSend = (Button) rootView.findViewById(R.id.compose_send_message_button);
        btnSend.setOnClickListener(this);
        mMessageTextView = (EditText) rootView.findViewById(R.id.compose_message_text);
        mContactTextView = (EditText) rootView.findViewById(R.id.compose_contact_text);

        if (getArguments() != null) {
            String un = getArguments().getString("TO_USERNAME");
            if (!un.equals("")) {
                mContactTextView.setText(getArguments().getString("TO_USERNAME"));
            }
        }

        // Set the actionbar title
        Activity activity = getActivity();
        ((ActivityMain) activity).setActionBarTitle(getString(R.string.compose_action_bar_title));

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.compose_send_message_button:
                sendMessage();
                break;
            case R.id.compose_contact_text:
                mContactTextView.setText("");
                break;
        }
    }

    private static String message;
    private static String userToObjectID;
    private static String userToUserName;
    private static ArrayList<String> arrConvParticipantObjIDs;
    /*
        Sends the message that the user has typed.
     */
    private void sendMessage() {
        // Get the objectID for the username that is in the contact text view
        String contact = mContactTextView.getText().toString();

        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("username", contact.toLowerCase());
        query.getFirstInBackground(new GetCallback<ParseUser>() {
            public void done(ParseUser retrievedUser, ParseException e) {
                if (retrievedUser == null) {
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), "Invalid Username", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    userToObjectID = retrievedUser.getObjectId();

                    if (userToObjectID.equals(ParseUser.getCurrentUser().getObjectId().toString())) {
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(getActivity(), "Please enter a username other than your own.", Toast.LENGTH_LONG).show();
                            }
                        });

                        return;
                    }
                    // Return and notify if user did not enter a message
                    if (mMessageTextView.getText().toString().trim().equals("")) {
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(getActivity(), "Please enter a message", Toast.LENGTH_SHORT).show();
                            }
                        });

                        return;
                    }

                    /************************************************
                     *
                     * Send the message to the corresponding objectID
                     *
                     */
                    message = mMessageTextView.getText().toString();

                    userToUserName = retrievedUser.getUsername();

                    // Determine if we need to add to a message thread, or start a new one.
                    arrConvParticipantObjIDs = new ArrayList<String>();
                    arrConvParticipantObjIDs.add(ParseUser.getCurrentUser().getObjectId());
                    arrConvParticipantObjIDs.add(userToObjectID);

                    ParseQuery<ParseObject> sentMessageQuery = ParseQuery.getQuery("Conversations");
                    sentMessageQuery.whereContainsAll("convParticipantObjectIDs", arrConvParticipantObjIDs);

                    Log.v("PRBTEST", "Querying table for a record with convParticipantObjectIDs of " + ParseUser.getCurrentUser().getObjectId() + " and " + userToObjectID);

                    sentMessageQuery.getFirstInBackground(new GetCallback<ParseObject>() {
                        public void done(ParseObject messageThreadRecord, ParseException e) {
                            if (e == null) {
                                // Message thread already exists!
                                // Launch the message thread fragment, showing the conversation
                                FragmentMessageThread newFragMessageThread = new FragmentMessageThread().newInstance(messageThreadRecord.getObjectId(), userToUserName);
                                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.container, (newFragMessageThread));
                                ft.addToBackStack(null);
                                ft.commit();

                                UserFunctions.sendMessageNotFirstTime(messageThreadRecord.getObjectId(), ParseUser.getCurrentUser().getObjectId(), message, newFragMessageThread);
                            } else {
                                // Message thread does not already exist. So create one.
                                UserFunctions.sendMessageFirstTime(ParseUser.getCurrentUser().getObjectId(), ParseUser.getCurrentUser().getUsername(), userToObjectID, userToUserName, message);

                                // Find the conversation record that was just created/"sent"
                                ParseQuery<ParseObject> retrieveSentMessageQuery = ParseQuery.getQuery("Conversations");
                                retrieveSentMessageQuery.whereContainsAll("convParticipantObjectIDs", arrConvParticipantObjIDs);

                                retrieveSentMessageQuery.getFirstInBackground(new GetCallback<ParseObject>() {
                                    public void done(ParseObject sentMessageRecord, ParseException e) {
                                        if (e == null) {
                                            // Successful query
                                            // Launch the message thread fragment, showing the conversation
                                            //Log.v("PRBTEST","Found the message that we just sent: " + sentMessageRecord.getObjectId());
                                            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction()
                                                    .replace(R.id.container, (new FragmentMessageThread().newInstance(sentMessageRecord.getObjectId(), userToUserName)));
                                            ft.addToBackStack(null);
                                            ft.commit();
                                        } else {
                                            Log.v("PRBTEST", "ERROR finding the message that was just sent: " + e.getMessage());
                                        }
                                    }
                                });
                            }
                        }
                    });

                }
            }
        });
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FragmentComposeMessage newInstance(String strToUserName) {
        FragmentComposeMessage fragment = new FragmentComposeMessage();
        Bundle args = new Bundle();
        args.putString("TO_USERNAME", strToUserName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
}
