package sccapstone.fitnessevolution.Logging.DietaryLogging;

import com.parse.ParseUser;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import sccapstone.fitnessevolution.Utilities.Utilities;

/**
 * Created by Ben Aaron on 4/3/2016.
 */
public class RoutineDietReport {

    protected Map<String, DayStatusReport> report = new HashMap<>();

    /*--------------------------------------------------------------------

        GET REPORT

        Returns a read-only version of the report.

    --------------------------------------------------------------------*/

    public Map<String, DayStatusReport> getFullReport()
    {
        if( report.size() == 0 )
        {
            report = buildRoutineReport();
        }
        return Collections.unmodifiableMap(report);

    } // function


    /*--------------------------------------------------------------------

        GET DIET REPORT

        Retrieves a specific muscle's report.

    --------------------------------------------------------------------*/

    public DayStatusReport getDietReport( String day )
    {
        return report.containsKey( day ) ? report.get(day) : null;
    }


    /*--------------------------------------------------------------------

        IS TOO FEW CALORIES, IS TOO MANY CALORIES, IS TOO MUCH FAT, IS TOO LITTLE FAT, IS TOO MANY CARBOHYDRATES, ETC

        Helper methods that return the requested status item of a day.

    --------------------------------------------------------------------*/

    public boolean isTooManyCalories( String day )
    {
        if( report.containsKey(day))
        {
            return report.get(day).tooManyCalories;
        }

        return false;

    } // function


    public boolean isTooFewCalories( String day )
    {
        if( report.containsKey(day))
        {
            return report.get(day).tooFewCalories;
        }

        return false;

    } // function


    public boolean isTooMuchFat( String day )
    {
        if( report.containsKey(day))
        {
            return report.get(day).tooMuchFat;
        }

        return false;

    } // function

    public boolean isTooLittleFat( String day )
    {
        if( report.containsKey(day))
        {
            return report.get(day).tooLittleFat;
        }

        return false;

    } // function

    public boolean isTooManyCarbohydrates( String day )
    {
        if( report.containsKey(day))
        {
            return report.get(day).tooManyCarbohydrates;
        }

        return false;

    } // function

    public boolean isTooFewCarbohydrates( String day )
    {
        if( report.containsKey(day))
        {
            return report.get(day).tooFewCarbohydrates;
        }

        return false;

    } // function

    public boolean isTooMuchProtein( String day )
    {
        if( report.containsKey(day))
        {
            return report.get(day).tooMuchProtein;
        }

        return false;

    } // function

    public boolean isTooLittleProtein( String day )
    {
        if( report.containsKey(day))
        {
            return report.get(day).tooLittleProtein;
        }

        return false;

    } // function


    final double TOLERANCE = 0.1;
    final int CALORIES = 2000;
    final int CARBS = 130;
    final int FAT_LOW_PERCENT = 20;
    final int FAT_HIGH_PERCENT = 35;
    final int PROTEIN_MALE = 56;
    final int PROTEIN_FEMALE = 46;

    public Map<String, DayStatusReport> buildRoutineReport()
    {
        Map<String, DayStatusReport> report = new HashMap<>();

        Map<String, DaySummary> thisWeek = DietaryLog.buildAverageHistory( DietaryLog.xAxis.currentWeek );

        for( Map.Entry<String, DaySummary> entry : thisWeek.entrySet() )
        {
            String dayOfEntry = entry.getKey();
            DayStatusReport dayReport = new DayStatusReport();

            /*--------------------------------------------------------------------
                Check Calories
            --------------------------------------------------------------------*/

            if( entry.getValue().allMealsSummary.calories < (CALORIES - CALORIES*TOLERANCE) )
                dayReport.tooFewCalories = true;

            else if ( entry.getValue().allMealsSummary.calories > (CALORIES + CALORIES*TOLERANCE) )
                dayReport.tooManyCalories = true;

            /*--------------------------------------------------------------------
                    Check Carbohydrates
            --------------------------------------------------------------------*/

            if( entry.getValue().allMealsSummary.carbohydrates < (CARBS - CARBS*TOLERANCE) )
                dayReport.tooFewCarbohydrates = true;

            else if ( entry.getValue().allMealsSummary.carbohydrates > (CARBS + CARBS*TOLERANCE))
                dayReport.tooManyCarbohydrates = true;

            /*--------------------------------------------------------------------
                    Check Fat
            --------------------------------------------------------------------*/

            if( entry.getValue().allMealsSummary.fat < FAT_LOW_PERCENT )
                dayReport.tooLittleFat = true;

            else if ( entry.getValue().allMealsSummary.fat > FAT_HIGH_PERCENT )
                dayReport.tooMuchFat = true;

            /*--------------------------------------------------------------------
                For Male Users
            --------------------------------------------------------------------*/

            if(Utilities.isMale(ParseUser.getCurrentUser().getString("gender")) )
            {
                /*--------------------------------------------------------------------
                    Check Protein
                --------------------------------------------------------------------*/

                if( entry.getValue().allMealsSummary.protein < (PROTEIN_MALE - PROTEIN_MALE*TOLERANCE))
                    dayReport.tooLittleProtein = true;

                else if ( entry.getValue().allMealsSummary.protein > (PROTEIN_MALE + PROTEIN_MALE*TOLERANCE))
                    dayReport.tooMuchProtein = true;
            }

            /*--------------------------------------------------------------------
                For Female Users
            --------------------------------------------------------------------*/

            else
            {
                /*--------------------------------------------------------------------
                    Check Protein
                --------------------------------------------------------------------*/

                if( entry.getValue().allMealsSummary.protein < (PROTEIN_FEMALE - PROTEIN_FEMALE*TOLERANCE))
                    dayReport.tooLittleProtein = true;

                else if ( entry.getValue().allMealsSummary.protein > (PROTEIN_FEMALE + PROTEIN_FEMALE*TOLERANCE))
                    dayReport.tooMuchProtein = true;
            }

            report.put( dayOfEntry, dayReport );
        }

        return report;
    }

}
