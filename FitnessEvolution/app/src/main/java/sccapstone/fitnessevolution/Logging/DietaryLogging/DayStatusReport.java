package sccapstone.fitnessevolution.Logging.DietaryLogging;

/**
 * Created by Ben Aaron on 4/3/2016.
 *
 * Holder class which contains information to be used by the coaches about a user's diet.
 */
public class DayStatusReport
{
    public boolean tooManyCalories = false;
    public boolean tooFewCalories = false;
    public boolean tooMuchFat = false;
    public boolean tooLittleFat = false;
    public boolean tooManyCarbohydrates = false;
    public boolean tooFewCarbohydrates = false;
    public boolean tooLittleProtein = false;
    public boolean tooMuchProtein = false;

    public String problemMeal = "";
}
