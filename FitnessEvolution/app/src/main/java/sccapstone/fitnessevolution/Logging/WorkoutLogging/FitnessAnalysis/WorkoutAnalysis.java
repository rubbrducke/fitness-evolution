package sccapstone.fitnessevolution.Logging.WorkoutLogging.FitnessAnalysis;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import sccapstone.fitnessevolution.Histogram.WorkoutHistogram;


/*====================================================================

    WORKOUT ANALYSIS
    Created by Matthew Short

    The class is used for examining the contents of a workout log
    and retrieving a report on it.

====================================================================*/

public class WorkoutAnalysis
{

    private static RoutineStatusReport routineReport;

    public static RoutineStatusReport getRoutineReport()
    {
        if ( routineReport == null )
        {
            BuildStatusReport();
        }

        return routineReport;
    }


    /*--------------------------------------------------------------------

        BUILD STATUS REPORT

        Purpose:
        Condenses all of the workout log information into a
        usable set of information that describes the status
        of each muscle in the current week.

    --------------------------------------------------------------------*/

    public static void BuildStatusReport()
    {

        Log.v("BUILD STATUS REPORT", "Start of Method BuildStatusReport");

        Map<String, Map<String, MuscleWorkout>> thisWeek = WorkoutLog.buildHistory( WorkoutLog.historyLength.week,
                                                                                    WorkoutLog.historyType.average);

        Map<String, Map<String, MuscleWorkout>> averageWeek = WorkoutLog.buildHistory( WorkoutLog.historyLength.month,
                                                                                       WorkoutLog.historyType.average);

        Map<String, MuscleWorkout> currentTotalWorkout = buildTotalWorkout( thisWeek );
        Map<String, MuscleWorkout> totalWorkout = buildTotalWorkout( averageWeek );

        RoutineStatusReport newRoutineReport = new RoutineStatusReport();

        Log.v("BUILD STATUS REPORT", "Begin looping each muscle.");

        for( Map.Entry<String, MuscleWorkout> muscleWorkout : totalWorkout.entrySet() )
        {
            String muscle = muscleWorkout.getKey();
            MuscleStatusReport uniqueMuscleReport = new MuscleStatusReport();

            Log.v("BUILD STATUS REPORT", "Muscle: " + muscle);

            if (currentTotalWorkout.containsKey(muscle))
            {
                MuscleWorkout thisMuscleWorkout = currentTotalWorkout.get(muscle);
                uniqueMuscleReport.exercised = true;

                double percentChangeInWorkout;

                percentChangeInWorkout = thisMuscleWorkout.totalWeightLifted
                                         - muscleWorkout.getValue().totalWeightLifted;

                percentChangeInWorkout = percentChangeInWorkout
                                         / muscleWorkout.getValue().totalWeightLifted;

                percentChangeInWorkout = percentChangeInWorkout * 100;

                //region Debug
                Log.v("BUILD STATUS REPORT", "Change in workout: "
                                             + "\nTypical total weight lifted: "
                                             + muscleWorkout.getValue().totalWeightLifted
                                             + "\nThis week's total weight lifted so far: "
                                             + thisMuscleWorkout.totalWeightLifted
                                             + "\nPercent Change: " + percentChangeInWorkout);
                //endregion

                if (percentChangeInWorkout >= 50)
                {
                    uniqueMuscleReport.underworked = false;
                    uniqueMuscleReport.overworked = true;
                }
                else if (percentChangeInWorkout <= -50)
                {
                    uniqueMuscleReport.underworked = true;
                    uniqueMuscleReport.overworked = false;
                }
                else
                {
                    uniqueMuscleReport.underworked = false;
                    uniqueMuscleReport.overworked = false;
                }
            }
            else
            {
                uniqueMuscleReport.exercised = false;
            }

            //region Debug
            Log.v("BUILD STATUS REPORT", "Muscle Report: "
                                         + "\nExercised: " + uniqueMuscleReport.exercised
                                         + "\nOverworked: " + uniqueMuscleReport.overworked
                                         + "\nUnderworked: " + uniqueMuscleReport.underworked);
            //endregion

            newRoutineReport.report.put(muscle, uniqueMuscleReport);
        }

        routineReport = newRoutineReport;

        Log.v("BUILD STATUS REPORT", "End of Method BuildStatusReport");

    } // function


    /*--------------------------------------------------------------------

        BUILD TOTAL WORKOUT

        Purpose:
        Helper method to convert workout log's organized by date
        into a condensed map organized by each muscle.

        Arguments:
        The Map to convert.

        Returns:
        A Map< muscle, muscleWorkout >

    --------------------------------------------------------------------*/

    private static Map<String, MuscleWorkout> buildTotalWorkout( Map<String, Map<String, MuscleWorkout>> averageRoutine )
    {
        Map<String, MuscleWorkout> newTotalWorkout = new HashMap<>();

        for( Map<String, MuscleWorkout> workoutDay : averageRoutine.values() )
        {
            for( Map.Entry<String, MuscleWorkout> entry : workoutDay.entrySet() )
            {
                String muscle = entry.getKey();

                if( newTotalWorkout.containsKey( muscle ) )
                {
                    newTotalWorkout.get(muscle).averageWithWorkout(entry.getValue());
                }
                else
                {
                    newTotalWorkout.put( muscle, entry.getValue() );
                }
            }
        }

        return newTotalWorkout;

    } // method

} // class
