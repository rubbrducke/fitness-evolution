package sccapstone.fitnessevolution.Logging.WorkoutLogging.FitnessAnalysis;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/*====================================================================

    ROUTINE STATUS REPORT
    Created by Matthew Short

    This is a simple format given to other components and classes
    when they need to know what the workout analysis has determined.

====================================================================*/

public class RoutineStatusReport
{

    protected Map<String, MuscleStatusReport> report = new HashMap<>();


    /*--------------------------------------------------------------------

        GET REPORT

        Purpose:
        Accessor method that returns a read-only version of the report.

    --------------------------------------------------------------------*/

    public Map<String, MuscleStatusReport> getFullReport()
    {
        return Collections.unmodifiableMap( report );

    } // function


    /*--------------------------------------------------------------------

        GET MUSCLE REPORT

        Purpose:
        Helper method that retrieves a specific muscle's report.

    --------------------------------------------------------------------*/

    public MuscleStatusReport getMuscleReport( String muscle )
    {
        return report.containsKey( muscle ) ? report.get(muscle) : null;
    }


    /*--------------------------------------------------------------------

        IS EXERCISED, IS OVERWORKED, IS UNDERWORKED

        Helper methods that return the requested status item of a muscle.

    --------------------------------------------------------------------*/

    public boolean isExercised( String muscle )
    {
        if( report.containsKey(muscle))
        {
            return report.get(muscle).exercised;
        }

        return false;

    } // function


    public boolean isOverworked( String muscle )
    {
        if( report.containsKey(muscle))
        {
            return report.get(muscle).overworked;
        }

        return false;

    } // function


    public boolean isUnderworked( String muscle )
    {
        if( report.containsKey(muscle))
        {
            return report.get(muscle).underworked;
        }

        return false;

    } // function

}
