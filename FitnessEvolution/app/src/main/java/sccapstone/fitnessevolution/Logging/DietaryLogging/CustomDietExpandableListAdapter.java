package sccapstone.fitnessevolution.Logging.DietaryLogging;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import sccapstone.fitnessevolution.R;

/**
 * Created by Ben Aaron on 4/8/2016.
 * Last updated by Ben Aaron on 4/26/2016
 * 
 * This class adapts the ExpandableListView to create entries in the list and links those entries to a diet entry view fragment.
 */
public class CustomDietExpandableListAdapter extends BaseExpandableListAdapter {

    private FragmentDietExpandableList mContext;
    private ArrayList<String> headerList = new ArrayList<>();
    private ArrayList<ArrayList<DietEntry>> entryList = new ArrayList<>();
    private HashMap<String, ArrayList<FragmentDietExpandableList.Holder>> entryMap = new HashMap<>();
    private static LayoutInflater inflater = null;

    /**
     * Constructor method which sets the values for the list of headers to the list of meals, the map of entries and the list of entry holders.
     * @param context
     * @param hList
     * @param eMap
     * @param dietEntryList
     */
    public CustomDietExpandableListAdapter(FragmentDietExpandableList context, ArrayList<String> hList,
                                           HashMap<String, ArrayList<FragmentDietExpandableList.Holder>> eMap,
                                           ArrayList<ArrayList<DietEntry>> dietEntryList){
        this.mContext = context;
        this.headerList = hList;
        this.entryMap = eMap;
        this.entryList = dietEntryList;
        this.inflater = ( LayoutInflater )mContext.getActivity().
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Method to get the number of list headers
     * @return size of the headerList
     */
    @Override
    public int getGroupCount() {
        return this.headerList.size();
    }

    /**
     * Method to get the number of entries within a sub-list
     * @param groupPosition
     * @return size of a list given by it's header's position in the header list
     */
    @Override
    public int getChildrenCount(int groupPosition) {
        if (headerList.get(groupPosition) != null) {
            if (this.entryMap.get(this.headerList.get(groupPosition)) != null) {
                return this.entryMap.get(this.headerList.get(groupPosition)).size();
            }
            return 0;
        }
        return 0;
    }

    /**
     * Method to get the header object (meal name)
     * @param groupPosition
     * @return name of meal
     */
    @Override
    public Object getGroup(int groupPosition) {
        return this.headerList.get(groupPosition);
    }

    /**
     * Method to get an entry from a sub-list
     * @param groupPosition
     * @param childPosition
     * @return diet entry
     */
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.entryMap.get(this.headerList.get(groupPosition)).get(childPosition);
    }

    /**
     * Basic getGroudID method inherited from ExpandableListAdapter
     * @param groupPosition
     * @return groupPosition
     */
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    /**
     * Basic getChildId method inherited from ExpandableListAdapter
     * @param groupPosition
     * @param childPosition
     * @return childPosition
     */
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    /**
     * Method to check if the ids in the expandable list are stable
     * @return false
     */
    @Override
    public boolean hasStableIds() {
        return false;
    }

    /**
     * Expands the list view's layout
     * @param groupPosition
     * @param isExpanded
     * @param convertView
     * @param parent
     * @return convertView
     */
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.diet_list_header, null);
        }
        TextView header = (TextView) convertView.findViewById(R.id.ListHeader);
        header.setText(headerTitle);
        return convertView;
    }

    /**
     * Method to expand the layout for diet entries in the list view,
     * and set them up to start the diet entry view fragment when they are clicked.
     * @param groupPosition
     * @param childPosition
     * @param isLastChild
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        FragmentDietExpandableList.Holder holder = (FragmentDietExpandableList.Holder) getChild(groupPosition, childPosition);
        String name = holder.name;
        String info = holder.info;
        if (convertView == null){
            convertView = inflater.inflate(R.layout.diet_list_item_food_entry, null);
        }
        TextView childName = (TextView) convertView.findViewById(R.id.foodEntryName);
        TextView childInfo = (TextView) convertView.findViewById(R.id.foodEntryInfo);
        childName.setText(name);
        childInfo.setText(info);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DietEntry dietEntry = null;
                Log.v("ExpandableListAdapter", "groupPosition is "+groupPosition);
                Log.v("ExpandableListAdapter", "childPosition is "+childPosition);
                Log.v("ExpandableListAdapter", "entryList size is "+entryList.size());
                if (groupPosition < entryList.size()) {
                    Log.v("ExpandableListAdapter", "entryList["+entryList.size()+"] size is "+entryList.get(groupPosition).size());
                    if (childPosition < entryList.get(groupPosition).size()) {
                        dietEntry = entryList.get(groupPosition).get(childPosition);
                    }
                }
                if (dietEntry != null) {
                    FragmentTransaction ft = mContext.getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, (new FragmentDietEntryViewer().newInstance(dietEntry)));
                    ft.addToBackStack(null);
                    ft.commit();
                }
                else
                    Log.v("ExpandableListAdapter", "Diet entry is null");
            }
        });
        return convertView;
    }

    /**
     * Generice isChildSelectable method inherited from ExpandableListAdapter
     * @param groupPosition
     * @param childPosition
     * @return true
     */
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
