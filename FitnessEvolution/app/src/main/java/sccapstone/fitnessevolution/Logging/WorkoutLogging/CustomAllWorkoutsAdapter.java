package sccapstone.fitnessevolution.Logging.WorkoutLogging;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import sccapstone.fitnessevolution.Logging.FragmentWorkoutDietaryLogging;
import sccapstone.fitnessevolution.R;


/*====================================================================

    CUSTOM ALL-WORKOUTS ADAPTER
    Created by Preston

    Adapter for workout logs.

====================================================================*/

public class CustomAllWorkoutsAdapter extends BaseAdapter
{

    private ArrayList<String> mWorkoutDayDate;
    private ArrayList<String> mWorkoutTime;
    private ArrayList<String> mExerciseMuscleGroupsList;
    private ArrayList<String> mExerciseNamesList;
    private ArrayList<String> mWorkoutObjectIDs;
    private FragmentWorkoutDietaryLogging mContext;
    private static LayoutInflater inflater = null;


    /*--------------------------------------------------------------------

        CUSTOM ALL-WORKOUTS ADAPTER

        Purpose:
        Adapter for displaying workout logs.

    --------------------------------------------------------------------*/

    public CustomAllWorkoutsAdapter(FragmentWorkoutDietaryLogging paramContext, ArrayList<String> workoutDayDate, ArrayList<String> workoutTime, ArrayList<String> exerciseMuscGroupList, ArrayList<String> exerciseNamesList, ArrayList<String> workoutObjectID)
    {
        mWorkoutDayDate = workoutDayDate;
        mWorkoutTime = workoutTime;
        mExerciseMuscleGroupsList = exerciseMuscGroupList;
        mExerciseNamesList = exerciseNamesList;
        Log.v("PRBTEST", "CustomAllWorkoutAdapter: exerciseMuscGroupList: " + exerciseMuscGroupList.toString());
        mWorkoutObjectIDs = workoutObjectID;
        mContext = paramContext;
        inflater = ( LayoutInflater )mContext.getActivity().
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount()               { return mWorkoutDayDate.size(); }

    @Override
    public Object getItem(int position) { return position; }

    @Override
    public long getItemId(int position) { return position; }

    public class Holder
    {
        TextView dayDateLine;
        TextView timeLine;
        TextView muscleGroupLine;
        TextView exerciseNameLine;
    }

    /*--------------------------------------------------------------------

        GET VIEW

        Purpose:
        This is where the magic happens.

    --------------------------------------------------------------------*/

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.workout_list_item, null);
        holder.dayDateLine=(TextView) rowView.findViewById(R.id.workoutDayDate);
        holder.muscleGroupLine=(TextView) rowView.findViewById(R.id.workoutMuscleGroups);
        holder.exerciseNameLine=(TextView) rowView.findViewById(R.id.workoutExerciseNames);

        if (position < mWorkoutDayDate.size())
            holder.dayDateLine.setText(mWorkoutDayDate.get(position));

        if (position < mWorkoutTime.size())
            holder.timeLine.setText(mWorkoutTime.get(position));

        if (position < mExerciseMuscleGroupsList.size())
            holder.muscleGroupLine.setText(mExerciseMuscleGroupsList.get(position));

        if (position < mExerciseNamesList.size())
            holder.exerciseNameLine.setText(mExerciseNamesList.get(position));

        rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v)
            {
                FragmentTransaction ft = mContext.getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, (new FragmentEditWorkout().newInstance(mWorkoutObjectIDs.get(position), "", "")));
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        return rowView;
    }
}