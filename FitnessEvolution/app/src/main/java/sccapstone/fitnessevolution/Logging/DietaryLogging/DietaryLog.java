package sccapstone.fitnessevolution.Logging.DietaryLogging;

import android.util.Log;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import sccapstone.fitnessevolution.Utilities.Utilities;


/*====================================================================

    DIETARY LOG

    Contains a dietary log <Date, DietEntry>, and has methods
    for converting to and from Parse Objects.

====================================================================*/

public class DietaryLog
{

    public enum xAxis { averageWeek, currentWeek }
    public enum yAxis { calories, carbohydrates, protein, fat }

    public static Map<String, DaySummary> buildAverageHistory( xAxis length )
    {
        Map<String, DaySummary> averageHistory = new HashMap<>();

        if( length.equals(xAxis.currentWeek) )
        {
            Map<Date, DaySummary> verboseHistory = buildVerboseHistory(Utilities.getWeek(0).first, Utilities.getWeek(0).second);

            for( DaySummary daySum : verboseHistory.values() )
            {
                String day = new SimpleDateFormat("EEEE", Locale.US).format( daySum.date );

                if( averageHistory.containsKey(day) )
                {
                    averageHistory.get(day).add( daySum.allEntries );
                }

                else
                {
                    averageHistory.put( day, daySum );
                }
            }
        }

        else if( length.equals(xAxis.averageWeek) )
        {
            Map<Date, DaySummary> verboseHistory = buildVerboseHistory(Utilities.getWeek(4).first, Utilities.getWeek(0).second);

            for( DaySummary daySum : verboseHistory.values() )
            {
                String day = new SimpleDateFormat("EEEE", Locale.US).format( daySum.date );

                if( averageHistory.containsKey(day) )
                {
                    averageHistory.get(day).average( daySum.allEntries );
                }

                else
                {
                    averageHistory.put( day, daySum );
                }
            }
        }

        return averageHistory;
    }

    public static Map<Date, DaySummary> buildVerboseHistory( Date start, Date end )
    {
        Map<Date, DaySummary> dietHistory = new TreeMap<>();

        ParseUser user = ParseUser.getCurrentUser();

        ParseQuery<ParseObject> dietQuery = ParseQuery.getQuery("DietLogs");
        dietQuery.whereEqualTo("userObjectID", user.getObjectId());
        dietQuery.whereGreaterThanOrEqualTo("Date", start);
        dietQuery.whereLessThanOrEqualTo("Date", end);
        dietQuery.orderByAscending("Date");

        Log.v("BUILD VERBOSE HISTORY", "Parse Query: "
                                       + "Date between " + start + " and " + end);

        try
        {
            List<ParseObject> entriesList = dietQuery.find();

            Log.v("BUILD VERBOSE HISTORY", entriesList.size() + " entries.");

            for( ParseObject dietEntry : entriesList )
            {
                Log.v("BUILD VERBOSE HISTORY", "Creating diet entry from parseObject");
                DietEntry newEntry = new DietEntry( dietEntry );

                if( dietHistory.containsKey( newEntry.date ) )
                {
                    Log.v("BUILD VERBOSE HISTORY", "History already contains date.");
                    dietHistory.get( newEntry.date ).add( newEntry );
                }
                else
                {
                    Log.v("BUILD VERBOSE HISTORY", "Adding new date to history.");

                    DaySummary day = new DaySummary();
                    day.date = newEntry.date;
                    day.add( newEntry );

                    dietHistory.put( newEntry.date, day );
                }
            }
        }
        catch( Exception e )
        {
            Log.v("BUILD VERBOSE HISTORY", "Error: " + e);
        }

        Log.v("BUILD VERBOSE HISTORY", "History has " + dietHistory.size() + " entries.");

        return dietHistory;
    }
}
