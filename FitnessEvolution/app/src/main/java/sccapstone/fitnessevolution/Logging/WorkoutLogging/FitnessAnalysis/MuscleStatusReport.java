package sccapstone.fitnessevolution.Logging.WorkoutLogging.FitnessAnalysis;


/*====================================================================

    MUSCLE STATUS REPORT
    Created by Matthew Short

    Container class for booleans describing the status of a muscle.

====================================================================*/

public class MuscleStatusReport
{
    public boolean exercised = false;
    public boolean overworked = false;
    public boolean underworked = false;

} // subclass
