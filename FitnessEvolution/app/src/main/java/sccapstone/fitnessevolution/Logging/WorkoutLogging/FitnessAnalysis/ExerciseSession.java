package sccapstone.fitnessevolution.Logging.WorkoutLogging.FitnessAnalysis;

import java.util.ArrayList;


/*====================================================================

    EXERCISE SESSION
    Created by Matthew Short

    Class that represents a single exercise.

====================================================================*/

public class ExerciseSession
{
    public String exerciseName;
    public ArrayList<String> muscleGroups = new ArrayList<>();
    public ArrayList<ExerciseSet> sets = new ArrayList<>();

} // class
