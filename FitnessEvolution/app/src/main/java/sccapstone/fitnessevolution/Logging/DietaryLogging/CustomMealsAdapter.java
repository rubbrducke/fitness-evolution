package sccapstone.fitnessevolution.Logging.DietaryLogging;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import sccapstone.fitnessevolution.Logging.FragmentWorkoutDietaryLogging;
import sccapstone.fitnessevolution.R;

/**
 * Created by Ben Aaron on 3/22/2016.
 * Last updated by Ben Aaron on 4/26/2016
 *
 * This class adapts the ListView to hold a list of dates logged with the meals eaten on each of those days
 */
public class CustomMealsAdapter extends BaseAdapter {
    private Map<Date, DaySummary> dietLog;
    private ArrayList<String> mMeals = new ArrayList<>();
    private ArrayList<Date> mDates;
    private FragmentWorkoutDietaryLogging mContext;
    private static LayoutInflater inflater = null;

    /**
     * Constructor method which sets the dates list from the given log
     * @param paramContext
     * @param log
     */
    public CustomMealsAdapter(FragmentWorkoutDietaryLogging paramContext, Map<Date, DaySummary> log){
        Log.v("CustomMealsAdapter", "New custom meals adapter");
        mContext = paramContext;
        dietLog = log;
        Set<Date> dateSet = dietLog.keySet();
        mDates = new ArrayList<>(dateSet);
        if (mDates.isEmpty()){
            Log.v("CustomMealsAdapter", "Dates is empty");
        }
        inflater = ( LayoutInflater )mContext.getActivity().
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Method to get the number of entries in the list (i.e. the number of dates)
     * @return size of dates list
     */
    @Override
    public int getCount() {
        Log.v("CustomMealsAdapter", "count is"+mDates.size());
        return mDates.size();
    }

    /**
     * Basic getItem method inherited from BaseAdaptor
     * @param position
     * @return position
     */
    @Override
    public Object getItem(int position){
        return position;
    }

    /**
     * Basic getItemId method inherited from BaseAdapter
     * @param position
     * @return
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Class to hold the data for each entry's view
     */
    public class Holder
    {
        TextView dayDateLine;
        TextView mealsLine;
    }

    /**
     * Expands the layout for each item in the list and links the list item to a DietExpandableList fragment
     * @param position
     * @param convertView
     * @param parent
     * @return rowView
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Log.v("CustomMealsAdapter", "getView method started");
        Holder holder = new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.diet_list_item, null);
        holder.dayDateLine=(TextView) rowView.findViewById(R.id.dietDayDate);
        holder.mealsLine=(TextView) rowView.findViewById(R.id.mealsEaten);

        if (position < mDates.size()) {
            Date date = mDates.get(position);
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            String dateStr = df.format(date);
            DaySummary daySummary = dietLog.get(date);
            if (!daySummary.breakfastEntries.isEmpty() && !mMeals.contains("breakfast")){
                mMeals.add("breakfast");
            }
            if (!daySummary.lunchEntries.isEmpty() && !mMeals.contains("lunch")){
                mMeals.add("lunch");
            }
            if (!daySummary.dinnerEntries.isEmpty() && !mMeals.contains("dinner")){
                mMeals.add("dinner");
            }
            if (!daySummary.snackEntries.isEmpty() && !mMeals.contains("snack")){
                mMeals.add("snack");
            }
            if (!mMeals.isEmpty()){
                holder.dayDateLine.setText(dateStr);
                Log.v("CustomMealsAdapter", "Date added.");
                Log.v("CustomMealsAdapter", "Size of meals is "+mMeals.size());
                String meals = "";
                for (String meal: mMeals){
                    meals = meals.concat(meal + ", ");
                }
                int endL = meals.lastIndexOf(",");
                meals = meals.substring(0, endL);
                holder.mealsLine.setText(meals);
                Log.v("CustomMealsAdapter", "Meals added.");
            }
            else
                Log.v("CustomMealsAdapter", "Empty meals list");
        }
        else{
            Log.v("CustomMealsAdapter", "Empty dates list");
        }

        rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = mContext.getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, (new FragmentDietExpandableList().newInstance(dietLog.get(mDates.get(position)))));
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        return rowView;

    }
}
