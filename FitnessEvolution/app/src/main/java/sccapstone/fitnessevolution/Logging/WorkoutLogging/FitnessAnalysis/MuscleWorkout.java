package sccapstone.fitnessevolution.Logging.WorkoutLogging.FitnessAnalysis;

import android.util.Log;


/*====================================================================

    MUSCLE WORKOUT

    Class that contains information regarding how much
    exercise a muscle has recieved.

====================================================================*/

public class MuscleWorkout
{
    public String muscleName = "";

    public int setsAveraged = 1;
    public int totalSets = 0;
    public int totalReps = 0;
    public int totalWeightLifted = 0;

    public int averageSets = 0;
    public int averageReps = 0;

    /*--------------------------------------------------------------------

        AVERAGE WITH WORKOUT

        Purpose:
        Averages two MuscleWorkouts together.

        Arguments:
        A MuscleWorkout to merge.

    --------------------------------------------------------------------*/

    public void averageWithWorkout(MuscleWorkout other)
    {
        Log.v("MERGE WORKOUT", "Start of Method averageWithWorkout");

        if ( muscleName.equals(other.muscleName) )
        {

            Log.v("MERGE WORKOUT", "Merging the muscle workouts for " + muscleName + ":"
                                   + "\nAverage Sets of A: " + averageSets
                                   + "\nAverage Reps of A: " + averageReps
                                   + "\nTotal Sets of A: " + totalSets
                                   + "\nTotal Reps of A: " + totalReps
                                   + "\nAverage Sets of B: " + other.averageSets
                                   + "\nAverage Reps of B: " + other.averageReps
                                   + "\nTotal Sets of B: " + other.totalSets
                                   + "\nTotal Reps of B: " + other.totalReps
                                   );

            int updatedTotalReps = this.totalReps + other.totalReps;
            int updatedTotalSets = this.totalSets + other.totalSets;
            int updatedSetsAveraged = this.setsAveraged + other.setsAveraged;
            int updatedWeightLifted = this.totalWeightLifted + other.totalWeightLifted;

            /*--------------------------------------------------------------------
                Just making sure we don't collapse the universe in on itself
            --------------------------------------------------------------------*/

            int updatedAvgSets = updatedSetsAveraged == 0 ? 1 : updatedSetsAveraged;
            int updatedTotSets = updatedTotalSets == 0 ? 1 : updatedTotalSets;

            int updatedAverageSets = (this.totalSets + other.totalSets) / updatedAvgSets;
            int updatedAverageReps = (this.totalReps + other.totalReps) / updatedTotSets;

            this.totalSets = updatedTotalSets;
            this.totalReps = updatedTotalReps;
            this.averageSets = updatedAverageSets;
            this.averageReps = updatedAverageReps;
            this.setsAveraged = updatedSetsAveraged;
            this.totalWeightLifted = updatedWeightLifted;

            Log.v("MERGE WORKOUT", "Results of Merge:"
                                   + "\nTotal Weight Lifted: " + updatedWeightLifted
                                   + "\nTotal Sets: " + updatedTotalSets
                                   + "\nAverage Sets: " + updatedAverageSets
                                   + "\nTotal Reps: " + updatedTotalReps
                                   + "\nAverage Reps per Set: " + updatedAverageReps);
        } // if
        else
        {
            Log.v("MERGE WORKOUT", "Error: Cannot merge muscle workouts of two different muscles.");
        } // else

        Log.v("MERGE WORKOUT", "End of Method averageWithWorkout.");

    } // function


} // subclass