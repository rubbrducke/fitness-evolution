package sccapstone.fitnessevolution.Logging.WorkoutLogging;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.Logging.FragmentWorkoutDietaryLogging;
import sccapstone.fitnessevolution.R;
import sccapstone.fitnessevolution.Utilities.TimePickerFragment;


/*====================================================================

    FRAGMENT EDIT WORKOUT
    Created by Preston

====================================================================*/

public class FragmentEditWorkout extends android.support.v4.app.Fragment implements View.OnClickListener
{

    private OnFragmentInteractionListener mListener;
    private String mWorkoutObjectID;

    private ArrayList<LinearLayout> mExerciseSetsRepsHolders = new ArrayList<>();
    private ArrayList<LinearLayout> mExerciseDistanceDurationHolders = new ArrayList<>();
    private ArrayList<LinearLayout> mExerciseMuscGroupsHolders = new ArrayList<>();

    private LayoutInflater mInflater;
    private LinearLayout mExerciseHolderLayout;

    private ParseObject mParseWorkoutRecord;

    private ArrayList<Map.Entry<String, AutoCompleteTextView>> mExerciseNameAutoCompTextViewMapList = new ArrayList<>();
    private ArrayList<Map.Entry<String, EditText>> mExerciseRepsEditTextMapList = new ArrayList<>();
    private ArrayList<Map.Entry<String, EditText>> mExerciseWeightsEditTextMapList = new ArrayList<>();
    private ArrayList<Map.Entry<String, Button>> mDeleteSetButtonMapList = new ArrayList<>();
    private ArrayList<Map.Entry<String, View>> mExerciseSetRepsViewMapList = new ArrayList<>();
    private ArrayList<Map.Entry<String, Button>> mExerciseDeleteButtonMapList = new ArrayList<>();
    private ArrayList<Map.Entry<String, View>> mExerciseMuscGroupsViewMapList = new ArrayList<>();
    private ArrayList<Map.Entry<String, Button>> mDeleteMuscGroupButtonMapList = new ArrayList<>();
    private ArrayList<Map.Entry<String, Spinner>> mExerciseMuscGroupSpinnerMapList = new ArrayList<>();
    private ArrayList<Map.Entry<String, EditText>> mExerciseDistanceEditTextMapList = new ArrayList<>();
    private ArrayList<Map.Entry<String, EditText>> mExerciseDurationEditTextMapList = new ArrayList<>();
    private ArrayList<Map.Entry<String, Button>> mDeleteDistanceDurationButtonMapList = new ArrayList<>();
    private ArrayList<Map.Entry<String, View>> mExerciseDistanceDurationViewMapList = new ArrayList<>();
    private ArrayList<Map.Entry<String, TextView>> mExerciseSetsLabelMapList = new ArrayList<>();

    private String mWorkoutDate = "";
    private String mWorkoutDay = "";

    private TextView mTxtViewWorkoutStartTime;
    private TextView mTxtViewWorkoutFinishTime;
    private TextView mTxtViewWorkoutElapsedTime;

    public FragmentEditWorkout() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
        }
    }


    /*--------------------------------------------------------------------

        ON CREATE VIEW

        Purpose:
        This is where the magic happens.

    --------------------------------------------------------------------*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mInflater = inflater;
        final View rootView = inflater.inflate(R.layout.workout_fragment_edit, container, false);

        Utilities.hideKeyboardOnClickOfMainFragmentLayout(rootView.findViewById(R.id.editWorkoutMainLayout), this.getActivity());

        if (getArguments() != null) {
            mWorkoutObjectID = getArguments().getString("WORKOUT_OBJ_ID");
            mWorkoutDate = getArguments().getString("CALENDAR_DATE");
            mWorkoutDay = getArguments().getString("CALENDAR_DAY");
        }

        rootView.findViewById(R.id.btnSaveWorkout).setOnClickListener(this);
        rootView.findViewById(R.id.btnNewExercise).setOnClickListener(this);
        rootView.findViewById(R.id.btnDeleteWorkout).setOnClickListener(this);
        mTxtViewWorkoutStartTime= (TextView) rootView.findViewById(R.id.startTimeField);
        mTxtViewWorkoutFinishTime= (TextView) rootView.findViewById(R.id.finishTimeField);
        mTxtViewWorkoutFinishTime.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                /**Update the elapsed time field**/
                // Get start and finish times
                String startTime = Utilities.convertTo24Hour(mTxtViewWorkoutStartTime.getText().toString());
                String finishTime = Utilities.convertTo24Hour(mTxtViewWorkoutFinishTime.getText().toString());

                // Elapsed Time
                long elapsedTime = 0; // TO BE IN SECONDS
                if (mTxtViewWorkoutStartTime.getText().toString() != null && mTxtViewWorkoutFinishTime.getText().toString() != null) {
                    SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                    try {
                        Date startDate = format.parse(startTime);
                        Date finishDate = format.parse(finishTime);
                        Log.v("PRBTEST","startDate: " + startDate.toString() + " finishDate: " + finishDate);
                        long difference = (finishDate.getTime() - startDate.getTime()) / 1000;
                        if (difference < 0) {
                            Toast.makeText(getContext(), "Please select a finish time that precedes the start time!", Toast.LENGTH_LONG).show();
                            return;
                        }
                        elapsedTime = difference;
                    } catch (Exception e) {

                    }
                }
                long diffMinutes = (elapsedTime/ 60);
                String strElapsedTime = "Duration: " + diffMinutes + " min";
                mTxtViewWorkoutElapsedTime.setText(strElapsedTime);
            }
        });

        mTxtViewWorkoutElapsedTime= (TextView) rootView.findViewById(R.id.timeElapsedField);
        mExerciseHolderLayout = (LinearLayout) rootView.findViewById(R.id.exercise_layout_holder);

        Button pickStartTime = (Button) rootView.findViewById(R.id.btnPickStartTime);
        pickStartTime.setOnClickListener(showTimePickerDialog);
        Button pickFinishTime = (Button) rootView.findViewById(R.id.btnPickFinishTime);
        pickFinishTime.setOnClickListener(showTimePickerDialog);

        populateFields(rootView, inflater);

        return rootView;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnNewExercise:
                addNewExercise();
                break;
            case R.id.btnSaveWorkout:
                saveWorkout();
                break;
            case R.id.btnDeleteWorkout:
                deleteWorkout();
                break;
        }
    }

    /*
        Populates the fields in the fragment with data from PARSE
     */
    private void populateFields(final View rootView, final LayoutInflater inflater)
    {


        /*--------------------------------------------------------------------
            CASE 1:
            No workout object ID given, thus this is a new workout being created
        --------------------------------------------------------------------*/

        if (mWorkoutObjectID.equals("")) {
            TextView dateFieldTxtView = (TextView) rootView.findViewById(R.id.dateField);
            // Trim leading zeroes on date, if needed
            dateFieldTxtView.setText(mWorkoutDay + ", " + Utilities.trimLeadingZeroesOnDate(mWorkoutDate));

            addNewExercise();

            return;
        }

        /*--------------------------------------------------------------------
            CASE 2:
            Workout object ID given, so retrieve the info from Parse
        --------------------------------------------------------------------*/

        ParseQuery<ParseObject> workoutQuery = new ParseQuery<ParseObject>("WorkoutLogs");
        workoutQuery.whereEqualTo("objectId", mWorkoutObjectID);

        Utilities.showLoadingScreen(getContext());
        workoutQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                mParseWorkoutRecord = object;

                if (object == null) {
                    Utilities.hideLoadingScreen();
                    Toast.makeText(getContext(),"Unable to retrieve workout :(", Toast.LENGTH_LONG).show();
                    Log.v("PRBTEST", "Error retrieving workout " + e.getMessage());
                } else {
                    //Workout currentWorkout = new Workout();
                    mWorkoutDay = object.getString("workoutDay");
                    mWorkoutDate = object.getString("workoutDate");

                    // START AND FINISH TIME
                    mTxtViewWorkoutStartTime.setText(Utilities.convertTo12Hour(object.getString("workoutStartTime")));
                    mTxtViewWorkoutFinishTime.setText(Utilities.convertTo12Hour(object.getString("workoutEndTime")));

                    // ELAPSED TIME
                    long elapsedTime = object.getLong("workoutElapsedTime");
                    long diffMinutes = (elapsedTime/ 60);
                    String strElapsedTime = "Duration: " + diffMinutes + " min";
                    mTxtViewWorkoutElapsedTime.setText(strElapsedTime);

                    // DAY AND DATE FIELD
                    TextView dateFieldTxtView = (TextView) rootView.findViewById(R.id.dateField);
                    dateFieldTxtView.setText(mWorkoutDay + ", " + Utilities.trimLeadingZeroesOnDate(mWorkoutDate));

                    // EXERCISE INFO
                    View exerciseSingleView;

                    ArrayList<String> exerciseNamesList = (ArrayList<String>) object.get("exerciseNames");
                    ArrayList<Integer> exerciseSetsList = (ArrayList<Integer>) object.get("exerciseSets");
                    ArrayList<Integer> exerciseRepsList = (ArrayList<Integer>) object.get("exerciseReps");
                    ArrayList<Integer> exerciseWeightsList = (ArrayList<Integer>) object.get("exerciseWeights");
                    ArrayList<Integer> exerciseDistanceDurationCountList = (ArrayList<Integer>) object.get("exerciseDistanceDurationCount");
                    ArrayList<String> exerciseDistanceList = (ArrayList<String>) object.get("exerciseDistances");
                    ArrayList<String> exerciseDurationList = (ArrayList<String>) object.get("exerciseDurations");
                    ArrayList<String> exerciseMuscGroup = (ArrayList<String>) object.get("exerciseMuscGroup");

                    ArrayList<Integer> exerciseMuscGroupCountList = (ArrayList<Integer>) object.get("exerciseMuscGroupCount");
                    String exerciseName = "";

                    int muscleGroupCounter = 0;
                    if (exerciseNamesList != null) {
                        for (int i = 0; i < exerciseNamesList.size(); i++) {
                            // EXERCISE NAME
                            exerciseSingleView = inflater.inflate(R.layout.workout_single, mExerciseHolderLayout, false);
                            Log.v("PRBTEST","Adding a linear layout with tag "+ i);
                            exerciseName = exerciseNamesList.get(i);
                            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                                    R.array.all_exercises_array, android.R.layout.simple_dropdown_item_1line);
                            AutoCompleteTextView exerciseNameField = (AutoCompleteTextView) exerciseSingleView.findViewById(R.id.exerciseName);
                            exerciseNameField.setThreshold(1); // Number of chars the user has to type before autocomplete dropdown is shown
                            exerciseNameField.setAdapter(adapter);
                            exerciseNameField.setText(exerciseName);
                            Map.Entry<String, AutoCompleteTextView> nameMapEntry = new AbstractMap.SimpleEntry<String, AutoCompleteTextView>(create4DigitKey(i), exerciseNameField);
                            mExerciseNameAutoCompTextViewMapList.add(nameMapEntry);

                            // DELETE EXERCISE BUTTON
                            Button btnDeleteExercise = (Button) exerciseSingleView.findViewById(R.id.btnDeleteExercise);
                            btnDeleteExercise.setTag(R.string.EXERCISE_INDEX_KEY, create4DigitKey(i));
                            btnDeleteExercise.setOnClickListener(deleteExercise);
                            Map.Entry<String, Button> btnDeleteMapEntry = new AbstractMap.SimpleEntry<String, Button>(create4DigitKey(i), btnDeleteExercise);
                            mExerciseDeleteButtonMapList.add(btnDeleteMapEntry);

                            // MUSCLE GROUPS
                            LinearLayout exerciseMuscGroupsHolder = (LinearLayout) exerciseSingleView.findViewById(R.id.exercise_muscle_group_holder);
                            mExerciseMuscGroupsHolders.add(exerciseMuscGroupsHolder);

                            if (i < exerciseMuscGroupCountList.size()) {
                                exerciseMuscGroupsHolder.setTag(R.string.EXERCISE_INDEX_KEY, i);
                                View exerciseMuscGroupsView;

                                for (int j = 0; j < exerciseMuscGroupCountList.get(i); j++) {
                                    final String SET_KEY = create4DigitKey(i) + create4DigitKey(j);

                                    exerciseMuscGroupsView = inflater.inflate(R.layout.workout_muscle_groups, exerciseMuscGroupsHolder, false);
                                    Map.Entry<String, View> exerciseMuscGroupsViewMapsEntry = new AbstractMap.SimpleEntry<String, View>(SET_KEY, exerciseMuscGroupsView);
                                    mExerciseMuscGroupsViewMapList.add(exerciseMuscGroupsViewMapsEntry);

                                    // MUSCLE GROUPS
                                    Spinner muscGroupSpinner = (Spinner) exerciseMuscGroupsView.findViewById(R.id.spinnerMuscGroup);
                                    ArrayAdapter<CharSequence> spin_adapter = ArrayAdapter.createFromResource(getContext(), R.array.musc_groups_array, android.R.layout.simple_spinner_item);
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    muscGroupSpinner.setAdapter(spin_adapter);

                                    int spinnerIndex = Arrays.asList((getResources().getStringArray(R.array.musc_groups_array))).indexOf(exerciseMuscGroup.get(muscleGroupCounter));
                                    Log.v("PRBTEST","Spinner index: " + spinnerIndex);
                                    muscleGroupCounter++;
                                    muscGroupSpinner.setSelection(spinnerIndex);
                                    Map.Entry<String, Spinner> muscGroupMapEntry = new AbstractMap.SimpleEntry<String, Spinner>(SET_KEY, muscGroupSpinner);
                                    mExerciseMuscGroupSpinnerMapList.add(muscGroupMapEntry);

                                    Button btnDeleteMuscGroup = (Button) exerciseMuscGroupsView.findViewById(R.id.btnDeleteMuscGroup);
                                    btnDeleteMuscGroup.setOnClickListener(deleteMuscGroup);

                                    btnDeleteMuscGroup.setTag(R.string.EXERCISE_SET_INDEX_KEY, SET_KEY);
                                    Map.Entry<String, Button> btnDeleteMapsEntry = new AbstractMap.SimpleEntry<String, Button>(SET_KEY, btnDeleteMuscGroup);
                                    mDeleteMuscGroupButtonMapList.add(btnDeleteMapsEntry);

                                    exerciseMuscGroupsHolder.addView(exerciseMuscGroupsView);
                                }
                            }

                            // SETS AND REPS
                            LinearLayout exerciseSetRepsHolder = (LinearLayout) exerciseSingleView.findViewById(R.id.exercise_set_reps_holder);
                            mExerciseSetsRepsHolders.add(exerciseSetRepsHolder);

                            if (i < exerciseSetsList.size()) {
                                exerciseSetRepsHolder.setTag(R.string.EXERCISE_INDEX_KEY, i);
                                View exerciseSetRepsView;

                                for (int j = 0; j < exerciseSetsList.get(i); j++) {
                                    final String SET_KEY = create4DigitKey(i) + create4DigitKey(j);
                                    int currentReps = exerciseRepsList.get(j);
                                    exerciseSetRepsView = inflater.inflate(R.layout.workout_set_weight_reps, exerciseSetRepsHolder, false);
                                    Map.Entry<String, View> exerciseSetRepsViewMapsEntry = new AbstractMap.SimpleEntry<String, View>(SET_KEY, exerciseSetRepsView);
                                    mExerciseSetRepsViewMapList.add(exerciseSetRepsViewMapsEntry);

                                    // REPS
                                    EditText exerciseRepsField = (EditText) exerciseSetRepsView.findViewById(R.id.reps);
                                    exerciseRepsField.setText(currentReps + ""); // forcing to string here
                                    Map.Entry<String, EditText> repsMapEntry = new AbstractMap.SimpleEntry<String, EditText>(SET_KEY, exerciseRepsField);
                                    mExerciseRepsEditTextMapList.add(repsMapEntry);

                                    // SETS LABEL
                                    TextView txtVwSets = (TextView) exerciseSetRepsView.findViewById(R.id.setsLabel);
                                    txtVwSets.setText("Set " + (j + 1));
                                    Map.Entry<String, TextView> txtVwSetsMapEntry = new AbstractMap.SimpleEntry<String, TextView>(SET_KEY, txtVwSets);
                                    mExerciseSetsLabelMapList.add(txtVwSetsMapEntry);

                                    // WEIGHTS
                                    EditText exerciseWeightsField = (EditText) exerciseSetRepsView.findViewById(R.id.weightField);
                                    if (!exerciseWeightsList.get(j).toString().equals("-1")) { // -1 means the user chose to not enter a weight
                                        exerciseWeightsField.setText(exerciseWeightsList.get(j) + ""); // forcing to string here
                                    }
                                    Map.Entry<String, EditText> weightsMapEntry = new AbstractMap.SimpleEntry<String, EditText>(SET_KEY, exerciseWeightsField);
                                    mExerciseWeightsEditTextMapList.add(weightsMapEntry);

                                    Button btnDeleteSet = (Button) exerciseSetRepsView.findViewById(R.id.btnDeleteSet);
                                    btnDeleteSet.setOnClickListener(deleteSet);

                                    btnDeleteSet.setTag(R.string.EXERCISE_SET_INDEX_KEY, SET_KEY);
                                    Map.Entry<String, Button> btnDeleteMapsEntry = new AbstractMap.SimpleEntry<String, Button>(SET_KEY, btnDeleteSet);
                                    mDeleteSetButtonMapList.add(btnDeleteMapsEntry);

                                    exerciseSetRepsHolder.addView(exerciseSetRepsView);
                                }
                            }

                            // DISTANCE/DURATION
                            LinearLayout exerciseDistanceDurationHolder = (LinearLayout) exerciseSingleView.findViewById(R.id.exercise_distance_duration_holder);
                            mExerciseDistanceDurationHolders.add(exerciseDistanceDurationHolder);

                            if (i < exerciseDistanceDurationCountList.size()) {
                                exerciseDistanceDurationHolder.setTag(R.string.EXERCISE_INDEX_KEY, i);
                                View exerciseDistanceDurationView;

                                for (int j = 0; j < exerciseDistanceDurationCountList.get(i); j++) {
                                    final String SET_KEY = create4DigitKey(i) + create4DigitKey(j);
                                    exerciseDistanceDurationView = inflater.inflate(R.layout.workout_distance_duration, exerciseDistanceDurationHolder, false);
                                    Map.Entry<String, View> exerciseDistanceDurationViewMapsEntry = new AbstractMap.SimpleEntry<String, View>(SET_KEY, exerciseDistanceDurationView);
                                    mExerciseDistanceDurationViewMapList.add(exerciseDistanceDurationViewMapsEntry);

                                    // DISTANCE
                                    EditText exerciseDistanceField = (EditText) exerciseDistanceDurationView.findViewById(R.id.distanceField);
                                    exerciseDistanceField.setText(exerciseDistanceList.get(j)); // forcing to string here
                                    Map.Entry<String, EditText> distanceMapEntry = new AbstractMap.SimpleEntry<String, EditText>(create4DigitKey(i) + create4DigitKey(j), exerciseDistanceField);
                                    mExerciseDistanceEditTextMapList.add(distanceMapEntry);

                                    // DURATION
                                    EditText exerciseDurationField = (EditText) exerciseDistanceDurationView.findViewById(R.id.durationField);
                                    exerciseDurationField.setText(exerciseDurationList.get(j) + ""); // forcing to string here
                                    Map.Entry<String, EditText> durationMapEntry = new AbstractMap.SimpleEntry<String, EditText>(create4DigitKey(i) + create4DigitKey(j), exerciseDurationField);
                                    mExerciseDurationEditTextMapList.add(durationMapEntry);

                                    Button btnDeleteDistanceDuration = (Button) exerciseDistanceDurationView.findViewById(R.id.btnDeleteDistanceDuration);
                                    btnDeleteDistanceDuration.setOnClickListener(deleteDistanceDuration);

                                    btnDeleteDistanceDuration.setTag(R.string.EXERCISE_SET_INDEX_KEY, SET_KEY);
                                    Map.Entry<String, Button> btnDeleteMapsEntry = new AbstractMap.SimpleEntry<String, Button>(SET_KEY, btnDeleteDistanceDuration);
                                    mDeleteDistanceDurationButtonMapList.add(btnDeleteMapsEntry);

                                    exerciseDistanceDurationHolder.addView(exerciseDistanceDurationView);
                                }
                            }

                            Button btnAddMuscleGroup = (Button) exerciseSingleView.findViewById(R.id.btnAddMuscleGroup);
                            btnAddMuscleGroup.setTag(R.string.EXERCISE_INDEX_KEY, i);
                            btnAddMuscleGroup.setOnClickListener(addMuscleGroup);

                            Button btnAddSet = (Button) exerciseSingleView.findViewById(R.id.btnAddSet);
                            btnAddSet.setTag(R.string.EXERCISE_INDEX_KEY, i);
                            btnAddSet.setOnClickListener(addSet);

                            Button btnAddDistanceDuration = (Button) exerciseSingleView.findViewById(R.id.btnAddDistanceOrDuration);
                            btnAddDistanceDuration.setTag(R.string.EXERCISE_INDEX_KEY, i);
                            btnAddDistanceDuration.setOnClickListener(addDistanceDuration);

                            mExerciseHolderLayout.addView(exerciseSingleView);
                            Utilities.hideLoadingScreen();
                        }
                    }
                }
            }
        });
    }

    /*--------------------------------------------------------------------

        DELETE WORKOUT

    --------------------------------------------------------------------*/

    private void deleteWorkout()
    {
        if (mParseWorkoutRecord != null) {
            // Delete the workout
            mParseWorkoutRecord.deleteInBackground();
        }

        // Launch the workout dietary logging fragment
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, (new FragmentWorkoutDietaryLogging().newInstance(0)));
        ft.addToBackStack(null);
        ft.commit();
    }

    /*--------------------------------------------------------------------

        SAVE WORKOUT

        Gathers all the data in the fragment, and sends it to another method to be uploaded to Parse.

        Note: keys are of the form:
                0001 for everything except reps, weights, and sets
                000120000 for reps, weights, sets. First 4 digits are the exercise index, last 4 are the set index

    --------------------------------------------------------------------*/

    private void saveWorkout() {
        ArrayList<String> exerciseNames = new ArrayList<>();
        ArrayList<Integer> exerciseWeights = new ArrayList<>();
        ArrayList<Integer> exerciseReps = new ArrayList<>();
        ArrayList<Integer> exerciseSets = new ArrayList<>();
        ArrayList<String> exerciseDistance= new ArrayList<>();
        ArrayList<String> exerciseDuration = new ArrayList<>();
        ArrayList<Integer> exerciseDistanceDurationCount = new ArrayList<>();
        ArrayList<Integer> exerciseMuscGroupCount = new ArrayList<>();
        ArrayList<String> exerciseMuscGroups = new ArrayList<>();

        // TODO: this is inefficient. A for loop with multiple for loops inside of it... :( But it works.
        for (int exerciseIndex = 0; exerciseIndex < mExerciseNameAutoCompTextViewMapList.size(); exerciseIndex++) {
            // EXERCISE NAMES
            Log.v("PRBTEST", "mExerciseNameAutoCompTextViewMapList.get(exerciseIndex).getKey().substring(0,4): " + mExerciseNameAutoCompTextViewMapList.get(exerciseIndex).getKey().substring(0,4) + " exerciseIndex: " + exerciseIndex);
            if (Integer.parseInt(mExerciseNameAutoCompTextViewMapList.get(exerciseIndex).getKey().substring(0,4)) == exerciseIndex) {
                Log.v("PRBTEST", "Exercise name: " + mExerciseNameAutoCompTextViewMapList.get(exerciseIndex).getValue().getText());

                String nameText = mExerciseNameAutoCompTextViewMapList.get(exerciseIndex).getValue().getText().toString();
                if (nameText.isEmpty()) {
                    Toast.makeText(getContext(), "One of your exercises is missing a name!", Toast.LENGTH_SHORT).show();
                    return;
                }
                exerciseNames.add(nameText);
            }

            // WEIGHTS

            for (int x = 0; x < mExerciseWeightsEditTextMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseWeightsEditTextMapList.get(x).getKey().substring(0,4): " + mExerciseWeightsEditTextMapList.get(x).getKey().substring(0,4) + " exerciseIndex: " + exerciseIndex);
                if (Integer.parseInt(mExerciseWeightsEditTextMapList.get(x).getKey().substring(0,4)) == exerciseIndex) {
                    Log.v("PRBTEST", "Weight: " + mExerciseWeightsEditTextMapList.get(x).getValue().getText());

                    try {
                        String strWeight = mExerciseWeightsEditTextMapList.get(x).getValue().getText().toString();
                        // Validation
                        if (strWeight.equals("")) {
                            exerciseWeights.add(-1);
                        } else {
                            int intWeight = Integer.parseInt(strWeight);
                            if (intWeight > 0) {
                                exerciseWeights.add(intWeight); // VALID
                            }
                            else {
                                Toast.makeText(getContext(), "One of your exercises has a negative weight value!", Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                    } catch (Exception e) {
                        Toast.makeText(getContext(), "One of your exercises has an invalid weight value!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            }

            // REPS AND SETS
            // Note: simply count the number of Reps recordings (Ex) 15,14,12 would be 3 reps recordings.
            //       This is equivalent to the number of sets.
            int currentSets = 0;
            for (int x = 0; x < mExerciseRepsEditTextMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseRepsEditTextMapList.get(x).getKey().substring(0,4): " + mExerciseRepsEditTextMapList.get(x).getKey().substring(0,4) + " exerciseIndex: " + exerciseIndex);
                if (Integer.parseInt(mExerciseRepsEditTextMapList.get(x).getKey().substring(0,4)) == exerciseIndex) {
                        currentSets++;
                        Log.v("PRBTEST", "Reps: " + mExerciseRepsEditTextMapList.get(x).getValue().getText());

                    try {
                        String strReps = mExerciseRepsEditTextMapList.get(x).getValue().getText().toString();
                        // Validation
                        if (strReps.equals("")) {
                            Toast.makeText(getContext(), "One of your exercises is missing repetition values!", Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            int intReps = Integer.parseInt(strReps);
                            if (intReps > 0) {
                                exerciseReps.add(intReps); // VALID
                            }
                            else {
                                Toast.makeText(getContext(), "One of your exercises has a negative repetition value!", Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                    } catch (Exception e) {
                        Toast.makeText(getContext(), "One of your exercises has an invalid repetition value!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            }
            exerciseSets.add(currentSets);

            // DISTANCE
            int currentDistanceDurationCount = 0;
            for (int x = 0; x < mExerciseDistanceEditTextMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseDistanceEditTextMapList.get(x).getKey().substring(0,4): " + mExerciseDistanceEditTextMapList.get(x).getKey().substring(0,4) + " exerciseIndex: " + exerciseIndex);
                if (Integer.parseInt(mExerciseDistanceEditTextMapList.get(x).getKey().substring(0,4)) == exerciseIndex) {
                    currentDistanceDurationCount++;
                    Log.v("PRBTEST", "Distance: " + mExerciseDistanceEditTextMapList.get(x).getValue().getText());

                    try {
                        double dblDistance = Double.parseDouble(mExerciseDistanceEditTextMapList.get(x).getValue().getText().toString());
                        String strDistance = String.valueOf(dblDistance);
                        // Validation
                        if (strDistance.equals("")) {
                            Toast.makeText(getContext(), "One of your exercises is missing distance values!", Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            //double dblDistance = Double.parseDouble(strDistance);
                            if (dblDistance > 0) {
                                exerciseDistance.add(strDistance); // VALID
                            }
                            else {
                                Toast.makeText(getContext(), "One of your exercises has a negative distance value!", Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                    } catch (Exception e) {
                        Toast.makeText(getContext(), "One of your exercises has an invalid distance value!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            }
            exerciseDistanceDurationCount.add(currentDistanceDurationCount);

            // DURATION
            for (int x = 0; x < mExerciseDurationEditTextMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseDurationEditTextMapList.get(x).getKey().substring(0,4): " + mExerciseDurationEditTextMapList.get(x).getKey().substring(0,4) + " exerciseIndex: " + exerciseIndex);
                if (Integer.parseInt(mExerciseDurationEditTextMapList.get(x).getKey().substring(0,4)) == exerciseIndex) {
                    Log.v("PRBTEST", "Duration: " + mExerciseDurationEditTextMapList.get(x).getValue().getText());

                    try {
                        double dblDuration = Double.parseDouble(mExerciseDurationEditTextMapList.get(x).getValue().getText().toString());
                        String strDuration = String.valueOf(dblDuration);
                        // Validation
                        if (strDuration.equals("")) {
                            Toast.makeText(getContext(), "One of your exercises is missing a duration value!", Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            //double dblDuration = Double.parseDouble(strDuration);
                            if (dblDuration > 0) {
                                exerciseDuration.add(strDuration); // VALID
                            }
                            else {
                                Toast.makeText(getContext(), "One of your exercises has a negative duration value!", Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                    } catch (Exception e) {
                        Toast.makeText(getContext(), "One of your exercises has an invalid duration value!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            }

            // MUSCLE GROUPS
            int currentMuscleGroupCount = 0;
            for (int x = 0; x < mExerciseMuscGroupSpinnerMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseMuscGroupSpinnerMapList.get(x).getKey().substring(0,4): " + mExerciseMuscGroupSpinnerMapList.get(x).getKey().substring(0,4) + " exerciseIndex: " + exerciseIndex);
                if (Integer.parseInt(mExerciseMuscGroupSpinnerMapList.get(x).getKey().substring(0,4)) == exerciseIndex) {
                    currentMuscleGroupCount++;
                    Log.v("PRBTEST", "Exercise Musc Group: " + mExerciseMuscGroupSpinnerMapList.get(x).getValue().getSelectedItem().toString());

                    try {
                        String strMuscGroup = mExerciseMuscGroupSpinnerMapList.get(x).getValue().getSelectedItem().toString();
                        // Validation0
                        if (strMuscGroup.equals("...")) {
                            Toast.makeText(getContext(), "One of your exercises is missing a muscle group value!", Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                                exerciseMuscGroups.add(strMuscGroup); // VALID
                        }
                    } catch (Exception e) {
                        Toast.makeText(getContext(), "One of your exercises has an invalid muscle group value!", Toast.LENGTH_LONG).show();
                        return;
                    }
                }
            }
            if (currentMuscleGroupCount == 0) {
                Toast.makeText(getContext(), "WORKOUT NOT SAVED! Make sure to add at least one muscle group to each exercise.", Toast.LENGTH_SHORT).show();
                return;
            }

            exerciseMuscGroupCount.add(currentMuscleGroupCount);

            // Get start and finish times
            String startTime = Utilities.convertTo24Hour(mTxtViewWorkoutStartTime.getText().toString());
            String finishTime = Utilities.convertTo24Hour(mTxtViewWorkoutFinishTime.getText().toString());

            // Elapsed Time
            long elapsedTime = -1; // TO BE IN SECONDS
            if (mTxtViewWorkoutStartTime.getText().toString() != null && mTxtViewWorkoutFinishTime.getText().toString() != null) {
                SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                try {
                    Date startDate = format.parse(startTime);
                    Date finishDate = format.parse(finishTime);

                    long difference = (finishDate.getTime() - startDate.getTime()) / 1000;
                    if (difference < 0) {
                        Toast.makeText(getContext(), "WORKOUT NOT SAVED! Workout finish time currently precedes the start time.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    elapsedTime = difference;
                } catch (Exception e) {

                }
            }

            uploadWorkoutToParse(startTime, finishTime, elapsedTime, exerciseNames, exerciseSets, exerciseWeights, exerciseReps, exerciseMuscGroupCount, exerciseMuscGroups, exerciseDistanceDurationCount, exerciseDistance, exerciseDuration);
        }
    }

    private View.OnClickListener showTimePickerDialog = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.btnPickStartTime) {
                DialogFragment newFragment = new TimePickerFragment();
                newFragment.show(getFragmentManager(), "workoutStartTime");
            }
            else if (v.getId() == R.id.btnPickFinishTime) {
                DialogFragment newFragment = new TimePickerFragment();
                newFragment.show(getFragmentManager(), "workoutFinishTime");
            }
        }
    };


    /*--------------------------------------------------------------------

        UPLOAD WORKOUT TO PARSE

    --------------------------------------------------------------------*/

    private void uploadWorkoutToParse(String startTime, String finishTime, long elapsedTime, ArrayList<String> exerciseNames, ArrayList<Integer> exerciseSets, ArrayList<Integer> exerciseWeights, ArrayList<Integer> exerciseReps, ArrayList<Integer> exerciseMuscGroupCount, ArrayList<String> exerciseMuscGroups, ArrayList<Integer> exerciseDistanceDurationCount, ArrayList<String> exerciseDistance, ArrayList<String> exerciseDuration)
    {
        if (mParseWorkoutRecord == null) {
            // Create a new record
            mParseWorkoutRecord = new ParseObject("WorkoutLogs");
        }

        // Add data to PARSE.COM record
        mParseWorkoutRecord.put("exerciseNames", exerciseNames);
        mParseWorkoutRecord.put("exerciseSets", exerciseSets); //TODO: uncomment
        mParseWorkoutRecord.put("exerciseWeights", exerciseWeights);
        mParseWorkoutRecord.put("exerciseReps", exerciseReps);
        mParseWorkoutRecord.put("workoutDate", mWorkoutDate);
        mParseWorkoutRecord.put("workoutDay", mWorkoutDay);
        mParseWorkoutRecord.put("userObjectID", ParseUser.getCurrentUser().getObjectId().toString());
        mParseWorkoutRecord.put("workoutStartTime", startTime);
        mParseWorkoutRecord.put("workoutEndTime", finishTime);
        mParseWorkoutRecord.put("workoutElapsedTime", elapsedTime); // In seconds
        mParseWorkoutRecord.put("exerciseMuscGroup", exerciseMuscGroups);
        mParseWorkoutRecord.put("exerciseMuscGroupCount", exerciseMuscGroupCount);
        mParseWorkoutRecord.put("exerciseDistanceDurationCount", exerciseDistanceDurationCount);
        mParseWorkoutRecord.put("exerciseDistances", exerciseDistance);
        mParseWorkoutRecord.put("exerciseDurations", exerciseDuration);

        mParseWorkoutRecord.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    //Success in saving
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), "Workout saved :D", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    //Failed saving
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), "Error saving workout :(", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }


    /*--------------------------------------------------------------------

        ADD NEW EXERCISE

    --------------------------------------------------------------------*/

    private void addNewExercise()
    {
        final int KEY = mExerciseNameAutoCompTextViewMapList.size();
        View exerciseSingleView = mInflater.inflate(R.layout.workout_single, mExerciseHolderLayout, false);

        //****** Add the name field, exercise delete button to the list of name maps, so that it can be referred to (i.e. for saving data)******//
        // NAME
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.all_exercises_array, android.R.layout.simple_dropdown_item_1line);
        AutoCompleteTextView exerciseNameField = (AutoCompleteTextView) exerciseSingleView.findViewById(R.id.exerciseName);
        exerciseNameField.setThreshold(3); // Number of chars the user has to type before autocomplete dropdown is shown
        exerciseNameField.setAdapter(adapter);
        Map.Entry<String, AutoCompleteTextView> nameMapEntry = new AbstractMap.SimpleEntry<String, AutoCompleteTextView>(create4DigitKey(KEY), exerciseNameField);
        mExerciseNameAutoCompTextViewMapList.add(nameMapEntry);

        // DELETE EXERCISE BUTTON
        Button btnDeleteExercise = (Button) exerciseSingleView.findViewById(R.id.btnDeleteExercise);
        btnDeleteExercise.setTag(R.string.EXERCISE_INDEX_KEY, create4DigitKey(KEY));
        btnDeleteExercise.setOnClickListener(deleteExercise);
        Map.Entry<String, Button> btnDeleteMapEntry = new AbstractMap.SimpleEntry<String, Button>(create4DigitKey(KEY), btnDeleteExercise);
        mExerciseDeleteButtonMapList.add(btnDeleteMapEntry);
       //***************************************************************************************************************//

        Button btnAddMuscleGroup = (Button) exerciseSingleView.findViewById(R.id.btnAddMuscleGroup);
        btnAddMuscleGroup.setTag(R.string.EXERCISE_INDEX_KEY, KEY);
        btnAddMuscleGroup.setOnClickListener(addMuscleGroup);

        Button btnAddSet = (Button) exerciseSingleView.findViewById(R.id.btnAddSet);
        btnAddSet.setTag(R.string.EXERCISE_INDEX_KEY, KEY);
        btnAddSet.setOnClickListener(addSet);

        Button btnAddDistanceDuration = (Button) exerciseSingleView.findViewById(R.id.btnAddDistanceOrDuration);
        btnAddDistanceDuration.setTag(R.string.EXERCISE_INDEX_KEY, KEY);
        btnAddDistanceDuration.setOnClickListener(addDistanceDuration);

        LinearLayout exerciseSetRepsHolder = (LinearLayout) exerciseSingleView.findViewById(R.id.exercise_set_reps_holder);
        exerciseSetRepsHolder.setTag(R.string.EXERCISE_INDEX_KEY, KEY);
        mExerciseSetsRepsHolders.add(exerciseSetRepsHolder);

        LinearLayout exerciseMuscGroupsHolder = (LinearLayout) exerciseSingleView.findViewById(R.id.exercise_muscle_group_holder);
        exerciseMuscGroupsHolder.setTag(R.string.EXERCISE_INDEX_KEY, KEY);
        Log.v("PRBTEST","Adding a linear layout with tag "+ KEY);
        mExerciseMuscGroupsHolders.add(exerciseMuscGroupsHolder);

        LinearLayout exerciseDistanceDurationHolder = (LinearLayout) exerciseSingleView.findViewById(R.id.exercise_distance_duration_holder);
        exerciseDistanceDurationHolder.setTag(R.string.EXERCISE_INDEX_KEY, KEY);
        mExerciseDistanceDurationHolders.add(exerciseDistanceDurationHolder);

        mExerciseHolderLayout.addView(exerciseSingleView);
    }

    private View.OnClickListener deleteExercise = new View.OnClickListener()
    {
        @Override
        public void onClick(View v) {
            String tag = v.getTag(R.string.EXERCISE_INDEX_KEY).toString();
            String trimmedTag = String.valueOf(Integer.parseInt(tag));
            Log.v("PRBTEST", "DELETING exercise with tag: " + tag);

            int holdersSize = mExerciseMuscGroupsHolders.size(); // Note, mExerciseMuscGroupsHolders mExerciseSetsRepsHolders
                                                                 // and mExerciseDistanceDurationHolders should all have same size

            for (int x = 0; x < holdersSize; x++) {
                boolean foundHolder = false;
                //Log.v("PRBTEST", "trimmedTag: '" + trimmedTag + "' mExerciseMuscGroupsHolders["+ x+"].getTag(): '" + mExerciseMuscGroupsHolders.get(x).getTag(R.string.EXERCISE_INDEX_KEY)+ "'");
                if (mExerciseMuscGroupsHolders.get(x).getTag(R.string.EXERCISE_INDEX_KEY).toString().equals(trimmedTag)) {
                    Log.v("PRBTEST", "Removing mExerciseMuscGroupsHolders["+ x+"].getTag():" + mExerciseMuscGroupsHolders.get(x).getTag(R.string.EXERCISE_INDEX_KEY));
                    mExerciseMuscGroupsHolders.remove(x);
                    foundHolder = true; // Applies to all 3 holders, since if this occurs, the other two will too
                }
                if (mExerciseSetsRepsHolders.get(x).getTag(R.string.EXERCISE_INDEX_KEY).toString().equals(trimmedTag)) {
                    Log.v("PRBTEST", "Removing mExerciseSetsRepsHolders["+ x+"].getTag():" + mExerciseSetsRepsHolders.get(x).getTag(R.string.EXERCISE_INDEX_KEY));
                    mExerciseSetsRepsHolders.remove(x);
                }
                if (mExerciseDistanceDurationHolders.get(x).getTag(R.string.EXERCISE_INDEX_KEY).toString().equals(trimmedTag)) {
                    Log.v("PRBTEST", "Removing mExerciseMuscGroupsHolders["+ x+"].getTag():" + mExerciseDistanceDurationHolders.get(x).getTag(R.string.EXERCISE_INDEX_KEY));
                    mExerciseDistanceDurationHolders.remove(x);
                }

                if (foundHolder) {
                    break;
                }
            }

            for (int x = 0; x < mExerciseNameAutoCompTextViewMapList.size(); x++) {
                if (mExerciseNameAutoCompTextViewMapList.get(x).getKey().equals(tag)) {
                    Log.v("PRBTEST", "IF == true");
                    View vwExerciseToRemove = mExerciseNameAutoCompTextViewMapList.get(x).getValue();
                    ViewGroup vwExerciseLayout = (ViewGroup) vwExerciseToRemove.getParent().getParent(); // Layout hierarchy is [(Want to remove this one) Linear layout[Linear layout[ExerciseName]]]
                    ((ViewGroup) vwExerciseLayout.getParent()).removeView(vwExerciseLayout);
                    mExerciseNameAutoCompTextViewMapList.remove(x);
                    break;
                }
            }

            // Remove corresponding entries in the data structures used to keep track of all elements on the form
            for (int x = 0; x < mExerciseDeleteButtonMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseDeleteButtonMapList.get(x).getKey().substring(0,4) " + mExerciseDeleteButtonMapList.get(x).getKey().substring(0, 4) + " tag: " + tag);
                if (mExerciseDeleteButtonMapList.get(x).getKey().substring(0, 4).equals(tag)) {
                    Log.v("PRBTEST","IF == true");
                    //View vwSetToRemove = mExerciseDeleteButtonMapList.get(x).getValue();
                    //((ViewGroup) vwSetToRemove.getParent()).removeAllViewsInLayout();
                    mExerciseDeleteButtonMapList.remove(x);
                    break;
                }
            }
            for (int x = 0; x < mExerciseSetRepsViewMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseSetRepsViewMapList.get(x).getKey().substring(0,4) " + mExerciseSetRepsViewMapList.get(x).getKey().substring(0, 4) + " tag: " + tag);
                if (mExerciseSetRepsViewMapList.get(x).getKey().substring(0, 4).equals(tag)) {
                    Log.v("PRBTEST","IF == true");
                    //View vwSetToRemove = mExerciseSetRepsViewMapList.get(x).getValue();
                    //((ViewGroup) vwSetToRemove.getParent()).removeView(vwSetToRemove);
                    mExerciseSetRepsViewMapList.remove(x);
                }
            }

            for (int x = 0; x < mExerciseRepsEditTextMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseRepsEditTextMapList.get(x).getKey().substring(0, 4) " + mExerciseRepsEditTextMapList.get(x).getKey().substring(0, 4) + " tag: " + tag);
                if (mExerciseRepsEditTextMapList.get(x).getKey().substring(0, 4).equals(tag)) {
                    Log.v("PRBTEST","IF == true");
                    mExerciseRepsEditTextMapList.remove(x);
                }
            }

            for (int x = 0; x < mExerciseWeightsEditTextMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseWeightsEditTextMapList.get(x).getKey().substring(0, 4) " +mExerciseWeightsEditTextMapList.get(x).getKey().substring(0, 4) + " tag: " + tag);
                if (mExerciseWeightsEditTextMapList.get(x).getKey().substring(0, 4).equals(tag)) {
                    Log.v("PRBTEST", "IF == true");
                    mExerciseWeightsEditTextMapList.remove(x);
                }
            }

            for (int x = 0; x < mDeleteSetButtonMapList.size(); x++) {
                Log.v("PRBTEST", "mDeleteSetButtonMapList.get(x).getKey().substring(0, 4) " + mDeleteSetButtonMapList.get(x).getKey().substring(0, 4) + " tag: " + tag);
                if (mDeleteSetButtonMapList.get(x).getKey().substring(0, 4).equals(tag)) {
                    Log.v("PRBTEST", "IF == true");
                    mDeleteSetButtonMapList.remove(x);
                }
            }

            for (int x = 0; x < mExerciseDistanceDurationViewMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseDistanceDurationViewMapList.get(x).getKey().substring(0,4) " + mExerciseDistanceDurationViewMapList.get(x).getKey().substring(0, 4) + " tag: " + tag);
                if (mExerciseDistanceDurationViewMapList.get(x).getKey().substring(0, 4).equals(tag)) {
                    Log.v("PRBTEST","IF == true");
                    //View vwSetToRemove = mExerciseSetRepsViewMapList.get(x).getValue();
                    //((ViewGroup) vwSetToRemove.getParent()).removeView(vwSetToRemove);
                    mExerciseDistanceDurationViewMapList.remove(x);
                }
            }

            for (int x = 0; x < mExerciseDistanceEditTextMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseDistanceEditTextMapList.get(x).getKey().substring(0, 4) " + mExerciseDistanceEditTextMapList.get(x).getKey().substring(0, 4) + " tag: " + tag);
                if (mExerciseDistanceEditTextMapList.get(x).getKey().substring(0, 4).equals(tag)) {
                    Log.v("PRBTEST","IF == true");
                    mExerciseDistanceEditTextMapList.remove(x);
                }
            }

            for (int x = 0; x < mExerciseDurationEditTextMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseDurationEditTextMapList.get(x).getKey().substring(0, 4) " +mExerciseDurationEditTextMapList.get(x).getKey().substring(0, 4) + " tag: " + tag);
                if (mExerciseDurationEditTextMapList.get(x).getKey().substring(0, 4).equals(tag)) {
                    Log.v("PRBTEST","IF == true");
                    mExerciseDurationEditTextMapList.remove(x);
                }
            }

            for (int x = 0; x < mDeleteDistanceDurationButtonMapList.size(); x++) {
                Log.v("PRBTEST", "mDeleteDistanceDurationButtonMapList.get(x).getKey().substring(0, 4) " + mDeleteDistanceDurationButtonMapList.get(x).getKey().substring(0, 4) + " tag: " + tag);
                if (mDeleteDistanceDurationButtonMapList.get(x).getKey().substring(0, 4).equals(tag)) {
                    Log.v("PRBTEST", "IF == true");
                    mDeleteDistanceDurationButtonMapList.remove(x);
                }
            }

            for (int x = 0; x < mExerciseMuscGroupsViewMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseMuscGroupsViewMapList.get(x).getKey().substring(0, 4) " + mExerciseMuscGroupsViewMapList.get(x).getKey().substring(0, 4) + " tag: " + tag);
                if (mExerciseMuscGroupsViewMapList.get(x).getKey().substring(0, 4).equals(tag)) {
                    Log.v("PRBTEST", "IF == true");
                    mExerciseMuscGroupsViewMapList.remove(x);
                }
            }

            for (int x = 0; x < mExerciseMuscGroupSpinnerMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseMuscGroupSpinnerMapList.get(x).getKey().substring(0, 4) " + mExerciseMuscGroupSpinnerMapList.get(x).getKey().substring(0, 4) + " tag: " + tag);
                if (mExerciseMuscGroupSpinnerMapList.get(x).getKey().substring(0, 4).equals(tag)) {
                    Log.v("PRBTEST","IF == true");
                    mExerciseMuscGroupSpinnerMapList.remove(x);
                }
            }

            for (int x = 0; x < mDeleteMuscGroupButtonMapList.size(); x++) {
                Log.v("PRBTEST", "mDeleteMuscGroupButtonMapList.get(x).getKey().substring(0, 4) " + mDeleteMuscGroupButtonMapList.get(x).getKey().substring(0, 4) + " tag: " + tag);
                if (mDeleteMuscGroupButtonMapList.get(x).getKey().substring(0, 4).equals(tag)) {
                    Log.v("PRBTEST", "IF == true");
                    mDeleteMuscGroupButtonMapList.remove(x);
                }
            }

            for (int x = 0; x < mExerciseSetsLabelMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseSetsLabelMapList.get(x).getKey().substring(0, 4) " + mExerciseSetsLabelMapList.get(x).getKey().substring(0, 4) + " tag: " + tag);
                if (mExerciseSetsLabelMapList.get(x).getKey().substring(0, 4).equals(tag)) {
                    Log.v("PRBTEST", "IF == true");
                    mExerciseSetsLabelMapList.remove(x);
                }
            }
        }
    };

    /*
        Deletes a set from the fragment and the meta data variables
     */
    private View.OnClickListener deleteSet = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.v("PRBTEST", "Delete set button CLICKED " + v.getTag(R.string.EXERCISE_SET_INDEX_KEY));
            Log.v("PRBTEST", "mExerciseSetRepsViewMapList.size() " + mExerciseSetRepsViewMapList.size());
            String tag = v.getTag(R.string.EXERCISE_SET_INDEX_KEY).toString();
            int intSetTag = Integer.parseInt(tag.substring(4,8));

            /**********************************************
             Note: keys are of the form:
             0001 for everything except reps, weights, and sets
             00010002 for reps, weights, sets. First 4 digits are the exercise index, last 4 are the set index
             Index: 012345678
             Hence, the magic numbers 4 and 8 in the substrings, below.
             **********************************************/

            // Remove corresponding entries in the data structures used to keep track of all elements on the form
            for (int x = 0; x < mExerciseSetRepsViewMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseSetRepsViewMapList.get(x).getKey().substring(4, 8) " + mExerciseSetRepsViewMapList.get(x).getKey().substring(4, 8) + " tag: " + tag);
                if (mExerciseSetRepsViewMapList.get(x).getKey().equals(tag)) {
                    Log.v("PRBTEST","IF == true");
                    View vwSetToRemove = mExerciseSetRepsViewMapList.get(x).getValue();
                    ((ViewGroup) vwSetToRemove.getParent()).removeView(vwSetToRemove);
                    mExerciseSetRepsViewMapList.remove(x);
                    break;
                }
            }

            for (int x = 0; x < mExerciseRepsEditTextMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseRepsEditTextMapList.get(x).getKey().substring(4, 8) " + mExerciseRepsEditTextMapList.get(x).getKey().substring(4, 8) + " tag: " + tag);
                if (mExerciseRepsEditTextMapList.get(x).getKey().equals(tag)) {
                    Log.v("PRBTEST", "IF == true");
                    mExerciseRepsEditTextMapList.remove(x);
                    break;
                }
            }

            for (int x = 0; x < mExerciseWeightsEditTextMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseWeightsEditTextMapList.get(x).getKey().substring(4, 8) " +mExerciseWeightsEditTextMapList.get(x).getKey().substring(4, 8) + " tag: " + tag);
                if (mExerciseWeightsEditTextMapList.get(x).getKey().equals(tag)) {
                    Log.v("PRBTEST","IF == true");
                    mExerciseWeightsEditTextMapList.remove(x);
                    break;
                }
            }

            for (int x = 0; x < mDeleteSetButtonMapList.size(); x++) {
                Log.v("PRBTEST", "mDeleteSetButtonMapList.get(x).getKey().substring(4, 8) " + mDeleteSetButtonMapList.get(x).getKey().substring(4, 8) + " tag: " + tag);
                if (mDeleteSetButtonMapList.get(x).getKey().equals(tag)) {
                    Log.v("PRBTEST","IF == true");
                    mDeleteSetButtonMapList.remove(x);
                    break;
                }
            }

            for (int x = 0; x < mExerciseSetsLabelMapList.size(); x++) {
                if (mExerciseSetsLabelMapList.get(x).getKey().equals(tag)) {
                    mExerciseSetsLabelMapList.remove(x);
                    break;
                }
            }

            // Update the Set text view to reflect the new set count
            int tempSetCount = 1;
            String exerciseKey = tag.substring(0,4);
            Log.v("PRBTEST","mExerciseSetsLabelMapList.size(): " + mExerciseSetsLabelMapList.size());
            for (int x = 0; x < mExerciseSetsLabelMapList.size(); x++) {
                String currSetsTxtVwKey = mExerciseSetsLabelMapList.get(x).getKey();
                Log.v("PRBTEST", "currSetsTxtVwKey: " + currSetsTxtVwKey);
                if (currSetsTxtVwKey.substring(0,4).equals(exerciseKey)) {
                    Log.v("PRBTEST", "Setting text of setLabel to 'Set " + tempSetCount + "'");
                    mExerciseSetsLabelMapList.get(x).getValue().setText("Set " + tempSetCount);
                    mExerciseSetsLabelMapList.get(x).getValue().setText("");
                    TextView setLabel = mExerciseSetsLabelMapList.get(x).getValue();
                    setLabel.setText("Set " + tempSetCount);

                    tempSetCount++;
                }
            }
        }
    };

    /*
        Deletes a distance/duration recording from the fragment and the meta data variables
    */
    private View.OnClickListener deleteDistanceDuration = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.v("PRBTEST", "Delete distance duration button CLICKED " + v.getTag(R.string.EXERCISE_SET_INDEX_KEY));
            Log.v("PRBTEST","mExerciseDistanceDurationViewMapList.size() " + mExerciseDistanceDurationViewMapList.size());
            String tag = v.getTag(R.string.EXERCISE_SET_INDEX_KEY).toString();

            /**********************************************
             Note: keys are of the form:
             0001 for everything except reps, weights, and sets
             00010002 for reps, weights, sets. First 4 digits are the exercise index, last 4 are the set index
             Index: 012345678
             Hence, the magic numbers 4 and 8 in the substrings, below.
             **********************************************/

            // Remove corresponding entries in the data structures used to keep track of all elements on the form
            for (int x = 0; x < mExerciseDistanceDurationViewMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseSetRepsViewMapList.get(x).getKey() " + mExerciseDistanceDurationViewMapList.get(x).getKey() + " tag: " + tag);
                if (mExerciseDistanceDurationViewMapList.get(x).getKey().equals(tag)) {
                    Log.v("PRBTEST","IF == true");
                    View vwSetToRemove = mExerciseDistanceDurationViewMapList.get(x).getValue();
                    ((ViewGroup) vwSetToRemove.getParent()).removeView(vwSetToRemove);
                    mExerciseDistanceDurationViewMapList.remove(x);
                    break;
                }
            }

            for (int x = 0; x < mExerciseDistanceEditTextMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseDistanceEditTextMapList.get(x).getKey() " + mExerciseDistanceEditTextMapList.get(x).getKey() + " tag: " + tag);
                if (mExerciseDistanceEditTextMapList.get(x).getKey().equals(tag)) {
                    Log.v("PRBTEST","IF == true");
                    mExerciseDistanceEditTextMapList.remove(x);
                    break;
                }
            }

            for (int x = 0; x < mExerciseDurationEditTextMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseDurationEditTextMapList.get(x).getKey() " +mExerciseDurationEditTextMapList.get(x).getKey() + " tag: " + tag);
                if (mExerciseDurationEditTextMapList.get(x).getKey().equals(tag)) {
                    Log.v("PRBTEST","IF == true");
                    mExerciseDurationEditTextMapList.remove(x);
                    break;
                }
            }

            for (int x = 0; x < mDeleteDistanceDurationButtonMapList.size(); x++) {
                Log.v("PRBTEST", "mDeleteDistanceDurationButtonMapList.get(x).getKey() " + mDeleteDistanceDurationButtonMapList.get(x).getKey() + " tag: " + tag);
                if (mDeleteDistanceDurationButtonMapList.get(x).getKey().equals(tag)) {
                    Log.v("PRBTEST","IF == true");
                    mDeleteDistanceDurationButtonMapList.remove(x);
                    break;
                }
            }


        }
    };

    /*
        Deletes a muscle group from the fragment and the meta data variables
    */
    private View.OnClickListener deleteMuscGroup = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.v("PRBTEST", "Delete musc group button CLICKED " + v.getTag(R.string.EXERCISE_SET_INDEX_KEY));
            Log.v("PRBTEST", "mExerciseMuscGroupsViewMapList.size() " + mExerciseMuscGroupsViewMapList.size());
            String tag = v.getTag(R.string.EXERCISE_SET_INDEX_KEY).toString();

            /**********************************************
             Note: keys are of the form:
             0001 for everything except reps, weights, and sets
             00010002 for reps, weights, sets. First 4 digits are the exercise index, last 4 are the set index
             Index: 012345678
             Hence, the magic numbers 4 and 8 in the substrings, below.
             **********************************************/

            // Remove corresponding entries in the data structures used to keep track of all elements on the form
            for (int x = 0; x < mExerciseMuscGroupsViewMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseMuscGroupsViewMapList.get(x).getKey() " + mExerciseMuscGroupsViewMapList.get(x).getKey()+ " tag: " + tag);
                if (mExerciseMuscGroupsViewMapList.get(x).getKey().equals(tag)) {
                    Log.v("PRBTEST","IF == true");
                    View vwSetToRemove = mExerciseMuscGroupsViewMapList.get(x).getValue();
                    ((ViewGroup) vwSetToRemove.getParent()).removeView(vwSetToRemove);
                    mExerciseMuscGroupsViewMapList.remove(x);
                    break;
                }
            }

            for (int x = 0; x < mExerciseMuscGroupSpinnerMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseMuscGroupSpinnerMapList.get(x).getKey()" + mExerciseMuscGroupSpinnerMapList.get(x).getKey() + " tag: " + tag);
                if (mExerciseMuscGroupSpinnerMapList.get(x).getKey().equals(tag)) {
                    Log.v("PRBTEST","IF == true");
                    mExerciseMuscGroupSpinnerMapList.remove(x);
                    break;
                }
            }

            for (int x = 0; x < mDeleteMuscGroupButtonMapList.size(); x++) {
                Log.v("PRBTEST", "mExerciseWeightsEditTextMapList.get(x).getKey() " +mDeleteMuscGroupButtonMapList.get(x).getKey() + " tag: " + tag);
                if (mDeleteMuscGroupButtonMapList.get(x).getKey().equals(tag)) {
                    Log.v("PRBTEST","IF == true");
                    mDeleteMuscGroupButtonMapList.remove(x);
                    break;
                }
            }
        }
    };

    /*
        Adds a muscle group to the fragment and the corresponding meta data variables
    */
    private View.OnClickListener addMuscleGroup = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.v("PRBTEST", "BUTTON CLICKED " + v.getTag(R.string.EXERCISE_INDEX_KEY));
            String tag = v.getTag(R.string.EXERCISE_INDEX_KEY).toString();
            int intTag = Integer.parseInt(tag);
            final String KEY = create4DigitKey(intTag);
            final String MUSC_GROUP_KEY = KEY + create4DigitKey(mExerciseMuscGroupsHolders.get(intTag).getChildCount());

            View exerciseMuscGroupsView = mInflater.inflate(R.layout.workout_muscle_groups, mExerciseMuscGroupsHolders.get(intTag), false);
            Map.Entry<String, View> exerciseMuscGroupsViewMapsEntry = new AbstractMap.SimpleEntry<String, View>(MUSC_GROUP_KEY, exerciseMuscGroupsView);
            mExerciseMuscGroupsViewMapList.add(exerciseMuscGroupsViewMapsEntry);

            //****** Add the various fields/buttons to the lists of maps, so that the fields can be referred to (i.e. for saving data)******//
            // MUSCLE GROUP SPINNER
            Spinner muscGroupSpinner = (Spinner) exerciseMuscGroupsView.findViewById(R.id.spinnerMuscGroup);
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.musc_groups_array, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            muscGroupSpinner.setAdapter(adapter);
            Map.Entry<String, Spinner> muscGroupMapEntry = new AbstractMap.SimpleEntry<String, Spinner>(MUSC_GROUP_KEY, muscGroupSpinner);
            mExerciseMuscGroupSpinnerMapList.add(muscGroupMapEntry);

            // DELETE MUSC GROUP BUTTON
            Button btnDeleteMuscGroup = (Button) exerciseMuscGroupsView.findViewById(R.id.btnDeleteMuscGroup);
            btnDeleteMuscGroup.setOnClickListener(deleteMuscGroup);

            btnDeleteMuscGroup.setTag(R.string.EXERCISE_SET_INDEX_KEY, MUSC_GROUP_KEY);
            Map.Entry<String, Button> btnDeleteMuscGroupMapsEntry = new AbstractMap.SimpleEntry<String, Button>(MUSC_GROUP_KEY, btnDeleteMuscGroup);
            mDeleteMuscGroupButtonMapList.add(btnDeleteMuscGroupMapsEntry);
            //**********************************************************************************************************************//

            mExerciseMuscGroupsHolders.get(intTag).addView(exerciseMuscGroupsView);
        }
    };

    /*
        Adds a set to the fragment and the corresponding meta data variables
    */
    private View.OnClickListener addSet = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.v("PRBTEST","ADD SET BUTTON CLICKED " + v.getTag(R.string.EXERCISE_INDEX_KEY));
            String tag = v.getTag(R.string.EXERCISE_INDEX_KEY).toString();

            int intTag = Integer.parseInt(tag);
            Log.v("PRBTEST","tag in addset: " + tag + " intTag: " + intTag);
            final String KEY = create4DigitKey(intTag);
            final String SET_Key = KEY + create4DigitKey(mExerciseSetsRepsHolders.get(intTag).getChildCount());//mExerciseSetRepsViewMapList.size());
                    Log.v("PRBTEST","SET_Key is: " + SET_Key);
            View exerciseSetRepsView = mInflater.inflate(R.layout.workout_set_weight_reps, mExerciseSetsRepsHolders.get(intTag), false);
            Map.Entry<String, View> exerciseSetRepsViewMapsEntry = new AbstractMap.SimpleEntry<String, View>(SET_Key, exerciseSetRepsView);
            mExerciseSetRepsViewMapList.add(exerciseSetRepsViewMapsEntry);

            TextView setsLabel= (TextView) exerciseSetRepsView.findViewById(R.id.setsLabel);

            //****** Add the various fields/buttons to the lists of maps, so that the fields can be referred to (i.e. for saving data)******//
            // SETS LABEL
            TextView txtVwSets = (TextView) exerciseSetRepsView.findViewById(R.id.setsLabel);
            Map.Entry<String, TextView> txtVwSetsMapEntry = new AbstractMap.SimpleEntry<String, TextView>(SET_Key, txtVwSets);
            mExerciseSetsLabelMapList.add(txtVwSetsMapEntry);

            // WEIGHTS
            EditText exerciseWeightField = (EditText) exerciseSetRepsView.findViewById(R.id.weightField);
            Map.Entry<String, EditText> weightsMapEntry = new AbstractMap.SimpleEntry<String, EditText>(SET_Key, exerciseWeightField);
            mExerciseWeightsEditTextMapList.add(weightsMapEntry);

            // REPS
            EditText exerciseRepsField = (EditText) exerciseSetRepsView.findViewById(R.id.reps);
            Map.Entry<String, EditText> repsMapEntry = new AbstractMap.SimpleEntry<String, EditText>(SET_Key, exerciseRepsField);
            mExerciseRepsEditTextMapList.add(repsMapEntry);

            // DELETE SET BUTTON
            Button btnDeleteSet = (Button) exerciseSetRepsView.findViewById(R.id.btnDeleteSet);
            btnDeleteSet.setOnClickListener(deleteSet);

            btnDeleteSet.setTag(R.string.EXERCISE_SET_INDEX_KEY, SET_Key);
            Map.Entry<String, Button> btnDeleteMapsEntry = new AbstractMap.SimpleEntry<String, Button>(SET_Key, btnDeleteSet);
            mDeleteSetButtonMapList.add(btnDeleteMapsEntry);
            //**********************************************************************************************************************//

            int exerciseSetRepsHoldersChildCount = mExerciseSetsRepsHolders.get(intTag).getChildCount();
            setsLabel.setText("Set " + (exerciseSetRepsHoldersChildCount + 1));
            mExerciseSetsRepsHolders.get(intTag).addView(exerciseSetRepsView);
        }
    };

    /*
        Adds a distance/duration to the fragment and the corresponding meta data variables
    */
    private View.OnClickListener addDistanceDuration = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.v("PRBTEST","ADD SET BUTTON CLICKED " + v.getTag(R.string.EXERCISE_INDEX_KEY));
            String tag = v.getTag(R.string.EXERCISE_INDEX_KEY).toString();
            int intTag = Integer.parseInt(tag);
            final String KEY = create4DigitKey(intTag);
            final String SET_Key = KEY + create4DigitKey(mExerciseDistanceDurationHolders.get(intTag).getChildCount());//mExerciseSetRepsViewMapList.size());
            Log.v("PRBTEST","SET_Key is: " + SET_Key);
            View exerciseDistanceDurationView = mInflater.inflate(R.layout.workout_distance_duration, mExerciseDistanceDurationHolders.get(intTag), false);
            Map.Entry<String, View> exerciseDistanceDurationViewMapsEntry = new AbstractMap.SimpleEntry<String, View>(SET_Key, exerciseDistanceDurationView);
            mExerciseDistanceDurationViewMapList.add(exerciseDistanceDurationViewMapsEntry);

            //****** Add the various fields/buttons to the lists of maps, so that the fields can be referred to (i.e. for saving data)******//
            // DISTANCE
            EditText exerciseDistanceField = (EditText) exerciseDistanceDurationView.findViewById(R.id.distanceField);
            Map.Entry<String, EditText> distanceMapEntry = new AbstractMap.SimpleEntry<String, EditText>(SET_Key, exerciseDistanceField);
            mExerciseDistanceEditTextMapList.add(distanceMapEntry);

            // DURATION
            EditText exerciseDurationField = (EditText) exerciseDistanceDurationView.findViewById(R.id.durationField);
            Map.Entry<String, EditText> durationMapEntry = new AbstractMap.SimpleEntry<String, EditText>(SET_Key, exerciseDurationField);
            mExerciseDurationEditTextMapList.add(durationMapEntry);

            // DELETE DISTANCE/DURATION BUTTON
            Button btnDeleteDistanceDuration = (Button) exerciseDistanceDurationView.findViewById(R.id.btnDeleteDistanceDuration);
            btnDeleteDistanceDuration.setOnClickListener(deleteDistanceDuration);

            btnDeleteDistanceDuration.setTag(R.string.EXERCISE_SET_INDEX_KEY, SET_Key);
            Map.Entry<String, Button> btnDeleteMapsEntry = new AbstractMap.SimpleEntry<String, Button>(SET_Key, btnDeleteDistanceDuration);
            mDeleteDistanceDurationButtonMapList.add(btnDeleteMapsEntry);
            //**********************************************************************************************************************//

            mExerciseDistanceDurationHolders.get(intTag).addView(exerciseDistanceDurationView);
        }
    };

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FragmentEditWorkout newInstance(String workoutObjectID, String calendarDate, String calendarDay) {
        FragmentEditWorkout fragment = new FragmentEditWorkout();
        Bundle args = new Bundle();
        args.putString("WORKOUT_OBJ_ID", workoutObjectID); // TODO: change to global key
        args.putString("CALENDAR_DATE", calendarDate); //TODO: change to global key
        args.putString("CALENDAR_DAY", calendarDay); //TODO: change to global key
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    /*
        Helper method to prepend 0's to an integer so that it becomes 4 digits
     */
    private String create4DigitKey(int key) {
        String returnKey = key + "";

        if (key > 9999) return "-1"; // Won't ever reach 9999 or above...

        while (returnKey.length() != 4) {
            returnKey = "0" + returnKey;
        }

        return returnKey;
    }
}
