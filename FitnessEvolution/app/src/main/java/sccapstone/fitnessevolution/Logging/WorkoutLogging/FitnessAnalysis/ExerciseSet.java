package sccapstone.fitnessevolution.Logging.WorkoutLogging.FitnessAnalysis;


/*====================================================================

    EXERCISE SET
    Created by Matthew Short

    Class that represents a single exercise set.

====================================================================*/

public class ExerciseSet
{
    public int reps;
    public int weight;

    public ExerciseSet( int r, int w )
    {
        reps = r;
        weight = w;
    }

} // class