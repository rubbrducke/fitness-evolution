package sccapstone.fitnessevolution.Logging.DietaryLogging;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import sccapstone.fitnessevolution.Logging.FragmentWorkoutDietaryLogging;
import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.R;

/**
 * Created by Ben Aaron on 3/15/2016
 * Last updated by Ben Aaron on 4/26/2016
 *
 * This fragment handles creating new diet entries and uploading those entries to the parse database.
 */

public class FragmentEditDiet extends android.support.v4.app.Fragment implements View.OnClickListener {
    private OnFragmentInteractionListener mListener;
    private String mDietObjectID;
    private LayoutInflater mInflater;
    private ParseObject mParseDietRecord;
    private String mDietDate = "";
    private String mDietDay = "";
    private double numCals = -1;
    private double numFat = -1;
    private double numCarbs = -1;
    private double numProtein = -1;
    private String foodName = "";
    private String calStr = "";
    private String fatStr = "";
    private String carbStr= "";
    private String proStr = "";
    private String dateStr = "";
    private Date date;
    private String meal = "";
    private EditText calText;
    private EditText fatText;
    private EditText carbText;
    private EditText proText;
    private EditText nameText;
    private DatePicker dietDatePicker;
    private RadioButton bMeal, lMeal, dMeal, sMeal;

    public FragmentEditDiet() {
        // Required empty public constructor
    }

    /**
     * Basic onStart fragment method
     */
    @Override
    public void onStart() {
        super.onStart();

        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        String currentDate = dateFormat.format(new Date());
    }

    /**
     * Basic onCreate fragment method
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
        }
    }

    /**
     * Expands the XML layout file
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Hide the keyboard if it is currently shown
        Utilities.hideKeyboard(getContext(), getActivity());

        mInflater = inflater;
        final View rootView = mInflater.inflate(R.layout.diet_fragment_edit, container, false);

        Utilities.hideKeyboardOnClickOfMainFragmentLayout(rootView.findViewById(R.id.editDietLayout), this.getActivity());

        if (getArguments() != null) {
            mDietObjectID = getArguments().getString("DIET_OBJ_ID");
            mDietDate = getArguments().getString("CALENDAR_DATE");
            mDietDay = getArguments().getString("CALENDAR_DAY");
        }
        dietDatePicker = (DatePicker) rootView.findViewById(R.id.dietDatePicker);
        bMeal = (RadioButton) rootView.findViewById(R.id.Breakfast);
        lMeal = (RadioButton) rootView.findViewById(R.id.Lunch);
        dMeal = (RadioButton) rootView.findViewById(R.id.Dinner);
        sMeal = (RadioButton) rootView.findViewById(R.id.Snack);
        nameText = (EditText) rootView.findViewById(R.id.foodName);
        calText = (EditText) rootView.findViewById(R.id.calories);
        fatText = (EditText) rootView.findViewById(R.id.fat);
        carbText = (EditText) rootView.findViewById(R.id.carbohydrates);
        proText = (EditText) rootView.findViewById(R.id.protein);

        Button btnSubmit = (Button) rootView.findViewById(R.id.btnSubmitDiet);
        btnSubmit.setOnClickListener(this);
        return rootView;
    }

    /**
     * Checks to see if an object has been pressed and if it has initializes an action
     * @param v
     */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmitDiet:
                submitFoodEntryToParse();
                break;
        }
    }

    /**
     * Takes the data given by the user, checks to see if it is appropriate, and then uploads it to the parse database
     */
    private void submitFoodEntryToParse(){
        int year = dietDatePicker.getYear(),
                month = dietDatePicker.getMonth()+1,   //Zero Index Months
                day = dietDatePicker.getDayOfMonth();
        dateStr = String.valueOf(month) + "/" + String.valueOf(day) + "/" + String.valueOf(year);
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        try {
            date = df.parse(dateStr);
        } catch (java.text.ParseException e) {
            Log.v("EditDiet", "ERROR: date not parsed correctly");
            Toast.makeText(getActivity(),"There was an server error in selecting your date.",Toast.LENGTH_LONG).show();
            return;
        }
        if (bMeal.isChecked()){
            meal = "breakfast";
        }
        else if (lMeal.isChecked()){
            meal = "lunch";
        }
        else if (dMeal.isChecked()){
            meal = "dinner";
        }
        else if (sMeal.isChecked()){
            meal = "snack";
        }
        if (meal.equals("")){
            Toast.makeText(getActivity(), "Please update meal before submitting.", Toast.LENGTH_LONG).show();
            return;
        }
        if (nameText.getText() != null) {
            foodName = nameText.getText().toString();
        }
        else{
            Toast.makeText(getActivity(), "Please add the name of the food in entry before submitting.", Toast.LENGTH_LONG).show();
            return;
        }
        if (calText.getText() != null) {
            calStr = calText.getText().toString();
            numCals = Double.parseDouble(calStr);
            if (numCals < 0){
                Toast.makeText(getActivity(), "Please enter a positive value for the number of calories.", Toast.LENGTH_LONG).show();
                return;
            }
        }
        else {
            Toast.makeText(getActivity(), "Please add the number of calories before submitting.", Toast.LENGTH_LONG).show();
            return;
        }
        if (fatText.getText() != null) {
            fatStr = fatText.getText().toString();
            numFat = Double.parseDouble(fatStr);
            if (numFat < 0){
                Toast.makeText(getActivity(), "Please enter a positive value for the grams of fat.", Toast.LENGTH_LONG).show();
                return;
            }
        }
        else {
            Toast.makeText(getActivity(), "Please add the grams of fat before submitting.", Toast.LENGTH_LONG).show();
            return;
        }
        if (carbText.getText() != null) {
            carbStr = carbText.getText().toString();
            numCarbs = Double.parseDouble(carbStr);
            if (numCarbs < 0){
                Toast.makeText(getActivity(), "Please enter a positive value for the grams of carbohydrates.", Toast.LENGTH_LONG).show();
                return;
            }
        }
        else {
            Toast.makeText(getActivity(), "Please add the grams of carbohydrates before submitting.", Toast.LENGTH_LONG).show();
            return;
        }
        if (proText.getText() != null) {
            proStr = proText.getText().toString();
            numProtein = Double.parseDouble(proStr);
            if (numProtein < 0){
                Toast.makeText(getActivity(), "Please enter a positive value for the grams of protein.", Toast.LENGTH_LONG).show();
                return;
            }
        }
        else {
            Toast.makeText(getActivity(), "Please add the grams of protein before submitting.", Toast.LENGTH_LONG).show();
            return;
        }
        if (mParseDietRecord == null) {
            mParseDietRecord = new ParseObject("DietLogs");
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Submit Diet Entry");
        // set dialog message
        builder
                .setMessage("Would you like to submit the following information to your diet log?" +
                        "\n\n\tDate: " + dateStr +
                        "\n\tMeal: " + meal +
                        "\n\tFood name: " + foodName +
                        "\n\tNumber of Calories: " + calStr +
                        "\n\tGrams of Fat: " + fatStr +
                        "\n\tGrams of Carbohydrates: " + carbStr +
                        "\n\tGrams of Protein: " + proStr)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.v("Submit Diet, YES", foodName);
                        mParseDietRecord.put("Date", date);
                        mParseDietRecord.put("Meal", meal);
                        mParseDietRecord.put("foodName", foodName);
                        mParseDietRecord.put("numCals", numCals);
                        mParseDietRecord.put("numFat", numFat);
                        mParseDietRecord.put("numCarbs", numCarbs);
                        mParseDietRecord.put("numProtein", numProtein);
                        mParseDietRecord.put("userObjectID", ParseUser.getCurrentUser().getObjectId());
                        mParseDietRecord.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null){
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),"Diet entry for " + foodName + " saved successfully.",Toast.LENGTH_SHORT).show();
                                            // Launch the workout dietary logging fragment
                                            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction()
                                                    .replace(R.id.container, (new FragmentWorkoutDietaryLogging().newInstance(0)));
                                            ft.addToBackStack(null);
                                            ft.commit();
                                        }
                                    });
                                }
                                else {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),"Error: Diet entry for " + foodName + " did not save successfully.",Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }
                        });
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });
        // show it
        builder.show();
    }

    /**
     * Basic onDetach fragment method
     */
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FragmentEditDiet newInstance(String dietObjectID, String calendarDate, String calendarDay) {
        FragmentEditDiet fragment = new FragmentEditDiet();
        Bundle args = new Bundle();
        args.putString("DIET_OBJ_ID", dietObjectID); // TODO: change to global key
        args.putString("CALENDAR_DATE", calendarDate); //TODO: change to global key
        args.putString("CALENDAR_DAY", calendarDay); //TODO: change to global key
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Basic onAttach fragment method
     * @param activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
}
