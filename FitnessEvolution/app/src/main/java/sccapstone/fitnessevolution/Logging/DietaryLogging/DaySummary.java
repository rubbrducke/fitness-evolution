package sccapstone.fitnessevolution.Logging.DietaryLogging;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/*====================================================================

    DAY SUMMARY

====================================================================*/

public class DaySummary
{
    public Date date               = new Date();

    public DietEntry allMealsSummary = new DietEntry();
    public List<DietEntry> allEntries = new ArrayList<>();

    public DietEntry breakfastSummary = new DietEntry();
    public DietEntry lunchSummary = new DietEntry();
    public DietEntry dinnerSummary = new DietEntry();
    public DietEntry snackSummary = new DietEntry();

    public List<DietEntry> breakfastEntries = new ArrayList<>();
    public List<DietEntry> lunchEntries = new ArrayList<>();
    public List<DietEntry> dinnerEntries = new ArrayList<>();
    public List<DietEntry> snackEntries = new ArrayList<>();

    public void add( List<DietEntry> newEntries )
    {
        for( DietEntry entry : newEntries )
        {
            add( entry );
        }
    }

    public void add( DietEntry newEntry )
    {
        switch( newEntry.meal.toLowerCase() )
        {
            case "breakfast":
                add( newEntry, breakfastSummary, breakfastEntries );
                break;

            case "lunch":
                add(newEntry, lunchSummary, lunchEntries);
                break;

            case "dinner":
                add( newEntry, dinnerSummary, dinnerEntries );
                break;

            case "snack":
                add( newEntry, snackSummary, snackEntries );
                break;
        }
    }

    private void add( DietEntry newEntry, DietEntry mealSummary, List<DietEntry> mealEntries )
    {
        allMealsSummary.addEntry(newEntry);
        allEntries.add(newEntry);

        mealSummary.addEntry(newEntry);
        mealEntries.add( newEntry );
    }

    public void average( List<DietEntry> newEntries )
    {
        for( DietEntry entry : newEntries )
        {
            average(entry);
        }
    }

    public void average( DietEntry newEntry )
    {
        switch( newEntry.meal.toLowerCase() )
        {
            case "breakfast":
                average(newEntry, breakfastSummary, breakfastEntries);
                break;

            case "lunch":
                average(newEntry, lunchSummary, lunchEntries);
                break;

            case "dinner":
                average(newEntry, dinnerSummary, dinnerEntries);
                break;

            case "snack":
                average(newEntry, snackSummary, snackEntries);
                break;
        }
    }

    private void average( DietEntry newEntry, DietEntry mealSummary, List<DietEntry> mealEntries )
    {
        allMealsSummary.averageEntry(newEntry);
        allEntries.add(newEntry);

        mealSummary.averageEntry(newEntry);
        mealEntries.add( newEntry );
    }
}
