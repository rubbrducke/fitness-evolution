package sccapstone.fitnessevolution.Logging.DietaryLogging;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import sccapstone.fitnessevolution.R;
import sccapstone.fitnessevolution.Utilities.Utilities;

/**
 * Created by Ben Aaron on 4/8/2016
 * Last updated by Ben Aaron on 4/16/2016
 *
 * This class displays an expandable list of diet entries arranged by meal for a given day.
 */
public class FragmentDietExpandableList extends Fragment {

    private static DaySummary day;
    private static Date date;
    private static ArrayList<DietEntry> breakfastList, lunchList, dinnerList, snackList;
    private ArrayList<Holder> breakfastHolders, lunchHolders, dinnerHolders, snackHolders;
    private LayoutInflater mInflater;
    private ExpandableListView entryList = null;
    private HashMap<String, ArrayList<Holder>> entryMap = new HashMap<>();
    private ArrayList<String> mealList = new ArrayList<>();
    private ArrayList<ArrayList<DietEntry>> dietEntryList = new ArrayList<>();

    public FragmentDietExpandableList(){
        //required empty public constructor
    }

    /**
     * This sub-class contains the name and information to be displayed by the entries of the expandable list.
     */
    public class Holder {
        public String name = "";
        public String info = "";
    }

    /**
     * Basic onStart fragment method that clears the meal list to make sure that no duplicate expandable lists are created
     * and then calls upon populateLists() to create all of the arraylists.
     */
    @Override
    public void onStart() {

        super.onStart();
        //entryList.
        if (!mealList.isEmpty()) {
            mealList.clear();
        }
        populateLists();
    }

    /**
     * This method creates a list of meals and a list of holders for each meal.
     */
    private void populateLists() {
        mealList.add("Breakfast");
        mealList.add("Lunch");
        mealList.add("Dinner");
        mealList.add("Snacks");
        for (String meal: mealList){
            if (meal.equals("Breakfast")){
                if (!breakfastList.isEmpty()) {
                    ArrayList<DietEntry> dietEntries = new ArrayList<>();
                    breakfastHolders = new ArrayList<>();
                    for (DietEntry entry : breakfastList) {
                        dietEntries.add(entry);
                        String name = entry.name;
                        String info = "Cals: " + entry.calories + ", Fat: " + entry.fat + ", Carbs: " + entry.carbohydrates + ", Protein: " + entry.protein;
                        Holder holder = new Holder();
                        holder.name = name;
                        holder.info = info;
                        breakfastHolders.add(holder);
                    }
                    entryMap.put(meal, breakfastHolders);
                    dietEntryList.add(dietEntries);
                }
            }
            else if (meal.equals("Lunch")){
                if (!lunchList.isEmpty()) {
                    ArrayList<DietEntry> dietEntries = new ArrayList<>();
                    lunchHolders = new ArrayList<>();
                    for (DietEntry entry : lunchList) {
                        dietEntries.add(entry);
                        String name = entry.name;
                        String info = "Cals: " + entry.calories + ", Fat: " + entry.fat + ", Carbs: " + entry.carbohydrates + ", Protein: " + entry.protein;
                        Holder holder = new Holder();
                        holder.name = name;
                        holder.info = info;
                        lunchHolders.add(holder);
                    }
                    entryMap.put(meal, lunchHolders);
                    dietEntryList.add(dietEntries);
                }
            }
            else if (meal.equals("Dinner")){
                if (!dinnerList.isEmpty()) {
                    ArrayList<DietEntry> dietEntries = new ArrayList<>();
                    dinnerHolders = new ArrayList<>();
                    for (DietEntry entry : dinnerList) {
                        dietEntries.add(entry);
                        String name = entry.name;
                        String info = "Cals: " + entry.calories + ", Fat: " + entry.fat + ", Carbs: " + entry.carbohydrates + ", Protein: " + entry.protein;
                        Holder holder = new Holder();
                        holder.name = name;
                        holder.info = info;
                        dinnerHolders.add(holder);
                    }
                    entryMap.put(meal, dinnerHolders);
                    dietEntryList.add(dietEntries);
                }
            }
            else if (meal.equals("Snacks")){
                ArrayList<DietEntry> dietEntries = new ArrayList<>();
                snackHolders = new ArrayList<>();
                Log.v("DietExpandableList", "Snack list size is "+snackList.size());
                if (!snackList.isEmpty()) {
                    for (DietEntry entry : snackList) {
                        dietEntries.add(entry);
                        String name = entry.name;
                        String info = "Cals: " + entry.calories + ", Fat: " + entry.fat + ", Carbs: " + entry.carbohydrates + ", Protein: " + entry.protein;
                        Holder holder = new Holder();
                        holder.name = name;
                        holder.info = info;
                        snackHolders.add(holder);
                    }
                }
                entryMap.put(meal, snackHolders);
                dietEntryList.add(dietEntries);
            }
        }
        if (!entryMap.isEmpty() && !dietEntryList.isEmpty() && entryList.getExpandableListAdapter() == null){
            CustomDietExpandableListAdapter listAdapter = new CustomDietExpandableListAdapter(this, mealList, entryMap, dietEntryList);
            entryList.setAdapter(listAdapter);
        }
    }

    /**
     * Creates a new instance of the the fragment and initializes the lists of food entries and the date
     * @param daySummary
     * @return fragment
     */
    public static FragmentDietExpandableList newInstance(DaySummary daySummary){
        FragmentDietExpandableList fragment = new FragmentDietExpandableList();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        day = daySummary;
        date = day.date;
        breakfastList = (ArrayList<DietEntry>)day.breakfastEntries;
        Log.v("DietExpandableList", "breakfast list size=" + breakfastList.size());
        lunchList = (ArrayList<DietEntry>)day.lunchEntries;
        Log.v("DietExpandableList","lunch list size="+lunchList.size());
        dinnerList = (ArrayList<DietEntry>)day.dinnerEntries;
        Log.v("DietExpandableList","dinner list size="+dinnerList.size());
        snackList = (ArrayList<DietEntry>)day.snackEntries;
        Log.v("DietExpandableList", "snack list size=" + snackList.size());
        return fragment;
    }

    /**
     * Basic fragment onCreate method
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    /**
     * Expands the XML layout file
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Hide the keyboard if it is currently shown
        //if (entryList != null){
        //    entryList = null;
        //}
        Utilities.hideKeyboard(getContext(), getActivity());
        mInflater = inflater;
        View view;
        view = mInflater.inflate(R.layout.diet_fragment_expandable_list, container, false);
        TextView titleView = (TextView) view.findViewById(R.id.ExpandableListTitle);
        DateFormat df = new SimpleDateFormat("EEEE, MM/dd/yyyy");
        String dateStr = df.format(date);
        titleView.setText(dateStr);
        entryList = (ExpandableListView) view.findViewById(R.id.expandableListView);
        return view;
    }

    /**
     * Basic onAttach fragment method which nullifies the entryList
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        entryList = null;
    }

    /**
     * Basic onDetach fragment method
     */
    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}