package sccapstone.fitnessevolution.Logging;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Map;

import sccapstone.fitnessevolution.Activites.ActivityMain;
import sccapstone.fitnessevolution.Logging.WorkoutLogging.FragmentEditWorkout;
import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.Logging.DietaryLogging.CustomMealsAdapter;
import sccapstone.fitnessevolution.Logging.DietaryLogging.DaySummary;
import sccapstone.fitnessevolution.Logging.DietaryLogging.DietaryLog;
import sccapstone.fitnessevolution.Logging.DietaryLogging.FragmentEditDiet;
import sccapstone.fitnessevolution.Logging.WorkoutLogging.CustomAllWorkoutsAdapter;
import sccapstone.fitnessevolution.R;

/*
    Handles the overview page for workout and dietary logging

    Author: Preston Barbare
    Updated by: benaa
 */
public class FragmentWorkoutDietaryLogging extends android.support.v4.app.Fragment implements View.OnClickListener {
    private OnFragmentInteractionListener mListener;
    private TextView mTextViewNoWorkoutLogsNotif;
    private TextView mTextViewNoDietLogsNotif;
    private ListView mListViewWorkouts, mListViewMealLogs;
    private final ArrayList<String> mWorkoutObjectIDs = new ArrayList<>();
    private final ArrayList<String> mDietObjectIDs = new ArrayList<>();
    private String mCurrentDate = "";
    private String mCurrentDay = "";

    public FragmentWorkoutDietaryLogging() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        getWorkoutLogData();
        getDietLogData();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Activity activity = getActivity();
        ((ActivityMain) activity).setActionBarTitle( getResources().getString(R.string.nav_coach_routine));

        View rootView = inflater.inflate(R.layout.logs_fragment_workout_dietary, container, false);

        // Setup tabs
        TabHost tabHost = (TabHost) rootView.findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec tabSpec = tabHost.newTabSpec("fitness"); // 'fitness' is a tag for the tab being created
        tabSpec.setContent(R.id.tab_fitness_logging);
        tabSpec.setIndicator("Fitness"); // Name to appear on tab
        tabHost.addTab(tabSpec);

        TabHost.TabSpec tabSpec2 = tabHost.newTabSpec("dietary"); // 'dietary' is a tag for the tab being created
        tabSpec2.setContent(R.id.tab_dietary_logging);
        tabSpec2.setIndicator("Dietary"); // Name to appear on tab
        tabHost.addTab(tabSpec2);

        ListView listView = (ListView) rootView.findViewById(R.id.listview_all_workouts);
        mListViewWorkouts = listView;

        mTextViewNoWorkoutLogsNotif = (TextView) rootView.findViewById(R.id.textViewNoWorkoutLogsNotif);

        ListView dietListView = (ListView) rootView.findViewById(R.id.listview_all_dietary_logs);
        mListViewMealLogs = dietListView;

        mTextViewNoDietLogsNotif = (TextView) rootView.findViewById(R.id.textViewNoDietLogsNotif);

        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        mCurrentDate = sdf.format(date);

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        mCurrentDay = intToStringDay(day);

        Button btnNewWorkout = (Button) rootView.findViewById(R.id.btnNewWorkout);
        btnNewWorkout.setOnClickListener(this);

        Button btnNewDietLog = (Button) rootView.findViewById(R.id.btnNewDietLog);
        btnNewDietLog.setOnClickListener(this);

        // Hide the keyboard if it is currently shown
        Utilities.hideKeyboard(getContext(), getActivity());

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnNewWorkout:
                launchEditWorkoutFragment("");
                break;
            case R.id.btnNewDietLog:
                launchEditDietFragment("");
                break;
        }
    }

    /*
        Launches the edit workout fragment to edit the workout corresponding to the PARSE object with object ID of workoutObjectID
     */
    private void launchEditWorkoutFragment(String workoutObjectID) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, (new FragmentEditWorkout().newInstance(workoutObjectID, mCurrentDate, mCurrentDay)));
        ft.addToBackStack(null);
        ft.commit();
    }

    /*
        Launches the diet fragment to edit the diet log corresponding to the PARSE object with object ID of dietObjectID
    */
    private void launchEditDietFragment(String dietObjectID) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, (new FragmentEditDiet().newInstance(dietObjectID, mCurrentDate, mCurrentDay)));
        ft.addToBackStack(null);
        ft.commit();
    }


    /*
        Gets the workout data for a given date and calls a method to update a listview with this data
     */
    public void getWorkoutLogData() {
        Log.v("PRBTEST","getWorkoutLogData()");
        final ArrayList<String> exerciseMuscGroupList = new ArrayList<>();
        final ArrayList<String> exerciseNameList = new ArrayList<>();
        final ArrayList<String> timeList = new ArrayList<>();
        final ArrayList<String> dayDateList = new ArrayList<>();
        mWorkoutObjectIDs.clear();
        ParseQuery<ParseObject> workoutsQuery = new ParseQuery<ParseObject>("WorkoutLogs");
        workoutsQuery.whereEqualTo("userObjectID", ParseUser.getCurrentUser().getObjectId());
        workoutsQuery.orderByDescending("workoutDate");

        Utilities.showLoadingScreen(getContext());
        workoutsQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> workoutLogList, ParseException e) {
                if (e == null) {
                    for (ParseObject objWorkoutLog : workoutLogList) {
                        mWorkoutObjectIDs.add(objWorkoutLog.getObjectId().toString());

                        if (objWorkoutLog.get("exerciseMuscGroup") != null) {
                            ArrayList<String> exerciseMuscGroups = (ArrayList<String>) objWorkoutLog.get("exerciseMuscGroup");

                            // Remove duplicate muscle groups
                            Set<String> hs = new HashSet<>();
                            hs.addAll(exerciseMuscGroups);
                            exerciseMuscGroups.clear();
                            exerciseMuscGroups.addAll(hs);
                            Collections.sort(exerciseMuscGroups);

                            String strMuscGroupNames = exerciseMuscGroups.toString();
                            strMuscGroupNames = strMuscGroupNames.substring(1, strMuscGroupNames.length() - 1); // Trim off the brackets that naturally occur with ArrayList.toString()
                            exerciseMuscGroupList.add(strMuscGroupNames);
                        }
                        if (objWorkoutLog.get("exerciseNames") != null) {
                            ArrayList<String> exerciseNames = (ArrayList<String>) objWorkoutLog.get("exerciseNames");

                            // Remove duplicate exercise names (unlikely but whatever)
                            Set<String> hs = new HashSet<>();
                            hs.addAll(exerciseNames);
                            exerciseNames.clear();
                            exerciseNames.addAll(hs);
                            Collections.sort(exerciseNames);

                            String strExerciseNames = exerciseNames.toString();
                            strExerciseNames = strExerciseNames.substring(1, strExerciseNames.length() - 1); // Trim off the brackets that naturally occur with ArrayList.toString()
                            exerciseNameList.add(strExerciseNames);
                        }
                        if (objWorkoutLog.getString("workoutElapsedTime") != null) {
                            timeList.add(objWorkoutLog.getString("workoutElapsedTime").toString());
                        }

                        if (objWorkoutLog.getString("workoutDay") != null) {
                            dayDateList.add(objWorkoutLog.getString("workoutDay").toString() + ", " + Utilities.trimLeadingZeroesOnDate(objWorkoutLog.get("workoutDate").toString()));
                        }
                    }

                    Log.v("PRBTEST", "FragmentWorkoutDietaryLogging: exerciseList: " + exerciseMuscGroupList.toString());

                } else {
                    Utilities.hideLoadingScreen();
                    Toast.makeText(getContext(), "Unable to retrieve workout logs :(", Toast.LENGTH_LONG);
                    Log.v("PRBTEST", "Error retrieving workout logs: " + e.getMessage());
                }

                if (workoutLogList.isEmpty()) {
                    mTextViewNoWorkoutLogsNotif.setHint("You don't have any workout logs, yet! Click on \"New Workout\" below to get started! :)");
                }

                updateAdapter(dayDateList, timeList, exerciseMuscGroupList, exerciseNameList);
                Utilities.hideLoadingScreen();
            }
        });
        Utilities.hideLoadingScreen();
    }

    /*
        Gets the diet data
    */
    public void getDietLogData() {
        Log.v("PRBTEST", "getDietLogData()");
        final ArrayList<Date> dayDateList = new ArrayList<>();
        mDietObjectIDs.clear();
        ParseQuery<ParseObject> dietQuery = new ParseQuery<ParseObject>("DietLogs");
        dietQuery.whereEqualTo("userObjectID", ParseUser.getCurrentUser().getObjectId());
        dietQuery.orderByAscending("Date");

        Utilities.showLoadingScreen(getContext());
        dietQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> dietLogList, ParseException e) {
                if (e == null) {
                    for (ParseObject objDietLog : dietLogList) {
                        mDietObjectIDs.add(objDietLog.getObjectId());

                        if (objDietLog.getDate("Date") != null) {
                            Log.v("WorkoutDietaryLogging", "Found a date.");
                            Date date = objDietLog.getDate("Date");
                            dayDateList.add(date);
                        }
                    }
                } else {
                    Utilities.hideLoadingScreen();
                    Toast.makeText(getContext(), "Unable to retrieve diet logs :(", Toast.LENGTH_LONG);
                    Log.v("PRBTEST", "Error retrieving diet logs: " + e.getMessage());
                }
                if (!dayDateList.isEmpty()) {
                    Date startDate = dayDateList.get(0);
                    Date endDate = dayDateList.get(dayDateList.size() - 1);
                    DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                    String sDstr = df.format(startDate);
                    String eDstr = df.format(endDate);
                    if (eDstr.equals(sDstr)) {
                        endDate = startDate;
                    }
                    updateMealAdapter(startDate, endDate);
                    Utilities.hideLoadingScreen();
                } else {
                    Log.v("WorkoutDietaryLogging", "Empty date list.");
                    mTextViewNoDietLogsNotif.setHint("You haven't logged any diets yet. You can get started now by clicking the \"New Dietary Log\" below.");
                    Utilities.hideLoadingScreen();
                }
            }
        });

    }


    /**
    * Updates workout adapter with the supplied ArrayList, which will refresh the data in the ListView
    */
    private void updateAdapter(ArrayList<String> workoutDayDates, ArrayList<String> workoutTimes, ArrayList<String> exerciseMuscGroupList, ArrayList<String> exerciseNames) {
        if (workoutDayDates != null) {
            CustomAllWorkoutsAdapter workoutsAdapter = new CustomAllWorkoutsAdapter(this, workoutDayDates, workoutTimes, exerciseMuscGroupList, exerciseNames, mWorkoutObjectIDs);
            mListViewWorkouts.setAdapter(workoutsAdapter);
        }
    }

    /**
     * Updates meal adapter with the supplied ArrayList, which will refresh the data in the ListView
     *
     * Parameters:
     *   Date s - start date
     *   Date e - end date
     */
    private void updateMealAdapter(Date s, Date e) {
        Log.v("WorkoutDietaryLogging", "Updating meal adapter");
        Map<Date, DaySummary> log = DietaryLog.buildVerboseHistory(s, e);
        if (log.isEmpty()){
            Log.v("WorkoutDietaryLogging", "Log is empty");
        }
        CustomMealsAdapter mealsAdapter = new CustomMealsAdapter(this, log);
        Log.v("WorkoutDietaryLogging", "Meal adapter created.");
        mListViewMealLogs.setAdapter(mealsAdapter);
        Log.v("WorkoutDietaryLogging", "Meal adapter set.");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FragmentWorkoutDietaryLogging newInstance(int sectionNumber) {
        FragmentWorkoutDietaryLogging fragment = new FragmentWorkoutDietaryLogging();
        Bundle args = new Bundle();
        args.putInt(Utilities.ARG_SECTION_NUMBER_KEY, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((ActivityMain) activity).onSectionAttached(
                getArguments().getInt(Utilities.ARG_SECTION_NUMBER_KEY));
    }

    /*
        Helper method to convert value returned from Calendar (int) to a string of the day
     */
    private String intToStringDay(int intDay) {
        switch (intDay) {
            case 1: return ("Sunday");
            case 2: return ("Monday");
            case 3: return ("Tuesday");
            case 4: return ("Wednesday");
            case 5: return ("Thursday");
            case 6: return ("Friday");
            case 7: return ("Saturday");
            default: return "";
        }
    }
}
