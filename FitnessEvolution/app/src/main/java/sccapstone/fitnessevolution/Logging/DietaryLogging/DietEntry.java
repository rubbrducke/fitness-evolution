package sccapstone.fitnessevolution.Logging.DietaryLogging;

import android.util.Log;

import com.parse.ParseObject;

import java.util.Date;


/*====================================================================

    FOOD ENTRY

====================================================================*/

public class DietEntry
{
    public String name = "";
    public String meal = "";
    public Date date = new Date();


    public double fat = 0;
    public double carbohydrates = 0;
    public double protein = 0;
    public double calories = 0;


    public int averageCount = 1;

    public DietEntry()
    {
    }

    public DietEntry(ParseObject parseRecord) throws Exception
    {
        name = parseRecord.getString("foodName");
        meal = parseRecord.getString("Meal");
        date = parseRecord.getDate("Date");

        fat = parseRecord.getDouble("numFat");
        carbohydrates = parseRecord.getDouble("numCarbs");
        protein = parseRecord.getDouble("numProtein");
        calories = parseRecord.getDouble("numCals");
    }

    public void addEntry(DietEntry newEntry)
    {
        Log.v("ADD DIET ENTRY", "Adding to DietEntry.");

        fat += newEntry.fat;
        carbohydrates += newEntry.carbohydrates;
        protein += newEntry.protein;
        calories += newEntry.calories;
    }

    public void averageEntry(DietEntry newEntry)
    {
        Log.v("ADD DIET ENTRY", "Averaging with DietEntry.");

        averageCount++;
        fat = (fat * averageCount + newEntry.fat) / averageCount;
        carbohydrates = (carbohydrates * averageCount + newEntry.carbohydrates) / averageCount;
        calories = (calories * averageCount + newEntry.calories) / averageCount;
        protein = (protein * averageCount + newEntry.protein) / averageCount;
    }
}
