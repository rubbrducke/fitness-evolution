package sccapstone.fitnessevolution.Logging.WorkoutLogging.FitnessAnalysis;

import android.util.Log;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import sccapstone.fitnessevolution.Utilities.Utilities;


/*====================================================================

    WORKOUT LOG
    Created by Matthew Short

    This class is for assembling workout data into coherent data structures.

====================================================================*/

public class WorkoutLog
{

    /*--------------------------------------------------------------------

        BUILD VERBOSE HISTORY

        Purpose:
        Builds a history of workouts organized by date.

        Arguments:
        Pulls the workout history from between two dates.

        Returns:
        A Map correlating each date with a workout.

    --------------------------------------------------------------------*/

    public static Map<String, WorkoutSession> buildVerboseHistory( Date startDate, Date endDate )
    {
        Map<String, WorkoutSession> workoutHistory = new HashMap<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy", Locale.US);

        String start = dateFormat.format( startDate );
        String end = dateFormat.format(endDate);

        ParseUser user = ParseUser.getCurrentUser();

        /*--------------------------------------------------------------------
            Prepare a query for Parse
        --------------------------------------------------------------------*/

        ParseQuery<ParseObject> workoutQuery = ParseQuery.getQuery("WorkoutLogs");
        workoutQuery.whereEqualTo("userObjectID", user.getObjectId());
        workoutQuery.whereGreaterThan("workoutDate", start);
        workoutQuery.whereLessThanOrEqualTo("workoutDate", end);
        workoutQuery.orderByAscending("workoutDate");

        Log.v("BUILD VERBOSE HISTORY", "Parse Query: "
                                       + "Date between " + start + " and " + end);

        try {
            List<ParseObject> workoutsList = workoutQuery.find();

            Log.v("BUILD VERBOSE HISTORY", "Found " + workoutsList.size() + " workouts.");

            for (ParseObject WorkoutLog : workoutsList)
            {
                WorkoutSession current = new WorkoutSession( WorkoutLog );

                /*--------------------------------------------------------------------
                    If the workout history already contains a session for this date,
                    merge the sessions.
                --------------------------------------------------------------------*/

                if (workoutHistory.containsKey(current.date) )
                {
                    Log.v("BUILD VERBOSE HISTORY", "WorkoutHistory already contains " + current.date + ". Merge workout.");

                    WorkoutSession mergedSession = workoutHistory.get(current.date).MergeWorkout(current);
                    workoutHistory.put(current.date, mergedSession);
                }

                /*--------------------------------------------------------------------
                    Otherwise add a new session into the workout history.
                --------------------------------------------------------------------*/

                else
                {
                    Log.v("BUILD VERBOSE HISTORY", "WorkoutHistory does not contain " + current.date);

                    workoutHistory.put(current.date, current);
                }
            }

        } catch (ParseException e)
        {
            Log.e("BUILD VERBOSE HISTORY", "Error: " + e);
        }

        return workoutHistory;

    } // method


    /*--------------------------------------------------------------------

        BUILD HISTORY

        Purpose:
        Builds either an average or total history with the specified length.

        Arguments:
        Enums describing the length and type (average or sum) of history.

        Returns:
        A Map< date in string format, < muscle name, MuscleWorkout > >.

    --------------------------------------------------------------------*/

    public enum historyLength   { month, week }
    public enum historyType     { total, average }

    public static Map< String, Map< String, MuscleWorkout > > buildHistory( historyLength length, historyType type )
    {
        return buildHistory( type, length );
    }

    public static Map< String, Map< String, MuscleWorkout > > buildHistory( historyType type, historyLength length )
    {
        Log.v("BUILD HISTORY", "Start of method Build History.");

        Map< String, Map< String,MuscleWorkout > > history = new HashMap<>();

        ParseUser user = ParseUser.getCurrentUser();

        //SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy", Locale.US);
        Date start = Utilities.getWeek(4).first;
        Date end = new Date();

        /*--------------------------------------------------------------------
            Set the correct start date
        --------------------------------------------------------------------*/

        switch( length )
        {
            case month:
                Log.v("BUILD HISTORY", "Length: Last 4 weeks.");

                start = Utilities.getWeek(4).first;
                end = Utilities.getWeek(0).second;
                break;

            case week:
                Log.v("BUILD HISTORY", "Length: Last week.");

                start = Utilities.getWeek(0).first;
                end = Utilities.getWeek(0).second;
                break;
        }

        Map<String, WorkoutSession> verboseHistory = WorkoutLog.buildVerboseHistory( start, end );

        switch( type )
        {
            case average:

                /*--------------------------------------------------------------------
                    Iterate through each <Date, WorkoutSession> of the verbose history
                --------------------------------------------------------------------*/

                for( Map.Entry<String, WorkoutSession> entry : verboseHistory.entrySet() )
                {

                    /*--------------------------------------------------------------------
                        Translate the WorkoutSession to a Map <Muscle, MuscleWorkout>
                    --------------------------------------------------------------------*/

                    Map<String, MuscleWorkout> muscleMap = MuscleWorkoutFromSession( entry.getValue() );

                    /*--------------------------------------------------------------------
                        If the history already contains this day then merge the
                        MuscleWorkout map's
                    --------------------------------------------------------------------*/

                    if( history.containsKey( entry.getValue().day ) )
                    {
                        // get the muscleMap for this day
                        Map<String, MuscleWorkout> historyMuscles = history.get( entry.getValue().day );

                        /*--------------------------------------------------------------------
                            Iterate through each of the muscles in the muscleMap
                        --------------------------------------------------------------------*/

                        for( Map.Entry<String, MuscleWorkout> muscles : muscleMap.entrySet() )
                        {
                            if( historyMuscles.containsKey( muscles.getKey() ) )
                            {
                                historyMuscles.get( muscles.getKey() ).averageWithWorkout( muscles.getValue() );
                            }
                            else
                            {
                                historyMuscles.put( muscles.getKey(), muscles.getValue() );
                            }

                        } // iterate muscleMap

                        history.put( entry.getValue().day, historyMuscles );

                    } // if
                    else
                    {
                        history.put( entry.getValue().day, muscleMap );
                    }
                }
                break;

            case total:

                /*--------------------------------------------------------------------
                    Iterate through each <Date, WorkoutSession> of the verbose history
                --------------------------------------------------------------------*/

                for( Map.Entry<String, WorkoutSession> entry : verboseHistory.entrySet() )
                {

                    /*--------------------------------------------------------------------
                        Translate the WorkoutSession to a Map <Muscle, MuscleWorkout>
                    --------------------------------------------------------------------*/

                    Map<String, MuscleWorkout> muscleMap = MuscleWorkoutFromSession( entry.getValue() );

                    /*--------------------------------------------------------------------
                        If the history already contains this day then merge the
                        MuscleWorkout map's
                    --------------------------------------------------------------------*/

                    if( history.containsKey( entry.getValue().date ) )
                    {
                        // get the muscleMap for this day
                        Map<String, MuscleWorkout> historyMuscles = history.get( entry.getValue().date );

                        /*--------------------------------------------------------------------
                            Iterate through each of the muscles in the muscleMap
                        --------------------------------------------------------------------*/

                        for( Map.Entry<String, MuscleWorkout> muscles : muscleMap.entrySet() )
                        {
                            if( historyMuscles.containsKey( muscles.getKey() ) )
                            {
                                historyMuscles.get( muscles.getKey() ).averageWithWorkout( muscles.getValue() );
                            }
                            else
                            {
                                historyMuscles.put( muscles.getKey(), muscles.getValue() );
                            }

                        } // iterate muscleMap

                        history.put( entry.getValue().date, historyMuscles );

                    } // if
                    else
                    {
                        history.put( entry.getValue().date, muscleMap );
                    }
                }

                break;
        }

        return history;
    }


    /*--------------------------------------------------------------------

        MUSCLE WORKOUT FROM SESSION

        Purpose:
        Helper method that converts a WorkoutSession to a
        Map< muscle name, MuscleWorkout >.

        Arguments:
        A WorkoutSession.

        Returns:
        The aforementioned Map.
    --------------------------------------------------------------------*/

    private static Map<String, MuscleWorkout> MuscleWorkoutFromSession( WorkoutSession session )
    {
        Map< String, MuscleWorkout > muscleWorkoutMap = new HashMap<>();

        for( ExerciseSession exercise : session.exercises )
        {
            //region Debug
            Log.v("WORKOUT FROM SESSION", "Exercise: " + exercise.exerciseName);
            Log.v("WORKOUT FROM SESSION", "Begin looping each muscle.");
            //endregion

            for( String muscle : exercise.muscleGroups )
            {

                Log.v("WORKOUT FROM SESSION", "Muscle: " + muscle);

                MuscleWorkout muscleWorkout = new MuscleWorkout();

                /*--------------------------------------------------------------------
                    Calculate the average number of repetitions per set
                --------------------------------------------------------------------*/

                int totalReps = 0;
                int totalWeightLifted = 0;
                for( int i = 0; i < exercise.sets.size(); ++i )
                {
                    totalReps += exercise.sets.get(i).reps;
                    totalWeightLifted += exercise.sets.get(i).weight * exercise.sets.get(i).reps;
                }

                /*--------------------------------------------------------------------
                    Populate the muscleWorkout with data
                --------------------------------------------------------------------*/

                muscleWorkout.muscleName = muscle;
                muscleWorkout.totalWeightLifted = totalWeightLifted;
                muscleWorkout.totalSets = exercise.sets.size();
                muscleWorkout.totalReps = totalReps;

                muscleWorkout.averageSets = exercise.sets.size();

                /*--------------------------------------------------------------------
                    Ensure we don't divide by zero
                --------------------------------------------------------------------*/

                int sets = exercise.sets.size() == 0 ? 1: exercise.sets.size();
                muscleWorkout.averageReps = totalReps / sets;

                //region Debug
                Log.v("WORKOUT FROM SESSION", "New Muscle Workout:"
                                              + "\nTotal Sets: " + muscleWorkout.totalSets
                                              + "\nAverage Sets: " + muscleWorkout.averageSets
                                              + "\nTotal Reps: " + muscleWorkout.totalReps
                                              + "\nAverage Reps per Set: " + muscleWorkout.averageReps);
                //endregion

                /*--------------------------------------------------------------------
                    Check if the averageRoutine contains this day and muscle.

                    If it does, average the workouts.
                    Otherwise create a new entry.
                --------------------------------------------------------------------*/

                if (muscleWorkoutMap.containsKey(muscle))
                {
                    muscleWorkoutMap.get(muscle).averageWithWorkout(muscleWorkout);
                }
                else
                {
                    muscleWorkoutMap.put(muscle, muscleWorkout);
                }

            } //foreach muscle

        } // foreach exercise

        return muscleWorkoutMap;

    } // function

}
