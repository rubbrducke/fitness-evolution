package sccapstone.fitnessevolution.Logging.WorkoutLogging.FitnessAnalysis;


/*====================================================================

    WORKOUT BUILDER
    Created by Matthew Short

    Calculates an ideal workout.
    Calculates if you should be working out more often based on
    your goals.

====================================================================*/

public class WorkoutBuilder
{

    /*--------------------------------------------------------------------
        Constants

        MAX_SETS_WEEKLY's value is pulled from reading I did on
        building a workout plan.
    --------------------------------------------------------------------*/

    static final int MAX_HEART_RATE     = 220;
    static final int MAX_SETS_WEEKLY    = 12;

    /*--------------------------------------------------------------------
        User Information
    --------------------------------------------------------------------*/

    int age                 = 100;
    int maxHeartRate        = 0;

    int moderateLowHR       = 0;
    int moderateHighHR      = 0;
    int vigorousHR          = 0;

    /*--------------------------------------------------------------------
        Calculated Ideal Workout
    --------------------------------------------------------------------*/

    String suggestion       = "";
    int idealFrequency      = 0;
    int idealSetsPerMuscle  = 0;
    int idealMinRepsPerSet  = 0;
    int idealMaxRepsPerSet     = 0;

    /*--------------------------------------------------------------------
        User Preferences
    --------------------------------------------------------------------*/

    public int userFrequency        = 0;
    public int userSetsPerMuscle    = 0;
    public int userRepsPerSet       = 0;
    WorkoutType workoutType         = WorkoutType.Strength;

    public enum WorkoutType {
        Strength,
        Endurance,
        MuscleGain
    }


    /*--------------------------------------------------------------------

        SET AGE

        Purpose:
        Sets the user's age. Updates their maximum heart rate
        and target heart rates with the new information.

        Arguments:
        Integer that represents an age.

    --------------------------------------------------------------------*/

    public void SetAge( int newAge )
    {
        age = newAge;
        maxHeartRate = MAX_HEART_RATE - age;

        moderateLowHR   = (int) (maxHeartRate * 0.5);
        moderateHighHR  = (int) (maxHeartRate * 0.7);
        vigorousHR      = (int) (maxHeartRate * 0.85);

    } // method


    /*--------------------------------------------------------------------

        SET FREQUENCY

        Purpose:
        Sets the user's workout frequency. Updates the ideal number of
        sets per muscle based on this.

        Arguments:
        Integer representing the frequency.

    --------------------------------------------------------------------*/

    public void SetFrequency( int newFrequency )
    {
        userFrequency       = newFrequency;

        idealFrequency      = newFrequency;
        idealSetsPerMuscle  = MAX_SETS_WEEKLY / idealFrequency;

    } // method


    /*--------------------------------------------------------------------

        SET SETS

        Purpose:
        Sets the user's number of sets per muscle. Updates the ideal
        workout frequency based on this.

    --------------------------------------------------------------------*/

    public void SetSets( int newSetsPerMuscle )
    {
        userSetsPerMuscle   = newSetsPerMuscle;

        idealSetsPerMuscle  = newSetsPerMuscle;
        idealFrequency      = MAX_SETS_WEEKLY / idealSetsPerMuscle;

    } // method


    /*--------------------------------------------------------------------

        SET WORKOUT TYPE

        Purpose:
        Sets the user's workout type and assigns the ideal values for
        reps per set.

        Arguments:
        A WorkoutType enum.

        Additional Note:
        Ideal values based on analysis from the research paper:
        "The influence of frequency, intensity, volume and mode of
        strength training on whole muscle cross-sectional area in humans."
    --------------------------------------------------------------------*/

    public void SetWorkoutType( WorkoutType workout )
    {
        workoutType = workout;

        switch( workoutType )
        {
            case Strength:
                idealMinRepsPerSet = 1;
                idealMaxRepsPerSet = 5;
                break;

            case MuscleGain:
                idealMinRepsPerSet = 6;
                idealMaxRepsPerSet = 12;
                break;

            case Endurance:
                idealMinRepsPerSet = 12;
                idealMaxRepsPerSet = 20;
                break;

        } // switch

    } // method

} // class
