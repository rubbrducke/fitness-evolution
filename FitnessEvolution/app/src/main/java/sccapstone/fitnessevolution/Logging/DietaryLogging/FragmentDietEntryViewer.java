package sccapstone.fitnessevolution.Logging.DietaryLogging;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.R;

/**
 * Created by Ben Aaron on 3/31/2016
 * Last Updated by Ben Aaron on 4/16/2016
 *
 * A fragment that expands the FragmentDietEntryViewer.xml file to display the information in a logged entry.
 * Information includes the food name, meal, date logged, number of calories, grams of fat, protein and carbohydrates.
 */
public class FragmentDietEntryViewer extends android.support.v4.app.Fragment {

    private static DietEntry entry;
    private static String food, meal;
    private static double cals, fat, carbs, protein;
    private static Date date;
    private OnFragmentInteractionListener mListener;
    private LayoutInflater mInflater;

    public FragmentDietEntryViewer() {
        // Required empty public constructor
    }

    /**
     * Basic fragment onStart method.
     */
    @Override
    public void onStart() {
        super.onStart();
    }

    /**
     * Basic fragment newInstance method takes in a DietEntry object and sets up the information to be displayed from that entry.
     * @param dietEntry
     * @return fragment
     */
    public static FragmentDietEntryViewer newInstance(DietEntry dietEntry) {
        FragmentDietEntryViewer fragment = new FragmentDietEntryViewer();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        entry = dietEntry;
        food = entry.name;
        date = entry.date;
        meal = entry.meal;
        cals = entry.calories;
        fat = entry.fat;
        carbs = entry.carbohydrates;
        protein = entry.protein;
        return fragment;
    }

    /**
     * Basic onCreate fragment method
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    /**
     * Basic onCreateView fragment method that sets up all of the text views to display the dietEntry's information.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Hide the keyboard if it is currently shown
        Utilities.hideKeyboard(getContext(), getActivity());

        // Inflate the layout for this fragment
        mInflater = inflater;
        View view;
        view = mInflater.inflate(R.layout.diet_fragment_entry_viewer, container, false);
        TextView foodView = (TextView) view.findViewById(R.id.DietFood);
        foodView.setText("\n"+food);
        TextView dateView = (TextView) view.findViewById(R.id.DietDate);
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String dateStr = df.format(date);
        dateView.setText("\n"+dateStr);
        TextView mealView = (TextView) view.findViewById(R.id.DietMeal);
        mealView.setText("\n"+meal);
        TextView calsView = (TextView) view.findViewById(R.id.DietCals);
        calsView.setText("\nCalories: "+cals+"g");
        TextView fatView = (TextView) view.findViewById(R.id.DietFat);
        fatView.setText("\nFat: "+fat+"g");
        TextView carbsView = (TextView) view.findViewById(R.id.DietCarbs);
        carbsView.setText("\nCarbohydrates: "+carbs+"g");
        TextView proteinView = (TextView) view.findViewById(R.id.DietProtein);
        proteinView.setText("\nProtein: "+protein+"g");
        return view;
    }

    /**
     * Basic onButtonPressed fragment method
     * @param uri
     */
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /**
     * Basic onAttach fragment method
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    /**
     * Basic onDetach fragment method
     */
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
