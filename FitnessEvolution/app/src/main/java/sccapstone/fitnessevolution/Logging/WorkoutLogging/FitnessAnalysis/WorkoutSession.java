package sccapstone.fitnessevolution.Logging.WorkoutLogging.FitnessAnalysis;

import android.util.Log;

import com.parse.ParseObject;
import com.parse.ParseUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/*====================================================================

    WORKOUT SESSION
    Created by Matthew Short

    Class that represents a single workout session.
    This class can be transformed to and from a ParseObject.

====================================================================*/

public class WorkoutSession
{
    public String date = "";
    public String day = "";

    public String startTime = "";
    public String endTime = "";

    public ArrayList<ExerciseSession> exercises = new ArrayList<>();


    /*--------------------------------------------------------------------

        DEFAULT CONSTRUCTOR

    --------------------------------------------------------------------*/

    public WorkoutSession()
    {}


    /*--------------------------------------------------------------------

        PARSE CONSTRUCTOR

    --------------------------------------------------------------------*/

    public WorkoutSession( ParseObject parseRecord )
    {
        try
        {
            fromParseObject( parseRecord );
        }
        catch (Exception e)
        {
            Log.e("WORKOUT SESSION", "Could not create Workout Session from parse record.");
        }
    }


    /*--------------------------------------------------------------------

        FROM PARSE OBJECT

        Constructs a WorkoutSession from a parse record.

    --------------------------------------------------------------------*/

    public void fromParseObject( ParseObject parseRecord ) throws Exception
    {
        Log.v("WORKOUT SESSION", "Start of Method fromParseObject");

        date        = parseRecord.getString("workoutDate");
        day         = parseRecord.getString("workoutDay");

        startTime   = parseRecord.getString("workoutStartTime");
        endTime     = parseRecord.getString("workoutEndTime");

        /*--------------------------------------------------------------------
            Pull the data into flat lists as they're represented on Parse
        --------------------------------------------------------------------*/

        Log.d("WORKOUT INLINE", "Has exercises: " + parseRecord.containsKey("exerciseNames") );

        List<String> exerciseNames              = parseRecord.getList("exerciseNames");

        Log.d("WORKOUT INLINE", "Exercises size: " + exerciseNames.size() );


        List<Integer>   exerciseMusclesCount    = parseRecord.getList("exerciseMuscGroupCount");
        List<String>    exerciseMuscles         = parseRecord.getList("exerciseMuscGroup");
        List<Integer>   exerciseSets            = parseRecord.getList("exerciseSets");
        List<Integer>   exerciseReps            = parseRecord.getList("exerciseReps");
        List<Integer>   exerciseWeights         = parseRecord.getList("exerciseWeights");

        for( int num : exerciseReps )
        {
            Log.d("COLLECTION", "Content: " + num );
        }



        /*--------------------------------------------------------------------
            The start and end indexes for Sets are initially zero.

            Database structure:
            ExerciseNames   = "name1",      "name2",            "name3"

            ExerciseSets    = "2",          "3",                "1"
            ExerciseReps    = "4",  "4",    "2", "2",   "2",    "3"
            ExerciseWeights = "10", "10",   "15", "15", "15",   "5"

            ExerciseMuscCnt = "2",          "1",                "1"
            ExerciseMuscGrp = "n1", "n2",   "n3",               "n4"

            The value in ExerciseSets indicates how many of the
            values in ExerciseReps and ExerciseWeights correlate to it.

            Therefore we can use the ExerciseSet values to calculate where our
            start and end index is when traversing the other arrays.
        --------------------------------------------------------------------*/

        int repsStartIndex = 0;
        int repsEndIndex = 0;

        int musclesStartIndex = 0;
        int musclesEndIndex = 0;

        for( int exerciseIndex = 0; exerciseIndex < exerciseNames.size(); ++exerciseIndex )
        {

            ExerciseSession newSession = new ExerciseSession();

            newSession.exerciseName = exerciseNames.get(exerciseIndex);

            /*--------------------------------------------------------------------
                Iterate through the reps / weights.
            --------------------------------------------------------------------*/

            repsEndIndex = repsEndIndex + exerciseSets.get(exerciseIndex);

            for (int j = repsStartIndex; j < repsEndIndex; ++j)
            {
                Log.d("REPS ITERATOR", "index: " + j + " size: " + exerciseReps.size() + " " + exerciseWeights.size());
                newSession.sets.add(new ExerciseSet(exerciseReps.get(j), exerciseWeights.get(j)));
            }

            repsStartIndex = repsStartIndex + exerciseSets.get(exerciseIndex);

            /*--------------------------------------------------------------------
                Iterate through the muscle groups.
            --------------------------------------------------------------------*/

            musclesEndIndex = musclesEndIndex + exerciseMusclesCount.get( exerciseIndex );

            for (int j = musclesStartIndex; j < musclesEndIndex; ++j)
            {
                    newSession.muscleGroups.add(exerciseMuscles.get(j));
            }

            musclesStartIndex = musclesStartIndex + exerciseMusclesCount.get( exerciseIndex );

            /*--------------------------------------------------------------------
                Add the new session to the list of exercises.
            --------------------------------------------------------------------*/

            exercises.add( newSession );

        } // for exerciseIndex

        Log.v("WORKOUT SESSION", "End of Method fromParseObject");

    } // function


    /*--------------------------------------------------------------------

        MERGE WORKOUT

        Purpose:
        Utility function for merging two sessions.

        Arguments:
        A WorkoutSession

        Returns:
        The merged WorkoutSession.

    --------------------------------------------------------------------*/

    public WorkoutSession MergeWorkout( WorkoutSession otherSession )
    {
        Log.v("WORKOUT SESSION", "Start of Method MergeWorkout");

        WorkoutSession mergedSession = new WorkoutSession();
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.US);

        /*--------------------------------------------------------------------
            Determine the lower start time
        --------------------------------------------------------------------*/

        try {
            Date thisStart = dateFormat.parse(this.startTime);
            Date otherStart = dateFormat.parse(otherSession.startTime);

            if (thisStart.after(otherStart))
                mergedSession.startTime = otherSession.startTime;
            else
                mergedSession.startTime = this.startTime;

            //region Debug
            Log.v("WORKOUT SESSION", "Start A: " + thisStart.toString()
                                     + "\nStart B: " + otherStart.toString()
                                     + "\nEarliest Start Time: " + mergedSession.startTime);
            //endregion

            /*--------------------------------------------------------------------
                Determine the later end time
            --------------------------------------------------------------------*/

            Date thisEnd = dateFormat.parse(this.endTime);
            Date otherEnd = dateFormat.parse(otherSession.endTime);

            if (thisEnd.before(otherEnd))
                mergedSession.endTime = otherSession.endTime;
            else
                mergedSession.endTime = this.endTime;

            //region Debug
            Log.v("WORKOUT SESSION", "End A: " + thisEnd.toString()
                                     + "\nEnd B: " + otherEnd.toString()
                                     + "\nLatest End Time: " + mergedSession.endTime);
            //endregion

        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        /*--------------------------------------------------------------------
            Merge all of the exercises into a single list
        --------------------------------------------------------------------*/

        mergedSession.exercises.addAll(this.exercises);
        mergedSession.exercises.addAll(otherSession.exercises);

        Log.v("WORKOUT SESSION", "End of Method MergeWorkout");

        return mergedSession;

    } // method

} // class
