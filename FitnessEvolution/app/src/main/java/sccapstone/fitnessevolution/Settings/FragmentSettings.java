package sccapstone.fitnessevolution.Settings;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.support.v4.app.Fragment;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

import sccapstone.fitnessevolution.Activites.ActivityMain;
import sccapstone.fitnessevolution.Activites.ActivityStartup;
import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.R;

/**
 * @author wagner, Tyler
 *
 * Fragment that allows the all of the main settings for the user.
 *   Allows the user to choose the following
 *
 *   Private Information (Name, Gender, Coach)
 *   Email
 *   Password
 *   Logout
 *
 *   Uses the friend_fragment layout
 */
public class FragmentSettings extends Fragment
        implements View.OnClickListener
{
    boolean userWantsLogout = false;
    String newPassword = null,
           newEmail = null;

    /**
     * Inflate the layout
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                         Bundle savedInstanceState)
    {
        Activity activity = getActivity();
        ((ActivityMain) activity).setActionBarTitle( getResources().getString(R.string.nav_settings));

        View view = inflater.inflate(R.layout.settings_activity_fragment, container, false);

        //Set all button onClick Listeners to this
        Button btnUpdateInfo = (Button) view.findViewById(R.id.update_user_info);
        Button btnUpdateEmail = (Button) view.findViewById(R.id.update_user_email);
        Button btnUpdatePassword = (Button) view.findViewById(R.id.update_user_password);
        Button btnLogout = (Button) view.findViewById(R.id.button_logout);
        btnUpdateInfo.setOnClickListener(this);
        btnUpdateEmail.setOnClickListener(this);
        btnUpdatePassword.setOnClickListener(this);
        btnLogout.setOnClickListener(this);

        // Hide the keyboard if it is currently shown
        Utilities.hideKeyboard(getContext(), getActivity());

        // Inflate the layout for this fragment
        return view;
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FragmentSettings newInstance(int sectionNumber)
    {
        Log.v("FragmentSettings", "newInstance");
        FragmentSettings fragment = new FragmentSettings();
        Bundle args = new Bundle();
        args.putInt(Utilities.ARG_SECTION_NUMBER_KEY, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Logs the user out of the app and sends them back to activity startup
     */
    public void btnLogout()
    {
        //Ensure the user wants to logout
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        // set title
        alertDialogBuilder.setTitle("Logout");

        // set dialog message
        alertDialogBuilder
                .setMessage("Are you sure you want to logout of " + Utilities.user.getUsername() + "?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        userWantsLogout = true;
                        if (userWantsLogout)
                        {//We want to uncache the user and logout of the app.
                            startStartupActivity();
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        userWantsLogout = false;
                        dialog.cancel();
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();

        Log.v("Logout?", "" + userWantsLogout);

    }//btnLogout(View)

    /**
     * Updates the email address of the user
     */
    private void updateEmail()
    {
        //Ask if the user wants to update his or her name only if the user changes both!
        if(newEmail == null)
        {
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // set title
        builder.setTitle("Update Email");
        // set dialog message
        builder
                .setMessage("Are you sure you want to update your email to " + newEmail + "?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.v("UpdateEmail, YES", newEmail);
                        Utilities.user.setEmail(newEmail);
                        Utilities.user.saveInBackground();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });
        // show it
        builder.show();
    }

    /**
     * On Click Button for update email.
     */
    public void btnUpdateEmail()
    {
        // Set up the input for updating email
        final EditText input = new EditText(getActivity());

        //Get the New Last Name
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        builder.setTitle("Enter New Email Address");
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                newEmail = input.getText().toString();
                Log.v("NewEmail", newEmail);
                updateEmail();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                newEmail = null;
                dialog.cancel();
            }
        });
        builder.show();
    }//btnUpdateEmail()

    /**
     * On Click button for updating the password
     */
    public void btnUpdatePassword()
    {
        if(Utilities.user.getEmail() == null
                || Utilities.user.getEmail().trim().equals(""))
        {
            Toast.makeText(getActivity(), "To reset password, you must first have an email address", Toast.LENGTH_LONG).show();
            return;
        }

        ParseUser.requestPasswordResetInBackground(Utilities.user.getEmail(), new RequestPasswordResetCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    // An email was successfully sent with reset instructions.
                    Toast.makeText(getActivity(), "An email has been sent to: " + Utilities.user.getEmail() + " to update password", Toast.LENGTH_LONG)
                            .show();
                } else {
                    // Something went wrong. Look at the ParseException to see what's up.
                    Toast.makeText(getActivity(), "An issue occurred, please check your internet connectivity", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
    }//btnUpdatePassword()

    /**
     * Starts up the Main Activity and end this Activity
     */
    public void startStartupActivity()
    {
        //Logs out and uncaches the user.
        ParseUser.logOut();

        Utilities.loggedIn = false;
        Utilities.user = ParseUser.getCurrentUser(); // make sure our global user is null

        Log.v("User Postlogout", "" + Utilities.user);

        //We have successfully logout of the application, do not close the app
        //But go back to the startup activity for logging in / registration
        //TODO: MAKe sure this works
        Intent intent = new Intent(getActivity().getApplicationContext(), ActivityStartup.class);
        startActivity(intent);
        getActivity().finish();
    }

    /**
     * Starts the update info fragment
     */
    private void btnUpdateInfo()
    {
        FragmentSettingsPersonal viewPersonalFrag = new FragmentSettingsPersonal().newInstance(10);
        FragmentTransaction ft = this.getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, (viewPersonalFrag));
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onAttach(Context activity) {
        Log.v("onAttach", "FragmentSettings");
        super.onAttach(activity);
        ((ActivityMain) activity).onSectionAttached(
                getArguments().getInt(Utilities.ARG_SECTION_NUMBER_KEY));
//        scrollToBottomOfListView();
    }

    /**
     * Dictates what the on click button should do.
     * @param v
     */
    @Override
    public void onClick(View v)
    {
        Log.v("onClick", "" + v.getId());
        Log.v("LogoutId", "" + R.id.button_logout);
        switch (v.getId())
        {
            case R.id.update_user_info: //TODO: Display Toast on Success
//                getName();
                btnUpdateInfo();
                break;
            case R.id.update_user_email: //TODO: Display Toast on Success
                btnUpdateEmail();
                break;
            case R.id.update_user_password: //TODO: Display Toast on Success
                btnUpdatePassword();
                break;
            case R.id.button_logout:    //Logging Out
                btnLogout();
                break;
            default:    //Somehow something Broke
                Log.v("Issue", "FragmentSettings: onClick default case");
                break;
        }
    }

}//FragmentSettings Class
