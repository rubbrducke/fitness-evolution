package sccapstone.fitnessevolution.Settings;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

//Parse Imports
import com.parse.ParseException;
import com.parse.SaveCallback;

import sccapstone.fitnessevolution.Activites.ActivityMain;
import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.R;

/**
 * @author wagner, Tyler
 *
 * Fragment that allows the user to change his or her personal settings.
 *
 *   Allows the user to change his or her gender, name, or coach.
 *
 *   Uses the settings_fragment_personal layout
 */
public class FragmentSettingsPersonal extends Fragment
    implements View.OnClickListener
{
    EditText eUsername,
             eLast_name,
             eFirst_name;
    RadioButton gender_male,
                gender_female;
    RadioButton coachMadeleine,
                coachChris,
                coachCarol;
    DatePicker dobDate;
    Button btnUpdateInfo;

    Context ct = null;


    /**
     * Inflates the Fragment and initializes the Views that interact with the user.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.settings_fragment_personal, container, false);

        eUsername = (EditText) view.findViewById(R.id.username);
        eLast_name = (EditText) view.findViewById(R.id.last_name);
        eFirst_name = (EditText) view.findViewById(R.id.first_name);
        gender_male = (RadioButton) view.findViewById(R.id.gender_male);
        gender_female = (RadioButton) view.findViewById(R.id.gender_female);
//        dobDate = (DatePicker) view.findViewById(R.id.dob);
        coachMadeleine = (RadioButton) view.findViewById(R.id.btnMadeleine);
        coachChris = (RadioButton) view.findViewById(R.id.btnChris);
        coachCarol = (RadioButton) view.findViewById(R.id.btnCarol);

        btnUpdateInfo = (Button) view.findViewById(R.id.btnUpdate);
        btnUpdateInfo.setOnClickListener(this);

        ct = this.getContext();

        initialize();

        return view;
    }

    /**
     * Sets all the text the users have already there
     */
    private void initialize()
    {
        if(Utilities.user != null)
        {
            eUsername.setText(Utilities.user.getUsername());

            eLast_name.setText(Utilities.user.get("last_name").toString());
            eFirst_name.setText(Utilities.user.get("first_name").toString());

            //Gender
            if(Utilities.user.get("gender").toString().equals("Male"))
                gender_male.setChecked(true);
            else
                gender_female.setChecked(true);

            //Coach
            if(Utilities.user.get("coach").toString().equals("Madeleine"))
                coachMadeleine.setChecked(true);
            else if(Utilities.user.get("coach").toString().equals("Chris"))
                coachChris.setChecked(true);
            else
                coachCarol.setChecked(true);
        }
    }

    /**
     * On Click button for Settings
     * @param v
     */
    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnUpdate:
                Log.v("Clicked Button", "");
                btnUpdateInformation();
                break;
        }
    }

    /**
     * Sends the new information to Parse to save
     */
    public void btnUpdateInformation()
    {
        String  username = null,
                name_last = null,
                name_first = null,
                dob = null,
                gender = null,
                coach = null;

        username = eUsername.getText().toString().toLowerCase();
        name_last = eLast_name.getText().toString();
        name_first = eFirst_name.getText().toString();

        //Get Gender
        if(gender_male.isChecked())
            gender = "Male";
        else if(gender_female.isChecked())
            gender = "Female";
//        else
//            gender = "Other";

        //Get Coach
        if(coachMadeleine.isChecked())
            coach = "Madeleine";
        else if(coachChris.isChecked())
            coach = "Chris";
        else if(coachCarol.isChecked())
            coach = "Carol";

        //If the Registration is not valid, do not register in Parse up
        if(!validatedRegistration(username, name_last, name_first,gender, coach))
            return;

        //Update My Information
        Utilities.user.setUsername(username);
//        user.setEmail(null);

        //Other fields
        Utilities.user.put("last_name", name_last);
        Utilities.user.put("first_name", name_first);
        Utilities.user.put("gender", gender);
        Utilities.user.put("coach", coach);


        Utilities.user.saveInBackground(new SaveCallback() {
            public void done(ParseException e)
            {
                if (e == null)
                {
                    Toast.makeText(ct, "Saved successful!", Toast.LENGTH_LONG).show();
                    Log.v("Registration", "Successful");
                }
                else
                {
                    // Update didn't succeed, print what went wrong
                    Toast.makeText(ct, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * Validates the new information that should be saved.
     *
     *   Returns true if is valid, otherwise false.
     * @param username
     * @param name_last
     * @param name_first
     * @param gender
     * @param coach
     * @return
     */
    private boolean validatedRegistration(String username, String name_last, String name_first,
                                          String gender, String coach)
    {
        String invalidText = null;
        int durationToast = Toast.LENGTH_SHORT;
        boolean validRegistration = true;

        if(username == null || username.trim().equals(""))
        {
            invalidText = "Username is not Valid";
            validRegistration = false;
        }
        else if(name_last == null || name_last.trim().equals("")
                || name_first == null || name_first.trim().equals(""))
        {
            invalidText = "Name is not Valid";
            validRegistration = false;
        }
        else if(gender == null)
        {
            invalidText = "Gender is not Valid";
            validRegistration = false;
        }
        else if(coach == null)
        {
            invalidText = "Coach is not Valid";
            validRegistration = false;
        }

        //If it wasn't a valid registration, show the message why
        if(!validRegistration)
            Utilities.toastMakeText(this.getContext(), invalidText, durationToast).show();

        return validRegistration;
    }

    @Override
    public void onAttach(Context activity) {
        Log.v("onAttach", "FragmentSettingsPersonal");
        super.onAttach(activity);
        ((ActivityMain) activity).onSectionAttached(
                getArguments().getInt(Utilities.ARG_SECTION_NUMBER_KEY));
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FragmentSettingsPersonal newInstance(int sectionNumber)
    {
        Log.v("FragmentSettingsPersonl", "newInstance");
        FragmentSettingsPersonal fragment = new FragmentSettingsPersonal();
        Bundle args = new Bundle();
        args.putInt(Utilities.ARG_SECTION_NUMBER_KEY, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }
}//FragmentSettingsPersonal Class
