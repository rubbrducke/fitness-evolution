package sccapstone.fitnessevolution.Activites;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.parse.ParseUser;

import sccapstone.fitnessevolution.Friends.Friend;
import sccapstone.fitnessevolution.Utilities.UtilitiesGeo;
import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.R;

/**
 * @author wagner, Tyler
 *
 * Activity that is first ran by the App.
 *   Uses the activity_startup layout
 *
 *   If the user has a cached log in, it immediatly finishes this activity
 *     and creates ActivityMain
 *   Otherwise, the user can select from registering or logging in.
 */
public class ActivityStartup extends AppCompatActivity
{
    //Unique Identifiers for Result on Activities
    private final int ACTIVITY_REGISTER = 1000;
    private final int ACTIVITY_SIGN_IN = 1001;


    /**
     * Inflate the Activity, Initialize all of the Global Classes.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);

//        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        Utilities.user = ParseUser.getCurrentUser();
        if (Utilities.user == null) {
            Log.v("PRBTEST", "ParseUser is null in startup");
        }

        //Set up the GooglePlayService for GeoLocation
        Utilities.geo = new UtilitiesGeo(this);

        Utilities.friend = new Friend();

        if(Utilities.user != null)
        {
            //The cached user might be updated by other means than the app itself,
            //and if so, we need to ensure he or she's profile is updated
            try
            {
                Utilities.user.fetch();
            }
            catch (Exception e)
            {
                Log.v("Execption for fetching", e.getMessage());
            }

            Log.v("User Cached", Utilities.user.getUsername());
            //ParseUser.logIn(Utilities.user.getUsername(), password);

            //Has a user already, go to Main Activity
            startMainActivity();
        }
        else
        {
            Log.v("User Cached", "null");
        }
    }

    /**
     * On Click Button for Registering an account for the app.
     * @param v
     */
    public void btnRegister(View v)
    {
        //Creates the registration activity
        Intent intent = new Intent(this.getApplicationContext(), ActivityRegistration.class);
        startActivityForResult(intent, ACTIVITY_REGISTER);
    }//btnRegister(View)

    /**
     * On Click Button for Signing Into the app.
     * @param v
     */
    public void btnSignIn(View v)
    {
        Intent intent = new Intent(this, ActivitySignIn.class);
        startActivityForResult(intent, ACTIVITY_SIGN_IN);
    }//btnSignIn(View)

    /**
     * Called when one of the two activities have finished
     *   to decide if we should enter the main part of the app or not.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch(requestCode)
        {
            case ACTIVITY_REGISTER:
                if (resultCode == RESULT_OK)
                {
                    Bundle res = data.getExtras();
                    if(res.getBoolean("successful") == true)
                        startMainActivity();
                }
                break;
            case ACTIVITY_SIGN_IN:
                if(resultCode == RESULT_OK)
                {
                    Bundle res = data.getExtras();
                    if(res.getBoolean("successful") == true)
                        startMainActivity();
                }
                break;
        }
    }

    /**
     * Starts up the Main Activity and end this Activity
     *   Decided by onActivityOnResult or if the user's information is cached.
     */
    public void startMainActivity()
    {
        Utilities.loggedIn = true;
        if(Utilities.user == null)
            Utilities.user = ParseUser.getCurrentUser();
        Toast.makeText(this, "Welcome " + Utilities.user.get("first_name"), Toast.LENGTH_SHORT).show();

        //Saving Geolocation After Logging In / Registering / Starting App
        Log.v("Si/R/Cached User", "Saving GeoLocation");
        Utilities.geo.saveLocation();

        //We have successfully logged into the application
        Intent intent = new Intent(this.getApplicationContext(), ActivityMain.class);
        startActivity(intent);
        finish();
    }
} //ActivityStartup Class
