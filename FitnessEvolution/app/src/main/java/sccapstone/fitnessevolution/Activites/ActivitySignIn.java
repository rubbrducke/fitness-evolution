package sccapstone.fitnessevolution.Activites;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

//Parse
import com.parse.ParseException;
import com.parse.ParseUser;

import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.R;

/**
 * @author wagner, Tyler
 *
 * Activity that allows the user to log in to the app with
 *   known username and password.
 *
 *   Uses the activity_signin layout
 */
public class ActivitySignIn extends AppCompatActivity
{
    EditText txtUsername = null;
    EditText txtPassword = null;

    /**
     * Inflate the Activity and create the EditTexts for username and password.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        txtUsername = (EditText) findViewById(R.id.username);
        txtPassword = (EditText) findViewById(R.id.password);
    }

    /**
     * On Click for Button Sign In
     *   Attempts to log the user in with the data from the EditTexts
     * @param v
     */
    public void btnSignIn(View v)
    {
        String username = null,
               password = null;
        username = txtUsername.getText().toString().toLowerCase();
        password = txtPassword.getText().toString();

        try
        {
            ParseUser.logIn(username, password);
            Utilities.loggedIn = true;

            //For Passback because of succeeded.
            Bundle bndle = new Bundle();
            bndle.putBoolean("successful", true);

            Intent intent = new Intent();
            intent.putExtras(bndle);
            setResult(RESULT_OK, intent);
            finish();

            Log.v("Login", "Authentication succeeded");
        }
        catch (ParseException e)
        {
            Utilities.loggedIn = false;
            Toast.makeText(this, "Sign-in Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
            Log.v("Login", "Auth failed: " + e.getMessage());
            return;
        }
    }

}
