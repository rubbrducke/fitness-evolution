package sccapstone.fitnessevolution.Activites;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

//Parse Imports
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.R;

/**
 * @author wagner, Tyler
 * Last updated by Ben Aaron on 4/26/2016
 *
 * Activity that allows the user to register an account for the app
 *   Uses the activity_registration layout
 *
 * Information during Registration:
 *      Username - Required
 *      Password - Required
 *      Gender   - Required
 *      Coach    - Required
 *      DOB      - Required
 */
public class ActivityRegistration extends AppCompatActivity
{
    final Context context = this;   //Used to change the Activities after successful registration
    EditText eUsername,
             ePassword,
             eLast_name,
             eFirst_name;
    RadioButton gender_male,
                gender_female;
    RadioButton coachMadeleine,
                coachChris,
                coachCarol;
    DatePicker dobDate;
    ImageButton coachInfo;
    String coach = null;
    String username = null,
            password = null,
            name_last = null,
            name_first = null,
            dob = null,
            gender = null;

    /**
     * Expands the XML layout file
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        eUsername = (EditText) findViewById(R.id.username);
        ePassword = (EditText) findViewById(R.id.password);
        eLast_name = (EditText) findViewById(R.id.last_name);
        eFirst_name = (EditText) findViewById(R.id.first_name);
        gender_male = (RadioButton) findViewById(R.id.gender_male);
        gender_female = (RadioButton) findViewById(R.id.gender_female);
        dobDate = (DatePicker) findViewById(R.id.dob);
        coachMadeleine = (RadioButton) findViewById(R.id.btnMadeleine);
        coachChris = (RadioButton) findViewById(R.id.btnChris);
        coachCarol = (RadioButton) findViewById(R.id.btnCarol);
        coachInfo = (ImageButton) findViewById(R.id.coachInfoButton);
        coachInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, "Please select one of the coaches below. This coach will be responsible for providing you information and tips to help you reach your fitness goals. Each of the coaches has their own unique personality and approach to coaching. (Beware Carol, she is rude, crude, and without limits. You have been warned!) You can change your coach later under Settings.", Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Coach Selection Information");
                builder.setMessage("Please select one of the coaches below. " +
                        "This coach will be responsible for providing you information and tips to help you reach your fitness goals. " +
                        "Each of the coaches has their own unique personality and approach to coaching. " +
                        "(Beware Carol, she is rude, crude, and without limits. You have been warned!) " +
                        "You can change your coach later under Settings.");
                builder.setCancelable(false);
                builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });

    }

    /**
     * Gathers the information from the user upon pressing the register button, checks it for appropriateness, and then
     * creates a new Parse user on the parse database and stores in the information inside of it.
     * @param v
     */
    public void btnRegister(View v) {
        username = eUsername.getText().toString().toLowerCase();
        password = ePassword.getText().toString();
        name_last = eLast_name.getText().toString();
        name_first = eFirst_name.getText().toString();

        //Get Gender
        if (gender_male.isChecked())
            gender = "Male";
        else if (gender_female.isChecked())
            gender = "Female";
//        else
//            gender = "Other";
        //Get Coach
        if (coachMadeleine.isChecked()) {
            coach = "Madeleine";
            //coachSelected = true;
            //Get Date of Birth, and Create it
            int year = dobDate.getYear(),
                    month = dobDate.getMonth() + 1,   //Zero Index Months
                    day = dobDate.getDayOfMonth();
            dob = String.valueOf(month) + "/"
                    + String.valueOf(day) + "/"
                    + String.valueOf(year);

            //Debug for Date of Birth by DatePicker
//        Log.v("Date of Birth", dob);
//        Log.v("Month", String.valueOf(dobDate.getMonth()));
//        Log.v("Year", String.valueOf(dobDate.getYear()));
//        Log.v("Day", String.valueOf(dobDate.getDayOfMonth()));
            final Context t = this;

            //If the Registration is not valid, do not register in Parse up
            if (!validatedRegistration(username, password, name_last, name_first, dob, gender, coach))
                return;

            //https://parse.com/docs/android/guide#users

            //Create User
            ParseUser user = new ParseUser();
            user.setUsername(username);
            user.setPassword(password);
//        user.setEmail(null);
            //Other fields
            user.put("last_name", name_last);
            user.put("first_name", name_first);
            user.put("gender", gender);
            user.put("dob", dob);
            user.put("coach", coach);

            user.signUpInBackground(new SignUpCallback() {
                public void done(ParseException e) {
                    if (e == null) {
                        //Start up the Rest of the App
                        registrationSuccessful();

//                    Toast.makeText(t, "Registration successful!", Toast.LENGTH_LONG).show();
                        Log.v("Registration", "Successful");
                    } else {
                        // Sign up didn't succeed, print what went wrong
                        Toast.makeText(t, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            });
        } else if (coachChris.isChecked()) {
            coach = "Chris";
            //Get Date of Birth, and Create it
            int year = dobDate.getYear(),
                    month = dobDate.getMonth() + 1,   //Zero Index Months
                    day = dobDate.getDayOfMonth();
            dob = String.valueOf(month) + "/"
                    + String.valueOf(day) + "/"
                    + String.valueOf(year);

            //Debug for Date of Birth by DatePicker
//        Log.v("Date of Birth", dob);
//        Log.v("Month", String.valueOf(dobDate.getMonth()));
//        Log.v("Year", String.valueOf(dobDate.getYear()));
//        Log.v("Day", String.valueOf(dobDate.getDayOfMonth()));
            final Context t = this;

            //If the Registration is not valid, do not register in Parse up
            if (!validatedRegistration(username, password, name_last, name_first, dob, gender, coach))
                return;

            //https://parse.com/docs/android/guide#users

            //Create User
            ParseUser user = new ParseUser();
            user.setUsername(username);
            user.setPassword(password);
//        user.setEmail(null);
            //Other fields
            user.put("last_name", name_last);
            user.put("first_name", name_first);
            user.put("gender", gender);
            user.put("dob", dob);
            user.put("coach", coach);

            user.signUpInBackground(new SignUpCallback() {
                public void done(ParseException e) {
                    if (e == null) {
                        //Start up the Rest of the App
                        registrationSuccessful();

//                    Toast.makeText(t, "Registration successful!", Toast.LENGTH_LONG).show();
                        Log.v("Registration", "Successful");
                    } else {
                        // Sign up didn't succeed, print what went wrong
                        Toast.makeText(t, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            });
            //coachSelected = true;
        } else if (coachCarol.isChecked()) {
            coach = "Carol";
            final Context t = this;
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder
                    .setTitle("Registration: Coach Selection")
                    .setMessage("Are you sure that you would like to pick Coach Carol? She is rude, abusive, and without limits. You have been warned.")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Get Date of Birth, and Create it
                            int year = dobDate.getYear(),
                                    month = dobDate.getMonth() + 1,   //Zero Index Months
                                    day = dobDate.getDayOfMonth();
                            dob = String.valueOf(month) + "/"
                                    + String.valueOf(day) + "/"
                                    + String.valueOf(year);

                            //If the Registration is not valid, do not register in Parse up
                            if (!validatedRegistration(username, password, name_last, name_first, dob, gender, coach))
                                return;

                            //https://parse.com/docs/android/guide#users

                            //Create User
                            ParseUser user = new ParseUser();
                            user.setUsername(username);
                            user.setPassword(password);
//        user.setEmail(null);
                            //Other fields
                            user.put("last_name", name_last);
                            user.put("first_name", name_first);
                            user.put("gender", gender);
                            user.put("dob", dob);
                            user.put("coach", coach);

                            user.signUpInBackground(new SignUpCallback() {
                                public void done(ParseException e) {
                                    if (e == null) {
                                        //Start up the Rest of the App
                                        registrationSuccessful();

//                    Toast.makeText(t, "Registration successful!", Toast.LENGTH_LONG).show();
                                        Log.v("Registration", "Successful");
                                    } else {
                                        // Sign up didn't succeed, print what went wrong
                                        Toast.makeText(t, e.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                            dialog.cancel();
                        }
                    })
                    .setNeutralButton("No, Coach Chris", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            coach = "Chris";
                            //Get Date of Birth, and Create it
                            int year = dobDate.getYear(),
                                    month = dobDate.getMonth() + 1,   //Zero Index Months
                                    day = dobDate.getDayOfMonth();
                            dob = String.valueOf(month) + "/"
                                    + String.valueOf(day) + "/"
                                    + String.valueOf(year);

                            //If the Registration is not valid, do not register in Parse up
                            if (!validatedRegistration(username, password, name_last, name_first, dob, gender, coach))
                                return;

                            //https://parse.com/docs/android/guide#users

                            //Create User
                            ParseUser user = new ParseUser();
                            user.setUsername(username);
                            user.setPassword(password);
//        user.setEmail(null);
                            //Other fields
                            user.put("last_name", name_last);
                            user.put("first_name", name_first);
                            user.put("gender", gender);
                            user.put("dob", dob);
                            user.put("coach", coach);

                            user.signUpInBackground(new SignUpCallback() {
                                public void done(ParseException e) {
                                    if (e == null) {
                                        //Start up the Rest of the App
                                        registrationSuccessful();

//                    Toast.makeText(t, "Registration successful!", Toast.LENGTH_LONG).show();
                                        Log.v("Registration", "Successful");
                                    } else {
                                        // Sign up didn't succeed, print what went wrong
                                        Toast.makeText(t, e.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                            dialog.cancel();
                        }
                    })
                    .setNegativeButton("No, Coach Madeleine", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            coach = "Madeleine";
                            //Get Date of Birth, and Create it
                            int year = dobDate.getYear(),
                                    month = dobDate.getMonth() + 1,   //Zero Index Months
                                    day = dobDate.getDayOfMonth();
                            dob = String.valueOf(month) + "/"
                                    + String.valueOf(day) + "/"
                                    + String.valueOf(year);

                            //If the Registration is not valid, do not register in Parse up
                            if (!validatedRegistration(username, password, name_last, name_first, dob, gender, coach))
                                return;

                            //https://parse.com/docs/android/guide#users

                            //Create User
                            ParseUser user = new ParseUser();
                            user.setUsername(username);
                            user.setPassword(password);
//        user.setEmail(null);
                            //Other fields
                            user.put("last_name", name_last);
                            user.put("first_name", name_first);
                            user.put("gender", gender);
                            user.put("dob", dob);
                            user.put("coach", coach);

                            user.signUpInBackground(new SignUpCallback() {
                                public void done(ParseException e) {
                                    if (e == null) {
                                        //Start up the Rest of the App
                                        registrationSuccessful();

//                    Toast.makeText(t, "Registration successful!", Toast.LENGTH_LONG).show();
                                        Log.v("Registration", "Successful");
                                    } else {
                                        // Sign up didn't succeed, print what went wrong
                                        Toast.makeText(t, e.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                            dialog.cancel();
                        }
                    })
                    .setCancelable(false);

            builder.show();

        }
    }

    /**
     * Finishes the activity upon successfully registering
     */
    public void registrationSuccessful()
    {
        //For Passback because successful registration.
        Bundle bndle = new Bundle();
        bndle.putBoolean("successful", true);

        Intent intent = new Intent();
        intent.putExtras(bndle);
        setResult(RESULT_OK, intent);

        finish();
    }//registrationSuccessful()

    /**
     * Method to check if the data given by the user is appropriate for the fields in the parse database
     * @param username
     * @param password
     * @param name_last
     * @param name_first
     * @param dob
     * @param gender
     * @param coach
     * @return whether or not the user information is appropriate
     */
    private boolean validatedRegistration(String username, String password,
                                          String name_last, String name_first,
                                          String dob, String gender, String coach)
    {
        String invalidText = null;
        int durationToast = Toast.LENGTH_SHORT;
        boolean validRegistration = true;

        if(username == null || username.trim().equals(""))
        {
            invalidText = "Username is not Valid";
            validRegistration = false;
        }
        else if(password == null || password.trim().equals("") || password.trim().length() < 3)
        {
            invalidText = "Password is not Valid, must be greater than three characters";
            validRegistration = false;
        }
        else if(name_last == null || name_last.trim().equals("")
                || name_first == null || name_first.trim().equals(""))
        {
            invalidText = "Name is not Valid";
            validRegistration = false;
        }
        else if(dob == null)
        {//TODO: CHECK the Date past today
            invalidText = "Date of Birth is not Valid";
            validRegistration = false;
        }
        else if(gender == null)
        {
            invalidText = "Gender is not Valid";
            validRegistration = false;
        }
        else if(coach == null)
        {
            invalidText = "Coach is not Valid";
            validRegistration = false;
        }

        //If it wasn't a valid registration, show the message why
        if(!validRegistration)
            Utilities.toastMakeText(this, invalidText, durationToast).show();

        return validRegistration;
    }
}//ActivityRegistration Class
