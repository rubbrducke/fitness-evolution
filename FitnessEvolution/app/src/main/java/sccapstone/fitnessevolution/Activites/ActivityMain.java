package sccapstone.fitnessevolution.Activites;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;

import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import sccapstone.fitnessevolution.Coach.FragmentMyFitnessPage;
import sccapstone.fitnessevolution.About.FragmentAbout;
import sccapstone.fitnessevolution.Friends.FragmentFriends;
import sccapstone.fitnessevolution.Messaging.FragmentInbox;
import sccapstone.fitnessevolution.UserProfile.FragmentMyProfile;
import sccapstone.fitnessevolution.PeopleNearMe.FragmentPeopleNearMe;
import sccapstone.fitnessevolution.Settings.FragmentSettings;
import sccapstone.fitnessevolution.Logging.FragmentWorkoutDietaryLogging;
import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.Histogram.FragmentHistogram;
import sccapstone.fitnessevolution.Navigation.NavigationDrawerFragment;
import sccapstone.fitnessevolution.Parse.ParseErrorHandler;
import sccapstone.fitnessevolution.R;


public class ActivityMain extends AppCompatActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    public static final String TAG = "ActivityMain";

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onStart() {
        super.onStart();

        Log.v("parse installation", "updating user");
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("user", ParseUser.getCurrentUser().getUsername());
        installation.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    Log.v("parse installation", "Successful save of installation");
                } else {
                    Log.v("parse installation", "Unsuccessful save of installation: " + e.getMessage());

                    ParseErrorHandler.handleParseError(e, ActivityMain.this);
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.v("push:" + TAG, "starting onCreate");
        Bundle extras = null;
        int toOpen = -1;
        Log.v("push:" + TAG, "initialize toOpen to -1");
        if(getIntent().getExtras() != null) {
            Log.v("push:"+TAG,"intent has extras");
            extras = getIntent().getExtras();
            Log.v("push:"+TAG,"got the extras");
            if(extras.containsKey("openFromNotification")) {
                Log.v("push:"+TAG,"found an extra with key: openFromNotification");
                toOpen = extras.getInt("openFromNotification");
                Log.v("push:"+TAG,"set the extra to toOpen: "+toOpen);
            }
        }

        setContentView(R.layout.main_activity);

        Log.v("parse installation", "updated user: " + ParseInstallation.getCurrentInstallation().get("user"));

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                    getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        Log.v("push:" + TAG, "checking toOpen");
        if(toOpen != -1) {
            Log.v("push:"+TAG,"toOpen is not null");
            onNavigationDrawerItemSelected(toOpen);
            Log.v("push:" + TAG, "opened fragment from notification");
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) { //https://guides.codepath.com/android/Creating-and-Using-Fragments
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();

        //Log.v("PRBTEST", "onNavigationDrawerItemSelected: position: " + position);

        switch (position) {
            case Utilities.NAV_COACH:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, FragmentMyFitnessPage.newInstance(position + 1))
                        .addToBackStack(null)
                        .commit();
                break;
            case Utilities.NAV_MESSAGES:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, FragmentInbox.newInstance(position + 1))
                        .addToBackStack(null)
                        .commit();
                break;
            case Utilities.NAV_LOGGING:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, FragmentWorkoutDietaryLogging.newInstance(position + 1))
                        .addToBackStack(null)
                        .commit();
                break;
            case Utilities.NAV_HISTOGRAM:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, FragmentHistogram.newInstance(position + 1))
                        .addToBackStack(null)
                        .commit();
                break;
            case Utilities.NAV_MY_FRIENDS:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, FragmentFriends.newInstance(position + 1))
                        .addToBackStack(null)
                        .commit();
                break;
            case Utilities.NAV_PEOPLE_NEAR_ME:

                //Starts up the Map Activity
//                Intent intent = new Intent(this.getApplicationContext(), ActivityMap.class);
//                startActivity(intent);

                fragmentManager.beginTransaction()
                        .replace(R.id.container, FragmentPeopleNearMe.newInstance(position + 1))
                        .addToBackStack(null)
                        .commit();
                break;
            case Utilities.NAV_MY_PROFILE:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, FragmentMyProfile.newInstance(position + 1), "FRAG_MY_PROFILE")
                        .addToBackStack(null)
                        .commit();
                break;
            case Utilities.NAV_SETTINGS:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, FragmentSettings.newInstance(position + 1))
                        .addToBackStack(null)
                        .commit();
                break;
            case Utilities.NAV_ABOUT:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, FragmentAbout.newInstance(position + 1))
                        .addToBackStack(null)
                        .commit();
                break;
            default:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                        .addToBackStack(null)
                        .commit();
                break;
        }

    }


    //Each number corresponds to one specific toolbar menu item that can be selected
    // These are the strings that appear to the user in the navigation drawer
    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.nav_coach);
                break;
            case 2:
                mTitle = getString(R.string.nav_coach_messages);
                break;
            case 3:
                mTitle = getString(R.string.nav_coach_routine);
                break;
            case 4:
                mTitle = getString(R.string.nav_histogram);
                break;
            case 5:
                mTitle = getString(R.string.nav_my_friends);
                break;
            case 6:
                mTitle = getString(R.string.nav_social_nearme);
                break;
            case 7:
                mTitle = getString(R.string.nav_my_profile);
                break;
            case 8:
                mTitle = getString(R.string.nav_settings);
                break;
            case 9:
                mTitle = getString(R.string.nav_about);
                break;
        }
    }

    public void setActionBarTitle(String title) {
        mTitle = title;
        restoreActionBar();
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.ic_drawer);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Adds the Menu Options for Settins, the Colon in the top-right
//        if (!mNavigationDrawerFragment.isDrawerOpen()) {
//            // Only show items in the action bar relevant to this screen
//            // if the drawer is not showing. Otherwise, let the drawer
//            // decide what to show in the action bar.
//            getMenuInflater().inflate(R.menu.activity_main, menu);
//            restoreActionBar();
//            return true;
//        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
/*        if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Log.v("PRBTEST", "BACK BUTTON PRESSED. getFragmentManager().getBackStackEntryCount(): " + getFragmentManager().getBackStackEntryCount());

        //1 because the My Fitness Page fragment is inflated initially
        //and we need to exit on that fragment after logged in.
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            this.finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(Utilities.ARG_SECTION_NUMBER_KEY, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.main_activity_fragment, container, false);
            return rootView;

        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((ActivityMain) activity).onSectionAttached(
                    getArguments().getInt(Utilities.ARG_SECTION_NUMBER_KEY));
        }

    }

}
