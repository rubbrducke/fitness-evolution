package sccapstone.fitnessevolution.Activites;

//Github Site
//https://github.com/googlemaps/android-samples/blob/

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.R;
import sccapstone.fitnessevolution.Utilities.UtilitiesMaps;
//Google Maps Activity

/**
 * A simple activity with a map
 */
public class ActivityMap extends AppCompatActivity
        implements OnMapReadyCallback,
        //For GooglePlayServices
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{
    private final int MY_LOCATION_REQUEST_CODE = 1;
    private boolean isMapReady = false;
    private Location myLocation = null;
    private GoogleMap map;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Sets up Google Play Services to get My Current Location
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
    }

    /**
     * Maps is ready to go
     */
    @Override
    public void onMapReady(GoogleMap map)
    {
        this.map = map;
        this.isMapReady = true;

        Log.v("Loc_Permissions?", Boolean.toString(Utilities.geo.hasLocationPermission(this)));

        //To get the compass button clickable to go to my location (o-)
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED)
        {
            map.setMyLocationEnabled(true);
        } else
        {
            // Show rationale and request permission.
            Toast.makeText(this, "Fine Location is not granted", Toast.LENGTH_SHORT).show();
        }

        //Query People
        ParseGeoPoint userLocation = (ParseGeoPoint) Utilities.user.get("last_location");
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        //Find the people near me
        query.whereNear("last_location", userLocation);
        //Ensure we don't pull the last location of ourself
        query.whereNotEqualTo("username", Utilities.user.getUsername());
        query.setLimit(10);
        final GoogleMap m = this.map;
        query.findInBackground(new FindCallback<ParseUser>() {
            public void done(List<ParseUser> dataList, ParseException e) {
                if (e == null)
                {
                    //No Errors
                    Log.v("Num Users Collected", String.valueOf(dataList.size()));
                    for (ParseUser parseUsr : dataList)
                    {
//                        if(parseUsr.getUsername().equalsIgnoreCase("prestonTestDoNotDelete"))
//                        {
                            ParseGeoPoint gp = (ParseGeoPoint) parseUsr.get("last_location");
                            LatLng person = new LatLng(gp.getLatitude(), gp.getLongitude());
                            m.addMarker(UtilitiesMaps.createMarkerPerson(parseUsr, person));
//                        }
//                        else
//                        {
//                            ParseGeoPoint gp = (ParseGeoPoint) parseUsr.get("last_location");
//                            LatLng person = new LatLng(gp.getLatitude(), gp.getLongitude());
//                            m.addMarker(new MarkerOptions()
//                                    .title(parseUsr.getUsername())
//                                    .snippet(parseUsr.getString("last_name") + ", " + parseUsr.getString("first_name"))
//                                    .position(person));
//                        }
                    }
                }
                else
                {
                    Log.v("Parse Query Geo Error", e.getMessage());
                }
            }
        });

//        LatLng africa = new LatLng(0.0, 0.0);
//        map.moveCamera(CameraUpdateFactory.newLatLngZoom(africa, 13));
//        map.addMarker(new MarkerOptions()
//                .title("0.0-0.0")
//                .snippet("Lat / Long: 0,0")
//                .position(africa));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_LOCATION_REQUEST_CODE) {
            if (permissions.length == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                map.setMyLocationEnabled(true);
            }
            else
            {
                // Permission was denied. Display an error message.
                Toast.makeText(this, "Error: Check your GPS or Internet Connection", Toast.LENGTH_SHORT)
                        .show();
            }
        }

    }


    //For
    @Override
    public void onConnected(Bundle connectionHint)
    {
        this.myLocation = LocationServices.FusedLocationApi.getLastLocation
                (mGoogleApiClient);
        if (myLocation != null && isMapReady)
        {
            double lat = myLocation.getLatitude();
            double lon = myLocation.getLongitude();
            Log.v("Lat", String.valueOf(lat));
            Log.v("Long", String.valueOf(lon));
            LatLng myLocation = new LatLng(this.myLocation.getLatitude(), this.myLocation.getLongitude());
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 17));

//            ParseGeoPoint userGeopoint = new ParseGeoPoint(lat, lon);
//            Utilities.user.put("last_location", userGeopoint);
//            Utilities.user.saveInBackground();

//            ParseRelation relation = Global_Settings.user.getRelation("last_location");
//            ParseObject obj = new ParseObject("_User");
//
//            obj.add("username", Global_Settings.user.getUsername());
//            obj.add("last_location", myGeopoint);
//            relation.add(obj);
//            obj.saveInBackground();
//            relation.add(obj);
//            Global_Settings.user.();
//            Global_Settings.user.saveInBackground();

//            https://www.google.com/maps/@lat,long,15z
//            mLatitudeText.setText(String.valueOf(mLastLocation.getLatitude()));
//            mLongitudeText.setText(String.valueOf(mLastLocation.getLongitude()));
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

}
    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
//    private void enableMyLocation(GoogleMap map) {
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED) {
//            // Permission to access the location is missing.
//            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
//                    Manifest.permission.ACCESS_FINE_LOCATION, true);
//        } else if (map != null) {
//            // Access to the location has been granted to the app.
//            map.setMyLocationEnabled(true);
//        }
//    }

//public class MapActivity extends Activity implements OnMapReadyCallback {
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_map);
//
//        MapFragment mapFragment = (MapFragment) getFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
//    }
//
//    @Override
//    public void onMapReady(GoogleMap map) {
//        LatLng sydney = new LatLng(-33.867, 151.206);
//
//        map.setMyLocationEnabled(true);
//        map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 13));
//
//        map.addMarker(new MarkerOptions()
//                .title("Sydney")
//                .snippet("The most populous city in Australia.")
//                .position(sydney));
//    }
//}

//public class MapActivity extends AppCompatActivity {
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_map);
//    }
//}
