package sccapstone.fitnessevolution.Friends;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.Parse.ParseApplication;

/**
 * @author wagner, Tyler
 *
 * Class that holds information about the user's friend list.
 *   Functionality includes:
 *      Adding, removing, and detecting who is a friend.
 *
 */
public class Friend
{
    private ParseObject friendObj;
    private ArrayList<String> friendIds;
    private ArrayList<String> friendStatus;

    private ArrayList<ParseUser> friendList;

    private final String STATUS_ADDED = "ADDED";
    private final String STATUS_PENDING = "PENDING";
    private final String STATUS_REMVOED = "REMOVED";

    /**
     * Calls to Parse to gather the friends
     */
    public Friend()
    {
        friendIds = new ArrayList<>();
        friendStatus = new ArrayList<>();
        friendList = new ArrayList<>();

        getFriends(null, false);
    }

    public void getFriends(final ArrayAdapter<Object> friendAdapter, final boolean append)
    {
//<<<<<<< HEAD
//        //Get friends that I have added
//        ParseQuery<ParseObject> query  = new ParseQuery<ParseObject>("Friend");
//        query.whereEqualTo("id_u", Utilities.user.getObjectId());
//        query.findInBackground(new FindCallback<ParseObject>() {
//            @Override
//            public void done(List<ParseObject> objects, ParseException e) {
//                if (e == null) {
//                    Log.v("Found Friends", "" + objects.size());
//
//                    if (objects.size() != 0)
//                    {
//                        if ( !append )
//                        {
//                            friendIds.clear();
//                            friendStatus.clear();
//                            friendList.clear();
//                        }
//
//                        friendObj = objects.get(0);
//
//                        friendIds.addAll((ArrayList<String>) objects.get(0).get("id_friends"));
//                        friendStatus.addAll((ArrayList<String>) objects.get(0).get("status"));
//
//                        Utilities.getUserByObjectId(friendIds, friendList, friendAdapter, append);

        if (Utilities.user != null) {
            //Get friends that I have added
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Friend");
            query.whereEqualTo("id_u", Utilities.user.getObjectId());
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> objects, ParseException e) {
                    if (e == null) {
                        Log.v("Found Friends", "" + objects.size());

                        if (objects.size() != 0)
                        {

                            if ( !append )
                            {
                                friendIds.clear();
                                friendStatus.clear();
                                friendList.clear();
                            }

                            friendObj = objects.get(0);

                            friendIds.addAll((ArrayList<String>) objects.get(0).get("id_friends"));
                            friendStatus.addAll((ArrayList<String>) objects.get(0).get("status"));

                            Utilities.getUserByObjectId(friendIds, friendList, friendAdapter, append);
                        }
                        else
                        {//The User has no friends, create them a new ParseObject with the initial id
                            friendObj = new ParseObject("Friend");
                            friendObj.put("id_u", Utilities.user.getObjectId());
                        }
                    } else {
                    }
                }
            });
        }
    }

    //To Avoid Duplicates
    public void updateFriends()
    {
        friendObj.put("id_friends", friendIds);
        friendObj.put("status", friendStatus);
        friendObj.saveInBackground();
    }


    /************************** Adding Friends ******************************/
    /**
     * Add Friend by Username
     * @param ct
     * @param username
     */
    public void addFriend(final Context ct, final String username)
    {
        //TODO: Test for Logic of Friend: Currently Adding First Time

        //Add Friend for first time, but getting their information
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("username", username.toLowerCase());
        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> users, ParseException e) {
                if (e == null) {
                    //Check for Empty List
                    if (users.size() == 0)
                    {//Could not find the user;
                        Utilities.toastMakeText(ct,
                                                "User: \'" + username + "\' does not exist",
                                                Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Log.v("Found User", users.get(0).getUsername());
                        ParseUser userToAdd = users.get(0);

                        if(isFriend(userToAdd.getObjectId()) == -1)
                        {
                            Utilities.toastMakeText(ct,
                                                    "Adding: \'" + username + "\'",
                                                    Toast.LENGTH_SHORT).show();
                            addFriendFirstTime(userToAdd);  //Add the first time
                        }
                        else
                            Utilities.toastMakeText(ct,
                                                    "User: \'" + username + "\' is already a friend",
                                                    Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.v("Query Find User Error", e.getMessage());
                }
            }
        });
    }//addFriend(final Context, final String)

    private void addFriendFirstTime(ParseUser userToAdd)
    {
        if(userToAdd == null)
        {
            Log.v("addFirstTime", "null");
            return;
        }

        //Update the objIds, status and friendsList
        friendIds.add(userToAdd.getObjectId());
        friendStatus.add("added");
        friendList.add(userToAdd);

        updateFriends();

        ParseApplication.sendClientPush(userToAdd.getUsername(), Utilities.user.getUsername(), "friend", "added you.");

        //TODO: ADD PENDING REQUEST
        //User add's me (logic)
//        otherAddFriend.put("id_u", userToAdd.getObjectId());
//        otherAddFriend.put("id_friends", Utilities.user.getObjectId());
//        otherAddFriend.put("status", "added");

//        otherAddFriend.saveInBackground(); //TODO
    }
    /**************************** Adding Friends ******************************/

    /************************** Removing Friends ******************************/
    public void removeFriend(Context ct, int position)
    {
        //TODO: MAKE SURE IN BOUNDS, shouldn't ever fail, also change to 'deleted'
        ParseUser removedUser = this.getFriendFromFriendsList( friendIds.get(position) );

        //Remove the Friend at the position
        friendIds.remove(position);
        friendStatus.remove(position);
        friendList.remove(removedUser);
        this.updateFriends();

        Utilities.toastMakeText(ct, "Removed Friend: \'" + removedUser.getUsername() + "\'",
                                Toast.LENGTH_SHORT).show();
    }

    public void removeFriend(Context ct, String objectID)
    {
        int friendPos = isFriend(objectID);

        if( friendPos == -1 )
        {
            Log.v("removeFriend", "objectId not found: " + objectID);
            return;
        }

        //Remove the friend
        removeFriend(ct, friendPos);
    }


    /************************** Removing Friends ******************************/


    /************************** Helper Methods ********************************/

    /**
     * Returns the position in the List if the friend is added
     * Otherwise, return -1
     * @param objectId
     * @return
     */
    public int isFriend(String objectId)
    {
        int returnedPos = -1;

        for(int i = 0; friendIds != null && i < friendIds.size(); i++)
        {
            if( objectId.equals(friendIds.get(i)) )
                return i;
        }

        return returnedPos;
    }

    /**
     * Use for accessing from friendsList
     *   because friendsList's postions can be different than objectId and status
     * @param objectId
     * @return
     */
    private ParseUser getFriendFromFriendsList(String objectId)
    {
        ParseUser returnedUser = null;

        for(int i = 0; i < friendList.size(); i++)
            if( friendList.get(i).getObjectId().equals(objectId) )
                return friendList.get(i);

        return returnedUser;
    }

    /************************** Helper Methods ********************************/

    public ArrayList<ParseUser> getFriendList()
    {
        return friendList;
    }

}//Friend Class
