package sccapstone.fitnessevolution.Friends;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;

import sccapstone.fitnessevolution.Messaging.FragmentComposeMessage;
import sccapstone.fitnessevolution.Messaging.FragmentMessageThread;
import sccapstone.fitnessevolution.UserProfile.FragmentViewProfile;
import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.R;

/**
 * @author wagner, Tyler
 *
 * Extends an Array Adapter so we can store the friends here.
 *   The friends will be displayed in the FragmentFriends Fragment.
 *
 *   Uses the friends_list_item_friend layout
 */
public class CustomFriendAdapter extends ArrayAdapter<ParseUser>
{
    private ArrayList<ParseUser> friendsList;
    private FragmentFriends mFragFriends;

    /**
     * Initialize the Data
     * @param context
     * @param users
     * @param fragFriends
     */
    public CustomFriendAdapter(Context context, ArrayList<ParseUser> users, FragmentFriends fragFriends)
    {
        super(context, 0, users);

        //Setup the Friends List
        friendsList = users;
        mFragFriends = fragFriends;
    }

    /**
     * Inflate the ArrayAdapater part
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {

        // Get the data item for this position
        ParseUser user = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.friends_list_item_friend, parent, false);
        }

        String name = user.get("first_name") + " " + user.get("last_name");
        Bitmap image = null;

        // Lookup view for user
        ImageView friendAvatar = (ImageView) convertView.findViewById(R.id.friend_avatar);
        TextView userId = (TextView) convertView.findViewById(R.id.friend_username);
        TextView userName = (TextView) convertView.findViewById(R.id.friend_name);
        Button messageFriend = (Button) convertView.findViewById(R.id.btn_message_friend);
        messageFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getContext(), "Sending message to user: " + friendsList.get(position).getUsername() + " objID: " + friendsList.get(position).getObjectId(),Toast.LENGTH_SHORT).show();
                Log.v("Trying to send message", "");
                sendMessageToFriend(friendsList.get(position).getObjectId(), friendsList.get(position).getUsername());
            }
        });

        // Populate the data into the view
        userId.setText(user.getUsername());
        userName.setText(name);

        image = Utilities.getAvatar(getContext(), user);
        friendAvatar.setImageBitmap(image);

        //Sets the Clickable and on Long Click Listener
        convertView.setLongClickable(true);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getContext(), "You Clicked " + friendsList.get(position).getUsername(), Toast.LENGTH_LONG).show();
                Log.v("PRBTEST", "Calling new instancce of FragmentViewProfile with objid: " + friendsList.get(position).getObjectId().toString());
                FragmentViewProfile viewProfileFrag = new FragmentViewProfile().newInstance(friendsList.get(position).getObjectId());
                FragmentTransaction ft = mFragFriends.getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, (viewProfileFrag));
                ft.addToBackStack(null);
                ft.commit();
            }
        });


        // Return the completed view to render on screen
        return convertView;
    }

    /**
     * Tell the array adapter to refresh.
     *
     * This is during any removing / adding friend.
     * @param results
     */
    public void updateResults(ArrayList<ParseUser> results)
    {
        friendsList = results;

        //Triggers the list update
        notifyDataSetChanged();
    }

    /*
        sendMessageToFriend Author: Preston Barbare
     */
    private static ArrayList<String> arrConvParticipantObjIDs; // Must be a member var due to Parse API.
    public void sendMessageToFriend(String friendObjID, final String friendUserName) {
        // Determine if we need to add to a message thread, or start a new one.
        arrConvParticipantObjIDs = new ArrayList<String>();
        arrConvParticipantObjIDs.add(ParseUser.getCurrentUser().getObjectId());
        arrConvParticipantObjIDs.add(friendObjID);

        ParseQuery<ParseObject> sentMessageQuery = ParseQuery.getQuery("Conversations");
        sentMessageQuery.whereContainsAll("convParticipantObjectIDs", arrConvParticipantObjIDs);

        //Log.v("PRBTEST","Querying table for a record with convParticipantObjectIDs of " + ParseUser.getCurrentUser().getObjectId() + " and " + friendObjID);

        sentMessageQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject messageThreadRecord, ParseException e) {
                if (e == null) {
                    // A conversation with this friend already exists, so launch the message thread fragment for that conversation
                    FragmentMessageThread newFragMessageThread = new FragmentMessageThread().newInstance(messageThreadRecord.getObjectId(), friendUserName);
                    FragmentTransaction ft = mFragFriends.getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, (newFragMessageThread));
                    ft.addToBackStack(null);
                    ft.commit();
                } else {
                    // No previous conversation with this friend, so launch compose fragment.
                    FragmentTransaction ft = mFragFriends.getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, (new FragmentComposeMessage().newInstance(friendUserName)));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });
    }
}//CustomFriendAdapter
