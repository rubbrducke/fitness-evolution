package sccapstone.fitnessevolution.Friends;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import com.parse.ParseUser;

import sccapstone.fitnessevolution.Activites.ActivityMain;
import sccapstone.fitnessevolution.UserProfile.FragmentViewProfile;
import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.R;

/**
 * @author wagner, Tyler
 *
 * Fragment that allows the displays all of the users in a listview.
 *   Allows the user to add or remove friends.
 *
 *   Uses the friend_fragment layout
 */
public class FragmentFriends extends Fragment
        implements View.OnClickListener
{
    private Friend myFriends;
    private CustomFriendAdapter friendAdapter = null;

    //For Menu Options
    private final int ID_VIEW_PROFILE = 0,
                      ID_MESSAGE_USER = 1,
                      ID_REMOVE_FRIEND = 2;

    /**
     * Initializes the Views that are being used in this fragment.
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        myFriends = Utilities.friend;

        friendAdapter  = new CustomFriendAdapter(this.getContext(), myFriends.getFriendList(), this);


        //Test the view
//        friendAdapter.add(Utilities.user);
        myFriends.getFriends((ArrayAdapter) friendAdapter, false);
    }

    /**
     * Inflates the Layout and context menu.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState)
    {

        Activity activity = getActivity();
        ((ActivityMain) activity).setActionBarTitle( getResources().getString(R.string.nav_my_friends));

        View view = inflater.inflate(R.layout.friends_fragment, container, false);


        //Set all button onClick Listeners to this
        Button btnAddFriend = (Button) view.findViewById(R.id.btn_add_friend);
        Button btnRefresh = (Button) view.findViewById(R.id.btn_refresh);
        btnAddFriend.setOnClickListener(this);
        btnRefresh.setOnClickListener(this);

        //Set up the ListView and Array Adapter
        // Attach the adapter to a ListView and register for context menu
        ListView lv = (ListView) view.findViewById(R.id.listview_my_friends_list);
        lv.setAdapter(friendAdapter);
        registerForContextMenu(lv);

        // Hide the keyboard if it is currently shown
        Utilities.hideKeyboard(getContext(), getActivity());

        // Inflate the layout for this fragment
        return view;
    }

    /**
     * On Click for Add Friend Button.
     *   Allows the user to add someone if they know their username.
     */
    public void btnAddFriend()
    {
        //Sets up the input
        final EditText input = new EditText(getActivity());
        //Get the New First Name
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
        builder.setTitle("Specify User Name");
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String userNameToAdd = input.getText().toString();
                Log.v("Add This User", userNameToAdd);

                //Add the friend
                myFriends.addFriend(getContext(), userNameToAdd);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }//btnAddFriend(View)

    /**
     * On Click Context Menu for Removing a friend.
     *   Will remove a friend.
     * @param adapterPosition
     */
    public void btnRemoveFriend(int adapterPosition)
    {
        ParseUser removedUser = friendAdapter.getItem(adapterPosition);
        final String removedFriendObjectID = removedUser.getObjectId();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // set title
        builder.setTitle("Remove Friend");
        // set dialog message
        builder
                .setMessage("Are you sure you want to remove user " + removedUser.getUsername() + " from your friends?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Remove the friend
                        myFriends.removeFriend(getContext(), removedFriendObjectID);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });
        // show it
        builder.show();
    }//btnAddFriend(View)

    /**
     * On Click for Refresh Button
     *   Refreshes Changes.
     */
    public void btnRefreshFriends()
    {
        friendAdapter.updateResults(myFriends.getFriendList());
    }//btnRefreshFriends()

    /****************** Context Menu ********************/
    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int clickedPosition = info.position;

        switch(item.getItemId())
        {
            case ID_VIEW_PROFILE:
                FragmentViewProfile viewProfileFrag = new FragmentViewProfile().newInstance(friendAdapter.getItem(clickedPosition).getObjectId());
                FragmentTransaction ft = this.getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, (viewProfileFrag));
                ft.addToBackStack(null);
                ft.commit();
                return true;

            case ID_MESSAGE_USER:
                friendAdapter.
                        sendMessageToFriend(friendAdapter.getItem(clickedPosition).getObjectId(), friendAdapter.getItem(clickedPosition).getUsername());
                return true;

            case ID_REMOVE_FRIEND:
                Log.v("pressed", "remove");
                btnRemoveFriend(clickedPosition);
                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);

        //Set Title
        String title = "Friend Options";
        menu.setHeaderTitle(title);

        //Set the Rest of the Menu Options
        menu.add(Menu.NONE, ID_VIEW_PROFILE, Menu.NONE, "View Profile");
        menu.add(Menu.NONE, ID_MESSAGE_USER, Menu.NONE, "Message User");
        menu.add(Menu.NONE, ID_REMOVE_FRIEND, Menu.NONE, "Remove Friend");
    }
    /****************** Context Menu ********************/

    @Override
    public void onAttach(Context activity) {
        Log.v("onAttach", "FragmentFriends");
        super.onAttach(activity);
        ((ActivityMain) activity).onSectionAttached(
                getArguments().getInt(Utilities.ARG_SECTION_NUMBER_KEY));
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FragmentFriends newInstance(int sectionNumber)
    {
        Log.v("FragmentFriends", "newInstance");
        FragmentFriends fragment = new FragmentFriends();
        Bundle args = new Bundle();
        args.putInt(Utilities.ARG_SECTION_NUMBER_KEY, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * On Click Items that the XML Contains. This is the decision process
     * @param v
     */
    @Override
    public void onClick(View v)
    {
        Log.v("onClick", "" + v.getId());
        switch (v.getId())
        {
            case R.id.btn_add_friend:
                btnAddFriend();
                break;
            case R.id.btn_refresh:
                btnRefreshFriends();
                break;
            default:    //Somehow something Broke
                Log.v("Click", "FragmentFriends: onClick default case");
                break;
        }
    }
}//FragmentFriends Class
