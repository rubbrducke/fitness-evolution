package sccapstone.fitnessevolution.UserProfile;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import sccapstone.fitnessevolution.Activites.ActivityMain;
import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.R;


/**
 * This fragment handles the editing and saving of information for the current user's profile
 *
 * Author: Preston Barbare
 */
public class FragmentMyProfile extends android.support.v4.app.Fragment implements View.OnClickListener {
    private static final int SELECT_PHOTO = 100;
    private ImageView mProfImageView;
    private EditText mAboutMeEditText;
    private TextView mFitnessGoalsTextView;
    private String[] mFitnessGoalsArr;
    private String[] mSportsInterestArr;
    private TextView mSportsInterestsTextView;

    public FragmentMyProfile() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Activity activity = getActivity();
        ((ActivityMain) activity).setActionBarTitle( getResources().getString(R.string.nav_my_profile));

        View rootView = inflater.inflate(R.layout.profile_fragment_self, container, false);
        Utilities.showLoadingScreen(getContext());
        mFitnessGoalsArr = getResources().getStringArray(R.array.fitnessGoalsArray);
        mSportsInterestArr = getResources().getStringArray(R.array.sportsArray);

        TextView userNameEditText = (TextView) rootView.findViewById(R.id.myUserNameEditText);
        userNameEditText.setText(ParseUser.getCurrentUser().getUsername());

        mAboutMeEditText = (EditText) rootView.findViewById(R.id.aboutMe);
        Button btnSave = (Button) rootView.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(saveData);

        Button btnAddFitnessGoal = (Button) rootView.findViewById(R.id.btnAddFitnessGoal);
        btnAddFitnessGoal.setOnClickListener(addFitnessGoal);
        mFitnessGoalsTextView = (TextView) rootView.findViewById(R.id.txtViewFitnessGoals);

        Button btnAddSportsInterests = (Button) rootView.findViewById(R.id.btnAddSportsInterest);
        btnAddSportsInterests.setOnClickListener(addSportsInterest);
        mSportsInterestsTextView = (TextView) rootView.findViewById(R.id.txtViewSportsInterests);

        mProfImageView = (ImageView) rootView.findViewById(R.id.imgProfImage);
        mProfImageView.setImageBitmap(Utilities.getAvatar(getContext(), ParseUser.getCurrentUser()));

        //pick a photo
        mProfImageView.setOnClickListener(this);

        populateFields();
        Utilities.hideLoadingScreen();

        // Hide the keyboard if it is currently shown
        Utilities.hideKeyboard(getContext(), getActivity());

        // Inflate the layout for this fragment
        return rootView;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgProfImage:
                showGallery();
                break;
        }
    }

    /*
        High level method used to call appropriate functions to populate the fields
     */
    private void populateFields() {
        Object objAboutMe = ParseUser.getCurrentUser().get("aboutMe");
        String strAboutMe = "";
        if (objAboutMe != null) {
            strAboutMe = objAboutMe.toString();
        }

        ArrayList<String> fitnessGoals = (ArrayList<String>) ParseUser.getCurrentUser().get("fitnessGoals");
        ArrayList<String> sportsInterests = (ArrayList<String>) ParseUser.getCurrentUser().get("sportsInterests");

        mAboutMeEditText.setText(strAboutMe);

        updateFitnessGoalsTextView(fitnessGoals);
        updateSportsInterestsTextView(sportsInterests);
    }

    /*
        Adds all the fitness goals to the fitness goals text view
     */
    private void updateFitnessGoalsTextView(ArrayList<String> fitnessGoals) {
        if (fitnessGoals != null && !fitnessGoals.isEmpty()) {
            String strFitnessGoals = "";
            for (int j = 0; j < fitnessGoals.size(); j++) {
                strFitnessGoals += fitnessGoals.get(j) + "\n";
            }
            mFitnessGoalsTextView.setText(strFitnessGoals);
        }
    }

    /*
        Adds all the sports interests to the fitness goals text view
    */
    private void updateSportsInterestsTextView(ArrayList<String> sportsInterests) {
        if (sportsInterests != null && !sportsInterests.isEmpty()) {
            String strSportsInterests = "";
            for (int j = 0; j < sportsInterests.size(); j++) {
                strSportsInterests += sportsInterests.get(j) + "\n";
            }
            mSportsInterestsTextView.setText(strSportsInterests);
        }
    }

    /*
        Handles the display of the dialog box for checking sports interests
     */
    private View.OnClickListener addSportsInterest = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog dialog;
            // arraylist to keep the selected items
            final ArrayList selectedItems=new ArrayList();

            // Go ahead and select fitness goals the user has chosen
            boolean[] selectedSportsInterestsBoolArray = new boolean[mSportsInterestArr.length];

            for (int j = 0; j < mSportsInterestArr.length; j++) {
                if (mSportsInterestsTextView.getText().toString().contains(mSportsInterestArr[j])) {
                    selectedItems.add(j);
                    selectedSportsInterestsBoolArray[j] = true;
                }
                else {
                    selectedSportsInterestsBoolArray[j] = false;
                }
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("Select Sports Interests");

            builder.setMultiChoiceItems(mSportsInterestArr, selectedSportsInterestsBoolArray,
                    new DialogInterface.OnMultiChoiceClickListener() {
                        // indexSelected contains the index of item (of which checkbox checked)
                        @Override
                        public void onClick(DialogInterface dialog, int indexSelected,
                                            boolean isChecked) {
                            if (isChecked) {
                                // If the user checked the item, add it to the selected items
                                selectedItems.add(indexSelected);
                            } else if (selectedItems.contains(indexSelected)) {
                                // Else, if the item is already in the array, remove it
                                selectedItems.remove(Integer.valueOf(indexSelected));
                            }
                        }
                    })
                    // Set the action buttons
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            ArrayList<String> sportsInterestsSelections = new ArrayList<>();
                            for (int x = 0; x < selectedItems.size(); x++) {
                                sportsInterestsSelections.add(mSportsInterestArr[(int) selectedItems.get(x)]);
                            }

                            updateSportsInterestsTextView(sportsInterestsSelections);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });

            dialog = builder.create();
            dialog.getListView().setFastScrollEnabled(true);
            dialog.getListView().setFastScrollAlwaysVisible(true);
            dialog.show();
        }
    };

    /*
        Handles the display of the dialog box for checking fitness goals
    */
    private View.OnClickListener addFitnessGoal = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog dialog;
            // arraylist to keep the selected items
            final ArrayList selectedItems=new ArrayList();

            // Go ahead and select fitness goals the user has chosen
            boolean[] selectedGoalsBoolArray = new boolean[mFitnessGoalsArr.length];

            for (int j = 0; j < mFitnessGoalsArr.length; j++) {
                if (mFitnessGoalsTextView.getText().toString().contains(mFitnessGoalsArr[j])) {
                    selectedItems.add(j);
                    selectedGoalsBoolArray[j] = true;
                }
                else {
                    selectedGoalsBoolArray[j] = false;
                }
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("Select Fitness Goals");

            builder.setMultiChoiceItems(mFitnessGoalsArr, selectedGoalsBoolArray,
                    new DialogInterface.OnMultiChoiceClickListener() {
                        // indexSelected contains the index of item (of which checkbox checked)
                        @Override
                        public void onClick(DialogInterface dialog, int indexSelected,
                                            boolean isChecked) {
                            if (isChecked) {
                                // If the user checked the item, add it to the selected items
                                selectedItems.add(indexSelected);
                            } else if (selectedItems.contains(indexSelected)) {
                                // Else, if the item is already in the array, remove it
                                selectedItems.remove(Integer.valueOf(indexSelected));
                            }
                        }
                    })
                    // Set the action buttons
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            ArrayList<String> fitnessGoalsSelections = new ArrayList<>();
                            for (int x = 0; x < selectedItems.size(); x++) {
                                fitnessGoalsSelections.add(mFitnessGoalsArr[(int) selectedItems.get(x)]);
                            }

                            updateFitnessGoalsTextView(fitnessGoalsSelections);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });

            dialog = builder.create();
            dialog.getListView().setFastScrollEnabled(true);
            dialog.getListView().setFastScrollAlwaysVisible(true);
            dialog.show();
        }
    };

    /*
        Uploads information on the fragment to Parse
     */
    private View.OnClickListener saveData = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Utilities.showLoadingScreen(getContext());
            ArrayList<String> goalsArr;
            ArrayList<String> sportsInterestsArr = new ArrayList<>();

            String strAboutMe = mAboutMeEditText.getText().toString();

            final ParseUser pUser = ParseUser.getCurrentUser();

            goalsArr = new ArrayList<>((Arrays.asList(mFitnessGoalsTextView.getText().toString().split("\n"))));

            sportsInterestsArr = new ArrayList<>((Arrays.asList(mSportsInterestsTextView.getText().toString().split("\n"))));

            pUser.put("aboutMe", strAboutMe);
            pUser.put("sportsInterests", sportsInterestsArr);
            pUser.put("fitnessGoals", goalsArr);

            BitmapDrawable drawable = (BitmapDrawable) mProfImageView.getDrawable();
            Bitmap picture = drawable.getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            picture.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            // get byte array here
            final byte[] bytearray= stream.toByteArray();

            pUser.saveInBackground(new SaveCallback() {
                public void done(ParseException e) {
                    if (e == null) {
                        // Success in saving INFORMATION

                        // Now let's try to save the AVATAR
                        if (bytearray != null){
                            final ParseFile file = new ParseFile(ParseUser.getCurrentUser().getUsername()+"_avatar.jpg", bytearray);
                            file.saveInBackground(new SaveCallback() {
                                public void done(ParseException e) {
                                    if (e == null) {
                                        //Log.v("PRBTEST", "file name: " + file.getName() + " file url: " + file.getUrl());
                                        pUser.put("avatar", file);
                                        pUser.saveInBackground(new SaveCallback() {
                                            public void done(ParseException e) {
                                                Utilities.hideLoadingScreen();
                                                // Make sure the my profile fragment is still being displayed
                                                FragmentManager fm = getFragmentManager();
                                                if (fm != null) {
                                                    FragmentMyProfile fragMyProfile = (FragmentMyProfile) fm.findFragmentByTag("FRAG_MY_PROFILE");
                                                    if (fragMyProfile != null && fragMyProfile.isVisible()) {
                                                        if (e == null) {
                                                            Toast.makeText(getActivity(), "Profile saved :D", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            Utilities.hideLoadingScreen();
                                                            Toast.makeText(getActivity(), "Error: info saved, but image did not save :(", Toast.LENGTH_LONG).show();
                                                        }
                                                    }
                                                }
                                            }
                                        });

                                    } else {
                                        Utilities.hideLoadingScreen();
                                        // Make sure the my profile fragment is still being displayed
                                        FragmentManager fm = getFragmentManager();
                                        if (fm != null) {
                                            FragmentMyProfile fragMyProfile = (FragmentMyProfile) fm.findFragmentByTag("FRAG_MY_PROFILE");
                                            if (fragMyProfile != null && fragMyProfile.isVisible()) {

                                                Toast.makeText(getActivity(), "Error: info saved, but image did not save :(", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    }
                                    ;
                                }
                            });
                        }
                    } else {
                        //Failed saving
                        Utilities.hideLoadingScreen();
                        Toast.makeText(getActivity(), "Error saving profile :(", Toast.LENGTH_LONG).show();

                    }
                }
            });
        }
    };

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FragmentMyProfile newInstance(int sectionNumber) {
        FragmentMyProfile fragment = new FragmentMyProfile();
        Bundle args = new Bundle();
        args.putInt(Utilities.ARG_SECTION_NUMBER_KEY, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((ActivityMain) activity).onSectionAttached(
                getArguments().getInt(Utilities.ARG_SECTION_NUMBER_KEY));
    }

    /*
        Implicit gallery launch
     */
    private void showGallery() {
        final String WRITE_EXTERNAL_STORAGE = "android.permission.WRITE_EXTERNAL_STORAGE";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            // Check/Request runtime permissions
            if (getActivity().checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                showGalleryPreview();
            }
            else {
                if (getActivity().shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(getContext(),"Gallery permission is needed to select a profile image.", Toast.LENGTH_SHORT).show();
                }

                requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, SELECT_PHOTO);
            }
        }
        else {
            // Permissions already accepted at install time
            showGalleryPreview();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == SELECT_PHOTO) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showGalleryPreview();
            }
            else {
                Toast.makeText(getContext(), "Permission was not granted", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showGalleryPreview() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        photoPickerIntent.setType("image/*");
        Log.v("PRBTEST", "startActivityForResult");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }

    //previewing Image
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v("PRBTEST", "onActivityResult");
        Log.v("PRBTEST", "Result code: " + resultCode);
        if (requestCode == SELECT_PHOTO && data != null) {
            final int WIDTH = 600;
            final int HEIGHT = 800;

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            Log.v("PRBTEST", "Picture path: " + picturePath);
            cursor.close();

            if (picturePath != null) {
                Bitmap bitmapSelectedImage = BitmapFactory.decodeFile(picturePath);
                bitmapSelectedImage = Bitmap.createScaledBitmap(bitmapSelectedImage, WIDTH, HEIGHT, true);
                mProfImageView.setImageBitmap(bitmapSelectedImage);
            }
            else {
                Toast.makeText(getContext(), "Error selecting photo :(", Toast.LENGTH_SHORT).show();
            }
        }
    }
}


