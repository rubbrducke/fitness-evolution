package sccapstone.fitnessevolution.UserProfile;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import sccapstone.fitnessevolution.Activites.ActivityMain;
import sccapstone.fitnessevolution.Utilities.Utilities;
import sccapstone.fitnessevolution.Messaging.FragmentComposeMessage;
import sccapstone.fitnessevolution.Messaging.FragmentMessageThread;
import sccapstone.fitnessevolution.R;


/**
 * This fragment handles the viewing of information for a selected user's profile
 *
 * Author: Preston Barbare
 */
public class FragmentViewProfile extends android.support.v4.app.Fragment implements View.OnClickListener {
    private static final int SELECT_PHOTO = 100;
    private static final int CAMERA_REQUEST = 1888;
    private ImageView mProfImageView;
    private TextView mAboutMeTextView;
    private TextView mSportsInterestsTextView;
    private TextView mFitnessGoalsTextView;
    private TextView mUserNameTextView;
    private String mUserName = "";
    private String mUserObjID = "";

    public FragmentViewProfile() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.profile_fragment_other, container, false);

        Utilities.showLoadingScreen(getContext());

        if (getArguments() != null) {
            mUserObjID = getArguments().getString("USER_OBJ_ID");
        }

        mUserNameTextView = (TextView) rootView.findViewById(R.id.txtViewName);
        mAboutMeTextView = (TextView) rootView.findViewById(R.id.aboutMe);
        mProfImageView = (ImageView) rootView.findViewById(R.id.imgProfImage);
        mFitnessGoalsTextView = (TextView) rootView.findViewById(R.id.txtViewFitnessGoals);
        mSportsInterestsTextView = (TextView) rootView.findViewById(R.id.txtViewSportsInterests);

        mProfImageView = (ImageView) rootView.findViewById(R.id.imgProfImage);

        Button btnMessage = (Button) rootView.findViewById(R.id.btnMessage);
        btnMessage.setOnClickListener(this);

        populateFields();
        Utilities.hideLoadingScreen();

        // Hide the keyboard if it is currently shown
        Utilities.hideKeyboard(getContext(), getActivity());

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnMessage:
                sendMessage();
                break;
        }
    }

    private static ArrayList<String> arrConvParticipantObjIDs; // Must be a member var due to Parse API.
    /*
        Determines if a new message thread must be started, or add to an existing one. Then launches the FragmentMessageThread appropriately.
     */
    private void sendMessage() {
        // Determine if we need to add to a message thread, or start a new one.
        arrConvParticipantObjIDs = new ArrayList<String>();
        arrConvParticipantObjIDs.add(ParseUser.getCurrentUser().getObjectId());
        arrConvParticipantObjIDs.add(mUserObjID);

        ParseQuery<ParseObject> sentMessageQuery = ParseQuery.getQuery("Conversations");
        sentMessageQuery.whereContainsAll("convParticipantObjectIDs", arrConvParticipantObjIDs);

        //Log.v("PRBTEST","Querying table for a record with convParticipantObjectIDs of " + ParseUser.getCurrentUser().getObjectId() + " and " + friendObjID);

        sentMessageQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject messageThreadRecord, ParseException e) {
                if (e == null) {
                    // A conversation with this friend already exists, so launch the message thread fragment for that conversation
                    FragmentMessageThread newFragMessageThread = new FragmentMessageThread().newInstance(messageThreadRecord.getObjectId(), mUserName);
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, (newFragMessageThread));
                    ft.addToBackStack(null);
                    ft.commit();
                } else {
                    // No previous conversation with this friend, so launch compose fragment.
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, (new FragmentComposeMessage().newInstance(mUserName)));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        });
    }

    /*
        Populates fields in the fragment with the selected user's data
     */
    private void populateFields() {
        // Get the user
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("objectId", mUserObjID);
        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> returnedUsers, ParseException e) {
                if (e == null) {
                    // Success
                    if (!returnedUsers.isEmpty()) {
                        String strAboutMe = "";
                        Object objAboutMe = returnedUsers.get(0).get("aboutMe");
                        if (objAboutMe != null) {
                            strAboutMe = objAboutMe.toString();
                        }
                        ArrayList<String> fitnessGoals = (ArrayList<String>) returnedUsers.get(0).get("fitnessGoals");
                        ArrayList<String> sportsInterests = (ArrayList<String>) returnedUsers.get(0).get("sportsInterests");

                        Bitmap profImage = Utilities.getAvatar(getContext(), returnedUsers.get(0));
                        mProfImageView.setImageBitmap(profImage);

                        mUserName = returnedUsers.get(0).getUsername();
                        mUserNameTextView.setText(returnedUsers.get(0).getUsername());
                        if (!strAboutMe.equals("")) {
                            mAboutMeTextView.setText(strAboutMe);
                        }
                        Activity activity = getActivity();
                        ((ActivityMain) activity).setActionBarTitle(mUserName + "'s profile");

                        if (fitnessGoals != null) {
                            mFitnessGoalsTextView.setText(fitnessGoals.toString().substring(1, fitnessGoals.toString().length() - 1));
                        }
                        if (sportsInterests != null) {
                            mSportsInterestsTextView.setText(sportsInterests.toString().substring(1, sportsInterests.toString().length() - 1));
                        }

                    }
                } else {
                    Toast.makeText(getContext(), "Error retrieving profile :(", Toast.LENGTH_LONG);
                    Log.v("Query Find User Error", e.getMessage());
                }
            }
        });
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FragmentViewProfile newInstance(String userObjID) {
        FragmentViewProfile fragment = new FragmentViewProfile();
        Bundle args = new Bundle();
        args.putString("USER_OBJ_ID", userObjID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((ActivityMain) activity).onSectionAttached(
                getArguments().getInt(Utilities.ARG_SECTION_NUMBER_KEY));
    }

}









/*
        *//********************* List Views ***************************//*
        mFitnessGoalsListView = (ListView) rootView.findViewById(R.id.listViewFitnessGoals);
        // Allow scrolling in the listview
        mFitnessGoalsListView.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        mSportsInterestsListView = (ListView) rootView.findViewById(R.id.listViewSportsInterests);
        // Allow scrolling in the listview
        mSportsInterestsListView.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });*/
