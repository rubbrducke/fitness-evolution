package sccapstone.fitnessevolution.Parse;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;

import com.parse.ParseException;
import com.parse.ParseUser;

import sccapstone.fitnessevolution.Activites.ActivityStartup;
import sccapstone.fitnessevolution.Utilities.Utilities;


/**
 * Created by Matthew J on 2/26/2016.
 */
public class ParseErrorHandler {

    public static void handleParseError(ParseException e, Context context) {
        switch(e.getMessage()) {
            case "invalid session token": handleInvalidSessionToken(context);
                break;
            default: handleGeneralError(e, context);
                break;
        }
    }

    private static void handleInvalidSessionToken(final Context context) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);

        alert.setMessage("Oops! Your session is no longer valid. Please sign in again.")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        revertToSignIn(context);
                    }
                });
        alert.create().show();
    }

    private static void handleGeneralError(ParseException e, Context context) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);

        alert.setMessage("Oops! Fitness Evolution had a server issue.\n\nError: " + e.getMessage() +
                "\n\nSome functionality may be temporarily unavailable. We apologize for the inconvenience")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //If you want OK button to do anything special, put it here
                    }
                });
        alert.create().show();
    }

    private static void revertToSignIn(Context context) {
        ParseUser.logOut();

        Utilities.loggedIn = false;
        Utilities.user = ParseUser.getCurrentUser();

        Intent intent = new Intent(context, ActivityStartup.class);
        context.startActivity(intent);
    }

}
