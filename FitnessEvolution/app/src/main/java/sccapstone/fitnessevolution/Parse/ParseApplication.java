package sccapstone.fitnessevolution.Parse;

import android.app.Application;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SendCallback;

import org.json.JSONException;
import org.json.JSONObject;

import sccapstone.fitnessevolution.Utilities.Utilities;

/**
 * Created by Preston on 11/10/2015.
 */
public class ParseApplication extends Application {

    public static final String TAG = "ParseApplication";

    @Override
    public void onCreate() {
        super.onCreate();

        //ParseCrashReporting.enable(this);
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, Utilities.PARSE_APP_ID, Utilities.PARSE_CLIENT_ID);

        ParseInstallation.getCurrentInstallation().saveInBackground();

        //set defaults for push notification service
        ParsePush.subscribeInBackground(Utilities.PARSE_CHANNEL_DEFAULT);
    }

    /**
     * This method will build a basic push notification from a client, and send it to another.
     * When you message someone, you also send them a push notification.
     *
     * @param userTo - the username that the notification is to be sent to.
     * @param userFrom - the username OR COACH that the notification is sent from (if this is not
     *                 relevant to the context you may put anything)
     * @param context - options are "message", "coach", "friend", and "default"
     * @param message - the message that the notification is comprised of.
     * @return String with SUCCESS or an error description
     */
    public static String sendClientPush(String userTo, String userFrom, String context, String message)
    {

        String returnStr = "";

        ParseQuery pushQuery = ParseInstallation.getQuery();
        pushQuery.whereEqualTo("user", userTo);
        ParsePush push = new ParsePush();
        push.setQuery(pushQuery);

        if(message.length() > 32) {
            message = message.substring(0,31) + "...";
            Log.v("push:"+TAG,"truncated push message");
        }

        String data = "";

        if(context.equalsIgnoreCase("message"))
        {
            data = "{\n" +
                    "   \"data\" : {\n" +
                    "       \"message\" : \"" + userFrom + ": " + message + "\"" + ",\n" +
                    "       \"title\" : \"Inbox\"\n" +
                    "   }\n" +
                    "}";
        }
        else if(context.equalsIgnoreCase("coach"))
        {
            data = "{\n" +
                    "   \"data\": {\n" +
                    "       \"message\" : \"" + userFrom + ": " + message + "\"" + ",\n" +
                    "       \"title\": \"Coach\"\n" +
                    "   }\n" +
                    "}";
        }
        else if(context.equalsIgnoreCase("friend"))
        {
            data = "{\n" +
                    "   \"data\" : {\n" +
                    "       \"message\" : \"" + userFrom + ": " + message + "\"" + ",\n" +
                    "       \"title\" : \"New Friend\"\n" +
                    "   }\n" +
                    "}";
        }
        else if(context.equalsIgnoreCase("default"))
        {
            data = "{\n" +
                    "   \"data\": {\n" +
                    "       \"message\": " + message + ",\n" +
                    "       \"title\": \"Fitness Evolution\"\n" +
                    "   }\n" +
                    "}";
        }
        else {
            returnStr = "The context parameter must equal \"message\", \"coach\", or \"default\".";
            return returnStr;
        }

        Log.v("push:"+TAG,"formatted JSON data: " + data);

        JSONObject jsondata = null;
        try {
            jsondata = new JSONObject(data);
        } catch(JSONException e1) {
            e1.printStackTrace();
        }

        Log.v("push:"+TAG, "successfully created JSON object");

        push.setData(jsondata);
        push.sendInBackground(new SendCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.v("push:"+TAG, "send in background: success!");
                } else {
                    Log.v("push:"+TAG, "send in background: failure");
                }
            }
        });

        returnStr = "SUCCESS";
        return returnStr;
    }
}