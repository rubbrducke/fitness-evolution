[33mcommit 7e51afff58128c2d1dffcab59f92c6e81b00247a[m
Author: Preston Barbare <barbarep@email.sc.edu>
Date:   Thu Nov 12 13:54:16 2015 -0500

    Changed database structure to use a join table for UserDialogueHistory with a pointer to CoachDialogue instead of using a relation from User to CoachDialogue. This allows metadata to be recorded about sent messages (such as WHEN the message was sent)

[33mcommit 93d2d3951b9919a4adc8b20698d06c8894b1eddb[m
Author: Preston Barbare <barbarep@email.sc.edu>
Date:   Wed Nov 11 13:53:48 2015 -0500

    Created and implemented relation of user to coach dialogue in Activity_Coach_Log activity. This activity now shows all coach messages sent to the current user. Also created Coach_AI class to send messages to user. Functionality works in Coach_AI, however an algortihm for WHEN to send messages needs to be implemented, at which point Coach_AI may be called to send a user a message.

[33mcommit 36a32c358d1fabae5d853e3169bcaf812e226ae4[m
Merge: eb80646 721dcc5
Author: Preston Barbare <barbarep@email.sc.edu>
Date:   Tue Nov 10 00:31:54 2015 -0500

    Fixing merge conflic with Master

[33mcommit eb80646195a62bdb0bdf9a3a5e18ae55df53b06c[m
Author: Preston Barbare <barbarep@email.sc.edu>
Date:   Tue Nov 10 00:26:45 2015 -0500

    Forgot some files on last commit

[33mcommit 394ba55f5f180dde1957d6919239b84c527113ea[m
Author: Preston Barbare <barbarep@email.sc.edu>
Date:   Tue Nov 10 00:17:28 2015 -0500

    Created activity for coach message log. Needs navigation drawer. Currently (successfully) pulls arbitrary data from Parse, but needs to be updated to show the appropriate data. This will be done when Ben adds data to Parse.

[33mcommit 721dcc543bfdf22d1f4f037e27d67190fd73d015[m
Merge: 9d9ad78 9fc214b
Author: Tyler Wagner <wagnerjt@email.sc.edu>
Date:   Sun Nov 8 17:59:44 2015 -0500

    Merge branch 'master' of https://github.com/SCCapstone/FitnessEvolution

[33mcommit 9d9ad7845f8c4f00746d6e879d4188c187533ae3[m
Author: Tyler Wagner <wagnerjt@email.sc.edu>
Date:   Sun Nov 8 17:59:24 2015 -0500

    Integrate the Google Service JDK with project.

[33mcommit 9fc214bedffd5aa90fa02adc88add98fc06f70b3[m
Merge: 0516784 203451a
Author: Preston Barbare <Preston Barbare>
Date:   Thu Nov 5 15:48:19 2015 -0500

    Merge branch 'dev'

[33mcommit 203451a8c903f97d9cdebb59ab66099649fa0c17[m
Merge: 0912392 7125f44
Author: Preston Barbare <Preston Barbare>
Date:   Thu Nov 5 15:38:28 2015 -0500

    Merge branch 'master' of https://github.com/SCCapstone/FitnessEvolution into dev

[33mcommit 0516784998f10c10f7dbfdbc05004d218a0e048a[m
Merge: 0912392 7125f44
Author: Preston Barbare <Preston Barbare>
Date:   Thu Nov 5 15:23:18 2015 -0500

    Merge branch 'master' of https://github.com/SCCapstone/FitnessEvolution

[33mcommit 0912392dc44580590672acfe32344d77ab607a45[m
Author: Preston Barbare <Preston Barbare>
Date:   Thu Nov 5 15:16:53 2015 -0500

    Starter UML document. Author: Matt Short

[33mcommit 7125f443154537d20ac5eac0f7bbc59f2cdc8daa[m
Author: Tyler Wagner <wagnerjt@users.noreply.github.com>
Date:   Thu Nov 5 14:59:15 2015 -0500

    Update README.md
    
    Updated ReadMe only

[33mcommit 2dcaad9f31d6a3979139bc9e977a712b83363aba[m
Author: Preston Barbare <Preston Barbare>
Date:   Thu Nov 5 14:25:18 2015 -0500

    Integrated the Parse JDK with project.

[33mcommit 90114bdc5fdadd54d7cb299db73e3cd757e4d3b9[m
Author: Ben Aaron <aaronbt@email.sc.edu>
Date:   Tue Nov 3 14:08:45 2015 -0500

    Commit Test for Milestone 9

[33mcommit fd68df33e1118383bb1781f03816aae9ef529362[m
Author: Preston Barbare <barbarep@email.sc.edu>
Date:   Fri Oct 23 10:44:03 2015 -0400

    Adding .png for functional requirements (for easier viewing on the wiki

[33mcommit ee0b2aa5f176d5b1aaec322190d81386a3fba406[m
Author: Preston Barbare <barbarep@email.sc.edu>
Date:   Fri Oct 23 10:31:43 2015 -0400

    Adding a function requirement document in HTML form instead of PDF

[33mcommit 3614b3adb40a54b8162758221f9d401e83f88e35[m
Author: Preston Barbare <barbarep@email.sc.edu>
Date:   Fri Oct 23 10:19:34 2015 -0400

    Added functional requirement PDF

[33mcommit fe11dcdda8628f1a232a5207247f0e56de89b9f3[m
Author: Matt Short <matthew.rob.short@gmail.com>
Date:   Fri Oct 2 15:02:07 2015 -0400

    Images
    
    Created images folder and uploaded UI detail images.

[33mcommit 83e460997d9fe5ae3a2da85812da737619208daf[m
Author: Tyler Wagner <wagnerjt@email.sc.edu>
Date:   Mon Sep 21 01:39:45 2015 -0400

    Initial Android Application. Included the Navigation Menu Template as well as the strings in the /res/values/string.xml

[33mcommit c748f6db4f4a4673e2fd9d470d669e2854e8462f[m
Author: Tyler <wagnerjt@email.sc.edu>
Date:   Thu Sep 3 14:57:34 2015 -0400

    Initial Repo Commit with README
